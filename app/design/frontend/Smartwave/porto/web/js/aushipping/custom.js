require([
    'jquery',
    'js/jquery.lazyload'
], function ($) {
    $(document).ready(function(){
        $(".faq-question").each(function(){
            $(this).click(function(){
                if($(this).next().is(":visible")) {
                    $(this).next().hide();
                } else {
                    $(this).next().show();
                }
            });
        });
    });
});