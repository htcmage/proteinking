<?php
/**
 * Copyright © Bygmax All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace HTCMage\CustomFilterBestSeller\Setup\Patch\Data;

use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;

class CreateAttributeBestSeller implements DataPatchInterface, PatchRevertableInterface
{

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * Constructor
     *
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        /** @var EavSetup $eavSetup */
        $resource   = $this->moduleDataSetup;
        $connection = $this->moduleDataSetup->getConnection();
        
        $eagTbl = $resource->getTable('eav_attribute_group');
        

        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        if ( $eavSetup->getAttributeId(\Magento\Catalog\Model\Product::ENTITY, 'bestseller') ) {
             $eavSetup->removeAttribute(
                      \Magento\Catalog\Model\Product::ENTITY,
                        'bestseller');

        }
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'bestseller',
            [
                'type'                    => 'decimal',
                'label'                   => 'BestSeller',
                'input'                   => 'text',
                'sort_order'              => 100,
                'source'                  => '',
                'global'                  => ScopedAttributeInterface::SCOPE_STORE,
                'visible'                 => true,
                'required'                => false,
                'user_defined'            => false,
                'default'                 => '0',
                'group'                   => 'General',
                'backend'                 => '',
                'filterable'              => true,
                'visible_on_front'        => false,
                'used_in_product_listing' => true,
                'filterable_in_search'    => true,
                'used_for_sort_by'        => true
            ]
        );


        $this->moduleDataSetup->getConnection()->endSetup();
    }

    public function revert()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'bestseller');

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [

        ];
    }
}
