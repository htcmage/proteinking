<?php
 
namespace HTCMage\CustomFilterBestSeller\Plugin\Product\ProductList;
 
use Magento\Catalog\Block\Product\ProductList\Toolbar as Productdata;
 
class Toolbar
{
    public function aroundSetCollection(Productdata $subject, \Closure $proceed, $collection)
    {
        $selectedDirection = 'DESC';
        $collection->getSelect()->order('is_salable ' . $selectedDirection);
        return $proceed($collection);
    }
}