<?php
namespace HTCMage\CustomFilterBestSeller\Plugin\Catalog;

use Magento\Catalog\Block\Product\ProductList\Toolbar;
use Magento\Catalog\Model\Config;
use Magento\Framework\Data\Collection;

class Bestsellers 
{
    /**
    * Bestsellers sorting attribute
    */
    const BESTSELLERS_SORT_BY = 'bestseller';

    /**
     * @param Config $subject
     * @param $result
     * @return array
     */
    public function afterGetAttributeUsedForSortByArray(Config $subject, $result)
    {
        $result['bestseller'] = __('BestSellers');
        return $result;
    }

}