<?php
namespace HTCMage\CustomFilterBestSeller\Plugin\Catalog;

use Magento\Catalog\Block\Product\ProductList\Toolbar as ToolbarDefault;
use Magento\Catalog\Model\Config;
use Magento\Framework\Data\Collection;

class Toolbar 
{
	/**
	* Bestsellers sorting attribute
	*/
	const BESTSELLERS_SORT_BY = 'bestseller';


	public function afterGetCollection(
		\Magento\Catalog\Block\Product\ProductList\Toolbar $subject,
		$collection
	) {
		if ( $subject->getCurrentOrder() == self::BESTSELLERS_SORT_BY ) {
			$collection->addAttributeToSelect($subject->getCurrentOrder());
			$collection->addAttributeToSort($subject->getCurrentOrder(), $subject->getCurrentDirection());
		}

		return $collection;
	}
}
