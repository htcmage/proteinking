<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Api;

use HTCMage\PromotionBar\Model\PromotionBar;
use HTCMage\PromotionBar\Model\PromotionBarStoreView;

/**
 * Interface PromotionBarRepositoryInterface
 *
 * @package HTCMage\PromotionBar\Api
 */
interface PromotionBarStoreViewRepositoryInterface
{
    /**
     * Save
     *
     * @param PromotionBar $modelPromotionBarStoreView
     * @return PromotionBar
     */
    public function save(PromotionBarStoreView $modelPromotionBarStoreView);

    /**
     * Get promotion bar by id
     *
     * @param int|null $id
     * @return PromotionBar
     */
    public function getById($id);


    /**
     * Delete promotion bar by id
     *
     * @param PromotionBar $modelPromotionBarStoreView
     * @return mixed
     */
    public function delete($idBar);

}
