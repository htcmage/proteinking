<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Api;

use HTCMage\PromotionBar\Model\PromotionBar;

/**
 * Interface PromotionBarRepositoryInterface
 *
 * @package HTCMage\PromotionBar\Api
 */
interface PromotionBarRepositoryInterface
{
    /**
     * Save
     *
     * @param PromotionBar $modelPromotionBar
     * @return PromotionBar
     */
    public function save(PromotionBar $modelPromotionBar);

    /**
     * Get promotion bar by id
     *
     * @param int|null $id
     * @return PromotionBar
     */
    public function getById($id);


    /**
     * Delete promotion bar by id
     *
     * @param PromotionBar $modelPromotionBar
     * @return mixed
     */
    public function delete(PromotionBar $modelPromotionBar);

}
