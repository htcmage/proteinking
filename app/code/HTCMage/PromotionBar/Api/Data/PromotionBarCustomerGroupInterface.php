<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Api\Data;

/**
 * Interface PopupInterface
 *
 * @package HTCMage\PromotionBar\Api\Data
 */
interface PromotionBarCustomerGroupInterface
{

    /**
     * Const
     */
    const KEY_ID = 'id';
    /**
     *
     */
    const KEY_TITLE = 'title';
    /**
     *
     */
    const KEY_STATUS = 'status';
    /**
     *
     */
    const KEY_CUSTOMER_GROUPS = 'customer_groups';
    /**
     *
     */
    const KEY_DISPLAY_CONFIG = 'display_config';
    /**
     *
     */
    const KEY_DISPLAY_RULE = 'display_rule';
    /**
     *
     */
    const KEY_X_VALID = 'x_valid';
    /**
     *
     */
    const KEY_POSITION = 'position';
    /**
     *
     */
    const KEY_ANIMATION = 'animation';
    /**
     *
     */
    const KEY_CLOSE_BUTTON_CONFIG = 'close_config';
    /**
     *
     */
    const KEY_SHOW_CLOSE_BUTTON = 'show_close_button';
    /**
     *
     */
    const KEY_AUTO_CLOSE = 'auto_close';
    /**
     *
     */
    const KEY_FREQUENCY = 'frequency';
    /**
     *
     */
    const KEY_CONTENT = 'content';
    /**
     *
     */
    const KEY_CUSTOM_CSS = 'custom_css';
    /**
     *
     */
    const KEY_START_TIME = 'start_time';
    /**
     *
     */
    const KEY_END_TIME = 'end_time';
    /**
     *
     */
    const KEY_STORE_IDS = 'store_ids';

    /**
     * Get id
     *
     * @return int
     */
    public function getId();

    /**
     * Set title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title);

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle();

    /**
     * @param $content
     * @return mixed
     */
    public function setContent($content);

    /**
     * Get content
     *
     * @return string
     */
    public function getContent();
}
