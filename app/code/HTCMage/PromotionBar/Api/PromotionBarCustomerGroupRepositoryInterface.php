<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Api;

use HTCMage\PromotionBar\Model\PromotionBarCustomerGroup;
use HTCMage\PromotionBar\Model\PromotionBarCustomerGroupRepository;

/**
 * Interface PromotionBarRepositoryInterface
 *
 * @package HTCMage\PromotionBar\Api
 */
interface PromotionBarCustomerGroupRepositoryInterface
{
    /**
     * Save
     *
     * @param PromotionBarCustomerGroupRepository $modelPromotionBar
     * @return PromotionBarCustomerGroupRepository
     */
    public function save(PromotionBarCustomerGroup $modelPromotionBarCustomerGroup);

    /**
     * Get promotion bar by id
     *
     * @param int|null $id
     * @return PromotionBarCustomerGroupRepository
     */
    public function getById($id);


    /**
     * Delete promotion bar by id
     *
     * @param PromotionBarCustomerGroupRepository $modelPromotionBar
     * @return mixed
     */
    public function delete($id_bar);

}
