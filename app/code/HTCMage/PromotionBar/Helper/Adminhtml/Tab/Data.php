<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Helper\Adminhtml\Tab;

use Magento\Backend\Model\UrlInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

/**
 * Class Data
 *
 * @package Bss\ProductQuestions\Helper\Adminhtml\Tab
 */
class Data extends AbstractHelper
{

    /**
     * Url Interface
     *
     * @var UrlInterface $backendUrl
     */
    protected $backendUrl;

    /**
     * Data constructor.
     *
     * @param UrlInterface $backendUrl
     * @param Context $context
     */
    public function __construct(
        UrlInterface $backendUrl,
        Context $context
    )
    {
        parent::__construct($context);
        $this->backendUrl = $backendUrl;
    }

    /**
     * Get Products Grid Url
     *
     * @return string
     */
    public function getProductsGridUrl()
    {
        return $this->backendUrl->getUrl(
            'promotionbar/promotionbar/products',
            [
                'id' => $this->_request->getParam('id'),
                '_current' => true
            ]
        );
    }

}
