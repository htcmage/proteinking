<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Block\Adminhtml;

use Magento\Backend\Block\Widget\Context;
use Magento\Backend\Block\Widget\Form\Container;
use Magento\Framework\Phrase;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\AbstractBlock;

/**
 * Class Edit
 *
 * @package HTCMage\PromotionBar\Block\Adminhtml\PromotionBar
 */
class Edit extends Container
{
    /**
     * Core registry.
     *
     * @var Registry $coreRegistry
     */
    protected $coreRegistry = null;

    /**
     * Edit constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param array $data
     * @return void
     */
    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->coreRegistry = $registry;
    }

    /**
     * Retrieve text for header element depending on loaded question
     *
     * @return Phrase
     */
    public function getHeaderText()
    {
        $PromotionBar = $this->coreRegistry->registry('current_PromotionBar');
        if ($PromotionBar->getId()) {
            return __(
                "Edit PromotionBar of '%1'",
                $this->escapeHtml(
                    $PromotionBar->getTitle()
                )
            );
        } else {
            return __('New PromotionBar');
        }
    }

    /**
     * Initialize question edit block.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'HTCMage_PromotionBar';
        $this->_controller = 'adminhtml';

        parent::_construct();
        $this->buttonList->update('save', 'label', __('Save PromotionBar'));
        $this->buttonList->add(
            'saveandcontinue',
            [
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => [
                            'event' => 'saveAndContinueEdit',
                            'target' => '#edit_form'
                        ],
                    ],
                ]
            ],
            -100
        );

        $this->buttonList->update('delete', 'label', __('Delete PromotionBar'));
    }

    /**
     * Getter of url for "Save and Continue" button
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl(
            'promotionbar/*/save',
            ['_current' => true, 'back' => 'edit', 'active_tab' => '{{tab_id}}']
        );
    }

    /**
     * Prepare layout
     *
     * @return AbstractBlock
     */
    protected function _prepareLayout()
    {
        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('page_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'content');
                }
            };
        ";
        return parent::_prepareLayout();
    }
}
