<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Block\Adminhtml\Edit;

/**
 * Class Tabs
 *
 * @package HTCMage\PromotionBar\Block\Adminhtml\Edit
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * Construct
     *
     * @return voidpp
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('promotionbar_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('EDIT PromotionBar'));
    }
}
