<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Block\Adminhtml\Edit\Tab;

use HTCMage\PromotionBar\Model as PromotionBarModel;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Customer\Model\ResourceModel\Group\Collection;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Phrase;
use Magento\Framework\Registry;
use Magento\Store\Model\System\Store;

/**
 * Class Main
 *
 * @package HTCMage\PromotionBar\Block\Adminhtml\Popup\Edit\Tab
 */
class ExcludeCategory extends Generic implements TabInterface
{

    /**
     * System store
     *
     * @var Store $systemStore
     */
    protected $systemStore;

    /**
     * Customer Group Collection
     *
     * @var Collection $customerGroup
     */
    protected $categoryCollectionFactory;
    /**
     * @var
     */
    protected $categoryTree;

    /**
     * Main constructor.
     *
     * @param Collection $customerGroup
     * @param PromotionBarModel\Status $popupStatus
     * @param Store $systemStore
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        Collection $customerGroup,
        Store $systemStore,
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        CollectionFactory $categoryCollectionFactory,
        array $data = []
    )
    {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->systemStore = $systemStore;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
    }

    /**
     * Prepare label for tab
     *
     * @return Phrase
     */
    public function getTabLabel()
    {
        return __('Exclude Category');
    }

    /**
     * Prepare title for tab
     *
     * @return Phrase
     */
    public function getTabTitle()
    {
        return __('Exclude Category');
    }

    /**
     * Can Show Tab
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Is Hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * @return mixed
     */
    protected function _prepareForm()
    {
        $PromotionBarModel = $this->_coreRegistry->registry('current_promotionBar');
        $dataForm = $PromotionBarModel->getData();
        if (!empty($dataForm['id'])) {
            $dataForm['customer_group'] = $this->getCustomer($dataForm['id']);
            $dataForm['store_id'] = $this->getStoreView($dataForm['id']);
        }
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('page_');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Exclude Category')]
        );
        $fieldset->addField(
            'category_exclude',
            'multiselect',
            [
                'name' => 'category_exclude',
                'label' => __('Exclude Product'),
                'title' => __('Exclude Product'),
                'required' => false,
                'values' => $this->getCategori()
            ]
        );
        if (!empty($dataForm)) {
            $form->setValues($dataForm);
        }
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * @return mixed
     */
    public function getCategori()
    {
        if ($this->categoryTree === null) {
            $collection = $this->categoryCollectionFactory->create();
            $collection->addNameToResult();
            foreach ($collection as $category) {
                $categoryId = $category->getEntityId();
                if (!isset($categoryById[$categoryId])) {
                    $categoryById[$categoryId] = [
                        'value' => $categoryId
                    ];
                }
                $categoryById[$categoryId]['label'] = $category->getName();
            }
            $this->categoryTree = $categoryById;
        }
        return $this->categoryTree;
    }
}
