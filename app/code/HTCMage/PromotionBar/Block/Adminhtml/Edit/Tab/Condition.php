<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Block\Adminhtml\Edit\Tab;

use HTCMage\PromotionBar\Model;
use HTCMage\PromotionBar\Model as PromotionBarModel;
use HTCMage\PromotionBar\Ui\Component\Listing\Column\PageDisplay;
use HTCMage\PromotionBar\Ui\Component\Listing\Column\Position;
use Magento\Backend\Block\Store\Switcher\Form\Renderer\Fieldset\Element;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Config\Model\Config\Source\Yesno;
use Magento\Customer\Model\ResourceModel\Group\Collection;
use Magento\Framework\Data\Form\Element\Fieldset;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Phrase;
use Magento\Framework\Registry;
use Magento\Store\Model\System\Store;

/**
 * Class Main
 *
 * @package HTCMage\PromotionBar\Block\Adminhtml\Popup\Edit\Tab
 */
class Condition extends Generic implements TabInterface
{
    /**
     * PromotionBarModel\Status
     *
     * @var PromotionBarModel\Status $popupStatus
     */
    protected $popupStatus;

    /**
     * System store
     *
     * @var Store $systemStore
     */
    protected $systemStore;

    /**
     * Customer Group Collection
     *
     * @var Collection $customerGroup
     */
    protected $customerGroup;
    /**
     * @var Yesno
     */
    protected $yesNo;
    /**
     * @var PageDisplay
     */
    protected $pageDisplay;
    /**
     * @var Position
     */
    protected $position;
    /**
     * @var Model\PromotionBarCustomerGroupRepository
     */
    protected $promotionBarCustomerGroupRepository;
    /**
     * @var Model\PromotionBarStoreViewRepository
     */
    protected $promotionBarStoreViewRepository;

    /**
     * Main constructor.
     *
     * @param Collection $customerGroup
     * @param PromotionBarModel\Status $popupStatus
     * @param Store $systemStore
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        Collection $customerGroup,
        Store $systemStore,
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Yesno $yesNo,
        PageDisplay $pageDisplay,
        Position $position,
        Model\PromotionBarCustomerGroupRepository $promotionBarCustomerGroupRepository,
        Model\PromotionBarStoreViewRepository $promotionBarStoreViewRepository,
        array $data = []
    )
    {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->systemStore = $systemStore;
        $this->customerGroup = $customerGroup;
        $this->pageDisplay = $pageDisplay;
        $this->position = $position;
        $this->yesNo = $yesNo;
        $this->promotionBarCustomerGroupRepository = $promotionBarCustomerGroupRepository;
        $this->promotionBarStoreViewRepository = $promotionBarStoreViewRepository;
    }

    /**
     * Prepare label for tab
     *
     * @return Phrase
     */
    public function getTabLabel()
    {
        return __('Condition');
    }

    /**
     * Prepare title for tab
     *
     * @return Phrase
     */
    public function getTabTitle()
    {
        return __('Condition');
    }

    /**
     * Can Show Tab
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Is Hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * @return mixed
     */
    protected function _prepareForm()
    {
        $PromotionBarModel = $this->_coreRegistry->registry('current_promotionBar');
        $dataForm = $PromotionBarModel->getData();
        if (!empty($dataForm['id'])) {
            $dataForm['customer_group'] = $this->getCustomer($dataForm['id']);
            $dataForm['store_id'] = $this->getStoreView($dataForm['id']);
        }
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('page_');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Condition')]
        );
        $this->createGeneralConfig($PromotionBarModel, $fieldset);
        if (!empty($dataForm)) {
            $form->setValues($dataForm);
        }
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * @param $id
     * @return string
     */
    public function getCustomer($id)
    {
        $modelPromotionBarCustomer = $this->promotionBarCustomerGroupRepository->getCustomerGroupByFilter($id);
        $arrayCustomer = [];
        foreach ($modelPromotionBarCustomer as $key => $value) {
            array_push($arrayCustomer, $value['customer_group_id']);
        }
        return implode(',', $arrayCustomer);
    }

    /**
     * @param $id
     * @return string
     */
    public function getStoreView($id)
    {
        $modelPromotionBarStoreView = $this->promotionBarStoreViewRepository->getStoreViewByFilter($id);
        $arrayStoreView = [];
        foreach ($modelPromotionBarStoreView as $key => $value) {
            array_push($arrayStoreView, $value['store_id']);
        }
        return implode(',', $arrayStoreView);
    }

    /**
     * Create general config
     *
     * @param PromotionBarModel\Popup $PromotionBarModel
     * @param Fieldset $fieldset
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    protected function createGeneralConfig(PromotionBarModel\PromotionBar $PromotionBarModel, Fieldset &$fieldset)
    {
        $fieldset->addField(
            'show_close_button',
            'select',
            [
                'name' => 'show_close_button',
                'label' => __('Show Close Button'),
                'title' => __('Show Close Button'),
                'values' => $this->yesNo->toOptionArray()
            ]
        );
        $fieldset->addField(
            'page_display',
            'multiselect',
            [
                'name' => 'page_display',
                'label' => __('Page display'),
                'title' => __('Page display'),
                'required' => true,
                'values' => $this->pageDisplay->toOptionArray()
            ]
        );
        $fieldset->addField(
            'position',
            'select',
            [
                'name' => 'position',
                'label' => __('Position'),
                'title' => __('Position'),
                'required' => true,
                'values' => $this->position->toOptionArray()
            ]
        );
        $fieldset->addField(
            'customer_group',
            'multiselect',
            [
                'name' => 'customer_group',
                'label' => __('Customer Groups'),
                'title' => __('Customer Groups'),
                'required' => true,
                'values' => $this->customerGroup->toOptionArray()
            ]
        );
        if (!$this->_storeManager->isSingleStoreMode()) {
            $field = $fieldset->addField(
                'store_id',
                'multiselect',
                [
                    'name' => 'store_id',
                    'label' => __('Store View'),
                    'title' => __('Store View'),
                    'required' => true,
                    'values' => $this->systemStore->getStoreValuesForForm(false, true)
                ]
            );
            $renderer = $this->getLayout()->createBlock(
                Element::class
            );
            $field->setRenderer($renderer);
        } else {
            $fieldset->addField(
                'store_id',
                'hidden',
                [
                    'name' => 'store_id',
                    'value' => $this->_storeManager->getStore(true)->getId()
                ]
            );
        }
    }
}
