<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Block\Adminhtml\Edit\Tab;

use HTCMage\PromotionBar\Model;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Column;
use Magento\Backend\Block\Widget\Grid\Extended as GirdExtended;
use Magento\Backend\Helper\Data;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Products
 *
 * @package Bss\ProductQuestions\Block\Adminhtml\Question\Edit\Tab
 */
class ExcludeProduct extends GirdExtended
{

    /**
     * @var Model\ResourceModel\PromotionBar\CollectionFactory
     */
    protected $promotionBarCollection;
    /**
     * Collection Factory
     *
     * @var \Bss\ProductQuestions\Model\ResourceModel\Comment\CollectionFactory $collectionFactory
     */
    protected $collectionFactory;

    /**
     * Products constructor.
     *
     * @param Model\ResourceModel\CommentProductLink\CollectionFactory $commentProductLinkCollection
     * @param CollectionFactory $collectionFactory
     * @param Context $context
     * @param Data $backendHelper
     * @param array $data
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        Model\ResourceModel\PromotionBar\CollectionFactory $promotionBarCollection,
        Context $context,
        Data $backendHelper,
        array $data = []
    )
    {
        parent::__construct($context, $backendHelper, $data);
        $this->collectionFactory = $collectionFactory;
        $this->promotionBarCollection = $promotionBarCollection;
    }

    /**
     * Get grid url
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('promotionbar/promotionbar/products', ['_current' => true]);
    }

    /**
     * Initialize grid
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('productGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        if ($this->getRequest()->getParam('id')) {
            $this->setDefaultFilter(['in_product' => 1]);
        }
    }

    /**
     * Prepare Collection.
     *
     * @return GirdExtended
     * @throws NoSuchEntityException
     */
    protected function _prepareCollection()
    {
        $collection = $this->collectionFactory->create();
        $collection->addAttributeToSelect('name');
        $collection->addAttributeToSelect('sku');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Add column filter to collection
     *
     * @param Column $column
     * @return $this|GirdExtended
     * @throws LocalizedException
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_product') {
            $productIds = $this->getProductsSelected();

            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', ['in' => $productIds]);
            } else {
                if ($productIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', ['nin' => $productIds]);
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }

        return $this;
    }

    /**
     * Get product selected.
     *
     * @return array
     */
    public function getProductsSelected()
    {
        $selected = [];

        $barId = $this->getRequest()->getParam('id');
        if (!empty($barId)) {
            $collection = $this->promotionBarCollection->create();
            $collection->addFieldToFilter('id', ['eq' => $barId])->addFieldToSelect('promotionbar_product');
            $listProduct = $collection->getData()[0]['promotionbar_product'];

            foreach (explode('&', $listProduct) as $item) {
                array_push($selected, $item);
            }
        }
        return $selected;
    }

    /**
     * Prepare columns.
     *
     * @return GirdExtended
     * @throws LocalizedException
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_product',
            [
                'header_css_class' => 'a-center',
                'type' => 'checkbox',
                'name' => 'in_product',
                'align' => 'center',
                'values' => $this->getProductsSelected(),
                'index' => 'entity_id',
                'sortable' => false
            ]
        );

        $this->addColumn(
            'entity_id',
            [
                'header' => __('Product ID'),
                'type' => 'number',
                'index' => 'entity_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
            ]
        );
        $this->addColumn(
            'name',
            [
                'header' => __('Name'),
                'index' => 'name',
                'type' => 'text',
                'width' => '50px',
            ]
        );
        $this->addColumn(
            'sku',
            [
                'header' => __('SKU'),
                'index' => 'sku',
                'width' => '50px',
            ]
        );

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }
}
