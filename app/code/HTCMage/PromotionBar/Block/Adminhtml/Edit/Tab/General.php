<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Block\Adminhtml\Edit\Tab;

use HTCMage\PromotionBar\Model as PromotionBarModel;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Config\Model\Config\Source\Yesno;
use Magento\Customer\Model\ResourceModel\Group\Collection;
use Magento\Framework\Data\Form\Element\Fieldset;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Phrase;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime;
use Magento\Store\Model\System\Store;

/**
 * Class Main
 *
 * @package HTCMage\PromotionBar\Block\Adminhtml\Popup\Edit\Tab
 */
class General extends Generic implements TabInterface
{
    /**
     * PromotionBarModel\Status
     *
     * @var PromotionBarModel\Status $popupStatus
     */
    protected $popupStatus;

    /**
     * System store
     *
     * @var Store $systemStore
     */
    protected $systemStore;

    /**
     * Customer Group Collection
     *
     * @var Collection $customerGroup
     */
    protected $customerGroup;
    /**
     * @var Yesno
     */
    protected $yesNo;

    /**
     * Main constructor.
     *
     * @param Collection $customerGroup
     * @param PromotionBarModel\Status $popupStatus
     * @param Store $systemStore
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        Collection $customerGroup,
        Store $systemStore,
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Yesno $yesNo,
        array $data = []
    )
    {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->systemStore = $systemStore;
        $this->customerGroup = $customerGroup;
        $this->yesNo = $yesNo;
    }

    /**
     * Prepare label for tab
     *
     * @return Phrase
     */
    public function getTabLabel()
    {
        return __('General');
    }

    /**
     * Prepare title for tab
     *
     * @return Phrase
     */
    public function getTabTitle()
    {
        return __('General');
    }

    /**
     * Can Show Tab
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Is Hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * @return mixed
     */
    protected function _prepareForm()
    {
        $PromotionBarModel = $this->_coreRegistry->registry('current_promotionBar');

        $dataForm = $PromotionBarModel->getData();
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('page_');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General')]
        );

        $this->createGeneralConfig($PromotionBarModel, $fieldset);
        if (!empty($dataForm)) {
            $form->setValues($dataForm);
        }
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Create general config
     *
     * @param PromotionBarModel\Popup $PromotionBarModel
     * @param Fieldset $fieldset
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    protected function createGeneralConfig(PromotionBarModel\PromotionBar $PromotionBarModel, Fieldset &$fieldset)
    {
        if ($PromotionBarModel->getId()) {
            $fieldset->addField(
                'id',
                'hidden',
                ['name' => 'id']
            );
        }
        $fieldset->addField(
            'title',
            'text',
            [
                'name' => 'title',
                'required' => true,
                'label' => __('Title'),
                'title' => __('Title')
            ]
        );
        $fieldset->addField(
            'approve',
            'select',
            [
                'name' => 'approve',
                'label' => __('Approve'),
                'title' => __('Approve'),
                'values' => $this->yesNo->toOptionArray()
            ]
        );
        $fieldset->addField(
            'priority',
            'text',
            [
                'name' => 'priority',
                'required' => true,
                'label' => __('Priority'),
                'title' => __('Priority'),
                'class' => 'validate-number',
            ]
        );

        $fieldset->addField(
            'start_date',
            'date',
            [
                'name' => 'start_date',
                'label' => __('Start Time'),
                'class' => 'validate-date',
                'required' => false,
                'date_format' => DateTime::DATE_INTERNAL_FORMAT,
                'time_format' => 'HH:mm:ss'
            ]
        );
        $fieldset->addField(
            'end_date',
            'date',
            [
                'name' => 'end_date',
                'label' => __('End Time'),
                'class' => 'validate-date',
                'required' => false,
                'date_format' => DateTime::DATE_INTERNAL_FORMAT,
                'time_format' => 'HH:mm:ss'
            ]
        );

    }
}
