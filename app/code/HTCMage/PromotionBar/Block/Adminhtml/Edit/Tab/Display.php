<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Block\Adminhtml\Edit\Tab;

use HTCMage\PromotionBar\Model as PromotionBarModel;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Cms\Model\Wysiwyg\Config;
use Magento\Customer\Model\ResourceModel\Group\Collection;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Phrase;
use Magento\Framework\Registry;
use Magento\Store\Model\System\Store;

/**
 * Class Main
 *
 * @package HTCMage\PromotionBar\Block\Adminhtml\Popup\Edit\Tab
 */
class Display extends Generic implements TabInterface
{
    /**
     * @var Config
     */
    protected $wysiwygConfig;

    /**
     * Main constructor.
     *
     * @param Collection $customerGroup
     * @param PromotionBarModel\Status $popupStatus
     * @param Store $systemStore
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        Config $wysiwygConfig,
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        array $data = []
    )
    {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->wysiwygConfig = $wysiwygConfig;
    }

    /**
     * Prepare label for tab
     *
     * @return Phrase
     */
    public function getTabLabel()
    {
        return __('Display');
    }

    /**
     * Prepare title for tab
     *
     * @return Phrase
     */
    public function getTabTitle()
    {
        return __('Display');
    }

    /**
     * Can Show Tab
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Is Hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * @return mixed
     */
    protected function _prepareForm()
    {
        $PromotionBarModel = $this->_coreRegistry->registry('current_promotionBar');
        $dataForm = $PromotionBarModel->getData();
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('page_');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Display')]
        );

        /**
         * Config editor
         */
        $configWysiwyg['document_base_url'] = $this->getData('store_media_url');
        $configWysiwyg['store_id'] = $this->getData('store_id');
        $configWysiwyg['add_variables'] = true;
        $configWysiwyg['add_widgets'] = true;
        $configWysiwyg['add_directives'] = true;
        $configWysiwyg['use_container'] = true;
        $configWysiwyg['container_class'] = 'hor-scroll';
        $fieldset->addField(
            'content',
            'editor',
            [
                'name' => 'content',
                'label' => __('Content'),
                'title' => __('Content'),
                'style' => 'height:400px;',
                'wysiwyg' => true,
                'required' => true,
                'force_load' => true,
                'config' => $this->wysiwygConfig->getConfig($configWysiwyg)
            ]
        );

        $fieldset->addField(
            'style',
            'textarea',
            [
                'name' => 'style',
                'label' => __('Custom CSS'),
                'title' => __('Custom CSS'),
                'required' => false
            ]
        );
        if (!empty($dataForm)) {
            $form->setValues($dataForm);
        }
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
