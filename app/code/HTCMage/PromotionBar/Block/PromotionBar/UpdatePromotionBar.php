<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Block\PromotionBar;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class UpdatePromotionBar
 * @package HTCMage\PromotionBar\Block\PromotionBar
 */
class UpdatePromotionBar extends Template
{
    /**
     * @var Http
     */
    protected $request;
    /**
     * @var Registry
     */
    protected $registry;

    /**
     * UpdatePromotionBar constructor.
     * @param Context $context
     * @param Http $request
     * @param Registry $registry
     */
    public function __construct(Context $context,
                                Http $request,
                                Registry $registry)
    {
        parent::__construct($context);
        $this->request = $request;
        $this->registry = $registry;
    }

    /**
     * @return mixed
     */
    public function urlUpdate()
    {
        return $this->getUrl('promotionbar/promotionbar/resultajax');
    }

    /**
     * @return int
     */
    public function isPage()
    {
        if ($this->request->getFullActionName() == 'cms_index_index') {
            return 0;
        }
        if ($this->request->getFullActionName() == 'catalog_product_view') {
            return 1;
        }
        if ($this->request->getFullActionName() == 'catalog_category_view') {
            return 2;
        }
        if ($this->request->getFullActionName() == 'checkout_cart_index') {
            return 3;
        }
        if ($this->request->getFullActionName() == 'checkout_index_index') {
            return 4;
        }
        return 5;
    }

    /**
     * @return |null
     */
    public function isCategory()
    {
        $category = $this->registry->registry('current_category');
        if ($category != null) {
            return $category->getId();
        }
        return $category;
    }

    /**
     * @return |null
     */
    public function isProduct()
    {
        $product = $this->registry->registry('current_product');
        if ($product != null) {
            return $product->getId();
        }
        return $product;
    }
}