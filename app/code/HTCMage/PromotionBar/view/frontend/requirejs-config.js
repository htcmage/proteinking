/*
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

var config = {
    map: {
        '*': {
            owlCarouselTheme: 'HTCMage_PromotionBar/js/owl.carousel'
        }
    },
    shim: {
        owlCarouselTheme: ['jquery']
    }
};