/*
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

define([
    'jquery',
    "mage/template",
    'owlCarouselTheme',
], function ($, mageTemplate) {
    'use strict';
    $.widget('htc.DisPlayBar', {
        _create: function () {
            var url = this.options.url;
            var page = this.options.page;
            var category = this.options.category;
            var product = this.options.product;
            this._sendAjax(url, page, category, product);
        },
        _sendAjax: function (url, page, category, product) {
            var seft = this;
            $.ajax({
                type: 'post',
                url: url,
                data: {page: page, category: category, product: product},
                dataType: 'json',
                success: function (result) {
                    var top = result.top;
                    var elementTop = $('#top-promotionbar');

                    var aboveMenu = result.abovemenu;
                    var elementAboveMenu = $('#abovemenu-promotionbar');

                    var underMenu = result.undermenu;
                    var elementUnderMenu = $('#undermenu-promotionbar');

                    var abovePage = result.abovepage;
                    var elementAbovePage = $('#abovepage-promotionbar');

                    var underPage = result.underpage;
                    var elementUnderpage = $('#underpage-promotionbar');

                    var bottomPage = result.bottompage;
                    var elementBottomPage = $('#bottompage-promotionbar');

                    var dataElement = [top, aboveMenu, underMenu, abovePage, underPage, bottomPage];
                    $.each(top, function (key, value) {
                        if (typeof result.isSliderControl !== 'undefined') {
                            seft._setTemplate(elementTop, value.content, value.style);
                        } else {
                            seft._setTemplate(elementTop, value.content, value.style, value.show_close_button);
                        }
                    });
                    $.each(aboveMenu, function (key, value) {
                        if (typeof result.isSliderControl !== 'undefined') {
                            seft._setTemplate(elementAboveMenu, value.content, value.style);
                        } else {
                            seft._setTemplate(elementAboveMenu, value.content, value.style, value.show_close_button);
                        }
                    });
                    $.each(underMenu, function (key, value) {
                        if (typeof result.isSliderControl !== 'undefined') {
                            seft._setTemplate(elementUnderMenu, value.content, value.style);
                        } else {
                            seft._setTemplate(elementUnderMenu, value.content, value.style, value.show_close_button);
                        }
                    });
                    $.each(abovePage, function (key, value) {
                        if (typeof result.isSliderControl !== 'undefined') {
                            seft._setTemplate(elementAbovePage, value.content, value.style);
                        } else {
                            seft._setTemplate(elementAbovePage, value.content, value.style, value.show_close_button);
                        }
                    });
                    $.each(underPage, function (key, value) {
                        if (typeof result.isSliderControl !== 'undefined') {
                            seft._setTemplate(elementUnderpage, value.content, value.style);
                        } else {
                            seft._setTemplate(elementUnderpage, value.content, value.style, value.show_close_button);
                        }
                    });
                    $.each(bottomPage, function (key, value) {
                        if (typeof result.isSliderControl !== 'undefined') {
                            seft._setTemplate(elementBottomPage, value.content, value.style);
                        } else {
                            seft._setTemplate(elementBottomPage, value.content, value.style, value.show_close_button);
                        }
                    });
                    if (typeof result.isSliderControl !== 'undefined') {
                        var buttonclose = (result.isCloseButtonConfig == 1) ? true : false;
                        var isSliderControl = (result.isSliderControl == 1) ? true : false;
                        var isSliderPagination = (result.isSliderPagination == 1) ? true : false;
                        var isSpeedSlider = result.isSpeedSlider;
                        var listElement = [elementTop, elementAboveMenu, elementUnderMenu, elementAbovePage, elementUnderpage, elementBottomPage];
                        $.each(listElement, function (key, value) {
                            seft._setScrip(value, isSliderControl, isSpeedSlider);
                        });
                        if (buttonclose) {
                            $.each(listElement, function (key, value) {
                                seft._addCloseButtonMutipleBar(value);
                            });
                        }
                    }
                    $(".close-bar").click(function () {
                        $(this).parent().hide();
                    });
                }
            });
        },
        _setTemplate: function (element, content, style, close = null) {
            var template = mageTemplate('#promotion-bar-template');

            var newField = template({
                data: {
                    content: content,
                    style: style,
                }
            });
            element.append(newField);
            if (close == 1) {
                element.css('position', 'relative');
                var buttonCloseBar = "<p class='close-bar close'  aria-label='Close'><span aria-hidden='true'>&times;</span></p>";
                element.append(buttonCloseBar);
            }
        },
        _addCloseButtonMutipleBar: function (element) {
            var buttonCloseBar = "<p class='close-bar close' aria-label='Close'><span aria-hidden='true'>&times;</span></p>";
            element.css('position', 'relative');
            var checkItem = element.find('.wrapper-promotion-bar');
            if (checkItem.length > 0) {
                element.append(buttonCloseBar);
            }
        },
        _setScrip: function (element, isSliderControl, isSpeedSlider) {
            var checkItem = element.find('.wrapper-promotion-bar');
            if (checkItem.length > 1) {
                element.addClass("fadeOut").addClass("owl-carousel").addClass("owl-theme").owlCarousel({
                    items: 1,
                    animateOut: 'fadeOut',
                    loop: true,
                    margin: 10,
                    nav: isSliderControl,
                    autoplay: !isSliderControl,
                    autoplayTimeout: isSpeedSlider * 1000,
                });
            }
        },
    });

    return $.htc.DisPlayBar;
});
