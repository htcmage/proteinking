<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Ui\Component\Listing\Column;

use Magento\Framework\Option\ArrayInterface;

class Position implements ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 0, 'label' => __('Top Page')],
            ['value' => 1, 'label' => __('Above menu')],
            ['value' => 2, 'label' => __('Under menu')],
            ['value' => 3, 'label' => __('Above page content')],
            ['value' => 4, 'label' => __('Under page content')],
            ['value' => 5, 'label' => __('Bottom of page')],
        ];
    }
}