<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Ui\Component\Listing\Column;

use Magento\Framework\Option\ArrayInterface;

class PageDisplay implements ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [['value' => 0, 'label' => __('Home page')],
            ['value' => 1, 'label' => __('Product page')],
            ['value' => 2, 'label' => __('Category page')],
            ['value' => 3, 'label' => __('Cart page')],
            ['value' => 4, 'label' => __('Checkout page')],
            ['value' => 5, 'label' => __('Other page')]
        ];
    }
}