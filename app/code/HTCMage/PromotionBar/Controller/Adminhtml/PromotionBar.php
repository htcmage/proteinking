<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

/**
 * Created by AntsMage
 */

namespace HTCMage\PromotionBar\Controller\Adminhtml;

use HTCMage\PromotionBar\Helper\Data;
use HTCMage\PromotionBar\Model;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

/**
 * Class PromotionBar
 * @package HTCMage\PromotionBar\Controller\Adminhtml
 */
abstract class PromotionBar extends Action
{
    /**
     * const
     */
    const ADMIN_RESOURCE = 'HTCMage_PromotionBar§::main';
    /**
     * Popup Repository.
     *
     * @var Model\PopupRepository $promotionBarRepository
     */
    protected $promotionBarRepository;

    /**
     * Popup constructor.
     *
     * @param Data $helperData
     * @param Model\PopupRepository $popupRepository
     * @param Context $context
     * @return void
     */
    public function __construct(
        Model\PromotionBarRepository $promotionBarRepository,
        Context $context
    )
    {
        parent::__construct($context);
        $this->promotionBarRepository = $promotionBarRepository;
    }
}
