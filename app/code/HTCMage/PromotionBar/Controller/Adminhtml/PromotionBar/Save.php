<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Controller\Adminhtml\PromotionBar;

use Exception;
use HTCMage\PromotionBar\Controller\Adminhtml\PromotionBar;
use HTCMage\PromotionBar\Helper\Data;
use HTCMage\PromotionBar\Model;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class Save
 *
 * @package HTCMage\PromotionBar\Controller\Adminhtml\Popup
 */
class Save extends PromotionBar
{

    /**
     * const ADMIN_RESOURCE
     */
    // const ADMIN_RESOURCE = 'AntsMage_Popup::save';

    /**
     * @var Model\PromotionBarCustomerGroupRepository
     */
    protected $promotionBarCustomerGroupRepository;
    /**
     * @var Model\PromotionBarStoreViewRepository
     */
    protected $promotionBarStoreViewRepository;
    /**
     * ForwardFactory
     *
     * @var ForwardFactory
     */
    private $resultForwardFactory;

    /**
     * Save constructor.
     *
     * @param ForwardFactory $resultForwardFactory
     * @param Data $helperData
     * @param Model\PopupRepository $popupRepository
     * @param Context $context
     */
    public function __construct(
        ForwardFactory $resultForwardFactory,
        Model\PromotionBarRepository $promotionBarRepository,
        Model\PromotionBarCustomerGroupRepository $promotionBarCustomerGroupRepository,
        Model\PromotionBarStoreViewRepository $promotionBarStoreViewRepository,
        Context $context
    )
    {
        parent::__construct($promotionBarRepository, $context);
        $this->resultForwardFactory = $resultForwardFactory;
        $this->promotionBarCustomerGroupRepository = $promotionBarCustomerGroupRepository;
        $this->promotionBarStoreViewRepository = $promotionBarStoreViewRepository;
    }

    /**
     * @return mixed
     */
    public function execute()
    {

        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $data = $this->getRequest()->getPostValue();
            $id = $this->getRequest()->getParam('id');
            if (!$this->checkData($data)) {
                $this->messageManager->addError(__('Please enter all required information.'));
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
            $data['page_display'] = implode(',', $data['page_display']);
            if (isset($data['category_exclude']) && !empty($data['category_exclude'])) {
                $data['category_exclude'] = implode(',', $data['category_exclude']);
            } else {
                $data['category_exclude'] = "";
            }

            if (empty($data['id'])) {
                $data['id'] = null;
            }
            $modelPromotionBar = $this->promotionBarRepository->getById($id);
            $modelPromotionBar->setData($data);
            $modelPromotionBar = $this->promotionBarRepository->save($modelPromotionBar);
            $idBar = $modelPromotionBar->getId();
            $this->saveCustomerGroupBar($idBar, $data['customer_group']);
            $this->saveStoreViewBar($idBar, $data['store_id']);
            if ($this->getRequest()->getParam('back')) {
                $this->_getSession()->setFormData($data);
                return $resultRedirect->setPath('*/*/edit', [
                    'id' => $modelPromotionBar->getId(),
                    '_current' => true
                ]);
            }
            $this->messageManager->addSuccessMessage(__('The Promotionbar has been saved.'));
        } catch (Exception $exception) {
            echo $exception->getMessage();
            $this->messageManager->addErrorMessage(
                __('The Promotionbar was unable to be saved. Please try again.')
            );
        }

        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Execute
     *
     * @return ResponseInterface|Redirect|ResultInterface
     */
    public function checkData($data)
    {
        if (!isset($data['title'])) {
            return false;
        }
        return true;
    }

    /**
     * @param $idBar
     * @param $data
     */
    public function saveCustomerGroupBar($idBar, $data)
    {
        //save table htcmage_promotionbar_customer_group
        $dataCustomer['id_bar'] = $idBar;
        $modelPromotionBarCustomer = $this->promotionBarCustomerGroupRepository->createModel();
        $this->promotionBarCustomerGroupRepository->delete($idBar);
        foreach ($data as $custommer_group) {
            $dataCustomer['customer_group_id'] = $custommer_group;
            $modelPromotionBarCustomer->setData($dataCustomer);
            $modelPromotionBarCustomer = $this->promotionBarCustomerGroupRepository->save($modelPromotionBarCustomer);
        }
    }

    /**
     * @param $idBar
     * @param $data
     */
    public function saveStoreViewBar($idBar, $data)
    {
        //save table htcmage_promotionbar_store_view
        $dataStoreView['id_bar'] = $idBar;
        $modelPromotionBarStoreView = $this->promotionBarStoreViewRepository->createModel();
        $this->promotionBarStoreViewRepository->delete($idBar);
        foreach ($data as $store_id) {
            $dataStoreView['store_id'] = $store_id;
            $modelPromotionBarStoreView->setData($dataStoreView);
            $modelPromotionBarStoreView = $this->promotionBarStoreViewRepository->save($modelPromotionBarStoreView);
        }
    }
}
