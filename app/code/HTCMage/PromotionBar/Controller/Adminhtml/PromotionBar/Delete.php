<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Controller\Adminhtml\PromotionBar;

use Exception;
use HTCMage\PromotionBar\Controller\Adminhtml\PromotionBar;
use HTCMage\PromotionBar\Helper\Data;
use HTCMage\PromotionBar\Model;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class Delete
 *
 * @package HTCMage\PromotionBar\Controller\Adminhtml\PromotionBar
 */
class Delete extends PromotionBar
{

    /**
     * const ADMIN_RESOURCE
     */
    // const ADMIN_RESOURCE = 'AntsMage_PromotionBar::delete';

    /**
     * ForwardFactory
     *
     * @var ForwardFactory
     */
    private $resultForwardFactory;

    /**
     * Save constructor.
     *
     * @param ForwardFactory $resultForwardFactory
     * @param Data $helperData
     * @param Model\promotionBarRepository $promotionBarRepository
     * @param Context $context
     */
    public function __construct(
        ForwardFactory $resultForwardFactory,
        Model\PromotionBarRepository $promotionBarRepository,
        Context $context
    )
    {
        parent::__construct($promotionBarRepository, $context);
        $this->resultForwardFactory = $resultForwardFactory;
    }

    /**
     * Execute
     *
     * @return ResponseInterface|\Magento\Framework\Controller\Result\Redirect|ResultInterface
     */
    public function execute()
    {
        // Check if we know what should be deleted
        $id = $this->getRequest()->getParam('id');

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->promotionBarRepository->getById($id);
                $this->promotionBarRepository->delete($model);
                $this->messageManager->addSuccessMessage(
                    __('The PromotionBar has been deleted.')
                );
                return $resultRedirect->setPath('*/*/');
            } catch (Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath(
                    '*/*/edit',
                    ['id' => $id]
                );
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(
            __('We can\'t find a PromotionBar to delete.')
        );

        return $resultRedirect->setPath('*/*/');
    }
}
