<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Controller\Adminhtml\PromotionBar;

use Exception;
use HTCMage\PromotionBar\Model;
use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Edit
 * @package HTCMage\PromotionBar\Controller\Adminhtml\PromotionBar
 */
class Edit extends Action
{

    /**
     * Admin resource
     */
    // const ADMIN_RESOURCE = 'HTCMage_PromotionBar::save';

    /**
     * Result Page
     *
     * @var PageFactory $resultPageFactory
     */
    protected $resultPageFactory;

    /**
     * Registry
     *
     * @var Registry $registry
     */
    protected $registry;
    /**
     * @var Model\PromotionBarRepository
     */
    protected $promotionbarRepository;

    /**
     * Edit constructor.
     * @param PageFactory $resultPageFactory
     * @param Registry $registry
     * @param Model\PromotionBarRepository $promotionbarRepository
     * @param Action\Context $context
     */
    public function __construct(
        PageFactory $resultPageFactory,
        Registry $registry,
        Model\PromotionBarRepository $promotionbarRepository,
        Action\Context $context
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->registry = $registry;
        $this->promotionbarRepository = $promotionbarRepository;
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        $promotionBarId = $this->getRequest()->getParam('id');

        try {
            $promotionBarModel = $this->promotionbarRepository->getById($promotionBarId);
            if (!$promotionBarModel->getId() && $promotionBarId) {
                $this->messageManager->addErrorMessage(
                    __('This promotionBar no longer exists.')
                );
                /** \Magento\Framework\Controller\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }

            /**
             * Set data to form add or edit promotionBar.
             */
            $data = $this->_session->getFormData(true);
            if (!empty($data)) {
                $promotionBarModel->setData($data);
            }
            $this->registry->register('current_promotionBar', $promotionBarModel);
        } catch (Exception $exception) {
            // $this->helperData->writeLog($exception->getMessage());

            /** \Magento\Framework\Controller\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/');
        }

        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}