<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Controller\Adminhtml\PromotionBar;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\LayoutFactory;

/**
 * Class Products
 *
 * @package HTCMage\PromotionBar\Controller\Adminhtml\PromotionBar
 */
class Products extends Action
{

    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE_LIST_PROMOTIONBAR = 'HTCMage_PromotionBar::list_questions';

    /**
     * Layout Factory
     *
     * @var LayoutFactory
     */
    protected $resultLayoutFactory;

    /**
     * Constructor
     *
     * @param Context $context
     * @param LayoutFactory $resultLayoutFactory
     */
    public function __construct(
        Context $context,
        LayoutFactory $resultLayoutFactory
    )
    {
        parent::__construct($context);
        $this->resultLayoutFactory = $resultLayoutFactory;
    }

    /**
     * Save action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $resultLayout = $this->resultLayoutFactory->create();
        $block = $resultLayout->getLayout()->getBlock('productsgrid.edit.tab.products');
        $block->setUseAjax(true);
        return $resultLayout;
    }

    /**
     * Is Allowed
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(self::ADMIN_RESOURCE_LIST_PROMOTIONBAR);
    }
}
