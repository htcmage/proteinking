<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Controller\PromotionBar;

use HTCMage\PromotionBar\Model;
use Magento\Backend\App\Action\Context;
use Magento\Cms\Model\Template\FilterProvider;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\Serializer\Json as JsonHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Questions
 *
 * @package HTCMage\PromotionBar\Controller\PromotionBar
 */
class ResultAjax extends Action
{
    /**
     * @var Model\ResourceModel\PromotionBarCustomerGroup\CollectionFactory
     */
    protected $promotionBarCustomerGroupCollection;

    /**
     * @var Model\ResourceModel\PromotionBarStoreView\CollectionFactory
     */
    protected $promotionBarStoreViewCollection;
    /**
     * @var
     */
    protected $promotionBarCollectionpromotionBarCollection;
    /**
     * @var Session
     */
    protected $customerSession;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManagerInterface;
    /**
     * @var Http
     */
    protected $request;
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;
    /**
     * @var JsonHelper
     */
    protected $jsonHelper;
    /**
     * @var Registry
     */
    protected $registry;
    /**
     * @var FilterProvider
     */
    protected $contentProcessor;

    /**
     * ResultAjax constructor.
     * @param Context $context
     * @param Session $customerSession
     * @param Model\ResourceModel\PromotionBar\CollectionFactory $promotionBarCollection
     * @param Model\ResourceModel\PromotionBarCustomerGroup\CollectionFactory $promotionBarCustomerGroupCollection
     * @param Model\ResourceModel\PromotionBarStoreView\CollectionFactory $promotionBarStoreViewCollection
     * @param StoreManagerInterface $storeManagerInterface
     * @param Http $request
     * @param JsonHelper $jsonHelper
     * @param ScopeConfigInterface $scopeConfig
     * @param FilterProvider $contentProcessor
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        Model\ResourceModel\PromotionBar\CollectionFactory $promotionBarCollection,
        Model\ResourceModel\PromotionBarCustomerGroup\CollectionFactory $promotionBarCustomerGroupCollection,
        Model\ResourceModel\PromotionBarStoreView\CollectionFactory $promotionBarStoreViewCollection,
        StoreManagerInterface $storeManagerInterface,
        Http $request,
        JsonHelper $jsonHelper,
        ScopeConfigInterface $scopeConfig,
        FilterProvider $contentProcessor,
        Registry $registry
    )
    {
        parent::__construct($context);
        $this->promotionBarCustomerGroupCollection = $promotionBarCustomerGroupCollection;
        $this->promotionBarStoreViewCollection = $promotionBarStoreViewCollection;
        $this->promotionBarCollection = $promotionBarCollection;
        $this->customerSession = $customerSession;
        $this->storeManagerInterface = $storeManagerInterface;
        $this->request = $request;
        $this->scopeConfig = $scopeConfig;
        $this->jsonHelper = $jsonHelper;
        $this->registry = $registry;
        $this->contentProcessor = $contentProcessor;

    }

    /**
     * Execute
     *
     * @return ResponseInterface|ResultInterface
     */
    public function execute()
    {
        return $this->getResponse()->setBody($this->jsonHelper->serialize($this->listPromotionBar()));
    }

    /**
     * @return array
     */
    public function listPromotionBar()
    {
        $data = [];
        $arrayBarDisplayInPage = [];
        $allBar = $this->promotionBarCollection->create()->setOrder('priority', 'ASC')->getData();
        if (count($allBar) > 0) {
            if ($this->isEnable() == 1) {
                $mutipleBar = false;
                if ($this->isUseMultipleBars() == 1) {
                    foreach ($allBar as $bar) {
                        if ($this->displayBar($bar)) {
                            $bar['content'] = $this->processContent($bar['content']);
                            array_push($arrayBarDisplayInPage, $bar);
                        }
                    }
                    $data['isSliderControl'] = $this->isSliderControl();
                    $data['isSliderPagination'] = $this->isSliderPagination();
                    $data['isSpeedSlider'] = $this->isSpeedSlider();
                    $data['isCloseButtonConfig'] = $this->isCloseButtonConfig();
                    $mutipleBar = true;
                } else {
                    foreach ($allBar as $bar) {
                        if ($this->displayBar($bar)) {
                            $bar['content'] = $this->processContent($bar['content']);
                            array_push($arrayBarDisplayInPage, $bar);
                        }
                    }
                }
                $data['top'] = $this->listPromotionBarTop($arrayBarDisplayInPage, $mutipleBar);
                $data['abovemenu'] = $this->listPromotionBarAboveMenu($arrayBarDisplayInPage, $mutipleBar);
                $data['undermenu'] = $this->listPromotionBarUnderMenu($arrayBarDisplayInPage, $mutipleBar);
                $data['abovepage'] = $this->listPromotionBarAbovePageContent($arrayBarDisplayInPage, $mutipleBar);
                $data['underpage'] = $this->listPromotionBarUnderPageContent($arrayBarDisplayInPage, $mutipleBar);
                $data['bottompage'] = $this->listPromotionBarBottomOfPage($arrayBarDisplayInPage, $mutipleBar);
            }
        }
        return $data;
    }

    /**
     * @return mixed
     */
    public function isEnable()
    {
        $storeScope = ScopeInterface::SCOPE_STORES;
        return $this->scopeConfig->getValue('promotionbar/general/enable', $storeScope);
    }

    /**
     * @return mixed
     */
    public function isUseMultipleBars()
    {
        $storeScope = ScopeInterface::SCOPE_STORES;
        return $this->scopeConfig->getValue('promotionbar/general/use_multipe', $storeScope);
    }

    /**
     * @param $bar
     * @return bool
     */
    public function displayBar($bar)
    {
        $idBar = $bar['id'];
        $approve = $bar['approve'];
        $startDate = $bar['start_date'];
        $endDate = $bar['end_date'];
        $listPage = $bar['page_display'];
        $excludeProduct = $bar['promotionbar_product'];
        $excludeCategory = $bar['category_exclude'];
        if ($this->checkDateTime($startDate, $endDate) && $this->checkCustomerGroup($idBar) && $this->checkStoreView($idBar) && $this->checkApprove($approve) && $this->checkPage($listPage, $excludeProduct, $excludeCategory)) {
            return true;
        }
        return false;
    }

    /**
     * @param $startDate
     * @param $endDate
     * @return bool
     */
    public function checkDateTime($startDate, $endDate)
    {
        // $now = strtotime(time());
        // $startDate = strtotime($startDate);
        // $startDate = mktime($startDate['hour'], $startDate['minute'], $startDate['second'], $startDate['month'], $startDate['day'], $startDate['year']);
        // $endDate = strtotime($endDate);
        // $endDate = mktime($endDate['hour'], $endDate['minute'], $endDate['second'], $endDate['month'], $endDate['day'], $endDate['year']);
        // if (($now - $startDate) >= 0 && ($now - $endDate) <= 0) {
        //     return true;
        // }
        return true;
    }

    /**
     * @param $barId
     * @return bool
     */
    public function checkCustomerGroup($barId)
    {
        $customerGroup = $this->customerSession->getCustomer()->getGroupId();
        $customerBar = $this->promotionBarCustomerGroupCollection->create();
        $customerBar->addFieldToFilter('id_bar', ['eq' => $barId])->addFieldToSelect('customer_group_id');
        foreach ($customerBar as $customer) {
            if ($customerGroup == $customer['customer_group_id']) {
                return true;

            }
        }
        return false;
    }

    /**
     * @param $barId
     * @return bool
     */
    public function checkStoreView($barId)
    {
        $currentStore = $this->storeManagerInterface->getStore()->getId();
        $storeViewBar = $this->promotionBarStoreViewCollection->create();
        $storeViewBar->addFieldToFilter('id_bar', ['eq' => $barId])->addFieldToSelect('store_id');
        foreach ($storeViewBar as $store) {
            if ($currentStore == $store['store_id'] || $store['store_id'] == 0) {
                return true;

            }
        }
        return false;
    }

    /**
     * @param $approve
     * @return bool
     */
    public function checkApprove($approve)
    {
        if ($approve == 1) {
            return true;
        }
        return false;
    }

    /**
     * @param $listPage
     * @param $excludeProduct
     * @param $excludeCategory
     * @return bool
     */
    public function checkPage($listPage, $excludeProduct, $excludeCategory)
    {
        $listPage = explode(',', $listPage);
        $codePage = $this->getRequest()->getParam('page');
        foreach ($listPage as $page) {
            if ($codePage == $page) {
                if ($codePage == 2) {
                    $category = $this->getRequest()->getParam('category');
                    return !in_array($category, explode(',', $excludeCategory));
                }
                if ($codePage == 1) {
                    $product = $this->getRequest()->getParam('product');
                    return !in_array($product, explode('&', $excludeProduct));
                }
                return true;
            }
        }
        return false;
    }

    /**
     * @param $content
     * @return mixed
     */
    public function processContent($content)
    {
        return $this->contentProcessor->getPageFilter()->filter($content);
    }

    /**
     * @return mixed
     */
    public function isSliderControl()
    {
        $storeScope = ScopeInterface::SCOPE_STORES;
        return $this->scopeConfig->getValue('promotionbar/general/slider_control', $storeScope);
    }

    /**
     * @return mixed
     */
    public function isSliderPagination()
    {
        $storeScope = ScopeInterface::SCOPE_STORES;
        return $this->scopeConfig->getValue('promotionbar/general/slider_pagination', $storeScope);
    }

    /**
     * @return mixed
     */
    public function isSpeedSlider()
    {
        $storeScope = ScopeInterface::SCOPE_STORES;
        return $this->scopeConfig->getValue('promotionbar/general/speed_slider', $storeScope);
    }

    /**
     * @return mixed
     */
    public function isCloseButtonConfig()
    {
        $storeScope = ScopeInterface::SCOPE_STORES;
        return $this->scopeConfig->getValue('promotionbar/general/close_button', $storeScope);
    }

    /**
     * @param $data
     * @param $mutipleBar
     * @return array
     */
    public function listPromotionBarTop($data, $mutipleBar)
    {
        $topBar = [];
        foreach ($data as $bar) {
            if ($bar['position'] == 0) {
                array_push($topBar, $bar);
                if ($mutipleBar == false) {
                    break;
                }
            }

        }
        return $topBar;
    }

    /**
     * @param $data
     * @param $mutipleBar
     * @return array
     */
    public function listPromotionBarAboveMenu($data, $mutipleBar)
    {
        $aboveMenuBar = [];
        foreach ($data as $bar) {
            if ($bar['position'] == 1) {
                array_push($aboveMenuBar, $bar);
                if ($mutipleBar == false) {
                    break;
                }
            }
        }
        return $aboveMenuBar;
    }

    /**
     * @param $data
     * @param $mutipleBar
     * @return array
     */
    public function listPromotionBarUnderMenu($data, $mutipleBar)
    {
        $underMenu = [];
        foreach ($data as $bar) {
            if ($bar['position'] == 2) {
                array_push($underMenu, $bar);
                if ($mutipleBar == false) {
                    break;
                }
            }
        }
        return $underMenu;
    }

    /**
     * @param $data
     * @param $mutipleBar
     * @return array
     */
    public function listPromotionBarAbovePageContent($data, $mutipleBar)
    {
        $AbovePageContent = [];
        foreach ($data as $bar) {
            if ($bar['position'] == 3) {
                array_push($AbovePageContent, $bar);
                if ($mutipleBar == false) {
                    break;
                }
            }
        }
        return $AbovePageContent;
    }

    /**
     * @param $data
     * @param $mutipleBar
     * @return array
     */
    public function listPromotionBarUnderPageContent($data, $mutipleBar)
    {
        $UnderPageContent = [];
        foreach ($data as $bar) {
            if ($bar['position'] == 4) {
                array_push($UnderPageContent, $bar);
                if ($mutipleBar == false) {
                    break;
                }
            }
        }
        return $UnderPageContent;
    }

    /**
     * @param $data
     * @param $mutipleBar
     * @return array
     */
    public function listPromotionBarBottomOfPage($data, $mutipleBar)
    {
        $BottomOfPage = [];
        foreach ($data as $bar) {
            if ($bar['position'] == 5) {
                array_push($BottomOfPage, $bar);
                if ($mutipleBar == false) {
                    break;
                }
            }
        }
        return $BottomOfPage;
    }
}
