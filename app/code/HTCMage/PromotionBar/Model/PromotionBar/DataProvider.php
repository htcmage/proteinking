<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Model\PromotionBar;

use HTCMage\PromotionBar\Model;
use HTCMage\PromotionBar\Model\ResourceModel\PromotionBar\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;

/**
 * Class DataProvider
 * @package HTCMage\PromotionBar\Model\PromotionBar
 */
class DataProvider extends AbstractDataProvider
{

    /**
     * @var \Prince\Faq\Model\ResourceModel\Faq\CollectionFactory
     */

    public $collection;
    /**
     * @var Model\PromotionBarCustomerGroupRepository
     */
    protected $promotionBarCustomerGroupRepository;
    /**
     * @var Model\PromotionBarStoreViewRepository
     */
    protected $promotionBarStoreViewRepository;
    /**
     * @var
     */
    private $loadedData;
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $blockCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        StoreManagerInterface $storeManager,
        Model\PromotionBarCustomerGroupRepository $promotionBarCustomerGroupRepository,
        Model\PromotionBarStoreViewRepository $promotionBarStoreViewRepository,
        array $meta = [],
        array $data = []
    )
    {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->storeManager = $storeManager;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->promotionBarCustomerGroupRepository = $promotionBarCustomerGroupRepository;
        $this->promotionBarStoreViewRepository = $promotionBarStoreViewRepository;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $model) {
            $this->loadedData[$model->getId()] = $model->getData();
            $this->loadedData[$model->getId()]['custommer_group'] = $this->getCustomer($model->getId());
            $this->loadedData[$model->getId()]['store_id'] = $this->getStoreView($model->getId());
        }
        $data = $this->dataPersistor->get('promotionbar');
        if (!empty($data)) {
            $model = $this->collection->getNewEmptyItem();
            $model->setData($data);
            $this->loadedData[$model->getId()] = $model->getData();
            $this->dataPersistor->clear('promotionbar');
        }
        return $this->loadedData;
    }

    /**
     * @param $id
     * @return string
     */
    public function getCustomer($id)
    {
        $modelPromotionBarCustomer = $this->promotionBarCustomerGroupRepository->getCustomerGroupByFilter($id);
        $arrayCustomer = [];
        foreach ($modelPromotionBarCustomer as $key => $value) {
            array_push($arrayCustomer, $value['customer_group_id']);
        }
        return implode(',', $arrayCustomer);
    }

    /**
     * @param $id
     * @return string
     */
    public function getStoreView($id)
    {
        $modelPromotionBarStoreView = $this->promotionBarStoreViewRepository->getStoreViewByFilter($id);
        $arrayStoreView = [];
        foreach ($modelPromotionBarStoreView as $key => $value) {
            array_push($arrayStoreView, $value['store_id']);
        }
        return implode(',', $arrayStoreView);
    }
}
