<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class PromotionBarCustomerGroup
 * @package HTCMage\PromotionBar\Model
 */
class PromotionBarCustomerGroup extends AbstractModel
{
    /**
     *
     */
    const CACHE_TAG = 'htcmage_promotionbar_customer_group';


    /**
     * @var string
     */
    protected $_cacheTag = 'htcmage_promotionbar_customer_group';


    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     *
     */
    protected function _construct()
    {
        $this->_init('HTCMage\PromotionBar\Model\ResourceModel\PromotionBarCustomerGroup');
    }
}
