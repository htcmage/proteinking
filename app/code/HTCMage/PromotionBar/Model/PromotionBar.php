<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Model;

use HTCMage\PromotionBar\Api;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\DateTime;

/**
 * Class PromotionBar
 * @package HTCMage\PromotionBar\Model
 */
class PromotionBar extends AbstractModel implements Api\Data\PromotionBarInterface
{

    /**
     * Date
     *
     * @var DateTime $date
     */
    protected $date;

    /**
     * Popup constructor.
     *
     * @param DateTime $date
     * @param PopupStoreLinkRepository $popupStoreLinkRepository
     * @param Context $context
     * @param Registry $registry
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        DateTime $date,
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->date = $date;
    }

    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::KEY_ID);
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->setData(self::KEY_TITLE, $title);
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->getData(self::KEY_TITLE);
    }

    /**
     * @param $content
     */
    public function setContent($content)
    {
        $this->setData(self::KEY_CONTENT, $content);
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->getData(self::KEY_CONTENT);
    }

    /**
     * Construct
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\PromotionBar::class);
    }


}
