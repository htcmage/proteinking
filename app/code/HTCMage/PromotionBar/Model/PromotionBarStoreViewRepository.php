<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Model;

use Exception;
use HTCMage\PromotionBar\Api;
use Magento\Customer\Model\Context;
use Magento\Customer\Model\Group;
use Magento\Customer\Model\Session;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class PromotionBarRepository
 *
 * @package HTCMage\PromotionBar\Model
 */
class PromotionBarStoreViewRepository implements Api\PromotionBarStoreViewRepositoryInterface
{

    /**
     * promotionBarStoreViewFactory
     *
     * @var promotionBarStoreViewFactory $promotionBarStoreViewFactory
     */
    private $promotionBarStoreViewFactory;

    /**
     * Resource
     *
     * @var ResourceModel\PromotionBar $resource
     */
    private $resource;

    /**
     * Customer Session
     *
     * @var Session $customerSession
     */
    private $customerSession;

    /**
     * HTTP context
     *
     * @var \Magento\Framework\App\Http\Context $httpContext
     */
    private $httpContext;

    /**
     * Store manager
     *
     * @var StoreManagerInterface $storeManager
     */
    private $storeManager;

    /**
     * PromotionBarRepository constructor.
     *
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param Session $customerSession
     * @param promotionBarStoreViewFactory $promotionBarStoreViewFactory
     * @param ResourceModel\PromotionBar $resource
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        \Magento\Framework\App\Http\Context $httpContext,
        Session $customerSession,
        PromotionBarStoreViewFactory $promotionBarStoreViewFactory,
        ResourceModel\PromotionBarStoreView $resource
    )
    {
        $this->storeManager = $storeManager;
        $this->httpContext = $httpContext;
        $this->customerSession = $customerSession;
        $this->promotionBarStoreViewFactory = $promotionBarStoreViewFactory;
        $this->resource = $resource;
    }

    /**
     * Get Customer Group
     *
     * @return int
     */
    public function getCustomerGroup()
    {
        if ($this->isCustomerLoggedIn()) {
            return $this->httpContext->getValue(Context::CONTEXT_GROUP);
        } else {
            return Group::NOT_LOGGED_IN_ID;
        }
    }

    /**
     * Check Customer logged in
     *
     * @return mixed|null
     */
    public function isCustomerLoggedIn()
    {
        return $this->httpContext->getValue(Context::CONTEXT_AUTH);
    }

    /**
     * @param null $id
     * @return mixed
     */
    public function createModel($id = null)
    {
        $model = $this->promotionBarStoreViewFactory->create();
        return $model;
    }

    /**
     * Save process
     *
     * @param PromotionBar $modelPromotionBarStoreView
     * @return PromotionBar|null
     */
    public function save(PromotionBarStoreView $modelPromotionBarStoreView)
    {
        try {
            $this->resource->save($modelPromotionBarStoreView);
            return $modelPromotionBarStoreView;
        } catch (Exception $exception) {
            return null;
        }
    }

    /**
     * Delete
     *
     * @param PromotionBar $modelPromotionBarStoreView
     * @return bool
     */
    public function delete($id_bar)
    {
        try {
            $data = $this->getStoreViewByFilter($id_bar);
            foreach ($data as $value) {
                $item = $this->getById($value['id']);
                $this->resource->delete($item);
            }

        } catch (Exception $exception) {
            return false;
        }

    }

    /**
     * @param $idBar
     * @return mixed
     */
    public function getStoreViewByFilter($idBar)
    {
        $collection = $this->promotionBarStoreViewFactory->create()->getCollection();
        $collection->addFieldToFilter('id_bar', ['eq' => "$idBar"]);
        return $collection->getData();
    }

    /**
     * Get PromotionBar by id
     *
     * @param int|null $id
     * @return PromotionBar
     */
    public function getById($id = null)
    {
        $model = $this->promotionBarStoreViewFactory->create();
        if ($id) {
            $this->resource->load($model, $id);
        }
        return $model;
    }

}
