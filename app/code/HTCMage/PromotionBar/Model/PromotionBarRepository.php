<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Model;

use Exception;
use HTCMage\PromotionBar\Api;
use Magento\Customer\Model\Context;
use Magento\Customer\Model\Group;
use Magento\Customer\Model\Session;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class PromotionBarRepository
 *
 * @package HTCMage\PromotionBar\Model
 */
class PromotionBarRepository implements Api\PromotionBarRepositoryInterface
{

    /**
     * PromotionBarFactory
     *
     * @var PromotionBarFactory $PromotionBarFactory
     */
    private $promotionBarFactory;

    /**
     * Resource
     *
     * @var ResourceModel\PromotionBar $resource
     */
    private $resource;

    /**
     * Customer Session
     *
     * @var Session $customerSession
     */
    private $customerSession;

    /**
     * HTTP context
     *
     * @var \Magento\Framework\App\Http\Context $httpContext
     */
    private $httpContext;

    /**
     * Store manager
     *
     * @var StoreManagerInterface $storeManager
     */
    private $storeManager;

    /**
     * PromotionBarRepository constructor.
     *
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param Session $customerSession
     * @param PromotionBarFactory $PromotionBarFactory
     * @param ResourceModel\PromotionBar $resource
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        \Magento\Framework\App\Http\Context $httpContext,
        Session $customerSession,
        PromotionBarFactory $promotionBarFactory,
        ResourceModel\PromotionBar $resource
    )
    {
        $this->storeManager = $storeManager;
        $this->httpContext = $httpContext;
        $this->customerSession = $customerSession;
        $this->promotionBarFactory = $promotionBarFactory;
        $this->resource = $resource;
    }

    /**
     * Get Customer Group
     *
     * @return int
     */
    public function getCustomerGroup()
    {
        if ($this->isCustomerLoggedIn()) {
            return $this->httpContext->getValue(Context::CONTEXT_GROUP);
        } else {
            return Group::NOT_LOGGED_IN_ID;
        }
    }

    /**
     * Check Customer logged in
     *
     * @return mixed|null
     */
    public function isCustomerLoggedIn()
    {
        return $this->httpContext->getValue(Context::CONTEXT_AUTH);
    }

    /**
     * Get PromotionBar by id
     *
     * @param int|null $id
     * @return PromotionBar
     */
    public function getById($id = null)
    {
        $model = $this->promotionBarFactory->create();
        if ($id) {
            $this->resource->load($model, $id);
        }
        return $model;
    }

    /**
     * @param null $id
     * @return mixed
     */
    public function createModel($id = null)
    {
        $model = $this->promotionBarFactory->create();
        return $model;
    }

    /**
     * Save process
     *
     * @param PromotionBar $modelPromotionBar
     * @return PromotionBar|null
     */
    public function save(PromotionBar $modelPromotionBar)
    {
        try {
            $this->resource->save($modelPromotionBar);
            return $modelPromotionBar;
        } catch (Exception $exception) {
            return null;
        }
    }

    /**
     * Delete
     *
     * @param PromotionBar $modelPromotionBar
     * @return bool
     */
    public function delete(PromotionBar $modelPromotionBar)
    {
        try {
            $this->resource->delete($modelPromotionBar);
        } catch (Exception $exception) {
            return false;
        }

    }

}
