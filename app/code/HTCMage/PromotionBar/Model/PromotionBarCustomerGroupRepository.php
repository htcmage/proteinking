<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Model;

use Exception;
use HTCMage\PromotionBar\Api;
use Magento\Customer\Model\Context;
use Magento\Customer\Model\Group;
use Magento\Customer\Model\Session;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class PromotionBarRepository
 *
 * @package HTCMage\PromotionBar\Model
 */
class PromotionBarCustomerGroupRepository implements Api\PromotionBarCustomerGroupRepositoryInterface
{

    /**
     * promotionBarCustomerGroupFactory
     *
     * @var promotionBarCustomerGroupFactory $promotionBarCustomerGroupFactory
     */
    private $promotionBarCustomerGroupFactory;

    /**
     * Resource
     *
     * @var ResourceModel\PromotionBar $resource
     */
    private $resource;

    /**
     * Customer Session
     *
     * @var Session $customerSession
     */
    private $customerSession;

    /**
     * HTTP context
     *
     * @var \Magento\Framework\App\Http\Context $httpContext
     */
    private $httpContext;

    /**
     * Store manager
     *
     * @var StoreManagerInterface $storeManager
     */
    private $storeManager;

    /**
     * PromotionBarRepository constructor.
     *
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param Session $customerSession
     * @param promotionBarCustomerGroupFactory $promotionBarCustomerGroupFactory
     * @param ResourceModel\PromotionBar $resource
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        \Magento\Framework\App\Http\Context $httpContext,
        Session $customerSession,
        PromotionBarCustomerGroupFactory $promotionBarCustomerGroupFactory,
        ResourceModel\PromotionBarCustomerGroup $resource
    )
    {
        $this->storeManager = $storeManager;
        $this->httpContext = $httpContext;
        $this->customerSession = $customerSession;
        $this->promotionBarCustomerGroupFactory = $promotionBarCustomerGroupFactory;
        $this->resource = $resource;
    }

    /**
     * Get Customer Group
     *
     * @return int
     */
    public function getCustomerGroup()
    {
        if ($this->isCustomerLoggedIn()) {
            return $this->httpContext->getValue(Context::CONTEXT_GROUP);
        } else {
            return Group::NOT_LOGGED_IN_ID;
        }
    }

    /**
     * Check Customer logged in
     *
     * @return mixed|null
     */
    public function isCustomerLoggedIn()
    {
        return $this->httpContext->getValue(Context::CONTEXT_AUTH);
    }

    /**
     * @return mixed
     */
    public function createModel()
    {
        $model = $this->promotionBarCustomerGroupFactory->create();
        return $model;
    }

    /**
     * Save process
     *
     * @param PromotionBar $modelPromotionBarCustomerGroupFactory
     * @return PromotionBar|null
     */
    public function save(PromotionBarCustomerGroup $modelPromotionBarCustomerGroup)
    {
        try {
            $this->resource->save($modelPromotionBarCustomerGroup);
            return $modelPromotionBarCustomerGroup;
        } catch (Exception $exception) {
            return null;
        }
    }

    /**
     * Delete
     *
     * @param PromotionBar $modelPromotionBarCustomerGroupFactory
     * @return bool
     */
    public function delete($id_bar)
    {
        try {
            $data = $this->getCustomerGroupByFilter($id_bar);
            foreach ($data as $value) {
                $item = $this->getById($value['id']);
                $this->resource->delete($item);
            }

        } catch (Exception $exception) {
            return false;
        }

    }
    // public function saveCustomerGroup($model,$idBar,$data = []){
    //     $model = $this->createModel();
    //     foreach ($data as $value) {
    //         $dataCustomer['customer_group_id'] = $idBar;
    //         $dataCustomer['id_bar'] = $value;
    //         $model = $model->setData($dataCustomer);
    //         $model = $this->save($model);
    //     }
    // }

    /**
     * @param $idBar
     * @return mixed
     */
    public function getCustomerGroupByFilter($idBar)
    {
        $collection = $this->promotionBarCustomerGroupFactory->create()->getCollection();
        $collection->addFieldToFilter('id_bar', ['eq' => "$idBar"]);
        return $collection->getData();
    }

    /**
     * Get PromotionBar by id
     *
     * @param int|null $id
     * @return PromotionBar
     */
    public function getById($id = null)
    {
        $model = $this->promotionBarCustomerGroupFactory->create();
        if ($id) {
            $this->resource->load($model, $id);
        }
        return $model;
    }

}
