<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class PromotionBarCustomerGroup
 *
 * @package HTCMage\PromotionBar\Model\ResourceModel
 */
class PromotionBarCustomerGroup extends AbstractDb
{
    /**
     * PromotionBarCustomerGroup constructor.
     *
     * @param Context $context
     */
    public function __construct(
        Context $context
    )
    {
        parent::__construct($context);
    }

    /**
     * Constructor
     */
    protected function _construct()
    {
        $this->_init('htcmage_promotionbar_customer_group', 'id');
    }
}
