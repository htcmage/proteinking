<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Model\ResourceModel\PromotionBarStoreView;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package HTCMage\PromotionBar\Model\ResourceModel\PromotionBarStoreView
 */
class Collection extends AbstractCollection
{

    /**
     * @var string $_idFieldName
     */
    protected $_idFieldName = 'id';

    /**
     * function constructor
     */
    protected function _construct()
    {
        $this->_init(
            '\HTCMage\PromotionBar\Model\PromotionBarStoreView',
            '\HTCMage\PromotionBar\Model\ResourceModel\PromotionBarStoreView'
        );
    }
}

