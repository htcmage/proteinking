<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Model\ResourceModel\PromotionBarCustomerGroup;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package HTCMage\PromotionBar\Model\ResourceModel\PromotionBarCustomerGroup
 */
class Collection extends AbstractCollection
{

    /**
     * @var string $_idFieldName
     */
    protected $_idFieldName = 'id';

    /**
     * function constructor
     */
    protected function _construct()
    {
        $this->_init(
            '\HTCMage\PromotionBar\Model\PromotionBarCustomerGroup',
            '\HTCMage\PromotionBar\Model\ResourceModel\PromotionBarCustomerGroup'
        );
    }
}

