<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Model\ResourceModel\PromotionBar;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @var string $_idFieldName
     */
    protected $_idFieldName = 'id';

    /**
     * function constructor
     */
    protected function _construct()
    {
        $this->_init(
            '\HTCMage\PromotionBar\Model\PromotionBar',
            '\HTCMage\PromotionBar\Model\ResourceModel\PromotionBar'
        );
    }
}

