<?php
/**
 * *
 *  * @author HTCMage Team
 *  * @copyright Copyright (c) 2020 HTCMage (https://www.htcmage.com)
 *  * @package HTCMage_PromotionBar
 *
 */

namespace HTCMage\PromotionBar\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 *
 * @package HTCMage\PromotionBar\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        /**
         * Create table 'htcmage_promotionbar'
         */
        if (!$installer->tableExists('htcmage_promotionbar')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('htcmage_promotionbar')
            )->addColumn(
                'id',
                Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Bar Id'
            )->addColumn(
                'title',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Title'
            )->addColumn(
                'approve',
                Table::TYPE_SMALLINT,
                null,
                ['nullable' => false, 'default' => '2'],
                'Approve'
            )->addColumn(
                'content',
                Table::TYPE_TEXT,
                1024,
                ['nullable' => false],
                'Content'
            )->addColumn(
                'style',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Style'
            )->addColumn(
                'priority',
                Table::TYPE_INTEGER,
                255,
                ['nullable' => false],
                'Priority'
            )->addColumn(
                'start_date',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                'Start date'
            )->addColumn(
                'end_date',
                Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                'End date'
            )
                ->addColumn(
                    'page_display',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Page display '
                )
                ->addColumn(
                    'position',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Position'
                )
                ->addColumn(
                    'show_close_button',
                    Table::TYPE_SMALLINT,
                    null,
                    ['nullable' => false, 'default' => '2'],
                    'Show close button'
                )->addColumn(
                    'promotionbar_product',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Promotionbar Product'
                )
                ->addColumn(
                    'category_exclude',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Exclude Product'
                );
            $installer->getConnection()->createTable($table);
        }
        /**
         * Create table 'htcmage_promotionbar_store_view'
         */
        if (!$installer->tableExists('htcmage_promotionbar_store_view')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('htcmage_promotionbar_store_view')
            )->addColumn(
                'id',
                Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Link Id'
            )->addColumn(
                'store_id',
                Table::TYPE_SMALLINT,
                5,
                ['unsigned' => true, 'nullable' => false],
                'Store View'
            )->addColumn(
                'id_bar',
                Table::TYPE_SMALLINT,
                255,
                ['nullable' => false],
                'Id Bar'
            )->addForeignKey(
                $setup->getFkName('htcmage_promotionbar_store_view', 'id_bar', 'htcmage_promotionbar', 'id'),
                'id_bar',
                $setup->getTable('htcmage_promotionbar'),
                'id',
                Table::ACTION_CASCADE
            )
                ->addForeignKey(
                    $setup->getFkName('htcmage_promotionbar_store_view', 'store_id', 'store', 'store_id'),
                    'store_id',
                    $setup->getTable('store'),
                    'store_id',
                    Table::ACTION_CASCADE
                );
            $installer->getConnection()->createTable($table);
        }
        /**
         * Create table 'htcmage_promotionbar_customer_group'
         */
        if (!$installer->tableExists('htcmage_promotionbar_customer_group')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('htcmage_promotionbar_customer_group')
            )->addColumn(
                'id',
                Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'Link Id'
            )->addColumn(
                'customer_group_id',
                Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false],
                'Customer Group'
            )->addColumn(
                'id_bar',
                Table::TYPE_SMALLINT,
                255,
                ['nullable' => false],
                'Id Bar'
            )->addForeignKey(
                $setup->getFkName('htcmage_promotionbar_customer_group', 'id_bar', 'htcmage_promotionbar', 'id'),
                'id_bar',
                $setup->getTable('htcmage_promotionbar'),
                'id',
                Table::ACTION_CASCADE
            )->addForeignKey(
                $setup->getFkName('htcmage_promotionbar_customer_group', 'customer_group_id', 'customer_group', 'customer_group_id'),
                'customer_group_id',
                $setup->getTable('customer_group'),
                'customer_group_id',
                Table::ACTION_CASCADE
            );
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}
