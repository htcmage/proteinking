<?php

namespace HTCMage\CustomUrlBlog\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class CustomPredispatch implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        $request = $observer->getEvent()->getRequest();
        if(substr($request->getRequestUri(), -1) !== '/'){
            $observer->getEvent()->getControllerAction()->getResponse()->setRedirect($request->getRequestUri() . '/', 301)->sendResponse();
        }
    }

}