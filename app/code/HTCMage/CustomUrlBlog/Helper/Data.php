<?php

namespace HTCMage\CustomUrlBlog\Helper;

use Mageplaza\Blog\Helper\Data as MageBlogData;

class Data extends MageBlogData
{
    /**
     * @param null $urlKey
     * @param null $type
     * @return string
     */
    public function getBlogUrl($urlKey = null, $type = null)
    {
        if ($type == 'post') {
            $type = '';
        }

        if (is_object($urlKey)) {
            $urlKey = $urlKey->getUrlKey();
        }

        $urlKey = ($type ? $type . '/' : '') . $urlKey;
        $url = $this->getUrl($this->getRoute() . '/' . $urlKey);
        
        $url = explode('?', $url);
        $url = $url[0];
        return rtrim($url, '/') . $this->getUrlSuffix();
    }
}
