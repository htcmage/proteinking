<?php

namespace HTCMage\DifferentQty\Model\ResourceModel\Product;

use Magento\Catalog\Model\ResourceModel\Product\Collection as AbstractCollection;

class Collection extends AbstractCollection
{

	/**
     * Initialize collection select
     * Redeclared for remove entity_type_id condition
     * in catalog_product_entity we store just products
     *
     * @return $this
     */
    protected function _initSelect()
    {
        if ($this->isEnabledFlat()) {
            $this->getSelect()->from(
                [self::MAIN_TABLE_ALIAS => $this->getEntity()->getFlatTableName()],
                null
            )->columns(
                ['status' => new \Zend_Db_Expr(ProductStatus::STATUS_ENABLED)]
            );
            $this->addAttributeToSelect($this->getResource()->getDefaultAttributes());
            if ($this->_catalogProductFlatState->getFlatIndexerHelper()->isAddChildData()) {
                $this->getSelect()->where('e.is_child=?', 0);
                $this->addAttributeToSelect(['child_id', 'is_child']);
            }
        } else {
            $this->getSelect()->from([self::MAIN_TABLE_ALIAS => $this->getEntity()->getEntityTable()]);
        }
        $inventorySubQuery = new \Zend_Db_Expr('(SELECT sku, SUM(quantity) as quantity_ordered FROM inventory_reservation GROUP BY sku)');

        $this->getSelect()->join( array( 'inventory' => "cataloginventory_stock_status" ), 'e.entity_id = inventory.product_id', array('inventory.qty AS qty'))
        ->join( array( 'inventorySubQuery' => $inventorySubQuery ), 'e.sku = inventorySubQuery.sku', array('inventorySubQuery.quantity_ordered', ('(inventory.qty + inventorySubQuery.quantity_ordered) as salable_qty')))
        ->where('inventorySubQuery.quantity_ordered != 0');
        return $this;
    }
}