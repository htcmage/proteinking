<?php

namespace HTCMage\DifferentQty\Ui\Component\Listing\Column;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Escaper;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\App\ResourceConnection;
use Magento\Sales\Model\Order as OrderSales;

/**
 * Class Actions
 * @package HTCMage\DifferentQty\Ui\Component\Listing\Column
 */
class Order extends Column
{
    /**
     * Url path
     */
    const URL_PATH_ORDER = 'sales/order/view';
    /**
     * @var Escaper
     */
    private $escaper;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        ResourceConnection $resource,
        array $components = [],
        array $data = []
    )
    {
        $this->urlBuilder = $urlBuilder;
        $this->resource = $resource;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @inheritDoc
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$items) {  
                $orderIds = $this->getOrders($items['entity_id']);
                $orderIsNotComplete = $this->getOrderIsNotComplete($orderIds);
                $items['order'] = html_entity_decode($this->getHtmlDownload($orderIsNotComplete)); 
            }
        }

        return $dataSource;
    }

    public function getHtmlDownload($orderIsNotComplete){
        $html = "";
        foreach ($orderIsNotComplete as $order) {
            $url = $this->urlBuilder->getUrl(static::URL_PATH_ORDER,['order_id' => $order['entity_id']]);
            $html .= '<p><a target="_blank" href="' . $url . '">'. $order['entity_id'] . '</a></p>';
        }
        return $html;
    }

    public function getOrders($productId)
    {
        $adapter = $this->resource->getConnection();
        $itemTbl = $adapter->getTableName("sales_order_item");
        $select = $adapter->select()->from(
            ['itemTbl' => $itemTbl],
            ['order_id']
        )
        ->where(
            'product_id = ?', $productId
        );
        $listOrderItem = $adapter->fetchAll($select);
        $data = [];
        foreach ($listOrderItem as $orderItem) {
            $data[] = $orderItem['order_id'];
        }
        return $data;
    }

    public function getOrderIsNotComplete($orderIds){
        $adapter = $this->resource->getConnection();
        $orderTbl = $adapter->getTableName("sales_order");
        $select = $adapter->select()->from(
            ['orderTbl' => $orderTbl],
            ['entity_id', 'increment_id']
        )
        ->where(
            'entity_id IN (?)', $orderIds
        )
        ->where(
            'state != ?', OrderSales::STATE_COMPLETE
        );
        return $adapter->fetchAll($select);
    }

    /**
     * Get instance of escaper
     *
     * @return Escaper
     * @deprecated 101.0.7
     */
    private function getEscaper()
    {
        if (!$this->escaper) {
            $this->escaper = ObjectManager::getInstance()->get(Escaper::class);
        }
        return $this->escaper;
    }
}
