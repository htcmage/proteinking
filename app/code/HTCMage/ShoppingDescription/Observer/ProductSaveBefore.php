<?php
namespace HTCMage\ShoppingDescription\Observer;

use Magento\Framework\Event\ObserverInterface;

class ProductSaveBefore implements ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getProduct();

        $nutritionInfo = $product->getNutritionInfo();
        $description = $product->getDescription();
        if (empty($nutritionInfo) && empty($description)) {
        	return;
        }
        $shoppingDescription = $description . $nutritionInfo;

        $attributeCode = 'shopping_description';
        $product->setData($attributeCode, $shoppingDescription);
    }
}