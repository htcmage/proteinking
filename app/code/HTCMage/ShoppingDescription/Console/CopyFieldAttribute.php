<?php

namespace HTCMage\ShoppingDescription\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CopyFieldAttribute extends Command
{
 	/**
   	* @var \Psr\Log\LoggerInterface $logger
   	*/
  	protected $logger;

    public function __construct(
    	\Psr\Log\LoggerInterface $logger,
    	string $name = null
    ){
        $this->logger = $logger;
        parent::__construct($name);
    }

    protected function configure()
    {
    	$this->setName('copy:nutrition-info');
	    $this->setDescription("Copy field 'Nutrition Info' from grouped product to associated simple products");
	         
	    parent::configure();
    }

    protected function getConnection()
  	{
  		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		return $resource->getConnection();
  	}

  	protected function execute(InputInterface $input, OutputInterface $output)
    {
    	try {
    		$connection = $this->getConnection();
			$attrNutritionId = $this->getAttributeId('nutrition_info');

			$tableCataProdEn = $connection->getTableName('catalog_product_entity');
			$tableCataProdEnText = $connection->getTableName('catalog_product_entity_text');
			$tableCataProdRelation = $connection->getTableName('catalog_product_relation');
			

			$select = $connection->select()
				    ->from(
				        ['t1' => $tableCataProdEn],
				        't1.entity_id'
				    )->joinLeft(
				    	['t2' => $tableCataProdEnText],
				    	"t1.entity_id = t2.entity_id AND t2.attribute_id = $attrNutritionId",
			    		't2.value as description'
				    )->join(
				    	['t3' => $tableCataProdRelation],
				    	"t3.parent_id = t1.entity_id",
				    	'group_concat(t3.child_id) as children'
				    )->where(
				    	't1.type_id = ?', 'grouped'
				    )->where(
				    	't2.store_id = ?', 0
				    )->group(
				    	't1.entity_id'
				    );
			
		    $data = $connection->fetchAll($select);

		    $dataInsert = [];
		    foreach ($data as $value) {
		    	$nutriDescription = $value['description'];
		    	$childProIds = explode(',', $value['children']);
		    	foreach ($childProIds as $childProId) {
		    		$row = [];
			    	$row['entity_id'] = $childProId;
			    	$row['value'] = $nutriDescription;
			    	$row['attribute_id'] = $attrNutritionId;
			    	$row['store_id'] = 0;
			    	$dataInsert[] = $row;
		    	}
		    }
			
		    $connection->insertOnDuplicate($tableCataProdEnText, $dataInsert);


    	} catch (Exception $e) {
    		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/copy-nutrition.log');
	      	$logger = new \Zend\Log\Logger();
	      	$logger->addWriter($writer);
	      	$logger->info($e->getMessage());
    	}
    }

    protected function getAttributeId($code)
    {
		$connection = $this->getConnection();
	    $tableEav = $connection->getTableName('eav_attribute');
	    $eavAttr = $connection->select()
	    		 ->from(
	    		 	$tableEav 
	    		 )->where(
	    		 	'attribute_code = ?', $code
	    		 )->where(
	    		 	'entity_type_id = ?', 4
	    		 );
	    $dataEavAtrr = $connection->fetchRow($eavAttr);
	    return $dataEavAtrr['attribute_id'];
    }
}