<?php

namespace HTCMage\ShoppingDescription\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CombineAttributes extends Command
{
    /**
   	* @var \Psr\Log\LoggerInterface $logger
   	*/
  	protected $logger;

    public function __construct(
    	\Psr\Log\LoggerInterface $logger,
    	string $name = null
    ){
        $this->logger = $logger;
    	parent::__construct($name);
    }

    protected function configure()
    {
    	$this->setName('combine:atrribute');
	    $this->setDescription("Combine attribute 'description' and 'nutrition information' ");
	         
	    parent::configure();
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null|int
     */

  	protected function getConnection()
  	{
  		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		return $resource->getConnection();
  	}

    protected function execute(InputInterface $input, OutputInterface $output)
    {
    	try {

			$connection = $this->getConnection();
			//Get attribute id
			$attrNutritionId = $this->getAttributeId('nutrition_info');
			$attrDescriptionId = $this->getAttributeId('description');
			$attrShoppingDesId =  $this->getAttributeId('shopping_description');

			$tableCataProdEnText = $connection->getTableName('catalog_product_entity_text');
			$select = $connection->select()
				    ->from(
				        ['t1' => $tableCataProdEnText],
				        ['t1.entity_id', 't1.value as des']
				    )->joinLeft(
				    	['t2' => $tableCataProdEnText],
				    	"t1.entity_id = t2.entity_id AND t2.attribute_id = $attrNutritionId",
				    	't2.value as nutri'
				    )->where(
				    	't1.attribute_id = ?', $attrDescriptionId
				    );

		    $data = $connection->fetchAll($select);

		    $dataInsert = [];
		    foreach ($data as $value) {
		    	$row = [];

		    	if (empty($value['des']) && empty($value['nutri'])) {
		        	continue;
		        }

		        // if (isset($value['entity_id'])) {
		        // 	break;
		        // }

		    	$row['entity_id'] = $value['entity_id'];
		    	$row['value'] = $value['des'] . $value['nutri'];
		    	$row['attribute_id'] = $attrShoppingDesId;
		    	$row['store_id'] = 0;
		    	$dataInsert[] = $row;
		    }
		    echo 'test';die();
		    $connection->insertOnDuplicate($tableCataProdEnText, $dataInsert);

    	} catch (Exception $e) {
    		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/combine-attribute.log');
	      	$logger = new \Zend\Log\Logger();
	      	$logger->addWriter($writer);
	      	$logger->info($e->getMessage());
    	}
    }
    protected function getAttributeId($code)
    {
		$connection = $this->getConnection();
	    $tableEav = $connection->getTableName('eav_attribute');
	    $eavAttr = $connection->select()
	    		 ->from(
	    		 	$tableEav 
	    		 )->where(
	    		 	'attribute_code = ?', $code
	    		 )->where(
	    		 	'entity_type_id = ?', 4
	    		 );
	    $dataEavAtrr = $connection->fetchRow($eavAttr);
	    return $dataEavAtrr['attribute_id'];
    }
}
