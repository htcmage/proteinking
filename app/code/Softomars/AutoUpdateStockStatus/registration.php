<?php
/**
 *  * @author Softomars Applications Team
 *  * @copyright Copyright (c) 2020 Softomars (https://www.softomars.com)
 *  * @package Softomars_AutoUpdateStockStatus
 *
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Softomars_AutoUpdateStockStatus',
    __DIR__
);
