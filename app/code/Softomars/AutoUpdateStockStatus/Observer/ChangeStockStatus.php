<?php

namespace Softomars\AutoUpdateStockStatus\Observer;

use Exception;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\GroupedProduct\Model\Product\Type\Grouped;
use Psr\Log\LoggerInterface;

class ChangeStockStatus implements ObserverInterface
{
    protected $stockRegistry;
    protected $logger;

    /**
     * @var Grouped
     */
    protected $groupedInstance;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    public function __construct(
        StockRegistryInterface $stockRegistry,
        LoggerInterface $logger,
        Grouped $groupedInstance,
        ProductRepositoryInterface $productRepository
    ) {
        $this->stockRegistry = $stockRegistry;
        $this->logger = $logger;
        $this->groupedInstance = $groupedInstance;
        $this->productRepository = $productRepository;
    }

    /**
     * @inheritDoc
     * @return void
     */
    public function execute(Observer $observer)
    {
        /**
         * @var $product Product
         */
        $product = $observer->getProduct();
        $sku = $product->getSku();
        try {
            $stockItem = $this->stockRegistry->getStockItemBySku($sku);
            $qty = $stockItem->getQty();
            if ($qty) {
                $stockItem->setIsInStock((bool)$qty ? 1 : 0);
                $this->stockRegistry->updateStockItemBySku($sku, $stockItem);
                if ($product->getTypeId() == 'simple') {
                    $groupedIds = $this->groupedInstance->getParentIdsByChild($product->getId());
                    foreach ($groupedIds as $groupedId) {
                        $parent = $this->productRepository->getById($groupedId);
                        $parentStockItem = $this->stockRegistry->getStockItemBySku($parent->getSku());
                        $parentStockItem->setIsInStock(1);
                        $this->stockRegistry->updateStockItemBySku($parent->getSku(), $parentStockItem);
                    }
                }
            }
        } catch (Exception $e) {
            $this->logger->debug("Softomars Auto Stock Update Log: " . $e->getMessage());
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/malaz.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info($e->getMessage());
        }
    }
}

