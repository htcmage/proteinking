<?php

namespace Softomars\ToolTipRemoval\Plugin;

use Magento\Checkout\Block\Checkout\LayoutProcessor;

class LayoutProcessorPlugin
{
   
    public function afterProcess(
        LayoutProcessor $subject,
        $jsLayout
    ) {
   
      //Remove tooltip for email
        unset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['customer-email']['tooltip']);
	unset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['email']['config']['tooltip']);
	//$jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['customer-email']['config']['tooltip'] = null;
      //Remove tooltip for telephone
        unset($jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['telephone']['config']['tooltip']);
     
     


        return $jsLayout;
    }
}