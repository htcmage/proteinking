<?php

namespace Softomars\EnableDuplicate\Plugin;

use Magento\Catalog\Model\Product\Copier;
use Magento\Store\Api\StoreRepositoryInterface;

class EnableDuplicate
{
     /**
     * @var StoreRepositoryInterface
     */
    private $storeRepository;

    public function __construct(StoreRepositoryInterface $storeRepository)
    {
        $this->storeRepository = $storeRepository;
    }

    public function afterCopy(Copier $subject, $result)
    {
	$stores = $this->storeRepository->getList();
        foreach ($stores as $store) {
            $result->setStoreId($store->getId());
            $result->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
            $result->save();
        }
        return $result;
    }
}
