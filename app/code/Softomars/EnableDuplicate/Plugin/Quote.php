<?php

namespace Softomars\EnableDuplicate\Plugin;
use Magento\Quote\Model\Quote as QuoteModel;

class Quote
{
    public function afterGetCustomerIsGuest(QuoteModel $subject, $result)
    {
        $customer = $subject->getCustomer();

        return $result && ($customer->getId() === null);
    }

}
