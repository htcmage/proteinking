<?php

namespace Softomars\BestSellerFilter\Cron;

use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\DB\TransactionFactory;
use Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory;
use Psr\Log\LoggerInterface;

class UpdateSellsCountValue
{
    const BATCH_SIZE = 50;

    /**
     * @var ProductRepository;
     */
    protected $productRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var TransactionFactory
     */
    protected $transactionFactory;

    /**
     * @var CollectionFactory
     */
    protected $itemCollection;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(
        ProductRepository $productRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        TransactionFactory $transactionFactory,
        CollectionFactory $itemCollection,
        LoggerInterface $logger
    ) {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->productRepository = $productRepository;
        $this->transactionFactory = $transactionFactory;
        $this->itemCollection = $itemCollection;
        $this->logger = $logger;
    }

    public function execute()
    {
        $this->logger->debug("Softomars Starting the Cron . ");
        $products = $this->productRepository->getList(
            $this->searchCriteriaBuilder
                ->addFilter('bestseller', null, "null")
                ->setPageSize(static::BATCH_SIZE)
                ->create()
        )->getItems();

        $saveTransaction = $this->transactionFactory->create();
        foreach ($products as $product) {
            $itemCollection = $this->itemCollection->create()
                ->addFieldToFilter('product_id', $product->getId());
            $itemCollection->getSelect()
                ->columns('SUM(qty_ordered) as total_qty')
                ->group('product_id');
            $soldCount = (int)$itemCollection->getFirstItem()->getTotalQty() ?? 0;
            $this->logger->debug("sold count for " . $product->getSku() . " is " . $soldCount);
            $product->setCustomAttribute("bestseller", $soldCount);
            $this->logger->debug("softomars adding product " . $product->getSku());
            $this->productRepository->save($product);
            $saveTransaction->addObject($product);
        }
        try {
            $saveTransaction->save();
            $this->logger->debug("Softomars save transaction ");
        } catch (\Exception $e) {
            $this->logger->debug("Softomars Cron Sold Count Updater " . $e->getMessage());
        }

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/softomars_bestseller.log');
        $loggerC = new \Zend\Log\Logger();
        $loggerC->addWriter($writer);
        $left = $this->productRepository->getList(
            $this->searchCriteriaBuilder
                ->addFilter('bestseller', null, "null")
                ->create()
        )->getItems();
        $loggerC->info("Products Count is " . count($products) . " Left: " . count($left));

    }
}
