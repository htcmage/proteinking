<?php
/**
 * @author Softomars Team
 * @copyright Copyright (c) 2019 Softomars Applications (https://www.softomars.com)
 * @package Softomars_Whatsapp
 */

namespace Softomars\BestSellerFilter\Setup;

use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    private $eavSetupFactory;

    /**
     * InstallData constructor.
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Zend_Validate_Exception
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            Product::ENTITY,
            'bestseller',
            [
                'type'         => 'int',
                'label'        => 'BestSeller',
                'input'        => 'text',
                'sort_order'   => 100,
                'source'       => '',
                'global'       => ScopedAttributeInterface::SCOPE_STORE,
                'visible'      => true,
                'required'     => false,
                'user_defined' => false,
                'default'      => '0',
                'group'        => 'General',
                'backend'      => '',
                'filterable' => true,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'filterable_in_search' => true,
                'used_for_sort_by' => true
            ]
        );
        $setup->endSetup();
    }
}
