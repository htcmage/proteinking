<?php

namespace Softomars\BestSellerFilter\Plugin;

use Exception;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\DB\TransactionFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Service\OrderService;
use Psr\Log\LoggerInterface;

class IncrementSellsCount
{
    const BESTSELLER_ATTRIBUTE_CODE = 'bestseller';

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var TransactionFactory
     */
    protected $transactionFactory;

    /**
     * IncrementSellsCount constructor.
     * @param LoggerInterface $logger
     * @param ProductRepositoryInterface $productRepository
     * @param TransactionFactory $transactionFactory
     */
    public function __construct(
        LoggerInterface $logger,
        ProductRepositoryInterface $productRepository,
        TransactionFactory $transactionFactory
    ) {
        $this->logger = $logger;
        $this->productRepository = $productRepository;
        $this->transactionFactory = $transactionFactory;
    }

    public function afterPlace(OrderService $subject, $result, OrderInterface $order)
    {
        foreach ($order->getItems() as $item) {
            try {
                $product = clone $this->productRepository->getById($item->getProductId());
                $this->logger->debug($product->getSku());
                // here to increment the sells count
                $bestSellerAttribute = $product->getCustomAttribute(static::BESTSELLER_ATTRIBUTE_CODE);
                $currentSells = 0;
                if ($bestSellerAttribute) {
                    $currentSells = $bestSellerAttribute->getValue() ?? 0;
                }
                $product->addAttributeUpdate(
                    static::BESTSELLER_ATTRIBUTE_CODE,
                    $currentSells + $item->getQtyOrdered(),
                    0
                );
            } catch (NoSuchEntityException $e) {
                $this->logger->debug("Can't find the product");
            } catch (Exception $e) {
                $this->logger->debug("Can't save the product after increasing sells count");
            }
        }
        return $result;
    }
}

