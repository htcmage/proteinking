require(['jquery'], function($) {
	$(document).ready(function() {
		setTimeout( () => {
			$('.owl-carousel .owl-item img').css('max-height', '653px');
			$('.owl-item').find('.product.photo.product-item-photo').css('min-height','380px');
			$('.products-grid.product-item.product-item-details.product-item-name').css('min-height','60px');
			$('.item.product.product-item').css('min-height','520px');

			$('.brandLogoSlider').css('margin-bottom','130px');
			$('.parallax.initial').css('background-position','center center');
			$('form#search_mini_form').css('height', '53px');
		}, 1200);
	});
});
