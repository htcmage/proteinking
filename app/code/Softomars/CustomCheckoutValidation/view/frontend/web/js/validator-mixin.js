define([
    'jquery',
    'moment',
    'Magento_Checkout/js/model/quote'
], function ($, moment, quote) {
    'use strict';

    return function (validator) {
        validator.addRule(
            'phoneAU',
            function (value, params, additionalParams) {
                var countryId = quote.shippingAddress().countryId;
                if(countryId === 'AU'){
                    return $.mage.isEmptyNoTrim(value) || value.length > 7;
                }
                return true;
            },
            $.mage.__("Please provide a valid phone (8 digits at least)")
        );
        return validator;
    };
});
