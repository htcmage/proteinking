var config = {
    config: {
        mixins: {
            'Magento_Ui/js/lib/validation/validator': {
                'Softomars_CustomCheckoutValidation/js/validator-mixin': true
            }
        }
    },
    map: {
        '*': {
            'Magento_Checkout/js/model/checkout-data-resolver': 'Softomars_CustomCheckoutValidation/js/model/checkout-data-resolver',
	    'Magento_Review/js/process-reviews': 'Softomars_CustomCheckoutValidation/js/process-reviews'
        }
    }
};

