<?php
/**
 *  * @author Softomars Applications Team
 *  * @copyright Copyright (c) 2020 Softomars (https://www.softomars.com)
 *  * @package Softomars_CustomCheckoutValidation
 *
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Softomars_CustomCheckoutValidation',
    __DIR__
);
