<?php
/**
 *  * @author Softomars Applications Team
 *  * @copyright Copyright (c) 2020 Softomars (https://www.softomars.com)
 *  * @package Softomars_DecimalPoints
 *
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Softomars_DecimalPoints',
    __DIR__
);
