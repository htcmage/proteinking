<?php

namespace Softomars\DecimalPoints\Ui\Component\Listing\Columns;

class SpecialPrice extends Price
{
    /**
     * Column name
     */
    const NAME = 'column.special_price';
}
