<?php

namespace Softomars\DecimalPoints\Ui\Component\Listing\Columns;

use Magento\Directory\Model\Currency;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Store\Model\StoreManagerInterface;

class Price extends \Magento\Ui\Component\Listing\Columns\Column
{
    protected $storeManager;
    protected $currency;

    public function __construct(
        StoreManagerInterface $storeManager,
        Currency $currency,
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->storeManager = $storeManager;
        $this->currency = $currency;
    }

    /**
     * Column name
     */
    const NAME = 'column.price';

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item[$fieldName])) {
                    $item[$fieldName] = number_format((float) $item[$fieldName], 2);
                }
            }
        }
        return $dataSource;
    }
}
