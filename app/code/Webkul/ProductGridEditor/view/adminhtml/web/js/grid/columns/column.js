/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_ProductGridEditor
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

define([
    'underscore',
    'uiRegistry',
    'mageUtils'
], function (_, registry, utils) {
        'use strict';

        return function (Column) {
            return Column.extend({
                _getFieldCallback: function (action, rowIndex) {
                    var args     = action.params || [],
                        callback = action.target;
                    if (!window.isProductGridEditorEnabled) {
                        action.params[0] = 'edit';
                        action.params[1] = rowIndex;
                        action.provider = 'product_listing.product_listing.product_columns.actions';
                        action.target = 'applyAction';
                    }

                    if (action.provider && action.target) {
                        args.unshift(action.target);

                        callback = registry.async(action.provider);
                    }

                    if (!_.isFunction(callback)) {
                        return false;
                    }

                    return function () {
                        callback.apply(callback, args);
                    };
                },

                applySingleAction: function (rowIndex, action) {
                    var callback;

                    action = action || this.fieldAction;
                    action = utils.template(action, {
                        column: this,
                        rowIndex: rowIndex
                    }, true);

                    callback = this._getFieldCallback(action, rowIndex);

                    if (_.isFunction(callback)) {
                        callback();
                    }
                }
            });
        }
    });
