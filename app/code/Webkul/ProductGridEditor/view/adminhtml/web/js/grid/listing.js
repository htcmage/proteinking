/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_ProductGridEditor
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

define([
    'uiLayout'
], function (layout) {
        'use strict';

        return function (Listing) {
            return Listing.extend({
                initEditor: function () {
                    if (this.editorConfig.name == 'product_listing.product_listing.product_columns_editor') {
                        if (window.isProductGridEditorEnabled) {
                            if (this.editorConfig.enabled) {
                                layout([this.editorConfig]);
                            }
                        }
                    } else {
                        if (this.editorConfig.enabled) {
                            layout([this.editorConfig]);
                        }
                    }

                    return this;
                }
            });
        }
    });
