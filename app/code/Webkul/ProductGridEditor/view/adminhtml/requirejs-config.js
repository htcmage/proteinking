/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_ProductGridEditor
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

var config = {
    config: {
        mixins: {
            'Magento_Ui/js/grid/listing': {
                'Webkul_ProductGridEditor/js/grid/listing': true
            },
            'Magento_Ui/js/grid/columns/column': {
                'Webkul_ProductGridEditor/js/grid/columns/column': true
            }
        }
    },

    map: {
        '*': {
            'Magento_Ui/js/form/element/select': 'Webkul_ProductGridEditor/js/form/element/select'
        }
    }
};
