<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_ProductGridEditor
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\ProductGridEditor\Block\Adminhtml\Product;

class GridJs extends \Magento\Backend\Block\Template
{
    /**
     * @var $_dataHelper
     */
    protected $_dataHelper;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Webkul\ProductGridEditor\Helper\Data $dataHelper
    ) {
        parent::__construct($context);
        $this->_dataHelper = $dataHelper;
    }

    /**
     * check module enable/disable
     * @param void
     * @return boolean
     */
    public function isModuleEnabled()
    {
        return $this->_dataHelper->isModuleEnabled();
    }
}
