<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_ProductGridEditor
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\ProductGridEditor\Ui\Component\Product;

use Magento\Framework\AuthorizationInterface;
use Magento\Framework\View\Element\UiComponentInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;

class MassAction extends \Magento\Catalog\Ui\Component\Product\MassAction
{
    /**
     * @var $_dataHelper
     */
    protected $_dataHelper;

    /**
     * @param \Webkul\ProductGridEditor\Helper\Data $dataHelper
     * @param AuthorizationInterface $authorization
     * @param ContextInterface $context
     * @param UiComponentInterface[] $components
     * @param array $data
     */
    public function __construct(
        \Webkul\ProductGridEditor\Helper\Data $dataHelper,
        AuthorizationInterface $authorization,
        ContextInterface $context,
        array $components = [],
        array $data = []
    ) {
        $this->_dataHelper = $dataHelper;
        $this->authorization = $authorization;
        parent::__construct($authorization, $context, $components, $data);
    }

    /**
     * @inheritdoc
     */
    public function prepare() : void
    {
        $config = $this->getConfiguration();

        foreach ($this->getChildComponents() as $actionComponent) {
            $actionType = $actionComponent->getConfiguration()['type'];

            if ($this->_dataHelper->isModuleEnabled()) {
                if ($this->isActionAllowed($actionType)) {
                    $config['actions'][] = $actionComponent->getConfiguration();
                }
            } else {
                if ($actionType != 'duplicate') {
                    if ($this->isActionAllowed($actionType)) {
                        $config['actions'][] = $actionComponent->getConfiguration();
                    }
                }
            }
        }
        $origConfig = $this->getConfiguration();
        if ($origConfig !== $config) {
            $config = array_replace_recursive($config, $origConfig);
        }

        $this->setData('config', $config);
        $this->components = [];

        parent::prepare();
    }
}
