<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_ProductGridEditor
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\ProductGridEditor\Ui\Component\Listing;

class Columns extends \Magento\Ui\Component\Listing\Columns
{
    /**
     * Default columns max order
     */
    const DEFAULT_COLUMNS_MAX_ORDER = 100;

    /**
     * @var \Magento\Catalog\Ui\Component\Listing\Attribute\RepositoryInterface
     */
    protected $attributeRepository;

    /**
     * @var array
     */
    protected $filterMap = [
        'default' => 'text',
        'select' => 'select',
        'boolean' => 'select',
        'multiselect' => 'select',
        'date' => 'dateRange',
    ];

    /**
     * @var array
     */
    protected $dataTypeMap = [
        'text' => 'text',
        'boolean' => 'select',
        'select' => 'select',
        'date' => 'date',
        'price' => 'text',
        'weight' => 'text'
    ];

    /**
     * @var $_dataHelper
     */
    protected $_dataHelper;

    /**
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface $context
     * @param \Magento\Catalog\Ui\Component\ColumnFactory $columnFactory
     * @param \Magento\Catalog\Ui\Component\Listing\Attribute\RepositoryInterface $attributeRepository
     * @param \Webkul\ProductGridEditor\Helper\Data $dataHelper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Catalog\Ui\Component\ColumnFactory $columnFactory,
        \Magento\Catalog\Ui\Component\Listing\Attribute\RepositoryInterface $attributeRepository,
        \Webkul\ProductGridEditor\Helper\Data $dataHelper,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $components, $data);
        $this->columnFactory = $columnFactory;
        $this->attributeRepository = $attributeRepository;
        $this->_dataHelper = $dataHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function prepare()
    {
        $columnSortOrder = self::DEFAULT_COLUMNS_MAX_ORDER;
        $isGridEditorEnabled = $this->_dataHelper->isModuleEnabled();

        foreach ($this->attributeRepository->getList() as $attribute) {
            // Softomars/ execluding price and special price
	    if(in_array($attribute->getAttributeCode(), ['price','special_price'])) {
		continue;
	    }

	    $config = [];

            if ($isGridEditorEnabled) {
                $config['sortOrder'] = ++$columnSortOrder;
                if ($attribute->getIsFilterableInGrid()) {
                    $config['filter'] = $this->getFilterType($attribute->getFrontendInput());
                }

                $dataType = $this->getDataType($attribute);
                if ($dataType != '') {
                    $config['editor']['editorType'] = $dataType;
                }

                $column = $this->columnFactory->create($attribute, $this->getContext(), $config);
                $column->prepare();
                $this->addComponent($attribute->getAttributeCode(), $column);
            } else {
                if (!isset($this->components[$attribute->getAttributeCode()])) {
                    $config['sortOrder'] = ++$columnSortOrder;
                    if ($attribute->getIsFilterableInGrid()) {
                        $config['filter'] = $this->getFilterType($attribute->getFrontendInput());
                    }

                    $column = $this->columnFactory->create($attribute, $this->getContext(), $config);
                    $column->prepare();
                    $this->addComponent($attribute->getAttributeCode(), $column);
                }
            }
        }
        parent::prepare();
    }

    /**
     * Retrieve filter type by $frontendInput
     * @param string $frontendInput
     * @return string
     */
    protected function getFilterType($frontendInput)
    {
        return $this->filterMap[$frontendInput] ?? $this->filterMap['default'];
    }

    /**
     * get the datatype of attribute
     * @param \Magento\Catalog\Api\Data\ProductAttributeInterface $attribute
     * @return string
     */
    protected function getDataType($attribute)
    {
        return isset($this->dataTypeMap[$attribute->getFrontendInput()])
            ? $this->dataTypeMap[$attribute->getFrontendInput()]
            : '';
    }
}
