<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_ProductGridEditor
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\ProductGridEditor\Ui\Component\Listing\Attribute;

/**
 * @api
 * @since 100.0.2
 */
class Repository extends \Magento\Catalog\Ui\Component\Listing\Attribute\Repository
{
    /**
     * @var $_dataHelper
     */
    protected $_dataHelper;

    /**
     * @param \Webkul\ProductGridEditor\Helper\Data $dataHelper
     * @param \Magento\Catalog\Api\ProductAttributeRepositoryInterface $productAttributeRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        \Webkul\ProductGridEditor\Helper\Data $dataHelper,
        \Magento\Catalog\Api\ProductAttributeRepositoryInterface $productAttributeRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        parent::__construct($productAttributeRepository, $searchCriteriaBuilder);
        $this->_dataHelper = $dataHelper;
    }

    /**
     * {@inheritdoc}
     */
    protected function buildSearchCriteria()
    {
        if ($this->_dataHelper->isModuleEnabled()) {
            return $this->searchCriteriaBuilder->create();
        } else {
            return $this->searchCriteriaBuilder->addFilter('additional_table.is_used_in_grid', 1)->create();
        }
    }
}
