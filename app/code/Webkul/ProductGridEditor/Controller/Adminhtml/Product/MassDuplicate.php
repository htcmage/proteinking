<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_ProductGridEditor
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\ProductGridEditor\Controller\Adminhtml\Product;

use Magento\Framework\Controller\ResultFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class MassDuplicate extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
     const ADMIN_RESOURCE = 'Webkul_ProductGridEditor::productgrideditor';

    /**
     * @var Filter
     */
    protected $_filter;

    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var $_productRepository
     */
    protected $_productRepository;

    /**
     * @var $_productCopier
     */
    protected $_productCopier;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Model\Product\Copier $copier,
        Filter $filter,
        CollectionFactory $collectionFactory
    ) {
        parent::__construct($context);
        $this->_productRepository = $productRepository;
        $this->_filter = $filter;
        $this->_collectionFactory = $collectionFactory;
        $this->_productCopier = $copier;
    }

    /**
     * Execute action
     * It call when admin create duplicate product
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $collection = $this->_filter->getCollection(
            $this->_collectionFactory->create()
        );

        $orderId = 0;

        foreach ($collection->getItems() as $item) {
            $this->_productRepository->cleanCache();
            $product = $this->_productRepository->getById($item->getEntityId());
            $this->createDuplicate($product);
            $orderId = $item->getOrderId();
        }

        $this->messageManager->addSuccess(
            __(
                'A total of %1 duplicate product(s) created.',
                $collection->getSize()
            )
        );

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(
            ResultFactory::TYPE_REDIRECT
        );

        return $resultRedirect->setPath('catalog/product/index');
    }

    /**
     * create duplicate products
     * @param \Magento\Catalog\Api\Data\ProductInterfaceFactory $product
     * @return void
     */
    private function createDuplicate($product)
    {
        $duplicateProduct = $this->_productCopier->copy($product);
        $this->_productRepository->save($duplicateProduct);
    }
}
