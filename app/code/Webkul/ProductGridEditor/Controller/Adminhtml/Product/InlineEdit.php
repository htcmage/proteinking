<?php
/**
 * Webkul Software
 *
 * @category  Webkul
 * @package   Webkul_ProductGridEditor
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\ProductGridEditor\Controller\Adminhtml\Product;

class InlineEdit extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
     const ADMIN_RESOURCE = 'Webkul_ProductGridEditor::productgrideditor';

    /**
     * @var $_jsonFactory
     */
    protected $_jsonFactory;

    /**
     * @var $_productRepository
     */
    protected $_productRepository;

    /**
     * @var $_logger
     */
    protected $_logger;

    /**
     * @var $_errMsgArr
     */
    protected $_errMsgArr = [];

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Psr\Log\LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->_init();
        $this->_jsonFactory = $jsonFactory;
        $this->_productRepository = $productRepository;
        $this->_logger = $logger;
    }

    /**
     * Initialize values
     */
    private function _init()
    {
        $this->_errMsgArr = [
            __('Make sure the To Special Price Date is later than or the same as the From Special Price Date.'),
            __('Make sure the To New Product Date is later than or the same as the From New Product Date.'),
            __('Make sure the To Custom Design Date is later than or the same as the From Custom Design Date.')
        ];
    }

    /**
     * Save Inline Row Data
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->_jsonFactory->create();
        $error = false;
        $messages = [];

        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);

            if (empty($postItems)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $entityId) {
                    try {
                        $result = $this->saveGridData($postItems[$entityId], $entityId, $resultJson);
                        if ($result['error']) {
                            $messages[] = $result['message'];
                            $error = true;
                            break;
                        }
                    } catch (\Exception $e) {
                        $messages[] = "[Error:]  {$e->getMessage()}";
                        $error = true;
                    }
                }
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * Save inline row data
     * @param array $postItems
     * @param int $entityId
     * @return array
     */
    private function saveGridData($postItems, $entityId)
    {
        try {
            $product = $this->_productRepository->getById($entityId);
            $stockItem = $product->getExtensionAttributes()->getStockItem();
            $result = [];
            $result['error'] = false;

            foreach ($postItems as $key => $value) {
                $res = $this->saveDataInProductObject($key, $value, $product, $stockItem);
                if ($res['error']) {
                    $result['error'] = $res['error'];
                    $result['message'] = $res['message'];
                    return $result;
                } else {
                    $product = $res['productObj'];
                    $stockItem = $res['stockObj'];
                }
            }

            $storeId = \Magento\Store\Model\Store::DEFAULT_STORE_ID;
            $product->setData('store_id', $storeId);
            $this->_productRepository->save($product);
            $stockItem->save();
            return $result;
        } catch (\Exception $ex) {
            $this->_logger->info($ex->getMessage());
        }
    }

    /**
     * Save data in product object
     * @param string $key
     * @param int|string|boolean $value
     * @param object $product
     * @param object $stockItem
     * @return object
     */
    private function saveDataInProductObject($key, $value, $product, $stockItem)
    {
        $result = [];
        $result['error'] = false;

        switch ($key) {
            case 'qty':
                $stockItem->setQty($value);
                break;

            case 'special_to_date':
		// Softomars Disabling Set special_to_date
		break;
                if (strtotime($value) >= strtotime($product->getSpecialFromDate())) {
                    $product->setSpecialToDate($value);
                } else {
                    $result['error'] = true;
                    $result['message'] = $this->_errMsgArr[0];
                }
                break;

            case 'news_to_date':
                if (strtotime($value) >= strtotime($product->getNewsFromDate())) {
                    $product->setNewsToDate($value);
                } else {
                    $result['error'] = true;
                    $result['message'] = $this->_errMsgArr[1];
                }
                break;

            case 'custom_design_to':
                if (strtotime($value) >= strtotime($product->getCustomDesignFrom())) {
                    $product->setCustomDesignTo($value);
                } else {
                    $result['error'] = true;
                    $result['message'] = $this->_errMsgArr[2];
                }
                break;

            default:
                $product->setData($key, $value);
        }

        $result['productObj'] = $product;
        $result['stockObj'] = $stockItem;
        return $result;
    }
}
