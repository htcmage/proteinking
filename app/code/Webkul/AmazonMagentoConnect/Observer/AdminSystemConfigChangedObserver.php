<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use \Magento\Framework\Exception\LocalizedException;

class AdminSystemConfigChangedObserver implements ObserverInterface
{

    const XSD_PATH = '/view/adminhtml/web/files';

    /**
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
    private $dir;

    /**
     * @var \Magento\Framework\Filesystem
     */
    private $fileSystem;

    /**
     * @var \Webkul\AmazonMagentoConnect\Helper\Data
     */
    private $helper;

    /**
     * @var Magento\Framework\App\RequestInterface
     */
    private $requestInterface;

    /**
     *
     * @param RequestInterface $requestInterface
     * @param \Magento\Framework\Filesystem\DirectoryList $dir
     * @param \Magento\Framework\Filesystem $fileSystem
     * @param \Webkul\AmazonMagentoConnect\Helper\Data $helper
     */
    public function __construct(
        RequestInterface $requestInterface,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        \Magento\Framework\Filesystem $fileSystem,
        \Webkul\AmazonMagentoConnect\Helper\Data $helper
    ) {
    
        $this->_request = $requestInterface;
        $this->_dir = $dir;
        $this->_fileSystem = $fileSystem;
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $manifestData = [];
        $data =  $this->_request->getParams();
        $fields = isset($data['groups']['general']['fields'])?
        $data['groups']['general']['fields']:[];
        $modulePath = $this->helper->getModulePath();
        try {
            if (!empty($fields['category_type']['value'])) {
                $attributeSet = $fields['default_attiribute_set']['value'];
                foreach ($fields['category_type']['value'] as $xsdName) {
                    $mageDir = $modulePath.self::XSD_PATH."/$xsdName.xsd";
                    $attributes = [];
                    $doc = new \DOMDocument();
                    $doc->preserveWhiteSpace = false;
                    $doc->load($mageDir);
                    $doc->save('temp.xml');
                    $xmlfile = file_get_contents('temp.xml');
                    $parseObj = str_replace($doc->lastChild->prefix.':', "", $xmlfile);
                    $ob= simplexml_load_string($parseObj);
                    $json  = $this->helper->getJsonHelper()->jsonEncode($ob);
                    $data = $this->helper->getJsonHelper()->jsonDecode($json, true);
                    
                    $xsdRequiredAttrs = [];
                    $extraAttribute = [];
                    $hasProductType = 0;
                    $allXsdElements = isset($data['element'][0]) ? $data['element'] : [0 => $data['element']];

                    $hasNewRecord = false;
                    foreach ($allXsdElements as $key => $element) {
                        $baseVariationData = isset($element['complexType']['sequence']['element'][0]) ?
                                            $element['complexType']['sequence']['element'] :
                                            [0 => $element['complexType']['sequence']['element']];
                        $xsd = $element['@attributes']['name'];
                        if (!$key) {
                            $xsdBaseName = $element['@attributes']['name'];
                        }
                        $categoryGroup = $this->helper->getCategoryGroup()->getCollection()
                                                ->addFieldToFilter('category_group', ['eq' => $xsdBaseName])
                                                ->setPageSize(1);

                        if ($categoryGroup->getSize()) {
                            continue;
                        }
                        $hasNewRecord = true;
                        $allXsdData = $this->manageXsdData($baseVariationData, $xsd);

                        $extraAttribute[] = $allXsdData;
                        if ($allXsdData['has_product_type']) {
                            $hasProductType = 1;
                        }
                    }
                    if (!$hasNewRecord) {
                        continue;
                    }
                    $categoryGroup = [
                        'category_group' => $xsdBaseName,
                        'has_product_type' => $hasProductType
                    ];
                    $this->helper->saveInDbTable('wk_amazon_category_group', $categoryGroup);
                    
                    $completeData = [];
                    $categroyData = [];
                    foreach ($extraAttribute as $compltData) {
                        if (isset($compltData['xsd_data']) && !empty($compltData['xsd_data'])) {
                            $completeData['dropdown_attrs'][] = $compltData['xsd_data']['dropdown_attrs'][0];
                        }
                        if (isset($compltData['category_data']) && !empty($compltData['category_data'])) {
                            foreach ($compltData['category_data'] as $cateData) {
                                $categroyData[] = [
                                   'category_group' => $cateData['category_group'],
                                   'amz_cate_name'  => $cateData['amz_cate_name'],
                                ];
                            }
                        }
                    }
                    $completeData['required_fields'] = $this->getRequiredSpecification($xsdName, $xsd);
                    $this->helper->saveInDbTable('wk_amazon_category', $categroyData);
                    $this->helper->createProductAttribute($completeData, $xsd, $attributeSet);
                }
            }
        } catch (\Exception $e) {
            $this->helper->getLogger()->addError('AdminSystemConfigChangedObserver error :: '.$e->getMessage());
        }
    }

    /**
     * manage xsd elements
     *
     * @param array $baseVariationData
     * @param string $xsd
     * @return array
     */
    public function manageXsdData($baseVariationData, $xsd)
    {
        $xsdRequiredAttrs = [];
        $variation = [];
        $hasProductType = false;
        $productTypes = [];
        $categoryData = [];
        foreach ($baseVariationData as $element) {
            if (isset($element['@attributes']['name']) && $element['@attributes']['name'] =='ProductType') {
                $hasProductType = true;
                if (isset($element['complexType'])) {
                    $xsdProductTypes = isset($element['complexType']['choice']['element'][0]) ?
                                        $element['complexType']['choice']['element'] :
                                        [0 => $element['complexType']['choice']['element']];
                    foreach ($xsdProductTypes as $productType) {
                        if (isset($productType['@attributes']['ref'])) {
                            $categoryData[] = [
                                'category_group' => $xsd,
                                'amz_cate_name' => $productType['@attributes']['ref']
                            ];
                        }
                    }
                } else {
                    $xsdProductTypes = isset($element['simpleType']['restriction']['enumeration'][0]) ?
                                        $element['simpleType']['restriction']['enumeration'] :
                                        [0 => $element['simpleType']['restriction']['enumeration']];
                    foreach ($xsdProductTypes as $productType) {
                        $categoryData[] = [
                            'category_group' => $xsd,
                            'amz_cate_name' => $productType['@attributes']['value']
                        ];
                    }
                }
            }
            if (!isset($element['complexType']['sequence']['element'])) {
                continue;
            }
            $variationData = $element['complexType']['sequence']['element'];
            foreach ($variationData as $variationTheme) {
                if (!isset($variationTheme['@attributes']['name'])) {
                    continue;
                }
                if ($variationTheme['@attributes']['name'] !== 'VariationTheme') {
                    continue;
                }
                $variationThemeData = $variationTheme['simpleType']['restriction']['enumeration'];
                foreach ($variationThemeData as $variations) {
                    $variation[] = $variations['@attributes']['value'];
                }
                $pieces = preg_split('/(?=[A-Z])/', 'VariationTheme');
                $fileName = implode(' ', array_filter($pieces));
                $xsdRequiredAttrs['dropdown_attrs'][] = [
                    'code' => $xsd."_variation",
                    'label' => "$fileName ($xsd)",
                    'values' => $variation,
                    'xsd_name' => $xsd
                ];
                break;
            }
            if ($element['@attributes']['name'] !== 'ClassificationData') {
                continue;
            }
            foreach ($element['complexType']['sequence']['element'] as $elementTypes) {
                if (!isset($elementTypes['@attributes']['name'])) {
                    continue;
                }
                if ($elementTypes['@attributes']['name'] !== $xsd.'Type') {
                    continue;
                }
                foreach ($elementTypes['simpleType']['restriction']['enumeration'] as $elementType) {
                    $categoryData[] = [
                        'category_group' => $xsd,
                        'amz_cate_name' => $elementType['@attributes']['value']
                    ];
                }
            }
        }
        return [
            'xsd_data' => $xsdRequiredAttrs,
            'category_data' => $categoryData,
            'has_product_type'  => $hasProductType
        ];
    }

    /**
     * get required specification
     *
     * @param string $xsdFile
     * @param string $xsd
     * @return array
     */
    private function getRequiredSpecification($xsdFile, $xsd)
    {
        if ($xsdFile === 'ProductClothing') {
            return [
                ['code' => 'department', 'label' => __("Department ($xsd)")],
                ['code' => 'color_map', 'label' => __("Color Map ($xsd)")],
                ['code' => 'size_map', 'label' => __("Size Map ($xsd)")],
                ['code' => 'material_type', 'label' => __("Material Type ($xsd)")],
                ['code' => 'fit_type', 'label' => __("Fit type ($xsd)")],
                ['code' => 'sleeve_type', 'label' => __("Sleeve Type ($xsd)")],
                ['code' => 'item_length_description', 'label' => __("Item Length Description ($xsd)")]
            ];
        } elseif ($xsdFile === 'Beauty') {
            return [
                ['code' => 'color_map', 'label' => __("Color Map ($xsd)")],
                ['code' => 'scent', 'label' => __("Scent ($xsd)")],
                ['code' => 'capacity', 'label' => __("Capacity ($xsd)")],
            ];
        }
    }
}
