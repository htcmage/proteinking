<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\AmazonMagentoConnect\Api\ProductMapRepositoryInterface;
use Magento\Framework\Session\SessionManager;

class CatalogProductSaveAfter implements ObserverInterface
{
    private $amzClient;

    /**
     * @var \Webkul\AmazonMagentoConnect\Model\Productmap
     */
    private $productMap;

    /**
     * @var \Magento\CatalogInventory\Model\Stock\StockItemRepository
     */
    private $stockItemRepository;

    /**
     * \Webkul\AmazonMagentoConnect\Helper\ProductOnAmazon
     */
    private $productOnAmazon;

    /**
     * @var \Magento\Backend\Model\Session
     */
    private $backendSession;

    /**
     *
     * @param \Webkul\AmazonMagentoConnect\Logger\Logger $amzLogger
     * @param \Webkul\AmazonMagentoConnect\Model\ProductMap $productMap
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockItemRepository
     * @param \Webkul\AmazonMagentoConnect\Helper\Data $helper
     * @param \Webkul\AmazonMagentoConnect\Helper\ProductOnAmazon $productOnAmazon
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param SessionManager $coreSession
     * @param \Magento\Backend\Model\Session $backendSession
     */
    public function __construct(
        \Webkul\AmazonMagentoConnect\Logger\Logger $amzLogger,
        \Webkul\AmazonMagentoConnect\Model\ProductMap $productMap,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockItemRepository,
        \Webkul\AmazonMagentoConnect\Helper\Data $helper,
        \Webkul\AmazonMagentoConnect\Helper\ProductOnAmazon $productOnAmazon,
        \Magento\Catalog\Model\Product $product,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        SessionManager $coreSession,
        \Magento\Backend\Model\Session $backendSession
    ) {
        $this->logger = $amzLogger;
        $this->productMap = $productMap;
        $this->stockItemRepository = $stockItemRepository;
        $this->helper = $helper;
        $this->productOnAmazon = $productOnAmazon;
        $this->product = $product;
        $this->objectManager = $objectManager;
        $this->coreSession = $coreSession;
        $this->backendSession = $backendSession;
    }
    /**
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/softomars_amazon.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);

        $cronSession = $this->coreSession->getData('amz_cron');
        try {
            if (!$this->backendSession->getAmzSession() && !$cronSession) {
                $product = $observer->getProduct();
                $amzMappedProduct = $this->productMap->getCollection()
                                ->addFieldToFilter('magento_pro_id', ['eq'=>$product->getId()]);
                
                if ($accountId = $this->helper->getAmazonAccountId($amzMappedProduct)) {
                    $product = $this->product->load($product->getId());
                    $this->amzClient = $this->helper->getAmzClient($accountId);
                    $itemReviseStatus = $this->helper->getItemReviseStatus();
                    // Update qty of amazon product
                    $updateQtyData[] = $this->productOnAmazon->updateQtyData($product);
                    
                    //Update price of amazon product
                    $updatePriceData[] = $this->productOnAmazon->updatePriceData($product);
                    

            $logger->info("softomars stop updating price on amazon");

$logger->info("Update Price: ".json_encode($updatePriceData));
$logger->info("Update Qty: ".json_encode($updateQtyData));
$logger->info("Revise Status: " . $itemReviseStatus);
                    if ($itemReviseStatus && !empty($updateQtyData) && !empty($updatePriceData)) {
                        $this->_upateProductData($updateQtyData, $updatePriceData);
                    }
                    $baseImageRevised = $this->helper->isImgExported();
                    if ($baseImageRevised) {
                        $imageProductData = $this->productOnAmazon->postImages($product);
                        $this->_upateProductBaseImage($imageProductData);
                    }
                }
            }
        } catch (\Execption $e) {
            $this->logger->info('Observer CatalogProductSaveAfter execute : '.$e->getMessage());
        }
    }

    /**
     * update amazon  product
     * @param  array $updateQtyData
     * @param  array $updatePriceData
     * @param  object $product
     */
    private function _upateProductData($updateQtyData, $updatePriceData)
    {
        try {
//            $priceApiResponse = $this->amzClient->updatePrice($updatePriceData);
//            $this->logger->info('== Observer CatalogProductSaveAfter priceApiResponse ==');
//            $this->logger->info(json_encode($priceApiResponse));


            // Softomars
            $this->logger->info("Stop Updating price on amazon");

			$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/softomars_amazon.log');
	        $logger = new \Zend\Log\Logger();
    	    $logger->addWriter($writer);																																																																																        $logger->addWriter($writer);
			$logger->info("softomars stop updating price on amazon");

            $stockApiResponse = $this->amzClient->updateStock($updateQtyData);
            $this->logger->info('== Observer CatalogProductSaveAfter stockApiResponse ==');
            $this->logger->info(json_encode($stockApiResponse));
        } catch (\Execption $e) {
            $this->logger->info('Observer CatalogProductSaveAfter _upateProductData : '.$e->getMessage());
        }
    }
    
    /**
     * update amazon  product base image
     * @param  array $imageProductData
     */
    private function _upateProductBaseImage($imageProductData)
    {
        try {
            if (!empty($imageProductData)) {
                $imageApiResponse = $this->amzClient->postImages($imageProductData);
                $this->logger->info('== Observer CatalogProductSaveAfter imageApiResponse ==');
                $this->logger->info(json_encode($imageApiResponse));
            }
        } catch (\Execption $e) {
            $this->logger->info('Observer CatalogProductSaveAfter _upateProductBaseImage : '.$e->getMessage());
        }
    }
}
