<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Session\SessionManager;

class SalesOrderPlaceAfterObserver implements ObserverInterface
{
    private $amzClient;

    /**
     * @var \Webkul\AmazonMagentoConnect\Model\Productmap
     */
    private $productMapRecord;

    /**
     * @var \Magento\CatalogInventory\Model\Stock\StockItemRepository
     */
    private $stockItemRepository;

    /**
     * @var \Webkul\AmazonMagentoConnect\Helper\Data
     */
    private $helper;

    /**
     * @var \Webkul\AmazonMagentoConnect\Logger\Logger
     */
    private $amzLogger;

    /**
     * @var \Magento\Backend\Model\Session
     */
    private $backendSession;

    /**
     *
     * @param \Webkul\AmazonMagentoConnect\Logger\Logger $amzLogger
     * @param \Webkul\AmazonMagentoConnect\Model\ProductMap $productMapRecord
     * @param \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository
     * @param \Webkul\AmazonMagentoConnect\Helper\Data $helper
     * @param \Webkul\AmazonMagentoConnect\Helper\ProductOnAmazon $productOnAmazon
     * @param \Magento\Catalog\Model\Product $product
     * @param SessionManager $coreSession
     * @param \Magento\Backend\Model\Session $backendSession
     */
    public function __construct(
        \Webkul\AmazonMagentoConnect\Logger\Logger $amzLogger,
        \Webkul\AmazonMagentoConnect\Model\ProductMap $productMapRecord,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Webkul\AmazonMagentoConnect\Helper\Data $helper,
        \Webkul\AmazonMagentoConnect\Helper\ProductOnAmazon $productOnAmazon,
        \Magento\Catalog\Model\Product $product,
        SessionManager $coreSession,
        \Magento\Backend\Model\Session $backendSession
    ) {
        $this->amzLogger = $amzLogger;
        $this->productMapRecord = $productMapRecord;
        $this->stockItemRepository = $stockItemRepository;
        $this->helper = $helper;
        $this->productOnAmazon = $productOnAmazon;
        $this->product = $product;
        $this->coreSession = $coreSession;
        $this->backendSession = $backendSession;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        try {
            $wholeItemData = [];
            if (empty($this->coreSession->getData('amz_cron')) && !$this->backendSession->getAmzSession()) {
$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
$logger = new \Zend\Log\Logger();
$logger->addWriter($writer);
$logger->info('Your text message');

                $order = $observer->getOrder();
                $orderIncrementedId = $order->getIncrementId();
                $orderItems = $order->getAllItems();
                foreach ($orderItems as $item) {
                    $productId = $item->getProductId();
                    $this->amzLogger->info('observer SalesOrderPlaceAfterObserver 
                                    : Order quatity with product id - '.$productId);
                    $amzMappedProduct = $this->productMapRecord
                                        ->getCollection()
                                        ->addFieldToFilter('magento_pro_id', ['eq'=>$productId]);
                    if ($accountId = $this->helper->getAmazonAccountId($amzMappedProduct)) {
                        $product = $this->product->load($productId);
                        $updateQtyData = $this->productOnAmazon->updateQtyData($product);
                        $wholeItemData[$accountId][] = $updateQtyData;
                    }
                }
                if (!empty($wholeItemData)) {
                    foreach ($wholeItemData as $key => $itemData) {

$logger->info(print_r($itemData, 1));
//$logger->info($amzMappedProduct->getFirstItem()->getData('amazon_pro_id'));
                        $this->amzClient = $this->helper->getAmzClient($key);


//$productRe = $this->amzClient->getMatchingProduct([$amzMappedProduct->getFirstItem()->getData('amazon_pro_id')]);
//$logger->info(print_r($productRe, 1));
                        $stockApiResponse = $this->amzClient->updateStock($itemData);
                        $this->amzLogger->info('== Observer SalesOrderPlaceAfterObserver stockApiResponse ==');
                        $this->amzLogger->info(json_encode($stockApiResponse));
                    }
                }
            }
        } catch (\Exception $e) {
            $this->amzLogger->info('observer SalesOrderPlaceAfterObserver : '.$e->getMessage());
        }
    }
}
