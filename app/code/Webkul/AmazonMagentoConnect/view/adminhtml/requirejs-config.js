/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
var config = {
    map: {
        '*': {
            productimportscript: 'Webkul_AmazonMagentoConnect/js/product-import-script',
            orderimportscript: 'Webkul_AmazonMagentoConnect/js/order-import-script',
            productProfiler : 'Webkul_AmazonMagentoConnect/js/product-profiler-script',
            orderProfiler : 'Webkul_AmazonMagentoConnect/js/order-profiler-script',
            syncToAmazon : 'Webkul_AmazonMagentoConnect/js/sync-to-amazon-script',
            submitForm : 'Webkul_AmazonMagentoConnect/js/submit-form'
        }
    }
};