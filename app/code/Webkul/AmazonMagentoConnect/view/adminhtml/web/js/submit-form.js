/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
/*jshint jquery:true*/
require([
    'jquery',
    'jquery/ui'
  ], function ($) {
        $('#save, #saveandcontinue').on('click', function (e) {
            e.preventDefault();
            $('#amazon_order_map_grid_massaction-select').prop('disabled', true);
            $('#amazon_product_map_grid_massaction-select').prop('disabled', true);
            $('#mage_map_product_massaction-select').prop('disabled', true);
            $('#edit_form').submit();
        });
  });
