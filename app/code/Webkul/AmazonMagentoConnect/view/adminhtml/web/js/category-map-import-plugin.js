/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
/*jshint jquery:true*/
define([
    'jquery',
    'mage/translate',
    'Magento_Ui/js/modal/alert'
], function ($,$t,alert) {
    'use strict';
    $.widget('amazon.categoryMap', {
        _create: function () {
            var self = this;
            $('body').on('change','select[name="parent_category"], select[name="child_cate"]', function (e) {
                
                var selectedVal = $(this).val();
                if ($(e.target).hasClass('parent_cate')) {
                    $('.wk-child-container').empty();
                } else {
                    $(this).nextAll('select').remove();
                }
                $('select[name="amazon_category_child"]').empty();
                $('select[name="amazon_category_child"]').parents('.wk-amazon-catalog-container').removeClass('hide-container');
                $.ajax({
                    url : self.options.childNodeUrl,
                    type : 'post',
                    dateType : 'json',
                    data : {
                       'cate_id' :  selectedVal
                    },
                    showLoader: true,
                    success : function (response) {
                        $('.wk-child-container').removeClass('hide-container');
                        var childSelect = $('<select/>').
                                addClass('required-entry child_cate required-entry _required select admin__control-select select-child-cate').attr('name','child_cate');
                                childSelect.append($('<option/>').val('').text('select'));
                        if (response.hasChild) {
                            $.each(response['nodes'], function (key, value) {
                                childSelect.append($('<option/>').val(key).text(value));
                            });
                            $('.wk-child-container').append(childSelect);
                        }
                    }, error : function (error) {
                        console.log(' Something went wrong.');
                    }
                });
            });
            
            $('#wk-save-mapping').on('click', function (event) {
                event.preventDefault();
                var childCate = $('select[name="child_cate"]:last').val();
                if (childCate) {
                    var valText = $('body').find('.child_cate option[value='+childCate+']').text();
                    $('input[name="cate_name"]').val(valText);
                    $('#cate-mapping-save').submit();
                } else {
                    alert({
                        title: 'Warning',
                        content: 'Either check your fields or advertisement credentails.',
                        actions: {
                            always: function (){}
                        }
                    });
                }
            });
        }
    });
    return $.amazon.categoryMap;
});
