/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
/*jshint jquery:true*/
define([
    'jquery',
    'mage/translate',
    'Magento_Ui/js/modal/alert'
], function ($,$t,alert) {
    'use strict';
    $.widget('amazon.categoryMap', {
        _create: function () {
            var self = this;
            var catesJson = self.options.childNodes;

            $('select[name="category_group"]').on('change', function () {
                var selectedVal = $(this).val();
                $('select[name="amazon_category_child"]').empty();
                $('select[name="amazon_category_child"]').parents('.wk-amazon-catalog-container').removeClass('hide-container');
                $.each(catesJson[selectedVal], function (key, value) {
                    $('select[name="amazon_category_child"]').append(
                        $('<option/>').text(value).val(value)
                    );
                });
            });
            
            $('#wk-save-mapping').on('click', function (event) {
                event.preventDefault();
                var parentCate = $('select[name="category_group"]').val();
                var childCate = $('select[name="amazon_category_child"]').val();
                
                if (childCate && parentCate) {
                    $('#cate-mapping-save').submit();
                } else {
                    alert({
                        title: 'Warning',
                        content: 'Select all the fields.',
                        actions: {
                            always: function (){}
                        }
                    });
                }
            });
        }
    });
    return $.amazon.categoryMap;
});
