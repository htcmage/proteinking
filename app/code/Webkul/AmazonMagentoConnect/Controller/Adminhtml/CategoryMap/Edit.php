<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Controller\Adminhtml\CategoryMap;

use Magento\Framework\Locale\Resolver;
use Webkul\AmazonMagentoConnect\Model\CategoryMapFactory;
use Magento\Framework\Registry;
use Webkul\AmazonMagentoConnect\Controller\Adminhtml\CategoryMap;

class Edit extends CategoryMap
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $coreRegistry;

    /**
     * @param \Magento\Backend\App\Action\Context $context,
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory,
     * @param CategoryMapFactory $CategoryMapFactory,
     * @param Registry $registry
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        CategoryMapFactory $CategoryMapFactory,
        Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->CategoryMapFactory = $CategoryMapFactory;
        $this->coreRegistry = $registry;
        parent::__construct($context);
    }

   /**
    * Init actions
    *
    * @return \Magento\Backend\Model\View\Result\Page
    */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Webkul_AmazonMagentoConnect::category_map')
            ->addBreadcrumb(__('Lists'), __('Lists'))
            ->addBreadcrumb(__('Map Category'), __('Map Category'));
        return $resultPage;
    }

    /**
     * @return void
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $label =  __('Category Map');
        $resultPage->addBreadcrumb($label, $label);
        $resultPage->getConfig()->getTitle()->prepend(__('Category Mapping'));
        return $resultPage;
    }
}
