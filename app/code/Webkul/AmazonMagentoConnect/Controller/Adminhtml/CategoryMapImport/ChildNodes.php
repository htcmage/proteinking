<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Controller\Adminhtml\CategoryMapImport;

use Magento\Framework\Locale\Resolver;
use Webkul\AmazonMagentoConnect\Model\CategoryMapFactory;
use Magento\Framework\Registry;
use Webkul\AmazonMagentoConnect\Controller\Adminhtml\CategoryMapImport;

class ChildNodes extends CategoryMapImport
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    private $coreRegistry;

    /**
     * @var \Webkul\AmazonMagentoConnect\Helper\Data
     */
    private $helper;

    /**
     * @param \Magento\Backend\App\Action\Context $context,
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory,
     * @param CategoryMapFactory $CategoryMapFactory,
     * @param Registry $registry
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        CategoryMapFactory $CategoryMapFactory,
        \Webkul\AmazonMagentoConnect\Helper\Data $helper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->CategoryMapFactory = $CategoryMapFactory;
        $this->coreRegistry = $registry;
        $this->helper = $helper;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * @return void
     */
    public function execute()
    {
        $response = [];
        $data = $this->getRequest()->getParams();
        if (is_array($data) && isset($data['cate_id']) && $data['cate_id']) {
            $nodeInfo = $this->helper->getCategoryNode($data['cate_id']);
        }
        if ($nodeInfo) {
            $response = [
                'hasChild' => 1,
                'nodes' => $nodeInfo
            ];
        } else {
            $response = [
                'hasChild' => 0,
                'nodes' => []
            ];
        }
        $result = $this->resultJsonFactory->create();
        return $result->setData($response);
    }
}
