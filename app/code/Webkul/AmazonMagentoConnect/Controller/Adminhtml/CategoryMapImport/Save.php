<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Controller\Adminhtml\CategoryMapImport;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Locale\Resolver;
use Webkul\AmazonMagentoConnect\Controller\Adminhtml\CategoryMapImport;

class Save extends CategoryMapImport
{
     /**
      * @var \Magento\Framework\Controller\Result\JsonFactory
      */
    private $resultJsonFactory;

     /**
      * @var \Webkul\AmazonMagentoConnect\Model\CategoryMapFactory
      */
    private $CategoryMapFactory;
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Webkul\AmazonMagentoConnect\Helper\Data $helper,
        \Magento\Catalog\Model\Category $category
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->helper = $helper;
        $this->category = $category;
        parent::__construct($context);
    }

    /**
     * @return void
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $validationResponse = $this->validateData($params);
        if ($validationResponse['error']) {
            $this->messageManager->addNotice($validationResponse['msg']);
            $this->_redirect('*/*/');
        }
        $existIds = $this->getMappedCategoryIds($params);
        $records = 0;
        foreach ($params['product']['category_ids'] as $cateId) {
            if (array_key_exists($cateId, $existIds)) {
                $this->messageManager->addError($existIds[$cateId]);
                continue;
            }
            $rawData = [
                'mage_cate_id' => $cateId,
                'amz_cate_name' => $params['cate_name'],
                'amz_cate_id' => $params['child_cate']
            ];
            $records++;
            $this->helper->getCategoryMap()->setData($rawData)->save();
        }
        if ($records) {
            $this->messageManager->addSuccess(__("A total of %1 record(s) have been saved.", $records));
        }
        $this->_redirect('*/*/index');
    }

    /**
     * validate raw data
     *
     * @param array $data
     * @return array
     */
    private function validateData($data)
    {
        if (isset($data['product']['category_ids']) &&
        isset($data['cate_name']) && isset($data['cate_name']) && !empty($data['product']['category_ids'])) {
            return [
                    'error' => false
                ];
        }
        return [
            'error' => true,
            'msg' => __('Invalidate data.')
        ];
    }

    /**
     * validate record already exist or not
     *
     * @param array $data
     * @return boolean
     */
    private function getMappedCategoryIds(array $data)
    {
        $exitIds = [];
        $mapCategory = $this->helper->getCategoryMap()->getCollection();
        foreach ($data['product']['category_ids'] as $cateId) {
            $mapCategory->addFieldToFilter('mage_cate_id', ['eq' => $cateId])
                        ->addFieldToFilter('amz_cate_id', ['eq' => $data['child_cate']]);
            if ($mapCategory->getSize()) {
                $categoryName = $this->getCategoryName($cateId);
                $exitIds[$cateId] = __(
                    '%1 already mapped with %2.',
                    $categoryName,
                    $mapCategory->getData()[0]['amz_cate_name']
                );
            }
        }
        return $exitIds;
    }

    /**
     * get category name
     *
     * @param integer $categoryId
     * @return string
     */
    private function getCategoryName($categoryId)
    {
        return $this->category->load($categoryId)->getName();
    }
}
