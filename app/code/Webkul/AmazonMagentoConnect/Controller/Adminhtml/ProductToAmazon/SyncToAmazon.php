<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Controller\Adminhtml\ProductToAmazon;

use Webkul\AmazonMagentoConnect\Controller\Adminhtml\ProductToAmazon;
use Webkul\AmazonMagentoConnect\Helper;
use Magento\Framework\Controller\ResultFactory;
use Webkul\AmazonMagentoConnect\Api\ProductMapRepositoryInterface;

class SyncToAmazon extends ProductToAmazon
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;

    /**
     * Webkul\AmazonMagentoConnect\Api\ProductMapRepositoryInterface
     */
    private $productMapRepository;

    /**
     * @var Webkul\AmazonMagentoConnect\Helper\Data
     */
    private $helper;

    /**
     * @var Webkul\AmazonMagentoConnect\Helper\ProductOnAmazon
     */
    private $productOnAmazon;

    /**
     * @var \Webkul\AmazonMagentoConnect\Model\AccountsFactory
     */
    private $accountsFactory;

    /**
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param Helper\Data $helper
     * @param Helper\ProductOnAmazon $productOnAmazon
     * @param ProductMapRepositoryInterface $productMapRepository
     * @param \Webkul\AmazonMagentoConnect\Model\AccountsFactory $accountsFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        Helper\Data $helper,
        Helper\ProductOnAmazon $productOnAmazon,
        ProductMapRepositoryInterface $productMapRepository,
        \Webkul\AmazonMagentoConnect\Model\AccountsFactory $accountsFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->helper = $helper;
        $this->productOnAmazon = $productOnAmazon;
        $this->productMapRepository = $productMapRepository;
        $this->accountsFactory = $accountsFactory;
    }

    /**
     * SyncInAmazon action.
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $productIds = [];
        if (isset($params['account_id']) && $params['account_id']) {
            $this->helper->getAmzClient($params['account_id']);
            $isUpdate = isset($params['is_update']) ? 1 : 0;
            $operationName = $isUpdate ? 'updated' : 'exported';
            if ($isUpdate) {
                $collection =  $this->productMapRepository
                                    ->getCollectionByIds($params['productEntityIds']);
                foreach ($collection as $proMap) {
                    $productIds[] = $proMap->getMagentoProId();
                }
            } else {
                $productIds = $params['mageProEntityIds'];
            }
            $account = $this->accountsFactory->create()->load($params['account_id']);
            $storeId = $account->getDefaultStoreView();
            $result = $this->productOnAmazon->manageMageProduct($productIds, $params['is_fba'], $isUpdate, $storeId);

            if (!empty($result['count'])) {
                $this->messageManager->addSuccess(
                    __("A total of %1 record(s) have been %2 to amazon.", $result['count'], $operationName)
                );
            }
            if (isset($result['error_count']) && !empty($result['error_count'])) {
                $this->messageManager->addError(
                    __(
                        "A total of %1 record(s) have been failed to %2 at amazon.",
                        $result['error_count'],
                        $operationName
                    )
                );
                $this->messageManager->addWarning(
                    __("Please set product identifier code(UPC,EAN,ASIN etc) for the failed product(s).")
                );
                foreach ($result['error_array'] as $errorArr) {
                    if (is_array($errorArr)) {
                        $errorArr = json_encode($errorArr);
                    }
                    $this->messageManager->addError($errorArr);
                }
            }
        } else {
            $this->messageManager->addError(
                __("Invalid parameters.")
            );
        }

        return $this->resultFactory->create(
            ResultFactory::TYPE_REDIRECT
        )->setPath(
            '*/accounts/edit',
            [
                    'id'=>$params['account_id'],
                    'active_tab' => 'product_sync'
                ]
        );
    }
}
