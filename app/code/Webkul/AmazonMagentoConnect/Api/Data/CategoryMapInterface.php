<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Api\Data;

interface CategoryMapInterface
{
    /**
     * Constants for keys of data array.
     * Identical to the name of the getter in snake case.
     */
    const ID = 'entity_id';
    const MAGE_CATE_ID = 'mage_cate_id';
    const AMZ_CATE_NAME = 'amz_cate_name';
    const CATEGORY_GROUP = 'category_group';

    /**
     * Get ID.
     *
     * @return int|null
     */
    public function getId();

    /**
     * set ID.
     *
     * @return $this
     */
    public function setId($entityId);

   /**
    * Get Amazon category name.
    * @return string
    */
    public function getAmzCateName();

   /**
    * set Amazon category name
    * @return $this
    */
    public function setAmzCateName($categoryName);

   /**
    * Get mage category id.
    * @return string
    */
    public function getMageCateId();

   /**
    * set mage category id
    * @return $this
    */
    public function setMageCateId($amazonCategoryId);

   /**
    * Get amazon parent category.
    * @return string
    */
    public function getCategoryGroup();

   /**
    * set amazon category parent name
    * @return $this
    */
    public function setCategoryGroup($amazonCategoryId);
}
