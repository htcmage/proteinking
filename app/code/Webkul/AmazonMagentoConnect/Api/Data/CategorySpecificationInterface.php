<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Api\Data;

interface CategorySpecificationInterface
{
    /**
     * Constants for keys of data array.
     * Identical to the name of the getter in snake case.
     */
    const ID = 'entity_id';
    const CATEGORY_GROUP = 'category_group';
    const AMZ_SPECIFICATION_NAME = 'amz_specification_name';
    const MAGE_ATTRIBUTE_CODE = 'mage_attribute_code';

    /**
     * Get ID.
     *
     * @return int|null
     */
    public function getId();

    /**
     * set ID.
     *
     * @return $this
     */
    public function setId($entityId);

   /**
    * Get amazon category.
    * @return string
    */
    public function getCategoryGroup();

   /**
    * set amazon category.
    * @return $this
    */
    public function setCategoryGroup($amzCategory);

   /**
    * Get amazon specification data.
    * @return string
    */
    public function getAmzSpecificationName();

   /**
    * set amazon specification data..
    * @return $this
    */
    public function setAmzSpecificationName($specificationName);

   /**
    * Get mage attribute code.
    * @return string
    */
    public function getMageAttributeCode();

   /**
    * Get mage attribute code.
    * @return $this
    */
    public function setMageAttributeCode($attributeCode);
}
