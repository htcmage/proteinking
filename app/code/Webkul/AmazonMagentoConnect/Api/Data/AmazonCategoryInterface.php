<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Api\Data;

interface AmazonCategoryInterface
{
    /**
     * Constants for keys of data array.
     * Identical to the name of the getter in snake case.
     */
    const ID = 'entity_id';
    const CATEGORY_GROUP = 'category_group';
    const AMZ_CATE_NAME = 'amz_cate_name';

    /**
     * Get ID.
     *
     * @return int|null
     */
    public function getId();

    /**
     * set ID.
     *
     * @return $this
     */
    public function setId($entityId);

   /**
    * Get category group.
    * @return string
    */
    public function getCategoryGroup();

   /**
    * set price.
    * @return $this
    */
    public function setCategoryGroup($categoryGroup);

   /**
    * Get amazon category.
    * @return string
    */
    public function getAmzCateName();

   /**
    * set amazon category.
    * @return $this
    */
    public function setAmzCateName($amazonCategory);
}
