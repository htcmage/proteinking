<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Api\Data;

interface CategoryGroupInterface
{
    /**
     * Constants for keys of data array.
     * Identical to the name of the getter in snake case.
     */
    const ID = 'entity_id';
    const CATEGORY_GROUP = 'category_group';
    const HAS_PRODUCT_TYPE = 'has_product_type';

    /**
     * Get ID.
     *
     * @return int|null
     */
    public function getId();

    /**
     * set ID.
     *
     * @return $this
     */
    public function setId($entityId);

   /**
    * Get category group.
    * @return string
    */
    public function getCategoryGroup();

   /**
    * set price.
    * @return $this
    */
    public function setCategoryGroup($categoryGroup);

   /**
    * Get has product type
    * @return string
    */
    public function getHasProductType();

   /**
    * set has product type .
    * @return $this
    */
    public function setHasProductType($hasProductType);
}
