<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */

namespace Webkul\AmazonMagentoConnect\Block\Adminhtml\CategoryMapImport\Edit;

use Magento\Catalog\Model\Category;
use Magento\Catalog\Helper\Category as CategoryHelper;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;

class CategoryMapImport extends \Magento\Backend\Block\Template
{
    /**
     * @var string
     */
    protected $_template = 'Webkul_AmazonMagentoConnect::category/map-import.phtml';

    /**
     * @var \Webkul\AmazonMagentoConnect\Model\CategoryGroupFactory $CategoryMap
     */
    private $categoryGroup;

    /**
     *
     * @var \Webkul\AmazonMagentoConnect\Model\Config\Source\AmazonParentCates
     */
    private $amazonParentCates;

    /**
     * @var \Webkul\AmazonMagentoConnect\Helper\Data
     */
    private $helper;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\Registry $registry,
        \Webkul\AmazonMagentoConnect\Logger\Logger $logger,
        \Webkul\AmazonMagentoConnect\Helper\Data $helper,
        Category $category,
        CategoryHelper $categoryHelper,
        CollectionFactory $categoryCollectionFactory,
        \Webkul\AmazonMagentoConnect\Model\Config\Source\AmazonParentCates $amazonParentCates,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->jsonHelper = $jsonHelper;
        $this->coreRegistry = $registry;
        $this->logger = $logger;
        $this->category = $category;
        $this->categoryHelper = $categoryHelper;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->helper = $helper;
        $this->amazonParentCates = $amazonParentCates;
    }

    /**
     * getMappedVariables
     */
    public function getParentCategories()
    {
        try {
            return $this->amazonParentCates->toArray();
        } catch (\Exception $e) {
            $this->logger->addError('getMappedVariables : '. $e->getMessage());
            return [];
        }
    }

    /**
     * getMappedVariables
     */
    public function getAllChilds()
    {
        try {
            $parentCategory = [];
            $categoryGroup = $this->helper->getAmazonCategory()->getCollection();
            foreach ($categoryGroup as $cates) {
                $parentCategory[$cates->getCategoryGroup()][] = $cates->getAmzCateName();
            }
            return $this->jsonHelper->jsonEncode($parentCategory);
        } catch (\Exception $e) {
            $this->logger->addError('getMappedVariables : '. $e->getMessage());
            return [];
        }
    }
     /**
      * Retrieve categories tree
      *
      * @param string|null $filter
      * @return array
      */
    public function getCategoriesTree($filter = null)
    {
        $categoryCollection = $this->categoryCollectionFactory->create();

        $categoryCollection->addAttributeToSelect('path')
                    ->addAttributeToFilter('entity_id', ['neq' => Category::TREE_ROOT_ID]);
                    // ->setStoreId($storeId);
        $shownCategoriesIds = [];

        /** @var \Magento\Catalog\Model\Category $category */
        foreach ($categoryCollection as $category) {
            foreach (explode('/', $category['path']) as $parentId) {
                $shownCategoriesIds[$parentId] = 1;
            }
        }

        /* @var $collection \Magento\Catalog\Model\ResourceModel\Category\Collection */
        $collection = $this->categoryCollectionFactory->create();

        $collection->addAttributeToFilter('entity_id', ['in' => array_keys($shownCategoriesIds)])
            ->addAttributeToSelect(['name', 'is_active', 'parent_id']);

        $sellerCategory = [
            Category::TREE_ROOT_ID => [
                'value' => Category::TREE_ROOT_ID,
                'optgroup' => null,
            ],
        ];

        foreach ($collection as $category) {
            $catId = $category->getId();
            $catParentId = $category->getParentId();
            foreach ([$catId, $catParentId] as $categoryId) {
                if (!isset($sellerCategory[$categoryId])) {
                    $sellerCategory[$categoryId] = ['value' => $categoryId];
                }
            }

            $sellerCategory[$catId]['is_active'] = $category->getIsActive();
            $sellerCategory[$catId]['label'] = $category->getName();
            $sellerCategory[$catParentId]['optgroup'][] = &$sellerCategory[$catId];
        }
        return json_encode($sellerCategory[Category::TREE_ROOT_ID]['optgroup']);
    }


    /**
     * getCategoryObj
     * @return array
     */
    public function getCategories()
    {
        $categories = $this->categoryHelper->getStoreCategories();
        return $categories;
    }

    /**
     * isChildCategory
     * @param Category $category
     * @return boolean
     */
    public function isChildCategory($category)
    {
        $childCats = $this->category->getAllChildren($category);
        return count($childCats)-1 > 0 ? true : false;
    }

    /**
     * getCategoryObj
     * @param int $catId
     * @return array
     */
    public function getCategory($catId)
    {
        return $this->category->load($catId);
    }
}
