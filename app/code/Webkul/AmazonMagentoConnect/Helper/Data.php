<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Helper;

use Webkul\AmazonMagentoConnect\Api\AmazonTempDataRepositoryInterface;
use Webkul\AmazonMagentoConnect\Api\ProductMapRepositoryInterface;
use Webkul\AmazonMagentoConnect\Api\AccountsRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Eav\AttributeFactory;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Group\CollectionFactory as AttrGroupCollection;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory as AttrOptionCollectionFactory;
use Magento\Eav\Api\AttributeManagementInterface;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Eav\Model\Entity as EavEntity;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const URI = '/onca/xml';

    private static $_exportStatus = [
            0 => 'Pending',
            1 => 'Complete',
    ];

    private static $_amzProStatus = [
            0 => 'Failed',
            1 => 'Active',
            2 => 'Inactive',
            3 => 'Pending'
    ];

    private static $_operation = [
        '' => '--Select--',
        'Increase' => 'Increase',
        'Decrease' => 'Decrease',
    ];

    private static $_operationType = [
            '' => '--Select--',
            'Fixed' => 'Fixed',
            'Percent' => 'Percent',
    ];

    private static $_status = [
        '1' => 'Enable',
        '0' => 'Disable',
    ];

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory
     */
    private $attrOptionCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Eav\Attribute
     */
    private $attributeFactory;

    /*
    contain seller account id
    */
    public $accountId;

    /*
    contain amazon client
    */
    public $amzClient;

    /*
    contain configuration of seller
    */
    public $config;

    /**
     * @var string
     */
    private $attributeSetId;

    /**
     * @var AmazonTempDataRepositoryInterface
     */
    private $amazonTempDataRepo;

    /**
     * \Magento\Framework\Module\Manager
     */
    private $moduleManager;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    private $jsonHelper;

    /**
     * @var Webkul\AmazonMagentoConnect\Model\CategorySpecification
     */
    private $categorySpecification;

    /**
     * @var \Webkul\AmazonMagentoConnect\Model\CategoryGroupFactory
     */
    private $categoryGroup;

    /**
     *
     * @var \Webkul\AmazonMagentoConnect\Model\CategoryMapFactory
     */
    private $categoryMap;

    /**
     * @var \Webkul\AmazonMagentoConnect\Model\AccountsFactory
     */
    private $accounts;

    /**
     * @var \Webkul\AmazonMagentoConnect\Model\PriceRuleFactory
     */
    private $priceRule;

    /**
     * @var \Webkul\AmazonMagentoConnect\Model\AmazonCategoryFactory
     */
    private $amazonCategory;

    /**
     * @var \Magento\Framework\Module\Dir\Reader
     */
    private $modulePath;

    /**
     * Data helper constructor
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param AmazonTempDataRepositoryInterface $amazonTempDataRepo
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Locale\CurrencyInterface $currencyInterface
     * @param ProductMapRepositoryInterface $productMapRepo
     * @param AccountsRepositoryInterface $accountsRepository
     * @param \Webkul\AmazonMagentoConnect\Logger\Logger $logger
     * @param \Webkul\AmazonMagentoConnect\Model\Config\Source\AmazonMarketplace $amzMarketplace
     * @param \Webkul\AmazonMagentoConnect\Model\Storage\DbStorage $dbStorage
     * @param \Webkul\AmazonMagentoConnect\Model\AttributeMapFactory $attributeMapFactory
     * @param \Webkul\AmazonMagentoConnect\Model\Config\Source\AmazonProAttribute $amazonProAttribute
     * @param \Magento\Framework\Module\Manager $moduleManager
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param AttributeFactory $attributeFactory
     * @param AttrGroupCollection $attrGroupCollection
     * @param AttrOptionCollectionFactory $attrOptionCollectionFactory
     * @param AttributeManagementInterface $attributeManagement
     * @param ProductAttributeRepositoryInterface $productAttribute
     * @param \Webkul\AmazonMagentoConnect\Model\CategorySpecification $categorySpecification
     * @param EavEntity $eavEntity
     * @param \Webkul\AmazonMagentoConnect\Model\CategoryGroupFactory $categoryGroup
     * @param \Webkul\AmazonMagentoConnect\Model\CategoryMapFactory $categoryMap
     * @param \Webkul\AmazonMagentoConnect\Model\AccountsFactory $accounts
     * @param \Webkul\AmazonMagentoConnect\Model\PriceRuleFactory $priceRule
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        AmazonTempDataRepositoryInterface $amazonTempDataRepo,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\CurrencyInterface $currencyInterface,
        ProductMapRepositoryInterface $productMapRepo,
        AccountsRepositoryInterface $accountsRepository,
        \Webkul\AmazonMagentoConnect\Logger\Logger $logger,
        \Webkul\AmazonMagentoConnect\Model\Config\Source\AmazonMarketplace $amzMarketplace,
        \Webkul\AmazonMagentoConnect\Model\Storage\DbStorage $dbStorage,
        \Webkul\AmazonMagentoConnect\Model\AttributeMapFactory $attributeMapFactory,
        \Webkul\AmazonMagentoConnect\Model\Config\Source\AmazonProAttribute $amazonProAttribute,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        AttributeFactory $attributeFactory,
        AttrGroupCollection $attrGroupCollection,
        AttrOptionCollectionFactory $attrOptionCollectionFactory,
        AttributeManagementInterface $attributeManagement,
        ProductAttributeRepositoryInterface $productAttribute,
        \Webkul\AmazonMagentoConnect\Model\CategorySpecification $categorySpecification,
        EavEntity $eavEntity,
        \Webkul\AmazonMagentoConnect\Model\CategoryGroupFactory $categoryGroup,
        \Webkul\AmazonMagentoConnect\Model\CategoryMapFactory $categoryMap,
        \Webkul\AmazonMagentoConnect\Model\AccountsFactory $accounts,
        \Webkul\AmazonMagentoConnect\Model\PriceRuleFactory $priceRule,
        \Webkul\AmazonMagentoConnect\Model\AmazonCategoryFactory $amazonCategory,
        \Magento\Framework\Module\Dir\Reader $reader
    ) {
        parent::__construct($context);
        $this->amazonTempDataRepo = $amazonTempDataRepo;
        $this->storeManager = $storeManager;
        $this->currencyInterface = $currencyInterface;
        $this->productMapRepo = $productMapRepo;
        $this->accountsRepository = $accountsRepository;
        $this->logger = $logger;
        $this->amzMarketplace = $amzMarketplace;
        $this->dbStorage = $dbStorage;
        $this->attributeMapFactory = $attributeMapFactory;
        $this->amazonProAttribute = $amazonProAttribute;
        $this->moduleManager = $context->getModuleManager();
        $this->jsonHelper = $jsonHelper;
        $this->attributeFactory = $attributeFactory;
        $this->attrGroupCollection = $attrGroupCollection;
        $this->attrOptionCollectionFactory = $attrOptionCollectionFactory;
        $this->attributeManagement = $attributeManagement;
        $this->productAttribute = $productAttribute;
        $this->categorySpecification = $categorySpecification;
        $this->categoryGroup = $categoryGroup;
        $this->categoryMap = $categoryMap;
        $this->accounts = $accounts;
        $this->priceRule = $priceRule;
        $this->amazonCategory = $amazonCategory;
        $this->entityTypeId = $eavEntity->setType(\Magento\Catalog\Model\Product::ENTITY)->getTypeId();
        $this->entityType = \Magento\Catalog\Model\Product::ENTITY;
        $this->reader = $reader;
    }

    /**
     * @return mixed
     */
    public function getModulePath()
    {
        if (!$this->modulePath) {
            $this->modulePath = $this->reader->getModuleDir('', 'Webkul_AmazonMagentoConnect');
        }
        return $this->modulePath;
    }

    /**
     * get advertisement api credentials from configuration
     *
     * @return array
     */
    public function getAdvertisementApiCredentials()
    {
        $advertisementApiGroup = 'amazonmagentoconnect/advertisement_api/';
        return [
            'associate_tag' => $this->scopeConfig->getValue(
                $advertisementApiGroup.'associate_tag',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'access_key' => $this->scopeConfig->getValue(
                $advertisementApiGroup.'access_key',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'secret_key' => $this->scopeConfig->getValue(
                $advertisementApiGroup.'secret_key',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'country' => $this->scopeConfig->getValue(
                $advertisementApiGroup.'country',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
        ];
    }

    /**
     * get amazon configuration compliance values
     *
     * @return array
     */
    public function getAmazonComplianceConfigVals()
    {
        $complianceGroup = 'amazonmagentoconnect/compliance/';
        return [
            'AreBatteriesRequired' => $this->scopeConfig->getValue(
                $complianceGroup.'use_batteries',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'AreBatteriesIncluded' => $this->scopeConfig->getValue(
                $complianceGroup.'included_battery',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ),
            'SupplierDeclaredDGHZRegulation' => $this->scopeConfig->getValue(
                $complianceGroup.'goods_regulations',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            )
        ];
    }

    /**
     * get allowed category nodes
     *
     * @return void
     */
    public function getAllowedAmzCateNodes()
    {
        return $this->scopeConfig->getValue(
            'amazonmagentoconnect/general/category_type',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return \Webkul\AmazonMagentoConnect\Model\AmazonCategoryFactory
     */
    public function getAmazonCategory()
    {
        return $this->amazonCategory->create();
    }

    /**
     * @return \Webkul\AmazonMagentoConnect\Model\CategorySpecification
     */
    public function getCategorySpecification()
    {
        return $this->categorySpecification;
    }

    /**
     * @return \Webkul\AmazonMagentoConnect\Logger\Logger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @return \Webkul\AmazonMagentoConnect\Model\CategoryMapFactory
     */
    public function getCategoryMap()
    {
        return $this->categoryMap->create();
    }

    /**
     * @return \Webkul\AmazonMagentoConnect\Model\CategoryGroupFactory
     */
    public function getCategoryGroup()
    {
        return $this->categoryGroup->create();
    }

    /**
     * @return \Magento\Framework\Json\Helper\Data
     */
    public function getJsonHelper()
    {
        return $this->jsonHelper;
    }

    /**
     * \Magento\Framework\Module\Manager
     */
    public function getModuleManager()
    {
        return $this->moduleManager;
    }

    /**
     * save records in table
     *
     * @param string $tableName
     * @param array $data
     * @return int
     */
    public function saveInDbTable($tableName, $data)
    {
        if (!empty($data)) {
            return $this->dbStorage->insertMultiple($tableName, $data);
        }
    }

    /**
     * truncate a table
     *
     * @param string $tableName
     * @return void
     */
    public function truncateTable($tableName)
    {
        $this->dbStorage->truncateTable($tableName);
    }

    /**
     * get mapped attribute data
     *
     * @return array
     */
    public function getMappedAttributeData()
    {
        $amazonAttrCodes = [];
        $attributeColl = $this->attributeMapFactory->create()->getCollection();
        foreach ($attributeColl as $value) {
            $amazonAttrCodes[$value->getAmzAttr()] = $value->getMageAttr();
        }
        return $amazonAttrCodes;
    }

    /**
     * get amazon proudct with value
     *
     * @param \Magento\Catalog\Product\Model $product
     * @return array
     */
    public function getAmzAttrWithVal($product)
    {
        $mappedAttrs = $this->getMappedAttributeData();
        $amzAttrs = $this->amazonProAttribute->toArray();
        $amazAttrsValues = [];
        foreach ($amzAttrs as $key => $value) {
            if (isset($mappedAttrs[$key]) && $key !== 'qty') {
                $attrValue = $product->getResource()->getAttribute($mappedAttrs[$key]);
                if ($attrValue) {
                    $attrValue = $attrValue->getFrontend()->getValue($product);
                    $attrValue = is_array($attrValue) ? implode(", ", $attrValue) : $attrValue;
                    $attrValue = $attrValue == 'No' ? '' : $attrValue;
                    $amazAttrsValues[$key] = $attrValue;
                }
            } else {
                $amazAttrsValues[$key] = '';
            }
        }
        $amazAttrsValues['qty'] = $product->getQuantityAndStockStatus()['qty'];
        return $amazAttrsValues;
    }

    /**
     * get array of perform operation
     *
     * @return array
     */
    public function getOperations()
    {
        return self::$_operation;
    }

    /**
     * get opeation type
     *
     * @return array
     */
    public function getOperationsTypes()
    {
        return self::$_operationType;
    }

    /**
     * get rule status
     *
     * @return array
     */
    public function getStatus()
    {
        return self::$_status;
    }

    /**
     * get all stores of amazon
     *
     * @return array
     */
    public function getAllAmazonStores()
    {
        $amzStores = [];
        $accountCol = $this->accounts->create()->getCollection();
        
        foreach ($accountCol as $account) {
            $amzStores[$account->getId()] = $account->getStoreName();
        }
        return $amzStores;
    }

    /**
     * check status of product adversitesing api
     *
     * @return void
     */
    public function getProductApiStatus()
    {
        return $this->config['all_images'];
    }

    /**
     * check status of product adversitesing api
     *
     * @return void
     */
    public function isDelFromCatalog()
    {
        return $this->config['del_from_catalog'];
    }

    /**
     * check image exported or not
     *
     * @return void
     */
    public function isImgExported()
    {
        return $this->config['export_image'];
    }

    /**
     * get amazon feed status
     * @return string
     */
    public function getExportStatus($status)
    {
        return self::$_exportStatus[$status];
    }

    /**
     * get amazon product status
     * @return int
     */
    public function getAmzProductStatus($status)
    {
        if ($status!== '') {
            $status = self::$_amzProStatus[$status];
        } else {
            $status = '';
        }
        return $status;
    }

    /**
     * get list of required states
     *
     * @return array
     */
    public function getRequiredStateList()
    {
        return $this->scopeConfig->getValue(
            'general/region/state_required',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    
    /**
     * get import product type
     * @return int
     */
    public function getImportTypeOfProduct()
    {
        return $this->config['product_create'];
    }

    /**
     * get config value of priceurl
     * @return string
     */
    public function getPriceRuleConfigValue()
    {
        return $this->config['price_rule'];
    }

    /**
     * get status of amazon magento product
     * @return int
     */
    public function getItemReviseStatus()
    {
        return $this->config['revise_item'];
    }

    /**
     * get default website for amazon product
     * @return int|null
     */
    public function getDefaultWebsite()
    {
        return $this->config['default_website'];
    }

    /**
     * get default product qty
     * @return int
     */
    public function getDefaultProductQty()
    {
        return $this->config['default_qty'];
    }

    /**
     * get default product weight
     * @return int
     */
    public function getDefaultProductWeight()
    {
        return $this->config['default_weight'];
    }

    /**
     * get currency code of amazon account
     * @return string
     */
    public function getAmazonCurrencyCode()
    {
        return $this->config['currency_code'];
    }

    /**
     * get attribute set id
     * @param  int $accountId
     * @return int
     */
    public function getAttributeSet($accountId = false)
    {
        return $this->config['attribute_set'];
    }

    /**
     * get default Category
     * @return int
     */
    public function getDefaultCategory()
    {
        return $this->config['default_cate'];
    }

    /**
     * get default store for order sync
     * @return int
     */
    public function getDefaultStoreOrderSync()
    {
        return $this->config['default_store_view'];
    }

    public function getAmazonCountry()
    {
        return $this->config['country'];
    }

    /**
     * get shipped order status
     *
     * @return string
     */
    public function getShippedOrderStatus()
    {
        return $this->config['shipped_order'];
    }

    /**
     * get unshipped order status
     *
     * @return string
     */
    public function getUnshippedOrderStatus()
    {
        return $this->config['unshipped_order'];
    }

    /**
     * get Partially shipped order status
     *
     * @return string
     */
    public function getPartiallyshippedOrderStatus()
    {
        return $this->config['partiallyshipped_order'];
    }

    /**
     * get order status
     * @return int
     */
    public function getOrderStatus($orderStatus)
    {
        return (
            $orderStatus === 'Shipped' ? $this->getShippedOrderStatus() : (
                $orderStatus == 'Unshipped' ? $this->getUnshippedOrderStatus() : $this->getPartiallyshippedOrderStatus()));
    }

    /**
     * check street address line
     * @return int
     */
    public function getStreetLineNumber()
    {
        return $this->scopeConfig
                ->getValue('customer/address/street_lines');
    }

    /**
     * get count of imported item
     * @param  string $itemType
     * @param  int $accountId
     * @return object
     */
    public function getTotalImported($itemType, $accountId, $all = false)
    {
        $collection = $this->amazonTempDataRepo
        ->getCollectionByAccountIdnItemType($itemType, $accountId);
        if ($all) {
            return $collection;
        } else {
            foreach ($collection as $record) {
                return $record;
            }
        }
    }
    
    /**
     * get exported pending status count
     *
     * @param int $accountId
     * @return void | object
     */
    public function getExportedProColl($accountId)
    {
        $productMapColl = $this->productMapRepo->getCollectionByAccountId($accountId);
        $productMapColl->addFieldToFilter('export_status', 0);
        return $productMapColl;
    }

    /**
     * [getAmazonURL - get URL by country iso]
     * @return [type] [description]
     */
    public function getProductApiEndPoint()
    {
        $region = $this->config['country'];
        if ($region == 'US') {
            return ('webservices.amazon.com');
        } elseif ($region == 'CA') {
            //return ('mws.amazonservices.ca');
            return ('webservices.amazon.ca');
        } elseif ($region == 'JP') {
            return ('webservices.amazon.co.jp');
        } elseif ($region == 'MX') {
            //return ('mws.amazonservices.com.mx');
            return ('webservices.amazon.com.mx');
        } elseif ($region == 'CN') {
            return ('webservices.amazon.cn');
        } elseif ($region == 'IN') {
            return ('webservices.amazon.in');
        } elseif ($region == 'DE') {
            return ('webservices.amazon.de');
        } elseif ($region == 'BR') {
            return ('webservices.amazon.com.br');
        } elseif ($region == 'FR') {
            return ('webservices.amazon.fr');
        } elseif ($region == 'IT') {
            return ('webservices.amazon.it');
        } else {
            $msg = 'Incorrect Region Code';
        }
    }

    /**
     * get product attribute's value
     * @param  object $product
     * @param  string $attributCode
     * @return string
     */
    public function getProductAttrValue($product, $attributCode)
    {
        return (string)$product->getResource()->getAttribute($attributCode)
                ->getFrontend()->getValue($product);
    }

    /**
     * get all allowed currency
     *
     * @return array
     */
    public function getAllowedCurrencies()
    {
        $currenciesArray = [];
        $availableCurrencies = $this->storeManager->getStore()->getAvailableCurrencyCodes();
        foreach ($availableCurrencies as $currencyCode) {
            $currenciesArray[] = $currencyCode;
        }
        return $currenciesArray;
    }

    /**
     * get currency rate
     *
     * @param string $currency
     * @return void | int
     */
    public function getCurrencyRate($currency)
    {
        return $this->storeManager->getStore()
                    ->getBaseCurrency()->getRate($currency);
    }

        /**
         * get amazon client
         *
         * @param boolean $sellerId
         * @param boolean $amzCredentails
         * @return void
         */
    public function validateAmzCredentials($amzCredentails = false)
    {
        try {
            $config = [
                'Seller_Id' => $amzCredentails['seller_id'],
                'Marketplace_Id' => $amzCredentails['marketplace_id'],
                'Access_Key_ID' => $amzCredentails['access_key_id'],
                'Secret_Access_Key' => $amzCredentails['secret_key'],
                'MWSAuthToken' => $amzCredentails['marketplace_id'],
                'Application_Version' => '0.0.*'
            ];
            $this->amzClient = new MwsClient($config);
            if ($this->amzClient->validateCredentials()) {
                return $this->amzClient;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            $this->logger->info('Data validateAmzCredentials : '.$e->getMessage());
            return false;
        }
    }

    /**
     * get configuration field of amazon
     *
     * @param string $field
     * @return void
     */
    public function getStoreConfig($field)
    {
        return $this->scopeConfig->getValue("mpamazonconnect/configuration/$field");
    }

    /**
     * get Amazon client
     *
     * @param boolean $sellerId
     * @return void
     */
    public function getAmzClient($accountId = false, $needFresh = false)
    {
        try {
            if ($needFresh || !$this->accountId) {
                $this->accountId = $accountId;
            }
            if (!$this->amzClient) {
                $amzCredentails = $this->getSellerAmzCredentials($this->accountId)->toArray();
                $this->config = [
                    'attribute_set'         => $amzCredentails['attribute_set'],
                    'Seller_Id'             => $amzCredentails['seller_id'],
                    'Marketplace_Id'        => $amzCredentails['marketplace_id'],
                    'Access_Key_ID'         => $amzCredentails['access_key_id'],
                    'Secret_Access_Key'     => $amzCredentails['secret_key'],
                    'MWSAuthToken'          => $amzCredentails['marketplace_id'],
                    'Application_Version'   => '0.0.*',
                    'inventory_report_id'   => $amzCredentails['inventory_report_id'],
                    'listing_report_id'     => $amzCredentails['listing_report_id'],
                    'default_cate'          => $amzCredentails['default_cate'],
                    'default_store_view'    => $amzCredentails['default_store_view'],
                    'product_create'        => $amzCredentails['product_create'],
                    'default_website'       => $amzCredentails['default_website'],
                    'order_status'          => $amzCredentails['order_status'],
                    'associate_tag'         => $amzCredentails['associate_tag'],
                    'pro_api_secret_key'    => $amzCredentails['pro_api_secret_key'],
                    'pro_api_access_key_id' => $amzCredentails['pro_api_access_key_id'],
                    'currency_code'         => $amzCredentails['currency_code'],
                    'country'               => $amzCredentails['country'],
                    'revise_item'           => $amzCredentails['revise_item'],
                    'del_from_catalog'      => $amzCredentails['del_from_catalog'],
                    'all_images'            => $amzCredentails['all_images'],
                    'default_qty'           => $amzCredentails['default_qty'],
                    'default_weight'        => $amzCredentails['default_weight'],
                    'shipped_order'         => $amzCredentails['shipped_order'],
                    'unshipped_order'       => $amzCredentails['unshipped_order'],
                    'partiallyshipped_order'=> $amzCredentails['partiallyshipped_order'],
                    'price_rule'            => $amzCredentails['price_rule'],
                    'export_image'            => $amzCredentails['export_image'],
                    'store_name'            => $amzCredentails['store_name'],
                ];
                $this->amzClient = new MwsClient($this->config);
            }
            return $this->amzClient;
        } catch (\Exception $e) {
            $this->logger->info('Data getAmzClient : '.$e->getMessage());
            return false;
        }
    }

    /**
     * get amazon credentials by seller id
     *
     * @param int $sellerId
     * @return void
     */
    public function getSellerAmzCredentials($needObject = false)
    {
        if ($needObject || !$this->config) {
            return $this->accountsRepository->getCollectionById($this->accountId);
        } else {
            return $this->config;
        }
    }

    /**
     * get amazon account id from collection
     *
     * @param object $collection
     * @return void | int
     */
    public function getAmazonAccountId($collection)
    {
        $accountId = false;
        if ($collection->getSize()) {
            foreach ($collection as $account) {
                $accountId = $account->getMageAmzAccountId();
            }
        }
        return $accountId;
    }

    /**
     * get single record from collection
     *
     * @return object | void
     */
    public function getRecordModel($collection)
    {
        $recordModel = false;
        if ($collection->getSize()) {
            foreach ($collection as $record) {
                $recordModel = $record;
            }
        }
        return $recordModel;
    }

    /**
     * get amazon price rule by price
     *
     * @param integer $price
     * @return object
     */
    public function getPriceRuleByPrice($price)
    {
        if ($this->getPriceRuleConfigValue() !== 'none') {
            $amzPriceRuleCol = $this->priceRule->create()->getCollection()
                ->addFieldToFilter('amz_account_id', ['eq' => $this->accountId])
                ->addFieldToFilter('price_from', ['lteq' => round($price)])
                ->addFieldToFilter('price_to', ['gteq' => round($price)])
                ->addFieldToFilter('status', ['eq' => 1]);
            if ($amzPriceRuleCol->getSize()) {
                return $amzPriceRuleCol->getFirstItem();
            }
        }
        return false;
    }

    /**
     * get price after applied price rule
     *
     * @param object $ruleData
     * @param int $price
     * @param string $process
     * @return void
     */
    public function getPriceAfterAppliedRule($ruleData, $price, $process)
    {
        try {
            if ($price) {
                if ($ruleData->getOperationType() === 'Fixed') {
                    $price = $this->getFixedPriceCalculation($ruleData, $price, $process);
                } else {
                    $price = $this->getPercentPriceCalculation($ruleData, $price, $process);
                }
            }
            return $price;
        } catch (\Exception $e) {
            $this->logger->info('Helper Data getPriceAfterAppliedRule : '.$e->getMessage());
        }
    }

    /**
     * done fixed price rule calcuation
     *
     * @param object $ruleData
     * @param int $price
     * @param string $process
     * @return int
     */
    public function getFixedPriceCalculation($ruleData, $price, $process)
    {
        try {
            if ($ruleData->getOperation() === 'Increase') {
                if ($process === $this->getPriceRuleConfigValue()) {
                    $price = $price + $ruleData->getPrice();
                } else {
                    $price = $price - $ruleData->getPrice();
                }
            } else {
                if ($process === $this->getPriceRuleConfigValue()) {
                    $price = $price - $ruleData->getPrice();
                } else {
                    $price = $price + $ruleData->getPrice();
                }
            }
            return $price;
        } catch (\Exception $e) {
            $this->logger->info('Helper Data getFixedPriceCalculation : '.$e->getMessage());
        }
    }

    /**
     * done percent price rule calcuation
     *
     * @param object $ruleData
     * @param int $price
     * @param string $process
     * @return int
     */
    public function getPercentPriceCalculation($ruleData, $price, $process)
    {
        try {
            $percentPrice = ($price * $ruleData->getPrice())/100;
            if ($ruleData->getOperation() === 'Increase') {
                if ($process === $this->getPriceRuleConfigValue()) {
                    $price = $price + $percentPrice;
                } else {
                    $price = $price - $percentPrice;
                }
            } else {
                if ($process === $this->getPriceRuleConfigValue()) {
                    $price = $price - $percentPrice;
                } else {
                    $price = $price + $percentPrice;
                }
            }
            return $price;
        } catch (\Exception $e) {
            $this->logger->info('Helper Data getPercentPriceCalculation : '.$e->getMessage());
        }
    }

    /**
     * getAttributeInfo
     * @param string $mageAttrCode
     * @return false | Magento\Catalog\Model\ResourceModel\Eav\Attribute
     */
    private function _getAttributeInfo($mageAttrCode)
    {
        $attributeInfoColl = $this->attributeFactory->create()->getCollection()
                                    ->addFieldToFilter('attribute_code', ['eq' => $mageAttrCode]);
        $attributeInfo = false;
        foreach ($attributeInfoColl as $attrInfoData) {
            $attributeInfo = $attrInfoData;
        }
        return $attributeInfo;
    }

    /**
     * getAttributeGroupId
     * @param $groupName
     */
    private function getAttributeGroupId($groupName, $attributeSetId)
    {
        $group = $this->attrGroupCollection->create()
                                        ->addFieldToFilter('attribute_group_name', $groupName)
                                        ->addFieldToFilter('attribute_set_id', $attributeSetId)
                                        ->setPageSize(1)->getFirstItem();
        if (!$group->getAttributeGroupId()) {
            $data = [
                'attribute_group_name' => $groupName,
                'attribute_set_id' => $attributeSetId,
                'attribute_group_code' => md5($groupName)
            ];
            $group = $group->setData($data)->save();
        }
        return $group->getId();
    }

    /**
     * create category attributes and saved in database
     *
     * @param array $xsdRequiredAttrs
     * @param string $type
     * @param string $input
     * @param string $xsdName
     * @return void
     */
    public function createAttributes($xsdRequiredAttrs, $type, $input, $xsdName, $attributeSetId)
    {
        if ($input === 'select') {
                $mageAttrCode = substr('amazon_'.strtolower($xsdRequiredAttrs['code']), 0, 30);
                $attrLabel = $xsdRequiredAttrs['label'];
                $values = $xsdRequiredAttrs['values'];
                $xsdName = $xsdRequiredAttrs['xsd_name'];
        } else {
            $mageAttrCode = substr('amazon_'.strtolower($xsdRequiredAttrs['code']), 0, 30);
            $attrLabel = $xsdRequiredAttrs['label'];
        }
        $attributeInfo = $this->_getAttributeInfo($mageAttrCode);
        $allStores = $this->storeManager->getStores();
        $attributeGroupId = $this->getAttributeGroupId('Amazon Specification', $attributeSetId);
        if ($attributeInfo === false) {
            $attributeScope = \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_WEBSITE;
            $attribute = $this->attributeFactory->create();
            $attrData = [
                'entity_type_id' => $this->entityTypeId,
                'attribute_code' => $mageAttrCode,
                'frontend_label' => [0 => $attrLabel],
                'attribute_group_id' => $attributeGroupId,
                'attribute_set_id' => $attributeSetId,
                'backend_type' => $type,
                'frontend_input' => $input,
                'backend' => '',
                'frontend' => '',
                'source' => '',
                'global' => $attributeScope,
                'visible' => true,
                'required' => false,
                'is_user_defined' => true,
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'is_html_allowed_on_front' => true,
                'visible_in_advanced_search' => false,
                'unique' => false,
            ];

            if ($input != 'text') {
                $labels = [];
                foreach ($allStores as $store) {
                    $labels[$store->getId()] = $attrLabel;
                }

                $attrData['option'] = $this->_getAttributeOptions($mageAttrCode, $labels, $values);
            }
            try {
                $attribute->setData($attrData);
                $attribute->save();
                $data = [
                    'category_group' => $xsdName,
                    'amz_specification_name' => $attrLabel,
                    'mage_attribute_code' => $mageAttrCode
                ];
                $this->categorySpecification->setData($data)->save();
            } catch (\Exception $e) {
                $this->logger->addError('Create createProductAttribute : '.$e->getMessage());
            }
        } else {
            $categorySpecification = $this->categorySpecification->getCollection()
                                    ->addFieldToFilter('mage_attribute_code', $mageAttrCode)
                                    ->addFieldToFilter('amz_specification_name', $attrLabel);
            if (!$categorySpecification->getSize()) {
                $data = [
                    'category_group' => $xsdName,
                    'amz_specification_name' => $attrLabel,
                    'mage_attribute_code' => $mageAttrCode
                ];
                $this->categorySpecification->setData($data)->save();
            }
            if ($input != 'text') {
                try {
                    $option = $this->_getAttributeOptionsForEdit($attributeInfo->getAttributeId(), $values);
                    $this->attributeManagement->assign(
                        $this->entityType,
                        $attributeSetId,
                        $attributeGroupId,
                        $mageAttrCode,
                        $attributeInfo->getAttributeId()
                    );
                    if (isset($option['value'])) {
                        $attr = $this->productAttribute->get($attributeInfo->getAttributeCode());
                        $attr->setOption($option);
                        $this->productAttribute->save($attr);
                    }
                } catch (\Exception $e) {
                    $this->logger->addError('update createProductAttribute : '.$e->getMessage());
                }
            }
        }
    }

    /**
     * create category attribute of amazon
     *
     * @param array $xsdRequiredAttrs
     * @param string $xsdName
     * @return void
     */
    public function createProductAttribute($xsdRequiredAttrs, $xsdName, $attributeSet)
    {
        foreach ($xsdRequiredAttrs as $key => $amzAttrs) {
            if ($key == 'required_fields' && !empty($xsdRequiredAttrs['required_fields'])) {
                $type = 'varchar';
                $input = 'text';
                foreach ($amzAttrs as $attData) {
                    $this->createAttributes($attData, $type, $input, $xsdName, $attributeSet);
                }
            }
            if ($key == 'dropdown_attrs' && !empty($xsdRequiredAttrs['dropdown_attrs'])) {
                $type = 'int';
                $input = 'select';
                foreach ($xsdRequiredAttrs['dropdown_attrs'] as $attData) {
                    $this->createAttributes($attData, $type, $input, $xsdName, $attributeSet);
                }
            }
        }
    }

    /**
     * @param $mageAttrCode string
     * @param $labels array of options label according to store
     * @param $values array of spicification/variations options
     * @return array of prepared all options of attribute
     */
    private function _getAttributeOptions($mageAttrCode, $labels, $values)
    {
        $allStores = $this->storeManager->getStores();
        $option = [];
        if ($mageAttrCode) {
            foreach ($values as $key => $value) {
                $value = !is_array($value) ? [$value] : $value;
                foreach ($value as $subKey => $subValue) {
                    if ($subValue != '' && $subValue != ' ') {
                        $option['value']['wk'.$subValue][0] = $subValue;
                        foreach ($allStores as $store) {
                            $option['value']['wk'.$subValue][$store->getId()] = $subValue;
                        }
                    }
                }
            }
        }
        return $option;
    }

    /**
     * _getAttributeOptionsForEdit
     * @param $mageAttrId string
     * @param $values array of spicification/variations options
     * @return array of prepared all options of attribute
     */
    private function _getAttributeOptionsForEdit($mageAttrId, $values)
    {
        $attributeOptions = $this->attrOptionCollectionFactory->create()
                                            ->setPositionOrder('asc')
                                            ->setAttributeFilter($mageAttrId)
                                            ->setStoreFilter(0)->load();
        $optionsValues = [];
        foreach ($attributeOptions as $kay => $attributeOption) {
            array_push($optionsValues, strtolower($attributeOption->getDefaultValue()));
        }
        $allStores = $this->storeManager->getStores();
        $option = [];
        $option['attribute_id'] = $mageAttrId;
        foreach ($values as $key => $value) {
            if (in_array(strtolower($value), $optionsValues) === false && $value != '' && $value != ' ') {
                $option['value']['wk'.$value][0] = $value;
                foreach ($allStores as $store) {
                    $option['value']['wk'.$value][$store->getId()] = $value;
                }
            }
        }
        return $option;
    }

    /**
     * make a call for sign url
     *
     * @param string $url
     * @return string
     */
    public function getPage($url)
    {
        
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $html = curl_exec($curl);
        curl_close($curl);
        return $html;
    }

    /**
     * get category nodes
     *
     * @return array
     */
    public function getCategoryNode($browseNodeId)
    {
        $config = $this->getAdvertisementApiCredentials();
        $params = [
            "Service" => "AWSECommerceService",
            "Operation" => "BrowseNodeLookup",
            "AWSAccessKeyId" => $config['access_key'],
            "AssociateTag" => $config['associate_tag'],
            "BrowseNodeId" => $browseNodeId,
            "ResponseGroup" => "BrowseNodeInfo"
        ];

        if (!isset($params["Timestamp"])) {
            $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
        }
        ksort($params);

        $pairs = [];

        foreach ($params as $key => $value) {
            array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
        }

        $canonicalQueryString = join("&", $pairs);
        $this->config['country'] = $config['country'];
        $endPoint = $this->getProductApiEndPoint();
        $stringToSign = "GET\n".$endPoint."\n".self::URI."\n".$canonicalQueryString;
        $signature = base64_encode(hash_hmac("sha256", $stringToSign, $config['secret_key'], true));
        $requestUrl = 'http://'.$endPoint.self::URI.'?'.$canonicalQueryString.'&Signature='.rawurlencode($signature);
        $response = $this->getPage($requestUrl);
        $pxml = @simplexml_load_string($response);
        if ($pxml === false) {
            return false;
        } else {
            $jsonString = json_encode($pxml);
            $responseArray = json_decode($jsonString, true);
            return $this->manageCategoryNode($responseArray);
        }
    }

    /**
     * manage category nodes
     *
     * @param array $response
     * @return array
     */
    public function manageCategoryNode($response)
    {
        $childNodes = [];
        if (isset($response['BrowseNodes']['BrowseNode']['Children']['BrowseNode'])) {
            $browseNodes = isset($response['BrowseNodes']['BrowseNode']['Children']['BrowseNode'][0]) ?
                            $response['BrowseNodes']['BrowseNode']['Children']['BrowseNode'] :
                            [0 => $response['BrowseNodes']['BrowseNode']['Children']['BrowseNode']];
            foreach ($browseNodes as $category) {
                $childNodes[$category['BrowseNodeId']] = $category['Name'];
            }
        }
        return $childNodes;
    }
}
