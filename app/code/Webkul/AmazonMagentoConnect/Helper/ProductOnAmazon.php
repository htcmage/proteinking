<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */

namespace Webkul\AmazonMagentoConnect\Helper;

use Webkul\AmazonMagentoConnect\Api\ProductMapRepositoryInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableProTypeModel;

class ProductOnAmazon extends \Magento\Framework\App\Helper\AbstractHelper
{
    const SELECTED_PRICE_RULE = 'export';

    private $amzClient;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    private $productFactory;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var Webkul\AmazonMagentoConnect\Model\ProductMap
     */
    private $productMap;

    /**
     * @var Webkul\AmazonMagentoConnect\Helper\MwsProduct
     */
    private $mwsProduct;

    /**
     *
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param Data $helper
     * @param \Webkul\AmazonMagentoConnect\Model\ProductMap $productMap
     * @param \Webkul\AmazonMagentoConnect\Logger\Logger $logger
     * @param ProductMapRepositoryInterface $productMapRepo
     * @param \Webkul\AmazonMagentoConnect\Helper\MwsProduct $mwsProduct
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param ConfigurableProTypeModel $configurableProTypeModel
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        Data $helper,
        \Webkul\AmazonMagentoConnect\Model\ProductMap $productMap,
        \Webkul\AmazonMagentoConnect\Logger\Logger $logger,
        ProductMapRepositoryInterface $productMapRepo,
        \Webkul\AmazonMagentoConnect\Helper\MwsProduct $mwsProduct,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        ConfigurableProTypeModel $configurableProTypeModel
    ) {
        parent::__construct($context);
        $this->productFactory = $productFactory;
        $this->storeManager = $storeManager;
        $this->helper = $helper;
        $this->productMap = $productMap;
        $this->logger = $logger;
        $this->productMapRepo = $productMapRepo;
        $this->mwsProduct = $mwsProduct;
        $this->productRepository = $productRepository;
        $this->configurableProTypeModel = $configurableProTypeModel;
    }

    /**
     * get amazon client
     *
     * @return object
     */
    public function getInitilizeAmazonClient()
    {
        if (!$this->amzClient) {
            $this->amzClient = $this->helper->getAmzClient();
        }
    }

    /**
     * mange magento product to sync to amazon
     * @param  array $params
     * @return array
     */
    public function manageMageProduct($productIds, $isFba = 0, $isUpdate = 0, $storeId)
    {
        $this->getInitilizeAmazonClient();
        $result = null;
        $postProductData = [];
        $postNewData = [];
        $exportErrorsData = [];
        $totalCount = count($productIds);
        $errorCount = 0;

        foreach ($productIds as $productId) {
            $specificationData = [];
            $product = $this->productFactory->create()
                        ->setStoreId($storeId)
                        ->load($productId);
            $mwsProduct = $this->getMwsProductObject($product);
            if (!$specificationData = $this->isProductMapped($product)) {
                $errorCount++;
                $exportErrorsData[] = __('Product %1 is not mapped with any amazon category', $product->getName());
                continue;
            }
            $attrs = $this->helper->getAmzAttrWithVal($product);
            if (!isset($attrs['Manufacturer']) || $attrs['Manufacturer']=="") {
                $attrs['Manufacturer'] = $this->helper->config['store_name'];
            }
            $attrs = array_merge($attrs, $specificationData);
            if ($product->getTypeId() == 'configurable') {
                $amazonCompilance = $this->amazonComplianceAttributes($product);
                $attrs = array_merge($attrs, $amazonCompilance);
                if (!$mwsProduct->validate()) {
                    $exportErrorsData = $mwsProduct->getValidationErrors();
                    $errorCount++;
                    continue;
                }

                $postProductData[] = $mwsProduct;
                $attrs['type'] = $mwsProduct->productIdType;
                $attrs['value'] = $mwsProduct->productId;
                $attrs['Parentage'] = 'parent';
                $postNewData[] = $attrs;
                $variation = $this->getProductVariationForEbay($product, $attrs['category_type'], $attrs['category_group']);
                $variationSku = [];
                foreach ($variation['Variation'] as $key => $vari) {
                    $assocProduct = $vari['product'];
                    $variation['Variation'][$key]['category_group'] = $attrs['category_group'];
                    $variation['Variation'][$key]['category_type'] = $attrs['category_type'];
                    $variation['Variation'][$key]['has_product_type'] = $attrs['has_product_type'];
                    unset($variation['Variation'][$key]['product']);
                    $postNewData[] = $variation['Variation'][$key];
                    $variationSku[$attrs['sku']][] = $vari['sku'];
                    $stockProductData[] = $this->updateQtyData($assocProduct, $isFba, $isUpdate);
                    $PriceProductData[] = $this->updatePriceData($assocProduct);
                    $imageProductData = $this->postImages($assocProduct);
                }
            } else {
                if ($mwsProduct->validate()) {
                    $postProductData[] = $mwsProduct;
                    $attrs['type'] = $mwsProduct->productIdType;
                    $attrs['value'] = $mwsProduct->productId;
                    $amazonCompilance = $this->amazonComplianceAttributes($product);
                    $attrs = array_merge($attrs, $amazonCompilance);
                    $postNewData[] = $attrs;
                    $stockProductData[] = $this->updateQtyData($product, $isFba, $isUpdate);
                    $PriceProductData[] = $this->updatePriceData($product);
                    $imageProductData = $this->postImages($product);
                } else {
                    $exportErrorsData[] = $mwsProduct->getValidationErrors();
                    $errorCount++;
                }
            }
        }
        $exportedProducts = $totalCount - $errorCount;
        if (!empty($postNewData) && !$isUpdate) {
            $imgResponse = '';
            $result = $this->amzClient->postProduct($postNewData);
            $stockResponse = $this->amzClient->updateStock($stockProductData);
            $priceResponse = $this->amzClient->updatePrice($PriceProductData);
            if (!empty($variationSku)) {
                $relationFeedResult = $this->amzClient->PostRelation($variationSku);
            }
            if ($this->helper->isImgExported()) {
                $imgResponse   = $this->amzClient->postImages($imageProductData);
            }
            if (!isset($result['error'])) {
                $this->saveDataInTable(
                    $postProductData,
                    $result,
                    $stockResponse,
                    $priceResponse,
                    $imgResponse,
                    $isFba,
                    $isUpdate
                );
            } else {
                $exportErrorsData[] = $result['msg'];
                $errorCount++;
            }
        } elseif (!empty($stockProductData)) {
            $stockResponse = $this->amzClient->updateStock($stockProductData);
            if (!isset($stockResponse['error'])) {
                $this->saveStockeDataInTable(
                    $postProductData,
                    $stockResponse,
                    $isFba
                );
            } else {
                $exportErrorsData[] = $stockResponse['msg'];
                return ['error_count'=>count($postProductData), 'error_array' => $exportErrorsData];
            }
        }
        $errorData = $this->helper->getJsonHelper()->jsonEncode($exportErrorsData);
        $this->logger->info('Helper ProductOnAmazon manageMageProduct : error log '.$errorData);
        return ['count' => $exportedProducts, 'error_count'=>$errorCount, 'error_array' => $exportErrorsData];
    }

    /**
     * get amazon compliance attribures from product
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return void
     */
    public function amazonComplianceAttributes($product)
    {
        $productData = $product->getData();
        if (!isset($productData['wk_compliance_config']) || $productData['wk_compliance_config']) {
            return $this->helper->getAmazonComplianceConfigVals();
        } else {
            return [
                'AreBatteriesIncluded' => isset($productData['batteries_included']) ? $productData['batteries_included'] : 'not_applicable',
                'AreBatteriesRequired' => isset($productData['use_batteries']) ? $productData['use_batteries'] : '0',
                'SupplierDeclaredDGHZRegulation' => isset($productData['goods_regulation']) ? $productData['goods_regulation'] : '0'
            ];
        }
    }

    /**
     * create mws obejct
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return \Magento\Catalog\Helper\MwsProduct
     */
    public function getMwsProductObject($product)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->mwsProduct = $objectManager->create('Webkul\AmazonMagentoConnect\Helper\MwsProduct');
        $exportProType = $this->helper
                ->getProductAttrValue($product, 'identification_label');
        $exportProValue = $this->helper
                ->getProductAttrValue($product, 'identification_value');
        $amzCurrencyCode = $this->helper->config['currency_code'];
        $currencyRate = $this->helper->getCurrencyRate($amzCurrencyCode);
        $this->mwsProduct->mageProductId = $product->getId();
        $this->mwsProduct->sku = $product->getSku();
        $actualPrice = empty($currencyRate) ? $product->getPrice() :
                    ($product->getPrice()*$currencyRate);
        $ruleAppliedPrice = '';
        if ($ruleData = $this->helper->getPriceRuleByPrice($actualPrice)) {
            $ruleAppliedPrice = $this->helper->getPriceAfterAppliedRule($ruleData, $actualPrice, self::SELECTED_PRICE_RULE);
        }
        $newPrice = empty($ruleAppliedPrice) ? $actualPrice : $ruleAppliedPrice;
        $newPrice = str_replace(',', '', number_format($newPrice, 2));
        $this->mwsProduct->price = $newPrice;
        $this->mwsProduct->productId = $exportProValue;
        $this->mwsProduct->productIdType = $exportProType;
        $this->mwsProduct->conditionType = 'New';
        $this->mwsProduct->quantity = $product->getQuantityAndStockStatus()['qty'];
        return $this->mwsProduct;
    }

    /**
     * check product category is mapped or not
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return boolean | array
     */
    public function isProductMapped($product)
    {
        $cateIds = $product->getCategoryIds();
        if (!empty($cateIds)) {
            $mappedRecord = $this->helper->getCategoryMap()->getCollection()
                                         ->addFieldToFilter('mage_cate_id', ['in' => $cateIds])
                                         ->setPageSize(1)->getFirstItem();
            if ($mappedRecord->getId()) {
                $specificationData['specification'] = $this->getSpecificationData(
                    $product,
                    $mappedRecord->getAmzCateName(),
                    $mappedRecord->getCategoryGroup()
                );
                $categoryGroupCol =  $this->helper->getCategoryGroup()->getCollection()
                                ->addFieldToFilter('category_group', $mappedRecord->getCategoryGroup())
                                ->setPageSize(1)->getFirstItem();
                $specificationData['category_group'] = $mappedRecord->getCategoryGroup();
                $specificationData['category_type'] = $mappedRecord->getAmzCateName();
                $specificationData['has_product_type'] = $categoryGroupCol->getHasProductType();
                return $specificationData;
            } else {
                return false;
            }
        }
    }

    /**
     * get specification data
     *
     * @param \Webkul\AmazonMagentoConnect\Model\CategoryMapFactory $mappedRecord
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     */
    public function getSpecificationData($product, $amzCateName, $categoryGroup)
    {
        $specificationData = [];
        $specification = $this->helper->getCategorySpecification()
                            ->getCollection()
                            ->addFieldToFilter(
                                ['category_group', 'category_group'],
                                [
                                    ['eq' => $amzCateName],
                                    ['eq' => $categoryGroup]
                                ]
                            );
        foreach ($specification as $record) {
            $productData = $product->getData();
            if (strpos($record->getMageAttributeCode(), 'variation') !== false) {
                $variationVal = $this->helper
                            ->getProductAttrValue($product, $record->getMageAttributeCode());
                $specificationData[$record->getMageAttributeCode()] =  $variationVal;
                continue;
            }
            if (isset($productData[$record->getMageAttributeCode()])) {
                $specificationData[$record->getMageAttributeCode()] =  $productData[$record->getMageAttributeCode()];
            }
        }
        return $specificationData;
    }

    /**
     * save exported data in table
     *
     * @param object $submitedData
     * @param array $mwsResponse
     * @return void
     */
    public function saveDataInTable(
        $submitedData,
        $mwsResponse,
        $stockRespone,
        $priceResponse,
        $imgResponse = false,
        $isFba = 0,
        $isUpdate = 0
    ) {
        if (isset($mwsResponse['FeedSubmissionId']) && $mwsResponse['FeedSubmissionId']) {
            foreach ($submitedData as $subProduct) {
                $product = $this->productFactory->create()
                            ->load($subProduct->mageProductId);
                $fulfillCode = $isFba ? 'FBA' : 'FBM';

                if ($isUpdate) {
                    $proMapCol = $this->productMap->getCollection()
                                ->addFieldToFilter('magento_pro_id', $subProduct->mageProductId);
                    foreach ($proMapCol as $record) {
                        $record->setFulfillmentChannel($fulfillCode);
                        $record->setFeedsubmissionId($mwsResponse['FeedSubmissionId']);
                        $record->setQtyFeedsubmissionId($stockRespone['FeedSubmissionId']);
                        $record->setPriceFeedsubmissionId($priceResponse['FeedSubmissionId']);
                        $record->save();
                    }
                } else {
                    $cats = $product->getCategoryIds();
                    $firstCategoryId = null;
                    if (count($cats)) {
                        $firstCategoryId = $cats[0];
                    }
                    $data = [
                        'magento_pro_id'        => $product->getEntityId(),
                        'mage_cat_id'           => $firstCategoryId,
                        'name'                  => $product->getName(),
                        'product_type'          => $product->getTypeId(),
                        'amazon_pro_id'         => '',
                        'mage_amz_account_id'   => $this->helper->accountId,
                        'amz_product_id'        => $subProduct->productId,
                        'feedsubmission_id'     => $mwsResponse['FeedSubmissionId'],
                        'qty_feedsubmission_id' => $stockRespone['FeedSubmissionId'],
                        'price_feedsubmission_id'   => $priceResponse['FeedSubmissionId'],
                        'img_feedsubmission_id' => is_array($imgResponse)?$imgResponse['FeedSubmissionId'] : '',
                        'export_status'         =>'0',
                        'error_status'          =>'0',
                        'pro_status_at_amz'     => $product->getStatus(),
                        'product_sku'           => $subProduct->sku,
                        'fulfillment_channel'   => $fulfillCode
                    ];
                    $record = $this->productMap;
                    $record->setData($data)->save();
                }
                $product->setWkFulfillmentChannel(strtolower($fulfillCode))->save();
            }
        }
    }

    /**
    * save exported data in table
    *
    * @param object $submitedData
    * @param array $mwsResponse
    * @return void
    */
    public function saveStockeDataInTable(
        $submitedData,
        $stockRespone,
        $isFba
    ) {
        if (isset($stockRespone['FeedSubmissionId']) && $stockRespone['FeedSubmissionId']) {
            foreach ($submitedData as $subProduct) {
                $product = $this->productFactory->create()
                            ->load($subProduct->mageProductId);
                $fulfillCode = $isFba ? 'FBA' : 'FBM';
                $proMapCol = $this->productMap->getCollection()
                ->addFieldToFilter('magento_pro_id', $subProduct->mageProductId);
                foreach ($proMapCol as $record) {
                    $record->setFulfillmentChannel($fulfillCode);
                    $record->setQtyFeedsubmissionId($stockRespone['FeedSubmissionId']);
                    $record->save();
                }
                $product->setWkFulfillmentChannel(strtolower($fulfillCode))->save();
            }
        }
    }

    /**
     * get product quantity related data
     * @param  object $product
     * @return array
     */
    public function updateQtyData($product, $isFba = 0, $isUpdate = 0)
    {
        if ($isFba) {
            $updateQuantityArray = [
                'SKU'                   => $product->getSku(),
                'FulfillmentCenterID'   => 'AMAZON_'.$this->helper->getAmazonCountry(),
                'Lookup'                => 'FulfillmentNetwork',
                'SwitchFulfillmentTo'   => 'AFN',
            ];
        } else {
            $updateQuantityArray = [
                'SKU'   => $product->getSku(),
                'Quantity'   => $product->getQuantityAndStockStatus()['qty'],
            ];
            if ($isUpdate && ($product->getWkFulfillmentChannel() !== 'fbm')) {
                $updateQuantityArray['SwitchFulfillmentTo'] = 'MFN';
            }
        }

$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $StockState = $objectManager->get('\Magento\InventorySalesAdminUi\Model\GetSalableQuantityDataBySku');
    $qty = $StockState->execute($product->getSku());
$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
$logger = new \Zend\Log\Logger();
$logger->addWriter($writer);
$logger->info(print_r($qty, 1));
$updateQuantityArray['Quantity'] = $qty[0]['qty'];
        return $updateQuantityArray;
    }

    /**
     * get image data for submit feed
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return array
     */
     public function postImages($product)
     {
         $productImagesList = $product->getMediaGalleryImages();
         $count = 1;
         $defaultImage =  $product->getThumbnail();
         $imageArray = [];
         foreach ($productImagesList as $image) {
             if ($count < 10) {
                 $file = $image->getFile();
                 if ($file == $defaultImage) {
                     $imageArray[] = [
                         'sku'   => $product->getSku(),
                         'image_location' => $image->getUrl(),
                         'image_type' => 'Main',
                     ];
                 } else {
                     $imageArray[] = [
                         'sku'   => $product->getSku(),
                         'image_location' => $image->getUrl(),
                         'image_type' => 'PT'.$count,
                     ];
                     $count++;
                 }
             }
         }
         return $imageArray;
     }

    /**
     * get product price related data
     * @param  object $product
     * @return array
     */
    public function updatePriceData($product)
    {
        $amzCurrencyCode = $this->helper->config['currency_code'];
        $currencyRate = $this->helper->getCurrencyRate($amzCurrencyCode);
        $price = empty($currencyRate) ? round($product->getPrice(), 2) :
                round(($product->getPrice()*$currencyRate), 2);
        $updatePriceArray = [
            'sku'   => $product->getSku(),
            'price' => $price
        ];
        return $updatePriceArray;
    }

    /**
     * check exported product status
     *
     * @param array $feedIds
     * @return void
     */
    public function checkProductFeedStatus($feedIds)
    {
        $this->getInitilizeAmazonClient();
        $productFeedStatus = [];
        $result = $this->feedSubmitionResult($feedIds);
    }

    /**
     * proccessed exported product response
     *
     * @param [type] $feed
     * @param [type] $feedResponse
     * @return void
     */
    public function processFeedResult($feed, $feedResponse)
    {
        try {
            $response = [];
            $updatedRecods = 0;
            $failedErrorCodes = ['8058','8560','8047','8105','6024'];
            foreach ($feedResponse as $feedArray) {
                $errorCode = '';
                $errorMsg = '';
                $productAsign = null;
                $productStatus = null;
                $mapProductData = $this->productMapRepo->getBySku($feedArray['product_sku']);
                if ($mapProductData->getSize()) {
                    if (in_array($feedArray['error_code'], $failedErrorCodes)) {
                        $productStatus = '0';//failed
                        $errorMsg = $feedArray['error_msg']. '(error code '.$feedArray['error_code']. ')';
                    } else {
                        $amzProData = $this->amzClient->getMyPriceForSKU([$feedArray['product_sku']]);
                        if (isset($amzProData['GetMyPriceForSKUResult']['Product'])) {
                            $productStatus = '1';//active
                            $amzProductForSku = $amzProData['GetMyPriceForSKUResult']['Product'];
                            $productAsign = $amzProductForSku['Product']['Identifiers']['MarketplaceASIN']['ASIN'];
                        } else {
                            $productStatus = '2';//inactive
                        }
                    }
                    foreach ($mapProductData as $proData) {
                        $proData->setExportStatus('1');
                        $proData->setErrorStatus($errorMsg);
                        $proData->setProStatusAtAmz($productStatus);
                        $proData->setAmazonProId($productAsign);
                        $proData->save();
                    }
                }
            }
        } catch (\Exception $e) {
            $this->logger->info('Helper ProdudtOnAmazon processFeedResult : '.$e->getMessage());
        }
    }

    /**
     * send feed submit request
     */
    public function feedSubmitionResult($feedIds)
    {
        try {
            $feedResponse = [];
            foreach ($feedIds as $key => $feed) {
                $feedResponse = $this->convertTxtToArray($this->amzClient->getFeedSubmissionResult($feed));
                $this->processFeedResult($feed, $feedResponse);
            }
        } catch (\Exception $e) {
            $this->logger->info('Helper ProdudtOnAmazon feedSubmitionResult : '.$e->getMessage());
        }
        return $feedResponse;
    }

    /**
     * convert text to array
     *
     * @param string $content
     * @return array
     */
    public function convertTxtToArray($content)
    {
        $exportErrors = [];
        try {
            $reportContent = str_replace([ "\n" , "\t" ], [ "[NEW*LINE]" , "[tAbul*Ator]" ], $content);
            if (!is_array($reportContent)) {
                $reportArr = explode("[NEW*LINE]", $reportContent);
                $i = 4;
                for ($i =5; $i < count($reportArr); $i++) {
                    $errorReport = explode("[tAbul*Ator]", utf8_encode($reportArr[$i]));
                    if (isset($errorReport[1])) {
                        $exportErrors[] = [
                            'product_sku' => $errorReport[1],
                            'error_code' => $errorReport[2],
                            'error_msg' => $errorReport[4]
                        ];
                    }
                }
            } else {
                $responseApi = $this->helper->getJsonHelper()->jsonEncode($reportContent);
                $this->logger->info('Helper ProdudtOnAmazon convertTxtToArray response : '.$responseApi);
            }
        } catch (\Exception $e) {
            $this->logger->info('Helper ProdudtOnAmazon convertTxtToArray : '.$e->getMessage());
        }
        return $exportErrors;
    }

    /**
     * check product status by sku
     *
     * @param array $amazProSku
     * @return void
     */
    public function checkProductStatusBySku($amazProSku)
    {
        try {
            $this->getInitilizeAmazonClient();
            $exportedAmzIds = [];
            $response = $this->amzClient->getCompetitivePricingForSKU($amazProSku);
            if (isset($response['GetCompetitivePricingForSKUResult'])) {
                foreach ($response['GetCompetitivePricingForSKUResult'] as $result) {
                    if (isset($result['Product'])) {
                        $asinData = $result['Product']['Identifiers']['MarketplaceASIN'];
                        $skuData = $result['Product']['Identifiers']['SKUIdentifier'];
                        $exportedAmzIds[$skuData['SellerSKU']] = $asinData['ASIN'];
                    }
                }
            }
            foreach ($exportedAmzIds as $sku => $asin) {
                $mapProductData = $this->productMapRepo->getBySku($sku);
                foreach ($mapProductData as $proData) {
                    $proData->setExportStatus('1');
                    $proData->setProStatusAtAmz('1');
                    $proData->setAmazonProId($asin);
                    $proData->save();
                }
            }
        } catch (\Exception $e) {
            $this->logger->info('Helper ProdudtOnAmazon checkProductStatusBySku : '.$e->getMessage());
        }
    }

    /**
     * get product variation of eBay
     *
     * @param
     * \Magento\Catalog\Model\Product $product
     * @return array
     */
    public function getProductVariationForEbay($product, $amzCateName, $categoryGroup)
    {
        try {
            $product = $this->productRepository->getById($product->getEntityId());
            $optionsDataList = $product->getTypeInstance(true)->getConfigurableAttributesAsArray($product);
            $nameValueList = [];
            $superAttrList = [];
            $variation = [];
            $pictures = [];
            foreach ($optionsDataList as $optionsData) {
                $tempPictures = [];
                $optionsData['frontend_label'] = $optionsData['frontend_label'];
                $tempPictures['VariationSpecificName'] = $optionsData['frontend_label'];
                $allValueList = [];
                foreach ($optionsData['values'] as $value) {
                    $allValueList[] = $value['default_label'];
                    $tempPictures['VariationSpecificPictureSet'][]['VariationSpecificValue'] = $value['default_label'];
                }
                $pictures[] = $tempPictures;
                $nameValueList[] = ['Name' => $optionsData['frontend_label'], 'Value' => $allValueList];
                $superAttrList[] = ['Name' => $optionsData['frontend_label'], 'Code' => $optionsData['attribute_code']];
            }

            $assoProId = 0;
            $associateProList = $this->configurableProTypeModel->getChildrenIds($product->getEntityId());
            if (isset($associateProList[0]) && !empty($associateProList[0])) {
                $proVariation = [];
                foreach ($associateProList[0] as $associateProId) {
                    $assNameValueList = [];
                    $associatePro = $this->productFactory->create()->load($associateProId);
                    foreach ($superAttrList as $superAttr) {
                        $value = $associatePro->getAttributeText($superAttr['Code']);
                        $assNameValueList[] = ['Name' => $superAttr['Name'], 'Value' => $value];
                    }
                    $exportProType = $this->helper
                        ->getProductAttrValue($associatePro, 'identification_label');
                    $exportProValue = $this->helper
                        ->getProductAttrValue($associatePro, 'identification_value');
                    $specificationData = $this->getSpecificationData($associatePro, $amzCateName, $categoryGroup);
                    $attrs = $this->helper->getAmzAttrWithVal($associatePro);
                    $amazonCompilance = $this->amazonComplianceAttributes($associatePro);
                    $attrs = array_merge($attrs, $amazonCompilance);
                    $variationData = [
                        'Title' => $associatePro->getName(),
                        'description' => $associatePro->getDescription(),
                        'sku' => $associatePro->getSku(),
                        'variation' => $assNameValueList,
                        'assoc_id' => $associateProId,
                        'type' => $exportProType,
                        'value' => $exportProValue,
                        'product' => $associatePro,
                        'specification' => $specificationData
                    ];
                    $variationData = array_merge($variationData, $attrs);
                    $proVariation[] = $variationData;
                    $associatePro = $this->productRepository->getById($associatePro->getEntityId());
                }
                $variation['Variation'] = $proVariation;
            }
            return $variation;
        } catch (\Exception $e) {
            $this->logger->addError($e->getMessage());
            return $variation;
        }
    }
}
