<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Webkul\AmazonMagentoConnect\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\SchemaPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Webkul\AmazonMagentoConnect\Model\AttributeMapFactory;
use Magento\Eav\Setup\EavSetupFactory;

/**
* Patch is mechanism, that allows to do atomic upgrade data changes
*/
class amazonmagentoconnect implements
    DataPatchInterface
{
    /**
     * EAV setup factory.
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var ModuleDataSetupInterface $moduleDataSetup
     */
    private $moduleDataSetup;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        \Magento\Eav\Api\AttributeRepositoryInterface $attributeRepositoryInterface,
        AttributeMapFactory $attributeMapFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeRepository = $attributeRepositoryInterface;
        $this->attributeMapFactory = $attributeMapFactory;
    }

    /**
     * Do Upgrade
     *
     * @return void
     */
    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        
        try {
            $attribute = $this->attributeRepository
                    ->get(\Magento\Catalog\Model\Product::ENTITY, 'identification_label');
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'identification_label',
                [
                    'label' => 'Unique Identifier',
                    'input' => 'select',
                    'group' => 'Amazon Product Identifier',
                    'source' => '',
                    'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                    'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                    'option'     =>  [
                        'values' => [
                            'asin' => 'ASIN',
                            'ean' => 'EAN',
                            'upc' => 'UPC',
                            'isbn' => 'ISBN',
                            'gtin' => 'GTIN',
                            'jan' => 'JAN',
                        ]
                    ],
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'visible_on_front' => false,
                    'is_configurable' => false,
                    'searchable' => true,
                    'default' => '',
                    'filterable' => true,
                    'comparable' => true,
                    'visible_in_advanced_search' => true,
                    'apply_to' => 'simple,configurable',
                ]
            );
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'identification_value',
                [
                    'label' => 'Unique Identification Code',
                    'input' => 'text',
                    'group' => 'Amazon Product Identifier',
                    'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                    'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                    'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => true,
                    'visible_on_front' => false,
                    'is_configurable' => false,
                    'searchable' => true,
                    'default' => '',
                    'filterable' => true,
                    'comparable' => true,
                    'visible_in_advanced_search' => true,
                    'note' => ' Enter Unique Identification Code as per selected unique identifier. ',
                    'apply_to' => 'simple,configurable',
                ]
            );
        }

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'wk_fulfillment_channel',
            [
                'type' => 'varchar',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'frontend' => '',
                'label' => 'Fulfillment By',
                'input' => 'select',
                'class' => '',
                'source' => 'Webkul\AmazonMagentoConnect\Model\Config\Source\FulFillmentChannel',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => false,
                'required' => false,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'group' => 'General'
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'wk_compliance_config',
            [
                'label' => 'Use Config Compliance',
                'input' => 'select',
                'group' => 'Amazon Compliance',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'visible_on_front' => false,
                'is_configurable' => false,
                'searchable' => true,
                'default' => 1,
                'filterable' => true,
                'comparable' => true,
                'visible_in_advanced_search' => true,
                'note' => 'If yes, Amazon config compliance value will be used.',
                'apply_to' => 'simple,configurable',
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'goods_regulation',
            [
                'type' => 'varchar',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'frontend' => '',
                'label' => 'Applicable Dangerous Goods Regulations',
                'input' => 'select',
                'class' => '',
                'source' => 'Webkul\AmazonMagentoConnect\Model\Config\Source\DangerousGoodsRegulation',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => 'not_applicable',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false,
                'group' => 'Amazon Compliance'
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'use_batteries',
            [
                'label' => 'Does it use batteries?',
                'input' => 'select',
                'group' => 'Amazon Compliance',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'visible_on_front' => false,
                'is_configurable' => false,
                'searchable' => true,
                'default' => 0,
                'filterable' => true,
                'comparable' => true,
                'visible_in_advanced_search' => true,
                'note' => 'Is this product a battery or does it use batteries?',
                'apply_to' => 'simple,configurable',
            ]
        );
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'batteries_included',
            [
                'label' => 'Batteries are Included',
                'input' => 'select',
                'group' => 'Amazon Compliance',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
                'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'visible_on_front' => false,
                'is_configurable' => false,
                'searchable' => true,
                'default' => 0,
                'filterable' => true,
                'comparable' => true,
                'visible_in_advanced_search' => true,
                'apply_to' => 'simple,configurable',
            ]
        );

        // save default data in table
        $modeldata = [
            [
                'mage_attr' => 'sku',
                'amz_attr' => 'sku'
            ],
            [
                'mage_attr' => 'name',
                'amz_attr' => 'title'
            ],
            [
                'mage_attr' => 'description',
                'amz_attr' => 'description'
            ],
            [
                'mage_attr' => 'identification_label',
                'amz_attr' => 'type'
            ],
            [
                'mage_attr' => 'identification_value',
                'amz_attr' => 'value'
            ],
            [
                'mage_attr' => 'price',
                'amz_attr' => 'price'
            ],
            [
                'mage_attr' => 'quantity_and_stock_status',
                'amz_attr' => 'qty'
            ]
        ];
        
        foreach ($modeldata as $data) {
            $this->attributeMapFactory->create()->setData($data)->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [

        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '2.1.0';
    }
}
