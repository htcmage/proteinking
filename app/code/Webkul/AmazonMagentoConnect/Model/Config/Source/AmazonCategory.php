<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Model\Config\Source;

class AmazonCategory implements \Magento\Framework\Option\ArrayInterface
{
    const XSD_FILE_PATH = "/view/adminhtml/web/files/";
    
    /**
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     */
    public function __construct(\Webkul\AmazonMagentoConnect\Helper\Data $helper)
    {
        $this->helper = $helper;
    }
    
    /**
     * Return options array.
     *
     * @param int $store
     *
     * @return array
     */
    public function toOptionArray()
    {
        $importProductType = [];
        $modulePath = $this->helper->getModulePath();
        $xsds = glob($modulePath.self::XSD_FILE_PATH . "*.xsd");
        foreach ($xsds as $file) {
            $file = basename($file);
            $file = basename($file, ".xsd");
            $pieces = preg_split('/(?=[A-Z])/', $file);
            $fileName = implode(' ', array_filter($pieces));
            $importProductType[] = [
                'value' => $file,
                'label' => $fileName
            ];
        }

        return $importProductType;
    }


    /**
     * Get options in "key-value" format.
     *
     * @return array
     */
    public function toArray()
    {
        $optionList = $this->toOptionArray();
        $optionArray = [];
        foreach ($optionList as $option) {
            $optionArray[$option['value']] = $option['label'];
        }

        return $optionArray;
    }
}
