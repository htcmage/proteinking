<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */

namespace Webkul\AmazonMagentoConnect\Model\Config\Source;

class DangerousGoodsRegulation extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * get attribute option
     *
     * @param string $store
     * @return array
     */
    public function toOptionArray()
    {
        $goodsRegulation = [
                            ['value' => 'not_applicable','label' => 'Not Applicable'],
                            ['value' => 'ghs','label' => 'GHS'],
                            ['value' => 'storage','label' => 'Storage'],
                            ['value' => 'waste','label' => 'Waste'],
                            ['value' => 'transportation','label' => 'Transportation'],
                            ['value' => 'other','label' => 'Other'],
                            ['value' => 'unknown','label' => 'Unknown']
                        ];

        return $goodsRegulation;
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $goodsRegulation = $this->toOptionArray();
        if (!$this->_options) {
            $this->_options = $goodsRegulation;
        }
        return $this->_options;
    }
}
