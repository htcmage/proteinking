<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Model\Config\Source;

class AmazonProAttribute
{
    /**
     * Get options in "key-value" format.
     *
     * @return array
     */
    public function toArray()
    {
        $optionArray = [
            'sku'                   => __('Sku'),
            'price'                 => __('Price'),
            'qty'                   => __('qty'),
            'type'                  => __('Product Type'),
            'value'                 => __('Product Value'),
            'Title'                 => __('Title'),
            'Brand'                 => __('Brand'),
            'Manufacturer'          => __('Manufacturer'),
            'MfrPartNumber'         => __('MfrPartNumber'),
            'description'           => __('Description'),
        ];
        asort($optionArray);
        return $optionArray;
    }
}
