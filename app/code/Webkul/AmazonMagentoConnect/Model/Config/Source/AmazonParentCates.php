<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Model\Config\Source;

class AmazonParentCates
{
    /**
     * Get options in "key-value" format.
     *
     * @return array
     */
    public function toArray()
    {
        $optionArray = [
            '1571272031' => 'Clothing & Accessories',
            '5122349031' => 'Appliances',
            '4772061031' => 'Car & Motorbike',
            '1571275031' => 'Baby',
            '1355017031' => 'Beauty',
            '976390031' => 'Books',
            '976417031' => 'Movies & TV Shows',
            '976420031' => 'Electronics',
            '1380441031' => 'Furniture',
            '3704983031' => 'Gift Cards',
            '2454179031' => 'Gourmet & Specialty Foods',
            '1350385031' => 'Health & Personal Care',
            '2454176031' => 'Home & Kitchen',
            '5866079031' => 'Industrial & Scientific',
            '1951049031' => 'Jewellery',
            '1571278031' => 'Kindle Store',
            '2454175031' => 'Lawn & Garden',
            '2454170031' => 'Luggage & Bags',
            '5311359031' => 'Luxury Beauty',
            '976446031' => 'Music',
            '3677698031' => 'Musical Instruments',
            '2454173031' => 'Office Products',
            '976393031' => 'Computers & Accessories',
            '4740420031' => 'Pet Supplies',
            '1571284031' => 'Shoes & Handbags',
            '976452031' => 'Software',
            '1984444031' => 'Sports, Fitness & Outdoors',
            '1350381031' => 'Toys & Games',
            '976461031' => 'Video Games',
            '1350388031' => 'Watches',
        ];
        asort($optionArray);

        return $optionArray;
    }
}
