<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Model\Config\Source;

class AttributeSet implements \Magento\Framework\Option\ArrayInterface
{

    public function __construct(
        \Magento\Catalog\Model\Product\AttributeSet\Options $attributeSet
    ) {
        $this->attributeSet = $attributeSet;
    }
    /**
     * Return options array.
     *
     * @param int $store
     *
     * @return array
     */
    public function toOptionArray()
    {

        $attributSetArray = [];
        $attributeSet =  $this->attributeSet->toOptionArray();
        foreach ($attributeSet as $key => $value) {
            $attributSetArray[] = [
                'value' => $value['value'],
                'label' => $value['label']
            ];
        }
        return $attributSetArray;
    }


    /**
     * Get options in "key-value" format.
     *
     * @return array
     */
    public function toArray()
    {
        $optionList = $this->toOptionArray();
        $optionArray = [];
        foreach ($optionList as $option) {
            $optionArray[$option['value']] = $option['label'];
        }

        return $optionArray;
    }
}
