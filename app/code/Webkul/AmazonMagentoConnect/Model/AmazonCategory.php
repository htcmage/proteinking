<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Model;

use Webkul\AmazonMagentoConnect\Api\Data\AmazonCategoryInterface;
use Magento\Framework\DataObject\IdentityInterface;

class AmazonCategory extends \Magento\Framework\Model\AbstractModel implements AmazonCategoryInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'wk_amazon_category';

    /**
     * @var string
     */
    protected $_cacheTag = 'wk_amazon_category';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'wk_amazon_category';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Webkul\AmazonMagentoConnect\Model\ResourceModel\AmazonCategory');
    }
    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Set EntityId.
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

   /**
    * Get category group.
    * @return string
    */
    public function getCategoryGroup()
    {
        return $this->getData(self::CATEGORY_GROUP);
    }

   /**
    * set price.
    * @return $this
    */
    public function setCategoryGroup($AmazonCategory)
    {
        return $this->setData(self::CATEGORY_GROUP, $AmazonCategory);
    }

   /**
    * Get amazon category.
    * @return string
    */
    public function getAmzCateName()
    {
        return $this->getData(self::AMZ_CATE_NAME);
    }

   /**
    * set amazon category.
    * @return $this
    */
    public function setAmzCateName($amazonCategory)
    {
        return $this->setData(self::AMZ_CATE_NAME, $amazonCategory);
    }
}
