<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Model;

use Webkul\AmazonMagentoConnect\Api\Data\CategoryMapInterface;
use Magento\Framework\DataObject\IdentityInterface;

class CategoryMap extends \Magento\Framework\Model\AbstractModel implements CategoryMapInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'wk_amazon_category_map';

    /**
     * @var string
     */
    protected $_cacheTag = 'wk_amazon_category_map';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'wk_amazon_category_map';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Webkul\AmazonMagentoConnect\Model\ResourceModel\CategoryMap');
    }
    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Set EntityId.
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

   /**
    * Get Amazon category name.
    * @return string
    */
    public function getAmzCateName()
    {
        return $this->getData(self::AMZ_CATE_NAME);
    }

   /**
    * set Amazon category name
    * @return $this
    */
    public function setAmzCateName($categoryName)
    {
        return $this->setData(self::AMZ_CATE_NAME, $categoryName);
    }

   /**
    * Get mage category id.
    * @return string
    */
    public function getMageCateId()
    {
        return $this->getData(self::MAGE_CATE_ID);
    }

   /**
    * set mage category id
    * @return $this
    */
    public function setMageCateId($amazonCategoryId)
    {
        return $this->setData(self::MAGE_CATE_ID, $amazonCategoryId);
    }

   /**
    * Get amazon parent category.
    * @return string
    */
    public function getCategoryGroup()
    {
        return $this->getData(self::CATEGORY_GROUP);
    }

   /**
    * set amazon category parent name
    * @return $this
    */
    public function setCategoryGroup($amazonCategoryId)
    {
        return $this->setData(self::CATEGORY_GROUP, $amazonCategoryId);
    }
}
