<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Model;

use Webkul\AmazonMagentoConnect\Api\Data\CategorySpecificationInterface;
use Magento\Framework\DataObject\IdentityInterface;

class CategorySpecification extends \Magento\Framework\Model\AbstractModel implements CategorySpecificationInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'wk_amazon_category_specification';

    /**
     * @var string
     */
    protected $_cacheTag = 'wk_amazon_category_specification';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'wk_amazon_category_specification';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Webkul\AmazonMagentoConnect\Model\ResourceModel\CategorySpecification');
    }
    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Set EntityId.
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

   /**
    * Get amazon category.
    * @return string
    */
    public function getCategoryGroup()
    {
        return $this->getData(self::CATEGORY_GROUP);
    }

   /**
    * set amazon category.
    * @return $this
    */
    public function setCategoryGroup($amzCategory)
    {
        return $this->setData(self::CATEGORY_GROUP, $amzCategory);
    }

   /**
    * Get amazon specification data.
    * @return string
    */
    public function getAmzSpecificationName()
    {
        return $this->getData(self::AMZ_SPECIFICATION_NAME);
    }

   /**
    * set amazon specification data..
    * @return $this
    */
    public function setAmzSpecificationName($specificationName)
    {
        return $this->setData(self::AMZ_SPECIFICATION_NAME, $specificationName);
    }

   /**
    * Get mage attribute code.CategorySpecification
    * @return string
    */
    public function getMageAttributeCode()
    {
        return $this->getData(self::MAGE_ATTRIBUTE_CODE);
    }

   /**
    * Get mage attribute code.
    * @return $this
    */
    public function setMageAttributeCode($attributeCode)
    {
        return $this->setData(self::MAGE_ATTRIBUTE_CODE, $attributeCode);
    }
}
