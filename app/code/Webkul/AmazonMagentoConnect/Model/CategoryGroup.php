<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Model;

use Webkul\AmazonMagentoConnect\Api\Data\CategoryGroupInterface;
use Magento\Framework\DataObject\IdentityInterface;

class CategoryGroup extends \Magento\Framework\Model\AbstractModel implements CategoryGroupInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'wk_amazon_category_group';

    /**
     * @var string
     */
    protected $_cacheTag = 'wk_amazon_category_group';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'wk_amazon_category_group';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Webkul\AmazonMagentoConnect\Model\ResourceModel\CategoryGroup');
    }
    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * Set EntityId.
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

   /**
    * Get category group.
    * @return string
    */
    public function getCategoryGroup()
    {
        return $this->getData(self::CATEGORY_GROUP);
    }

   /**
    * set price.
    * @return $this
    */
    public function setCategoryGroup($categoryGroup)
    {
        return $this->setData(self::CATEGORY_GROUP, $categoryGroup);
    }

   /**
    * Get has product type
    * @return string
    */
    public function getHasProductType()
    {
        return $this->getData(self::HAS_PRODUCT_TYPE);
    }

   /**
    * set has product type .
    * @return $this
    */
    public function setHasProductType($hasProductType)
    {
        return $this->setData(self::HAS_PRODUCT_TYPE, $categoryGroup);
    }
}
