<?php

/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Ui\DataProvider\Category;
 
use Webkul\AmazonMagentoConnect\Model\ResourceModel\CategoryMap\CollectionFactory;
 
class CategoryImportProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
 
    public function __construct(
        CollectionFactory $collectionFactory,
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $collection = $collectionFactory->create();
        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
        $this->collection = $collectionFactory->create()
                            ->addFieldToFilter('amz_cate_id', ['neq' => 'NULL']);
    }
}
