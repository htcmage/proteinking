<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Ui\Component\Form;
use Magento\Framework\Stdlib\ArrayManager;

/**
 * Data provider for main panel of product page
 *
 * @api
 * @since 101.0.0
 */
class ComplianceModifier extends \Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier
{
    /**
     * @var LocatorInterface
     * @since 101.0.0
     */
    protected $locator;

    /**
     * @var ArrayManager
     * @since 101.0.0
     */
    protected $arrayManager;

    /**
     * @var \Magento\Framework\Locale\CurrencyInterface
     */
    private $localeCurrency;

    /**
     * @param LocatorInterface $locator
     * @param ArrayManager $arrayManager
     */
    public function __construct(
        LocatorInterface $locator,
        ArrayManager $arrayManager
    ) {
        $this->locator = $locator;
        $this->arrayManager = $arrayManager;
    }

    /**
     * {@inheritdoc}
     * @since 101.0.0
     */
    public function modifyMeta(array $meta)
    {
        $meta = $this->customizeMinQtyField($meta);

        return $meta;
    }
    /**
     * {@inheritdoc}
     * @since 101.0.0
     */
    public function modifyData(array $data)
    {
        return $data;
    }
    /**
     * Customize Weight filed
     *
     * @param array $meta
     * @return array
     * @since 101.0.0
     */
    protected function customizeMinQtyField(array $meta)
    {
        $weightPath = $this->arrayManager->findPath('use_batteries', $meta, null, 'children');

        if ($weightPath) {
            $meta = $this->arrayManager->merge(
                $weightPath . static::META_CONFIG_PATH,
                $meta,
                [
                    'dataScope' => 'use_batteries',
                    'additionalClasses' => 'admin__field-small',
                    'imports' => [
                        'visible' => '!${$.provider}:' . self::DATA_SCOPE_PRODUCT
                            . '.wk_compliance_config:value'
                    ]
                ]
            );
        }

        $weightPath = $this->arrayManager->findPath('goods_regulation', $meta, null, 'children');

        if ($weightPath) {
            $meta = $this->arrayManager->merge(
                $weightPath . static::META_CONFIG_PATH,
                $meta,
                [
                    'dataScope' => 'goods_regulation',
                    'additionalClasses' => 'admin__field-small',
                    'imports' => [
                        'visible' => '!${$.provider}:' . self::DATA_SCOPE_PRODUCT
                            . '.wk_compliance_config:value'
                    ]
                ]
            );
        }

        $weightPath = $this->arrayManager->findPath('batteries_included', $meta, null, 'children');

        if ($weightPath) {
            $meta = $this->arrayManager->merge(
                $weightPath . static::META_CONFIG_PATH,
                $meta,
                [
                    'dataScope' => 'batteries_included',
                    'additionalClasses' => 'admin__field-small',
                    'imports' => [
                        'visible' => '!${$.provider}:' . self::DATA_SCOPE_PRODUCT
                            . '.wk_compliance_config:value'
                    ]
                ]
            );
        }

        return $meta;
    }
}
