<?php
/**
 * @category   Webkul
 * @package    Webkul_AmazonMagentoConnect
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\AmazonMagentoConnect\Ui\Component\Listing\Column\CategoryMap;

use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class StoreCategoryName extends Column
{
    /**
     * @var CategoryFactory
     */
    public $categoryFactory;

    /**
     * Constructor.
     * @param ContextInterface   $context
     * @param UiComponentFactory $uiComponentFactory
     * @param CategoryFactory    $categoryFactory
     * @param array              $components
     * @param array              $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        CategoryFactory $categoryFactory,
        array $components = [],
        array $data = []
    ) {
        $this->categoryFactory = $categoryFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                $storeCat = $this->categoryFactory->create()->load($item['mage_cate_id']);
                $categories = $storeCat->getParentCategories();
                $catPath = '';
                foreach ($categories as $category) {
                    $catPath = $catPath.$category->getName().'/';
                }
                $catPath = rtrim($catPath, '/');
                $item[$fieldName] = $storeCat->getEntityId() ?
                                    (!empty($catPath) ? $catPath : $storeCat->getName()) :
                                     $item['mage_cate_id'];
            }
        }
        return $dataSource;
    }
}
