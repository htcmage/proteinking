<?php

namespace Doanh\Customd\Plugin\Catalog\Model;

use Magento\Catalog\Model\Product;
use Magento\Paypal\Model\Pro;

class ProductPlugin
{
    public function afterGetName(Product $subject, $result)
    {
//        model_name
        $type = $subject->getTypeId();
		// die("sdadasda");
        if (null !== $subject->getCustomAttribute('model_name')) {
            $modelName = $subject->getCustomAttribute('model_name')->getValue();

            if ($type == 'simple') {
                return $result . " ( ". $modelName ." )";
            } else {
                return $result;
            }
        } else {

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $product = $objectManager->create('Magento\Catalog\Model\Product')->load($subject->getId());
            $model_name = $product->getData('model_name');

            if ($type == 'simple') {
                return $result . " ( ". $model_name ." )";
            } else {
                return $result;
            }
        }

    }
}