<?php

namespace Doanh\Customd\Cron;

use \Psr\Log\LoggerInterface;

class CronLog {

    public function execute()
    {

        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/cron.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info(__METHOD__);

        return $this;

    }

}