# Mage2 Module Doanh Syncproducts

    ``doanh/module-syncproducts``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities


## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Doanh`
 - Enable the module by running `php bin/magento module:enable Doanh_Syncproducts`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require doanh/module-syncproducts`
 - enable the module by running `php bin/magento module:enable Doanh_Syncproducts`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - Enable (syncproducts/syncproducts_group/enable)

 - Sync Products to Domain (syncproducts/syncproducts_group/domain)

 - API Key (syncproducts/syncproducts_group/apikey)

 - API Secret (syncproducts/syncproducts_group/apisecret)


## Specifications

 - Helper
	- Doanh\Syncproducts\Helper\Data

 - Controller
	- frontend > syncproducts/index/index


## Attributes



