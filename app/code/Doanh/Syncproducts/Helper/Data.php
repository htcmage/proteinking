<?php


namespace Doanh\Syncproducts\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

    /**
     * @var \Magento\Catalog\Model\Category
     */
    protected $_category;
    protected $storeManager;
    /**
     * @var \Magento\GroupedProduct\Model\Product\Type\Grouped
     */
    protected $_model;

    /**
     * @param \Magento\Catalog\Model\Category $_category
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\GroupedProduct\Model\Product\Type\Grouped $grouped
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Catalog\Model\Category $_category,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\GroupedProduct\Model\Product\Type\Grouped $grouped,
        \Magento\Framework\App\Helper\Context $context
    ) {
        $this->_category = $_category;
        $this->storeManager = $storeManager;
        $this->_model = $grouped;
        parent::__construct($context);
    }

    public function createNewProduct ($product, $qty, $cronjob = false) {
        $productData = $product->getData();
        var_dump("<pre>");
//        var_dump($product->debug());
//        var_dump("</pre>");
//        die("ProductSaveAfter");
        if($cronjob) {
            $productData['stock_data'] = $product->getStockItem()->getData();
        }

        if($productData['sku']) {
            $baseImage = $product->getImage();
            $brandName = $product->getAttributeText('manufacturer');
            $categoryIds = $product->getCategoryIds();
            $categoryName = array();
            if (count($categoryIds) > 0){
                foreach ($categoryIds as $cat) {
                    $category = $this->_category->setStoreId($this->storeManager->getStore()->getId())->load($cat);
                    $categoryName[] = $category->getName();
                }
            }

            if($productData['type_id'] == 'grouped') {
                $grouped_products = $this->getAssociatedProductIds($product);
            }

            // Oauth credentials from wp-cli
            $Key = $this->scopeConfig->getValue('syncproducts/syncproducts_group/apikey', ScopeInterface::SCOPE_STORE);
            $Secret = $this->scopeConfig->getValue('syncproducts/syncproducts_group/apisecret', ScopeInterface::SCOPE_STORE);

            // set our end point
            $domain = $this->scopeConfig->getValue('syncproducts/syncproducts_group/domain', ScopeInterface::SCOPE_STORE);
            $headers[] = "key=$Key";
            $headers[] = "secret=$Secret";

            //GET WC PRODUCT CATEGORY
            $wp_categories_put = array();
            if(count($categoryName) > 0) {
                $wp_categories = array();
                $endpoint_getcategory = $domain."wp-json/wc/v2/products/categories?per_page=100&page=1";
//                $curl = curl_init($endpoint_getcategory);
//
//                curl_setopt_array($curl, [
//                    CURLOPT_HTTPHEADER     => $headers,
//                    CURLOPT_RETURNTRANSFER => true,
//                    CURLOPT_URL            => $endpoint_getcategory,
//                    CURLOPT_CUSTOMREQUEST  => "GET",
//                ]);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $endpoint_getcategory);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_PORT, 443);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


                $response_getcategory = curl_exec($ch);
                curl_close($ch);
                $wc_getcategory1 = json_decode($response_getcategory, true);
//                var_dump("<pre>");

                foreach ($wc_getcategory1 as $getcategory1) {
                    $wp_categories[$getcategory1['id']] = $getcategory1['name'];
                }

                var_dump("-----------------------");
                $endpoint_getcategory = $domain."wp-json/wc/v2/products/categories?per_page=100&page=2";
                $curl = curl_init($endpoint_getcategory);

                curl_setopt_array($curl, [
                    CURLOPT_HTTPHEADER     => $headers,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_URL            => $endpoint_getcategory,
                    CURLOPT_CUSTOMREQUEST  => "GET",
                ]);
                $response_getcategory = curl_exec($curl);
                $wc_getcategory2 = json_decode($response_getcategory, true);
                foreach ($wc_getcategory2 as $getcategory2) {
                    $wp_categories[$getcategory2['id']] = $getcategory2['name'];
                }

                foreach ($wp_categories as $key=>$cat) {
                    if (in_array($cat, $categoryName)) {
                        $wp_categories_put[] = $key;
                    }
                }
            }

            //CREATE NEW WC PRODUCTS
            $status = 'publish';
//            var_dump($productData['special_price']);
//            var_dump($productData['special_from_date']);
//            var_dump($productData['special_to_date']);

            if($productData['status'] == 0) {
                $status = 'pending';
            }

            $visibility = 'visible';
            if($productData['visibility'] == 1) {
                $visibility = 'hidden';
            } elseif(($productData['visibility'] == 2) || ($productData['visibility'] == 4)) {
                $visibility = 'catalog';
            } elseif($productData['visibility'] == 3) {
                $visibility = 'search';
            }

            if($productData['type_id'] == 'grouped') {
                $productData['price'] = 0;
                $productData['special_price'] = 0;
                $productData['special_from_date'] = null;
                $productData['special_to_date'] = null;
                $productData['weight'] = 0;
            }

            // added by Nand to update AC price = PK price x 1.2 (rounded up to nearest 0.95)

            $productData['price'] = (ceil($productData['price']*1.2) - 0.05);
            if(isset($productData['special_price']) && ($productData['special_price']>0))
            {
                $productData['special_price'] = (ceil($productData['special_price']*1.2) - 0.05);
            } else {
                $productData['special_price'] = null;
                $productData['special_from_date'] = null;
                $productData['special_to_date'] = null;
            }
            if(!isset($productData['stock_data']['use_config_manage_stock'])){
                $productData['stock_data']['use_config_manage_stock'] = 1;
            }
            if(!isset($productData['quantity_and_stock_status']['qty'])){
                $productData['quantity_and_stock_status']['qty'] = 0;
            }
            if(!isset($productData['quantity_and_stock_status']['is_in_stock'])){
                $productData['quantity_and_stock_status']['is_in_stock'] = 1;
            }
            if(!isset($productData['ebay_productid'])){
                $productData['ebay_productid'] = '';
            }
            if(!isset($productData['nutrition_info'])){
                $productData['nutrition_info'] = '';
            }
            if(!isset($productData['short_description'])){
                $productData['short_description'] = '';
            }
            if(!isset($productData['special_price'])){
                $productData['special_price'] = '';
            }
            if(!isset($productData['special_from_date'])){
                $productData['special_from_date'] = '';
            }
            if(!isset($productData['special_to_date'])){
                $productData['special_to_date'] = '';
            }

            if(!isset($productData['ebay_epid_attribute'])){
                $productData['ebay_epid_attribute'] = '';
            }
            if(!isset($productData['ebay_mpn_attribute'])){
                $productData['ebay_mpn_attribute'] = '';
            }
            if(!isset($productData['ebay_upc_attribute'])){
                $productData['ebay_upc_attribute'] = '';
            }
            if(!isset($productData['ebay_productid'])){
                $productData['ebay_productid'] = '';
            }
            if(!isset($productData['description'])){
                $productData['description'] = '';
            }
            if(!isset($productData['weight'])){
                $productData['weight'] = '';
            }
            if(!isset($productData['model_name'])){
                $productData['model_name'] = '';
            }

            // added by D
            $data = array(
                'name' => $productData['name'],
                'type' => $productData['type_id'],
                'slug' => $productData['url_key'],
                'permalink' => $domain . $productData['url_key'] .'/',
                'status' => $status,
                'catalog_visibility' => $visibility,
                'description' => $productData['description'] .' '. $productData['nutrition_info'],
                'short_description' => $productData['short_description'],
                'sku' => $productData['sku'],
                'regular_price' => $productData['price'],
                'sale_price' => $productData['special_price'],
                'date_on_sale_from' => $productData['special_from_date'],
                'date_on_sale_from_gmt' => $productData['special_from_date'],
                'date_on_sale_to' => $productData['special_to_date'],
                'date_on_sale_to_gmt' => $productData['special_to_date'],
                'tax_status' => 'taxable',
                'tax_class' => '',
                'weight' => $productData['weight'],
                /*'brands' => array(
                	0 => 46
                ),*/
                'attributes' => array(
                    0 => (object) array(
                        'id' => 10,
                        'name' => 'Ebay epid attribute',
                        'position' => 0,
                        'visible' => 1,
                        'options' => array(
                            0 => $productData['ebay_epid_attribute']
                        )
                    ),
                    1 => (object) array(
                        'id' => 12,
                        'name' => 'Ebay mpn attribute',
                        'position' => 0,
                        'visible' => 1,
                        'options' => array(
                            0 => $productData['ebay_mpn_attribute']
                        )
                    ),
                    2 => (object) array(
                        'id' => 11,
                        'name' => 'Ebay isbn attribute',
                        'position' => 0,
                        'visible' => 1,
                        'options' => array(
                            0 => $productData['ebay_isbn_attribute']
                        )
                    ),
                    3 => (object) array(
                        'id' => 9,
                        'name' => 'Ebay Ean Attribute',
                        'position' => 0,
                        'visible' => 1,
                        'options' => array(
                            0 => $productData['ebay_ean_attribute']
                        )
                    ),
                    4 => (object) array(
                        'id' => 7,
                        'name' => 'Ebay UPC attribute',
                        'position' => 0,
                        'visible' => 1,
                        'options' => array(
                            0 => $productData['ebay_upc_attribute']
                        )
                    ),
                    5 => (object) array(
                        'id' => 8,
                        'name' => 'Ebay Product id',
                        'position' => 0,
                        'visible' => 1,
                        'options' => array(
                            0 => $productData['ebay_productid']
                        )
                    )
                )
            );

            if($productData['type_id'] == 'grouped') {
//                foreach ($grouped_products as $simpleId) {
//                    $syncProduct = Mage::getModel('syncproducts/syncproducts')->getCollection();
//                    $syncProduct->addFieldToFilter('pk_product_id', $simpleId);
//                    $syncProductData = $syncProduct->getData();
//                    $wcpid = $syncProductData[0]['ac_product_id'];
//                    $data['grouped_products'][] = $wcpid;
//                }
            }

            if(isset($productData['model_name'])) {
                $data['attributes'][6] = (object) array(
                    'id' => 6,
                    'name' => 'Model Name',
                    'position' => 0,
                    'visible' => 1,
                    'options' => array(
                        0 => $productData['model_name']
                    )
                );
            }

            if($productData['type_id'] == 'simple') {
                $data['price'] = $productData['price'];
                $data['price_html'] = '<span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#36;</span>'. $productData['price'] .'</span>';

                if($productData['special_price'] && $productData['special_price'] < $productData['price']) {
                    $data['price'] = $productData['special_price'];
                    $data['price_html'] = '<del><span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#36;</span>'. $productData['price'] .'</span></del> <ins><span class=\"woocommerce-Price-amount amount\"><span class=\"woocommerce-Price-currencySymbol\">&#36;</span>'. $productData['special_price'] .'</span></ins>';
                }

                if(isset($productData['model_name']) && !empty($productData['model_name'])) {
                    $data['name'] = $productData['model_name'];
                }

                $data['manage_stock'] = false;
                if($productData['stock_data']['use_config_manage_stock'] == 1) {
                    $data['manage_stock'] = true;
                }
                $data['stock_quantity'] = null;
                if($productData['quantity_and_stock_status']['qty']) {
                    $data['stock_quantity'] = $productData['quantity_and_stock_status']['qty'];
                }
                $data['in_stock'] = false;
                if($productData['quantity_and_stock_status']['is_in_stock'] == 1) {
                    $data['in_stock'] = true;
                }
            }

            if($baseImage != NULL) {
                $file = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product'. $baseImage;
                //if (file_exists($file)) {
                $data['images'] = array(
                    0 => (object) array(
                        'src' => $file,
                        'position' => 0
                    )
                );
                //}
            }

//            if(count($wp_categories_put) > 0) {
//                $data['categories'] = array();
//                foreach ($wp_categories_put as $cat_put) {
//                    $data['categories'][] = (object) array('id' => $cat_put);
//                }
//            }

            //echo '<pre>'; print_r($data); exit;

            $endpoint = $domain."wp-json/wc/v2/products";
            $curl = curl_init($endpoint);

            curl_setopt_array($curl, [
                CURLOPT_HTTPHEADER     => $headers,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_URL            => $endpoint,
                CURLOPT_CUSTOMREQUEST  => "POST",
                CURLOPT_POSTFIELDS => http_build_query($data),
            ]);
            $response = curl_exec($curl);
            $decoderesponse = json_decode($response, true);
            //echo '<pre>';
//            var_dump($decoderesponse);
//
//            var_dump("</pre>");
//            die("done");
            if(isset($decoderesponse['id']) && $decoderesponse['id']) {
//                $newProductData = array(
//                    'product_sku' => $productData['sku'],
//                    'pk_product_id' => $product->getId(),
//                    'ac_product_id' => $decoderesponse['id'],
//                );

//                $sync_id = 0;
//                $_syncProducts = Mage::getModel('syncproducts/syncproducts')->load($product->getId(),'pk_product_id');
                //echo '<pre>'; print_r(count($_syncProducts->getData())); exit;
//                if( count($_syncProducts->getData()) > 0 ) {
//                    $syncData = $_syncProducts->getData();
//                    $sync_id = $syncData['id'];
//
//                    $syncproducts = Mage::getModel('syncproducts/syncproducts')->load($sync_id);
//                    $syncproducts->setAcProductId($decoderesponse['id']);
//                    $syncproducts->save();
//                } else {
//                    $syncproducts = Mage::getModel('syncproducts/syncproducts');
//                    $syncproducts->setData($newProductData);
//                    $syncproducts->save();
//                }
            }
        }
    }
    public function getAssociatedProductIds($groupedProductId)
    {
        return $this->_model->getAssociatedProductIds($groupedProductId);
    }
}
