<?php


namespace Doanh\Syncproducts\Model\Data;

use Doanh\Syncproducts\Api\Data\SyncproductsInterface;

class Syncproducts extends \Magento\Framework\Api\AbstractExtensibleObject implements SyncproductsInterface
{

    /**
     * Get syncproducts_id
     * @return string|null
     */
    public function getSyncproductsId()
    {
        return $this->_get(self::SYNCPRODUCTS_ID);
    }

    /**
     * Set syncproducts_id
     * @param string $syncproductsId
     * @return \Doanh\Syncproducts\Api\Data\SyncproductsInterface
     */
    public function setSyncproductsId($syncproductsId)
    {
        return $this->setData(self::SYNCPRODUCTS_ID, $syncproductsId);
    }

    /**
     * Get Syncproducts
     * @return string|null
     */
    public function getSyncproducts()
    {
        return $this->_get(self::SYNCPRODUCTS);
    }

    /**
     * Set Syncproducts
     * @param string $syncproducts
     * @return \Doanh\Syncproducts\Api\Data\SyncproductsInterface
     */
    public function setSyncproducts($syncproducts)
    {
        return $this->setData(self::SYNCPRODUCTS, $syncproducts);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Doanh\Syncproducts\Api\Data\SyncproductsExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Doanh\Syncproducts\Api\Data\SyncproductsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Doanh\Syncproducts\Api\Data\SyncproductsExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}
