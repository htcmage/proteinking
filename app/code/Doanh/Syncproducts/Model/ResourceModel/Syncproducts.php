<?php


namespace Doanh\Syncproducts\Model\ResourceModel;

class Syncproducts extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('doanh_syncproducts_syncproducts', 'syncproducts_id');
    }
}
