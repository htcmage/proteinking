<?php


namespace Doanh\Syncproducts\Model\ResourceModel\Syncproducts;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Doanh\Syncproducts\Model\Syncproducts::class,
            \Doanh\Syncproducts\Model\ResourceModel\Syncproducts::class
        );
    }
}
