<?php


namespace Doanh\Syncproducts\Model;

use Doanh\Syncproducts\Api\SyncproductsRepositoryInterface;
use Doanh\Syncproducts\Api\Data\SyncproductsSearchResultsInterfaceFactory;
use Doanh\Syncproducts\Api\Data\SyncproductsInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Doanh\Syncproducts\Model\ResourceModel\Syncproducts as ResourceSyncproducts;
use Doanh\Syncproducts\Model\ResourceModel\Syncproducts\CollectionFactory as SyncproductsCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\ExtensibleDataObjectConverter;

class SyncproductsRepository implements SyncproductsRepositoryInterface
{

    protected $resource;

    protected $syncproductsFactory;

    protected $syncproductsCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataSyncproductsFactory;

    protected $extensionAttributesJoinProcessor;

    private $storeManager;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;

    /**
     * @param ResourceSyncproducts $resource
     * @param SyncproductsFactory $syncproductsFactory
     * @param SyncproductsInterfaceFactory $dataSyncproductsFactory
     * @param SyncproductsCollectionFactory $syncproductsCollectionFactory
     * @param SyncproductsSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceSyncproducts $resource,
        SyncproductsFactory $syncproductsFactory,
        SyncproductsInterfaceFactory $dataSyncproductsFactory,
        SyncproductsCollectionFactory $syncproductsCollectionFactory,
        SyncproductsSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->syncproductsFactory = $syncproductsFactory;
        $this->syncproductsCollectionFactory = $syncproductsCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataSyncproductsFactory = $dataSyncproductsFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Doanh\Syncproducts\Api\Data\SyncproductsInterface $syncproducts
    ) {
        /* if (empty($syncproducts->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $syncproducts->setStoreId($storeId);
        } */
        
        $syncproductsData = $this->extensibleDataObjectConverter->toNestedArray(
            $syncproducts,
            [],
            \Doanh\Syncproducts\Api\Data\SyncproductsInterface::class
        );
        
        $syncproductsModel = $this->syncproductsFactory->create()->setData($syncproductsData);
        
        try {
            $this->resource->save($syncproductsModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the syncproducts: %1',
                $exception->getMessage()
            ));
        }
        return $syncproductsModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getById($syncproductsId)
    {
        $syncproducts = $this->syncproductsFactory->create();
        $this->resource->load($syncproducts, $syncproductsId);
        if (!$syncproducts->getId()) {
            throw new NoSuchEntityException(__('Syncproducts with id "%1" does not exist.', $syncproductsId));
        }
        return $syncproducts->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->syncproductsCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Doanh\Syncproducts\Api\Data\SyncproductsInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Doanh\Syncproducts\Api\Data\SyncproductsInterface $syncproducts
    ) {
        try {
            $syncproductsModel = $this->syncproductsFactory->create();
            $this->resource->load($syncproductsModel, $syncproducts->getSyncproductsId());
            $this->resource->delete($syncproductsModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Syncproducts: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($syncproductsId)
    {
        return $this->delete($this->getById($syncproductsId));
    }
}
