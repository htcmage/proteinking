<?php


namespace Doanh\Syncproducts\Model;

use Doanh\Syncproducts\Api\Data\SyncproductsInterface;
use Doanh\Syncproducts\Api\Data\SyncproductsInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Syncproducts extends \Magento\Framework\Model\AbstractModel
{

    protected $syncproductsDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'doanh_syncproducts_syncproducts';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param SyncproductsInterfaceFactory $syncproductsDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Doanh\Syncproducts\Model\ResourceModel\Syncproducts $resource
     * @param \Doanh\Syncproducts\Model\ResourceModel\Syncproducts\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        SyncproductsInterfaceFactory $syncproductsDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Doanh\Syncproducts\Model\ResourceModel\Syncproducts $resource,
        \Doanh\Syncproducts\Model\ResourceModel\Syncproducts\Collection $resourceCollection,
        array $data = []
    ) {
        $this->syncproductsDataFactory = $syncproductsDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve syncproducts model with syncproducts data
     * @return SyncproductsInterface
     */
    public function getDataModel()
    {
        $syncproductsData = $this->getData();
        
        $syncproductsDataObject = $this->syncproductsDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $syncproductsDataObject,
            $syncproductsData,
            SyncproductsInterface::class
        );
        
        return $syncproductsDataObject;
    }
}
