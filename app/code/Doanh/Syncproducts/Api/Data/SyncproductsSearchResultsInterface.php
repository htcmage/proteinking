<?php


namespace Doanh\Syncproducts\Api\Data;

interface SyncproductsSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Syncproducts list.
     * @return \Doanh\Syncproducts\Api\Data\SyncproductsInterface[]
     */
    public function getItems();

    /**
     * Set Syncproducts list.
     * @param \Doanh\Syncproducts\Api\Data\SyncproductsInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
