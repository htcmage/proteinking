<?php


namespace Doanh\Syncproducts\Api\Data;

interface SyncproductsInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const SYNCPRODUCTS_ID = 'syncproducts_id';
    const SYNCPRODUCTS = 'Syncproducts';

    /**
     * Get syncproducts_id
     * @return string|null
     */
    public function getSyncproductsId();

    /**
     * Set syncproducts_id
     * @param string $syncproductsId
     * @return \Doanh\Syncproducts\Api\Data\SyncproductsInterface
     */
    public function setSyncproductsId($syncproductsId);

    /**
     * Get Syncproducts
     * @return string|null
     */
    public function getSyncproducts();

    /**
     * Set Syncproducts
     * @param string $syncproducts
     * @return \Doanh\Syncproducts\Api\Data\SyncproductsInterface
     */
    public function setSyncproducts($syncproducts);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Doanh\Syncproducts\Api\Data\SyncproductsExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Doanh\Syncproducts\Api\Data\SyncproductsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Doanh\Syncproducts\Api\Data\SyncproductsExtensionInterface $extensionAttributes
    );
}
