<?php


namespace Doanh\Syncproducts\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface SyncproductsRepositoryInterface
{

    /**
     * Save Syncproducts
     * @param \Doanh\Syncproducts\Api\Data\SyncproductsInterface $syncproducts
     * @return \Doanh\Syncproducts\Api\Data\SyncproductsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Doanh\Syncproducts\Api\Data\SyncproductsInterface $syncproducts
    );

    /**
     * Retrieve Syncproducts
     * @param string $syncproductsId
     * @return \Doanh\Syncproducts\Api\Data\SyncproductsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($syncproductsId);

    /**
     * Retrieve Syncproducts matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Doanh\Syncproducts\Api\Data\SyncproductsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Syncproducts
     * @param \Doanh\Syncproducts\Api\Data\SyncproductsInterface $syncproducts
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Doanh\Syncproducts\Api\Data\SyncproductsInterface $syncproducts
    );

    /**
     * Delete Syncproducts by ID
     * @param string $syncproductsId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($syncproductsId);
}
