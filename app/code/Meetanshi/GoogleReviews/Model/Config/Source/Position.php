<?php

namespace Meetanshi\GoogleReviews\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Position implements ArrayInterface
{

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        $array = [];
        foreach ($this->toOptionArray() as $item) {
            $array[$item['value']] = $item['label'];
        }
        return $array;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'BOTTOM_LEFT', 'label' => __('BOTTOM LEFT')],
            ['value' => 'BOTTOM_RIGHT', 'label' => __('BOTTOM RIGHT')],
        ];
    }
}
