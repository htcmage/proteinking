<?php

namespace Meetanshi\GoogleReviews\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Language implements ArrayInterface
{

    public function toArray()
    {
        $array = [];
        foreach ($this->toOptionArray() as $item) {
            $array[$item['value']] = $item['label'];
        }
        return $array;
    }

    public function toOptionArray()
    {
        return [
            ['value' => 'cs_CZ', 'label' => __('Czech (Czech Republic)')],
            ['value' => 'da_DK', 'label' => __('Danish (Denmark)')],
            ['value' => 'de_DE', 'label' => __('German (Germany)')],
            ['value' => 'en_AU', 'label' => __('English (Australia)')],
            ['value' => 'en_GB', 'label' => __('English (United Kingdom)')],
            ['value' => 'en_US', 'label' => __('English (United States)')],
            ['value' => 'nl_NL', 'label' => __('Dutch (Netherlands)')],
            ['value' => 'fr_BE', 'label' => __('French (Belgium)')],
            ['value' => 'fr_CA', 'label' => __('French (Canada)')],
            ['value' => 'fr_FR', 'label' => __('French (France)')],
            ['value' => 'de_AT', 'label' => __('German (Austria)')],
            ['value' => 'de_CH', 'label' => __('German (Switzerland)')],
            ['value' => 'it_IT', 'label' => __('Italian (Italy)')],
            ['value' => 'it_CH', 'label' => __('Italian (Switzerland)')],
            ['value' => 'ja_JP', 'label' => __('Japanese (Japan)')],
            ['value' => 'pl_PL', 'label' => __('Polish (Poland)')],
            ['value' => 'pt_BR', 'label' => __('Portuguese (Brazil)')],
            ['value' => 'ru_RU', 'label' => __('Russian (Russia)')],
            ['value' => 'es_AR', 'label' => __('Spanish (Argentina)')],
            ['value' => 'es_CL', 'label' => __('Spanish (Chile)')],
            ['value' => 'es_CO', 'label' => __('Spanish (Colombia)')],
            ['value' => 'es_CR', 'label' => __('Spanish (Costa Rica)')],
            ['value' => 'es_MX', 'label' => __('Spanish (Mexico)')],
            ['value' => 'es_PA', 'label' => __('Spanish (Panama)')],
            ['value' => 'es_PE', 'label' => __('Spanish (Peru)')],
            ['value' => 'es_ES', 'label' => __('Spanish (Spain)')],
            ['value' => 'es_VE', 'label' => __('Spanish (Venezuela)')],
            ['value' => 'sv_SE', 'label' => __('Swedish (Sweden)')],
            ['value' => 'tr_TR', 'label' => __('Turkish (Turkey)')],
            ['value' => 'nl_BE', 'label' => __('Dutch (Belgium)')]
        ];
    }
}
