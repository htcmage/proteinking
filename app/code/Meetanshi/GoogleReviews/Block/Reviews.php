<?php

namespace Meetanshi\GoogleReviews\Block;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Meetanshi\GoogleReviews\Helper\Data;
use Magento\Sales\Model\OrderFactory;

class Reviews extends Template
{
    protected $orderFactory;
    protected $helper;

    public function __construct(Context $context, Registry $registry, Data $helper, OrderFactory $orderFactory, array $data = [])
    {
        $this->helper = $helper;
        $this->registry = $registry;
        $this->orderFactory = $orderFactory;
        parent::__construct($context, $data);
    }

    public function getHelper()
    {
        return $this->helper;
    }
}
