<?php

namespace Meetanshi\GoogleReviews\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const CONFIG_GOOGLE_ACTIVE = 'googlereviews/general/enabled';
    const CONFIG_GOOGLE_MERCHANT_ID = 'googlereviews/general/merchant_id';
    const CONFIG_GOOGLE_POSITION = 'googlereviews/general/position';
    const CONFIG_GOOGLE_LANGUAGE = 'googlereviews/general/language';
    const CONFIG_SURVEY_ACTIVE = 'googlereviews/survey/enabled';
    const CONFIG_SURVEY_EST_DAYS = 'googlereviews/survey/est_days';
    const CONFIG_SURVEY_STYLE = 'googlereviews/survey/style';

    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    public function isActive()
    {
        return $this->scopeConfig->getValue(self::CONFIG_GOOGLE_ACTIVE, ScopeInterface::SCOPE_STORE);
    }

    public function getMerchantId()
    {
        return $this->scopeConfig->getValue(self::CONFIG_GOOGLE_MERCHANT_ID, ScopeInterface::SCOPE_STORE);
    }

    public function getPosition()
    {
        return $this->scopeConfig->getValue(self::CONFIG_GOOGLE_POSITION, ScopeInterface::SCOPE_STORE);
    }

    public function getLanguage()
    {
        return $this->scopeConfig->getValue(self::CONFIG_GOOGLE_LANGUAGE, ScopeInterface::SCOPE_STORE);
    }

    public function isSurveyActive()
    {
        return $this->scopeConfig->getValue(self::CONFIG_SURVEY_ACTIVE, ScopeInterface::SCOPE_STORE);
    }

    public function getEstDays()
    {
        return $this->scopeConfig->getValue(self::CONFIG_SURVEY_EST_DAYS, ScopeInterface::SCOPE_STORE);
    }

    public function getStyle()
    {
        return $this->scopeConfig->getValue(self::CONFIG_SURVEY_STYLE, ScopeInterface::SCOPE_STORE);
    }
}
