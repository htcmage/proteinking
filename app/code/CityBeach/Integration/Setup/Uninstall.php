<?php

namespace CityBeach\Integration\Setup;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Setup\UninstallInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Integration\Api\IntegrationServiceInterface;
use Magento\Integration\Model\AuthorizationService;
use Magento\Integration\Model\ConfigBasedIntegrationManager;
use Magento\Integration\Model\Oauth\Token;
use Magento\Integration\Model\OauthService;

class Uninstall implements UninstallInterface
{
  /**
   * @var ConfigBasedIntegrationManager
   */
  private $integrationManager;

  /**
   * @var IntegrationServiceInterface
   */
  private $integrationService;

  /**
   * @var AuthorizationService
   */
  private $authorizationService;

  /**
   * @var ScopeConfigInterface
   */
  private $scopeConfig;

  /**
   * @var OauthService
   */
  private $oauthService;

  /**
   * @var Token
   */
  private $token;

  /**
   * @param ConfigBasedIntegrationManager $integrationManager
   * @param IntegrationServiceInterface $integrationService
   * @param AuthorizationService $scopeConfig
   * @param ScopeConfigInterface $oauthService
   * @param OauthService $oauthService
   * @param Token $token
   */
  public function __construct(
    ConfigBasedIntegrationManager $integrationManager,
    IntegrationServiceInterface $integrationService,
    AuthorizationService $authorizationService,
    ScopeConfigInterface $scopeConfig,
    OauthService $oauthService,
    Token $token
  ) {
    $this->integrationManager = $integrationManager;
    $this->integrationService = $integrationService;
    $this->authorizationService = $authorizationService;
    $this->scopeConfig = $scopeConfig;
    $this->oauthService = $oauthService;
    $this->token = $token;
  }

  public function uninstall(SchemaSetupInterface $setup, ModuleContextInterface $context)
  {
    $name = 'CityBeach Integration';
    $integration = $this->integrationService->findByName($name);
    if ( $integration ) {
      $consumerId = $integration->getConsumerId();
      if ( $consumerId ) {
        $this->oauthService->deleteConsumer($consumerId);
      }
      $this->integrationService->delete($integration->getId());
    }
  }

}