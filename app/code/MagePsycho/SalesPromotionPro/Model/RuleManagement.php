<?php

namespace MagePsycho\SalesPromotionPro\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\SalesRule\Api\Data\RuleExtensionFactory;
use Magento\SalesRule\Api\Data\RuleInterface;
use MagePsycho\SalesPromotionPro\Api\GiftRepositoryInterface;
use MagePsycho\SalesPromotionPro\Api\RuleManagementInterface;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class RuleManagement implements RuleManagementInterface
{
    /**
     * @var RuleExtensionFactory
     */
    private $ruleExtensionFactory;

    /**
     * @var GiftRepositoryInterface
     */
    private $giftRepository;

    public function __construct(
        GiftRepositoryInterface $giftRepository,
        RuleExtensionFactory $ruleExtensionFactory
    ) {
        $this->giftRepository       = $giftRepository;
        $this->ruleExtensionFactory = $ruleExtensionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function addGiftSkuToRule(RuleInterface $rule)
    {
        $extensionAttributes = $rule->getExtensionAttributes() === null
            ? $this->ruleExtensionFactory->create()
            : $rule->getExtensionAttributes();

        try {
            $gift    = $this->giftRepository->getByRuleId((int) $rule->getRuleId());
            $giftSku = $gift->getGiftSku();
        } catch (\Exception $exception) {
            $giftSku =  null;
        }

        $extensionAttributes->setGiftSku($giftSku);
        $rule->setExtensionAttributes($extensionAttributes);
        return $rule;
    }

    /**
     * {@inheritdoc}
     */
    public function saveGiftSku(
        RuleInterface $rule
    ) {
        $extensionAttributes = $rule->getExtensionAttributes();
        if (null !== $extensionAttributes
            && null !== $extensionAttributes->getGiftSku()
        ) {
            $giftSku = $extensionAttributes->getGiftSku();
            $this->giftRepository->saveByRuleId($rule->getRuleId(), $giftSku);
        }
        return $rule;
    }
}
