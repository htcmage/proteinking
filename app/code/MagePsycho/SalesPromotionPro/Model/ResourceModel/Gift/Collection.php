<?php

namespace MagePsycho\SalesPromotionPro\Model\ResourceModel\Gift;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Collection extends AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \MagePsycho\SalesPromotionPro\Model\Gift::class,
            \MagePsycho\SalesPromotionPro\Model\ResourceModel\Gift::class
        );
    }
}
