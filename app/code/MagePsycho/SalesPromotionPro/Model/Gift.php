<?php

namespace MagePsycho\SalesPromotionPro\Model;

use Magento\Framework\DataObject\IdentityInterface;
use MagePsycho\SalesPromotionPro\Api\Data\GiftInterface;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Gift extends \Magento\Framework\Model\AbstractExtensibleModel implements IdentityInterface, GiftInterface
{
    const CACHE_TAG         = 'magepsycho_salespromotionpro_gift';
    protected $_eventPrefix = 'magepsycho_salespromotionpro_gift';
    protected $_eventObject = 'magepsycho_salespromotionpro_gift';
    protected $_cacheTag    = self::CACHE_TAG;

    public function _construct()
    {
        $this->_init(\MagePsycho\SalesPromotionPro\Model\ResourceModel\Gift::class);
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return parent::getData(self::GIFT_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        return $this->setData(self::GIFT_ID, $id);
    }
    /**
     * {@inheritdoc}
     */
    public function getRuleId()
    {
        return $this->getData(self::RULE_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setRuleId($ruleId)
    {
        return $this->setData(self::RULE_ID, $ruleId);
    }

    /**
     * {@inheritdoc}
     */
    public function getGiftSku()
    {
        return $this->getData(self::GIFT_SKU);
    }

    /**
     * {@inheritdoc}
     */
    public function setGiftSku($giftSku)
    {
        return $this->setData(self::GIFT_SKU, $giftSku);
    }
}
