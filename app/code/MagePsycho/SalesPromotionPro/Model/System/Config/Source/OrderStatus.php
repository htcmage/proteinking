<?php

namespace MagePsycho\SalesPromotionPro\Model\System\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Magento\Sales\Model\Order\Config as OrderConfig;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class OrderStatus implements ArrayInterface
{
    protected $_options;

    /**
     * @var OrderConfig
     */
    private $orderConfig;

    public function __construct(
        OrderConfig $orderConfig
    ) {
        $this->orderConfig = $orderConfig;
    }

    public function getAllOptions($withEmpty = false)
    {
        if ($this->_options === null) {
            $statuses = $this->orderConfig->getStatuses();
            foreach ($statuses as $code => $label) {
                $this->_options[] = ['value' => $code, 'label' => $label];
            }
        }
        $options = $this->_options;
        if ($withEmpty) {
            array_unshift($options, ['value' => '', 'label' => '']);
        }
        return $options;
    }

    public function getOptionsArray($withEmpty = true)
    {
        $options = [];
        foreach ($this->getAllOptions($withEmpty) as $option) {
            $options[$option['value']] = $option['label'];
        }
        return $options;
    }

    public function getOptionText($value)
    {
        $options = $this->getAllOptions(false);
        foreach ($options as $item) {
            if ($item['value'] == $value) {
                return $item['label'];
            }
        }
        return false;
    }

    public function toOptionArray()
    {
        return $this->getAllOptions();
    }

    public function toOptionHash($withEmpty = true)
    {
        return $this->getOptionsArray($withEmpty);
    }
}
