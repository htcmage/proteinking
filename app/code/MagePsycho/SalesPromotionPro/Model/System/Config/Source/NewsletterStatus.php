<?php

namespace MagePsycho\SalesPromotionPro\Model\System\Config\Source;

use Magento\Newsletter\Model\Subscriber;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class NewsletterStatus implements \Magento\Framework\Option\ArrayInterface
{
    protected $_options;

    public function getAllOptions($withEmpty = false)
    {
        if ($this->_options === null) {
            $this->_options = [
                [
                    'value' => Subscriber::STATUS_SUBSCRIBED,
                    'label' => __('Subscribed'),
                ],
                [
                    'value' => Subscriber::STATUS_NOT_ACTIVE,
                    'label' => __('Not Activated'),
                ],
                [
                    'value' => Subscriber::STATUS_UNSUBSCRIBED,
                    'label' => __('Unsubscribed'),
                ],
                [
                    'value' => Subscriber::STATUS_UNCONFIRMED,
                    'label' => __('Unconfirmed'),
                ],
            ];
        }
        $options = $this->_options;
        if ($withEmpty) {
            array_unshift($options, ['value' => '', 'label' => '']);
        }
        return $options;
    }

    public function getOptionsArray($withEmpty = true)
    {
        $options = [];
        foreach ($this->getAllOptions($withEmpty) as $option) {
            $options[$option['value']] = $option['label'];
        }
        return $options;
    }

    public function getOptionText($value)
    {
        $options = $this->getAllOptions(false);
        foreach ($options as $item) {
            if ($item['value'] == $value) {
                return $item['label'];
            }
        }
        return false;
    }

    public function toOptionArray()
    {
        return $this->getAllOptions();
    }

    public function toOptionHash($withEmpty = true)
    {
        return $this->getOptionsArray($withEmpty);
    }
}
