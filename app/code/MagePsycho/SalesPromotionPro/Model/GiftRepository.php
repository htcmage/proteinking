<?php

namespace MagePsycho\SalesPromotionPro\Model;

use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use MagePsycho\SalesPromotionPro\Api\Data;
use MagePsycho\SalesPromotionPro\Api\Data\GiftSearchResultsInterfaceFactory;
use MagePsycho\SalesPromotionPro\Model\ResourceModel\Gift as GiftResource;
use MagePsycho\SalesPromotionPro\Model\ResourceModel\Gift\CollectionFactory;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class GiftRepository implements \MagePsycho\SalesPromotionPro\Api\GiftRepositoryInterface
{
    /**
     * @var \MagePsycho\SalesPromotionPro\Model\GiftFactory
     */
    private $giftFactory;

    /**
     * @var GiftResource
     */
    private $giftResource;

    /**
     * @var GiftSearchResultsInterfaceFactory
     */
    private $giftSearchResultsInterfaceFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var JoinProcessorInterface
     */
    private $extensionJoinProcessor;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        GiftFactory $giftFactory,
        GiftResource $giftResource,
        GiftSearchResultsInterfaceFactory $giftSearchResultsInterfaceFactory,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionJoinProcessor,
        CollectionFactory $collectionFactory
    ) {
        $this->giftFactory = $giftFactory;
        $this->giftResource = $giftResource;
        $this->giftSearchResultsInterfaceFactory = $giftSearchResultsInterfaceFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionJoinProcessor = $extensionJoinProcessor;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getById($giftId)
    {
        $gift = $this->giftFactory->create();
        $this->giftResource->load($gift, $giftId);
        if (!$gift->getId()) {
            throw new NoSuchEntityException(__('Unable to find the record with id: %1', $giftId));
        }
        return $gift;
    }

    /**
     * {@inheritdoc}
     */
    public function save(Data\GiftInterface $gift)
    {
        try {
            $this->giftResource->save($gift);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__('Unable to save the gift: %s', $exception->getMessage()));
        }

        return $gift;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->collectionFactory->create();
        $this->extensionJoinProcessor->process($collection);
        $this->collectionProcessor->process($searchCriteria, $collection);

        $giftSearchResults = $this->giftSearchResultsInterfaceFactory->create();
        $giftSearchResults->setSearchCriteria($searchCriteria);
        $giftSearchResults->setItems($collection);
        $giftSearchResults->setTotalCount($collection->count());
        return $giftSearchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(Data\GiftInterface $gift)
    {
        try {
            $this->giftResource->delete($gift);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Cannot delete the rule: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($giftId)
    {
        try {
            $this->giftResource->delete($this->getById($giftId));
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Cannot delete the rule: %1',
                $exception->getMessage()
            ));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getByRuleId($ruleId)
    {
        $gift = $this->giftFactory->create();
        $this->giftResource->load($gift, $ruleId, 'rule_id');
        if (!$gift->getId()) {
            throw new NoSuchEntityException(__('Unable to find the record with rule id: %1', $ruleId));
        }
        return $gift;
    }

    /**
     * {@inheritdoc}
     */
    public function saveByRuleId($ruleId, $giftSku)
    {
        $gift = $this->giftFactory->create();
        $this->giftResource->load($gift, $ruleId, 'rule_id');
        if (!$gift->getId()) {
            $gift->setRuleId($ruleId);
        }
        $gift->setGiftSku($giftSku);
        return $this->save($gift);
    }
}
