<?php

namespace MagePsycho\SalesPromotionPro\Model\SalesRule\Validator;

use MagePsycho\SalesPromotionPro\Model\SalesRule\Action\Discount\FreeGiftAction;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class RemoveFreeGift implements \Zend_Validate_Interface
{
    /**
     * @inheritdoc
     */
    public function isValid($item)
    {
        return $item->getOptionByCode(FreeGiftAction::QUOTE_ITEM_UNIQUE_ID) == null;
    }

    /**
     * @inheritdoc
     */
    public function getMessages()
    {
        return [];
    }
}
