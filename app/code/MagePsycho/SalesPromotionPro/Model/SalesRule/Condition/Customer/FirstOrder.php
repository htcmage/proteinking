<?php

namespace MagePsycho\SalesPromotionPro\Model\SalesRule\Condition\Customer;

use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Rule\Model\Condition\Context;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class FirstOrder extends \Magento\Rule\Model\Condition\AbstractCondition
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var \MagePsycho\SalesPromotionPro\Helper\Data
     */
    protected $salesPromotionProHelper;

    /**
     * @var \Magento\Config\Model\Config\Source\Yesno
     */
    protected $yesNoOption;

    public function __construct(
        Context $context,
        OrderCollectionFactory $orderCollectionFactory,
        \MagePsycho\SalesPromotionPro\Helper\Data $salesPromotionProHelper,
        \Magento\Config\Model\Config\Source\Yesno $yesNoOption,
        array $data = []
    ) {
        $this->orderCollectionFactory   = $orderCollectionFactory;
        $this->salesPromotionProHelper        = $salesPromotionProHelper;
        $this->yesNoOption              = $yesNoOption;
        parent::__construct($context, $data);
    }

    public function loadAttributeOptions()
    {
        $this->setAttributeOption(
            ['first_order' => 'First Order']
        );
        return $this;
    }

    /**
     * Get input type
     * @return string
     */
    public function getInputType()
    {
        return 'select';
    }

    /**
     * Get value element type
     * @return string
     */
    public function getValueElementType()
    {
        return 'select';
    }

    /**
     * Make Condition Option to Yes/No
     * @return array|mixed
     */
    public function getValueSelectOptions()
    {
        if (!$this->hasData('value_select_options')) {
            $options = $this->yesNoOption->toOptionArray();
            $this->setData('value_select_options', $options);
        }
        return $this->getData('value_select_options');
    }

    /**
     * Validate Condition: First  valid Purchase.
     * @param \Magento\Framework\Model\AbstractModel $model
     * @return bool
     */
    public function validate(\Magento\Framework\Model\AbstractModel $model)
    {
        $this->salesPromotionProHelper->log(__METHOD__, true);
        $quote = $model;
        if ($model instanceof \Magento\Quote\Model\Quote\Address) {
            $quote = $model->getQuote();
        }

        if (!$this->salesPromotionProHelper->isActive($quote->getStoreId())) {
            return false;
        }

        $customerId = $quote->getCustomerId();

        $orderCollection = $this->orderCollectionFactory->create()
            ->addAttributeToSelect('customer_id')
            ->addAttributeToSelect('status')
            ->addAttributeToFilter('customer_id', $customerId)
            ->addAttributeToFilter(
                'status',
                ['in' => $this->salesPromotionProHelper->getConfigHelper()->getValidOrderStatuses()]
            )
            ->setPageSize(1)
            ->setCurPage(1);

        $orderCount         = $orderCollection->count();
        $isFirstPurchase    = 0;
        if ($customerId > 0 && $orderCount < 1) {
            $isFirstPurchase = 1;
        }
        $model->setData('first_order', $isFirstPurchase);
        $this->salesPromotionProHelper->log(
            '$quoteId::' . $quote->getId() . ', $customerId::' . $customerId
            . ', $orderCount::' . $orderCount . ', $isFirstPurchase::' . $isFirstPurchase
        );

        return parent::validate($model);
    }
}
