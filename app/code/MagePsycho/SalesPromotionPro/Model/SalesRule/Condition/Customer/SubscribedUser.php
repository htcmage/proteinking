<?php

namespace MagePsycho\SalesPromotionPro\Model\SalesRule\Condition\Customer;

use Magento\Rule\Model\Condition\Context;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Config\Model\Config\Source\Yesno;
use MagePsycho\SalesPromotionPro\Helper\Data as SalesPromotionProHelper;
use Magento\Newsletter\Model\Subscriber;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class SubscribedUser extends \Magento\Rule\Model\Condition\AbstractCondition
{
    /**
     * @var Yesno
     */
    private $yesNoOption;

    /**
     * @var SalesPromotionProHelper
     */
    private $salesPromotionProHelper;

    /**
     * @var SubscriberFactory
     */
    private $subscriberFactory;

    public function __construct(
        Context $context,
        Yesno $yesNoOption,
        SalesPromotionProHelper $salesPromotionProHelper,
        SubscriberFactory $subscriberFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->yesNoOption              = $yesNoOption;
        $this->salesPromotionProHelper        = $salesPromotionProHelper;
        $this->subscriberFactory        = $subscriberFactory;
    }

    /**
     * Load attribute options
     *
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $this->setAttributeOption(
            ['subscribed_user' => 'Subscriber']
        );
        return $this;
    }

    /**
     *  Get input type
     * @return string
     */
    public function getInputType()
    {
        return 'select';
    }

    /**
     * Get value element type
     *
     * @return string
     */
    public function getValueElementType()
    {
        return 'select';
    }

    public function getValueSelectOptions()
    {
        if (!$this->hasData('value_select_options')) {
            $options = $this->yesNoOption->toOptionArray();
            $this->setData('value_select_options', $options);
        }
        return $this->getData('value_select_options');
    }

    /**
     * Validate against New Customer
     * @param \Magento\Framework\Model\AbstractModel $model
     * @return bool
     */
    public function validate(\Magento\Framework\Model\AbstractModel $model)
    {
        $this->salesPromotionProHelper->log(__METHOD__, true);
        $quote = $model;
        if ($model instanceof \Magento\Quote\Model\Quote\Address) {
            $quote = $model->getQuote();
        }
        if (!$this->salesPromotionProHelper->isActive($quote->getStoreId())) {
            return false;
        }

        $email          = $quote->getCustomerEmail();
        $subscriber     = $this->subscriberFactory->create()->loadByEmail($email);
        $isSubscribed   = 0;
        if ($subscriber->getId()
            && (int) $subscriber->getSubscriberStatus() === Subscriber::STATUS_SUBSCRIBED
        ) {
            $isSubscribed = 1;
        }
        $this->salesPromotionProHelper->log(
            '$quoteId::' . $quote->getId() . ', $email::' . $email . ', $isSubscribed::' . $isSubscribed
        );
        $model->setData('subscribed_user', $isSubscribed);

        return parent::validate($model);
    }
}
