<?php

namespace MagePsycho\SalesPromotionPro\Model\SalesRule\Condition\Customer;

use Magento\Rule\Model\Condition\Context;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class NewCustomer extends \Magento\Rule\Model\Condition\AbstractCondition
{
    /**
     * @var \Magento\Config\Model\Config\Source\Yesno
     */
    protected $yesNoOption;

    /**
     * @var OrderCollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var \MagePsycho\SalesPromotionPro\Helper\Data
     */
    protected $salesPromotionProHelper;

    public function __construct(
        Context $context,
        \Magento\Config\Model\Config\Source\Yesno $yesNoOption,
        OrderCollectionFactory $orderCollectionFactory,
        \MagePsycho\SalesPromotionPro\Helper\Data $salesPromotionProHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->yesNoOption              = $yesNoOption;
        $this->orderCollectionFactory   = $orderCollectionFactory;
        $this->salesPromotionProHelper        = $salesPromotionProHelper;
    }

    /**
     * Load attribute options
     *
     * @return $this
     */
    public function loadAttributeOptions()
    {
        $this->setAttributeOption(
            ['new_customer' => 'New Customer']
        );
        return $this;
    }

    /**
     *  Get input type
     * @return string
     */
    public function getInputType()
    {
        return 'select';
    }

    /**
     * Get value element type
     *
     * @return string
     */
    public function getValueElementType()
    {
        return 'select';
    }

    public function getValueSelectOptions()
    {
        if (!$this->hasData('value_select_options')) {
            $options = $this->yesNoOption->toOptionArray();
            $this->setData('value_select_options', $options);
        }
        return $this->getData('value_select_options');
    }

    /**
     * Validate against New Customer
     * @param \Magento\Framework\Model\AbstractModel $model
     * @return bool
     */
    public function validate(\Magento\Framework\Model\AbstractModel $model)
    {
        $this->salesPromotionProHelper->log(__METHOD__, true);
        $quote = $model;
        if ($model instanceof \Magento\Quote\Model\Quote\Address) {
            $quote = $model->getQuote();
        }
        if (!$this->salesPromotionProHelper->isActive($quote->getStoreId())) {
            return false;
        }

        $customerId = $quote->getCustomerId();
        $orderCollection = $this->orderCollectionFactory->create()
            ->addAttributeToSelect('customer_id')
            ->addAttributeToFilter('customer_id', $customerId)
            ->setPageSize(1)
            ->setCurPage(1);
        $orderCount = $orderCollection->count();

        $isNewCustomer = 0;
        $this->salesPromotionProHelper->log(
            '$quoteId::' . $quote->getId() . ', $customerId::' . $customerId . ', $orderCount::' . $orderCount
        );

        if ($customerId > 0 && $orderCount < 1) {
            $isNewCustomer = 1;
        }
        $model->setData('new_customer', $isNewCustomer);

        return parent::validate($model);
    }
}
