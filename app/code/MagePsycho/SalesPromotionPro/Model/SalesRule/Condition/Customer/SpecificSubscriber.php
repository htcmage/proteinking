<?php

namespace MagePsycho\SalesPromotionPro\Model\SalesRule\Condition\Customer;

use Magento\Rule\Model\Condition\Context;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class SpecificSubscriber extends \Magento\Rule\Model\Condition\AbstractCondition
{
    /**
     * @var \Magento\Backend\Helper\Data
     */
    protected $backendData;

    /**
     * @var \MagePsycho\SalesPromotionPro\Helper\Data
     */
    protected $salesPromotionProHelper;

    public function __construct(
        Context $context,
        \Magento\Backend\Helper\Data $backendData,
        \MagePsycho\SalesPromotionPro\Helper\Data $salesPromotionProHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->backendData          = $backendData;
        $this->salesPromotionProHelper    = $salesPromotionProHelper;
    }

    public function getDefaultOperatorInputByType()
    {
        $this->_defaultOperatorInputByType = parent::getDefaultOperatorInputByType();
        if (array_key_exists('magepsycho_salespromotionpro_multiselect', $this->_defaultOperatorInputByType)) {
            return $this->_defaultOperatorInputByType;
        }
        $this->_defaultOperatorInputByType ['magepsycho_salespromotionpro_multiselect'] = ['==', '!=','()','!()'];
        return $this->_defaultOperatorInputByType;
    }

    public function loadAttributeOptions()
    {
        $this->setAttributeOption(
            ['specific_subscriber' => 'Specific Subscriber']
        );
        return $this;
    }

    /**
     * Retrieve value element chooser URL
     *
     * @return string
     */
    public function getValueElementChooserUrl()
    {
        $url = false;
        $url .= 'salespromotionpro/promo_widget/chooser/attribute/' . $this->getAttribute();
        if ($this->getJsFormObject()) {
            $url .= '/form/' . $this->getJsFormObject();
        }
        return $this->backendData->getUrl($url);
    }

    /**
     * Retrieve input type
     *
     * @return string
     */
    public function getInputType()
    {
        return 'magepsycho_salespromotionpro_multiselect';
    }

    /**
     * Retrieve value element type
     *
     * @return string
     */
    public function getValueElementType()
    {
        return 'text';
    }

    /**
     * Retrieve Explicit Apply
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getExplicitApply()
    {
        return true;
    }

    /**
     * Retrieve after element HTML
     *
     * @return string
     */
    public function getValueAfterElementHtml()
    {
        $html = '';
        $image = $this->_assetRepo->getUrl('images/rule_chooser_trigger.gif');

        if (!empty($image)) {
            $html = '<a href="javascript:void(0)" class="rule-chooser-trigger"><img src="' .
                $image .
                '" alt="" class="v-middle rule-chooser-trigger" title="' .
                __(
                    'Open Chooser'
                ) . '" /></a>';
        }
        return $html;
    }

    /**
     * Newsletters Subscriber
     * @param \Magento\Framework\Model\AbstractModel $model
     * @return integer |boolean
     */
    public function validate(\Magento\Framework\Model\AbstractModel $model)
    {
        $this->salesPromotionProHelper->log(__METHOD__, true);
        $quote = $model;
        if ($model instanceof \Magento\Quote\Model\Quote\Address) {
            $quote = $model->getQuote();
        }

        if (!$this->salesPromotionProHelper->isActive($quote->getStoreId())) {
            return false;
        }

        $customerEmail = $quote->getCustomerEmail();
        $model->setData('specific_subscriber', $customerEmail);
        $this->salesPromotionProHelper->log(
            '$quoteId::' . $quote->getId() . ', $customerEmail::' . $customerEmail
        );
        return parent::validate($model);
    }

    /**
     * Get attribute element
     *
     * @return $this
     */
    public function getAttributeElement()
    {
        $element = parent::getAttributeElement();
        $element->setShowAsText(true);
        return $element;
    }
}
