<?php

namespace MagePsycho\SalesPromotionPro\Model\SalesRule\Condition\Customer;

use Magento\Rule\Model\Condition\Context;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class TotalSalesAmount extends \Magento\Rule\Model\Condition\AbstractCondition
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var \MagePsycho\SalesPromotionPro\Helper\Data
     */
    protected $salesPromotionProHelper;

    public function __construct(
        Context $context,
        OrderCollectionFactory $orderCollectionFactory,
        \MagePsycho\SalesPromotionPro\Helper\Data $salesPromotionProHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->orderCollectionFactory   = $orderCollectionFactory;
        $this->salesPromotionProHelper        = $salesPromotionProHelper;
    }

    public function loadAttributeOptions()
    {
        $this->setAttributeOption(
            ['customer_total_sales_amount' => 'Total Sales Amount']
        );
        return $this;
    }

    /**
     * Get input type
     * @return string
     */
    public function getInputType()
    {
        return 'numeric';
    }

    /**
     * Get value element type
     * @return string
     */
    public function getValueElementType()
    {
        return 'text';
    }

    /**
     * Validate Customer total order Amount.
     * @param \Magento\Framework\Model\AbstractModel $model
     * @return bool
     */
    public function validate(\Magento\Framework\Model\AbstractModel $model)
    {
        $quote = $model;
        if ($model instanceof \Magento\Quote\Model\Quote\Address) {
            $quote = $model->getQuote();
        }

        if (!$this->salesPromotionProHelper->isActive($quote->getStoreId())) {
            return false;
        }
        $customerId = $quote->getCustomerId();
        $orderCollection = $this->orderCollectionFactory->create()
            ->addAttributeToSelect('customer_id')
            ->addAttributeToSelect('base_grand_total')
            ->addAttributeToSelect('status')
            ->addAttributeToFilter('customer_id', $customerId)
            ->addAttributeToFilter(
                'status',
                ['in' => $this->salesPromotionProHelper->getConfigHelper()->getValidOrderStatuses()]
            )
            ->addExpressionFieldToSelect(
                'customer_total_sales_amount',
                'SUM({{base_grand_total}})',
                'base_grand_total'
            );
        $orderCollection->getSelect()->group('customer_id');
        $orderCollection->setPageSize(1)->setCurPage(1);

        $totalOrderAmount = 0;
        if ($orderCollection->count() > 0) {
            $firstOrderItem = $orderCollection->getFirstItem();
            $totalOrderAmount = $firstOrderItem->getCustomerTotalSalesAmount();
        }
        $this->salesPromotionProHelper->log(
            '$quoteId::' . $quote->getId() . ', $customerId::' . $customerId .
            ', $totalOrderAmount::' . $totalOrderAmount
        );
        $model->setData('customer_total_sales_amount', $totalOrderAmount);

        return parent::validate($model);
    }
}
