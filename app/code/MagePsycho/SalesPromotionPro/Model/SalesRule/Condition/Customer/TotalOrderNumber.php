<?php

namespace MagePsycho\SalesPromotionPro\Model\SalesRule\Condition\Customer;

use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Rule\Model\Condition\Context;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class TotalOrderNumber extends \Magento\Rule\Model\Condition\AbstractCondition
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var \MagePsycho\SalesPromotionPro\Helper\Data
     */
    protected $salesPromotionProHelper;

    public function __construct(
        Context $context,
        OrderCollectionFactory $orderCollectionFactory,
        \MagePsycho\SalesPromotionPro\Helper\Data $salesPromotionProHelper,
        array $data = []
    ) {
        $this->orderCollectionFactory   = $orderCollectionFactory;
        $this->salesPromotionProHelper        = $salesPromotionProHelper;
        parent::__construct($context, $data);
    }

    public function loadAttributeOptions()
    {
        $this->setAttributeOption(
            ['customer_total_no_order' => 'Total Number of Orders']
        );
        return $this;
    }

    /**
     * Get input type
     * @return string
     */
    public function getInputType()
    {
        return 'numeric';
    }

    /**
     * Get value element type
     * @return string
     */
    public function getValueElementType()
    {
        return 'text';
    }

    /**
     * Validate Customer with N no of orders.
     * @param \Magento\Framework\Model\AbstractModel $model
     * @return bool
     */
    public function validate(\Magento\Framework\Model\AbstractModel $model)
    {
        $quote = $model;
        if ($model instanceof \Magento\Quote\Model\Quote\Address) {
            $quote = $model->getQuote();
        }
        if (!$this->salesPromotionProHelper->isActive($quote->getStoreId())) {
            return false;
        }

        $customerId = $quote->getCustomerId();
        $orderCollection = $this->orderCollectionFactory->create()
            ->addAttributeToSelect('customer_id')
            ->addAttributeToSelect('total_qty_ordered')
            ->addAttributeToSelect('status')
            ->addAttributeToFilter('customer_id', $customerId)
            ->addAttributeToFilter(
                'status',
                ['in' => $this->salesPromotionProHelper->getConfigHelper()->getValidOrderStatuses()]
            )
            ->addExpressionFieldToSelect(
                'customer_total_qty_ordered',
                'SUM({{total_qty_ordered}})',
                'total_qty_ordered'
            );
        $orderCollection->getSelect()->group('customer_id');
        $orderCollection->setPageSize(1)
            ->setCurPage(1);

        $totalQtyOrdered = 0;
        if ($orderCollection->count() > 0) {
            $orderFirstItem = $orderCollection->getFirstItem();
            $totalQtyOrdered = $orderFirstItem->getCustomerTotalQtyOrdered();
        }
        $model->setData('customer_total_no_order', $totalQtyOrdered);
        $this->salesPromotionProHelper->log(
            '$quoteId::' . $quote->getId() . ', $customerId::' . $customerId .
            ', $totalQtyOrdered::' . $totalQtyOrdered
        );
        return parent::validate($model);
    }
}
