<?php

namespace MagePsycho\SalesPromotionPro\Model\SalesRule\Action\Discount;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Quote\Model\Quote\Address;
use Magento\SalesRule\Model\Rule\Action\Discount\DiscountInterface;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class FreeGiftAction implements DiscountInterface
{
    const ACTION                = 'mp_free_gift';
    const QUOTE_ITEM_UNIQUE_ID  = 'mp_free_gift_quote_item_unique_id';
    const APPLIED_RULE_IDS      = 'mp_free_gift_applied_rule_ids';
    const SKU_DATA_KEY          = 'gift_sku';
    const QTY_DATA_KEY          = 'discount_amount';
    const PRODUCT_TYPE          = 'mp_cart_free_gift';
    const SALESRULE_NAME        = 'name';

    /**
     * @var \Magento\SalesRule\Model\Rule\Action\Discount\DataFactory
     */
    protected $dataFactory;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \MagePsycho\SalesPromotionPro\Helper\Data
     */
    protected $salesPromotionProHelper;

    /**
     * @var \Magento\Framework\DataObject\Factory
     */
    protected $objectFactory;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $serializer;

    /**
     * @var \MagePsycho\SalesPromotionPro\Api\GiftRepositoryInterface
     */
    private $giftRepository;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    private $messageManager;

    public function __construct(
        \Magento\SalesRule\Model\Rule\Action\Discount\DataFactory $dataFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\DataObject\Factory $objectFactory,
        \MagePsycho\SalesPromotionPro\Helper\Data $salesPromotionProHelper,
        \MagePsycho\SalesPromotionPro\Api\GiftRepositoryInterface $giftRepository,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        Json $serializer = null
    ) {
        $this->dataFactory          = $dataFactory;
        $this->productRepository    = $productRepository;
        $this->logger               = $logger;
        $this->salesPromotionProHelper    = $salesPromotionProHelper;
        $this->objectFactory        = $objectFactory;
        $this->serializer           = $serializer ?: ObjectManager::getInstance()->get(Json::class);
        $this->giftRepository       = $giftRepository;
        $this->messageManager       = $messageManager;
    }

    /**
     * @param \Magento\SalesRule\Model\Rule $rule
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem $item
     * @param float $qty
     * @return \Magento\SalesRule\Model\Rule\Action\Discount\Data
     */
    public function calculate($rule, $item, $qty)
    {
        $this->salesPromotionProHelper->log(__METHOD__, true);
        $appliedRuleIds = $item->getAddress()->getData(static::APPLIED_RULE_IDS);

        if (($item->getAddress()->getAddressType() != Address::ADDRESS_TYPE_SHIPPING)
            || ($appliedRuleIds != null && isset($appliedRuleIds[$rule->getId()]))
        ) {
            $this->getDiscountData($item);
        }

        $freeItemSku = $this->getGiftSku($rule->getId());
        $freeProduct = $this->loadFreeProductBySku($freeItemSku, $rule, $item->getStoreId());
        if ($freeProduct) {
            $this->addFreeGiftItemToQuote($item, $freeProduct, $rule, $freeItemSku);
        }
        return $this->getDiscountData($item);
    }

    /**
     * @param float $qty
     * @param \Magento\SalesRule\Model\Rule $rule
     * @return float
     */
    public function fixQuantity($qty, $rule)
    {
        return $qty;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem $item
     * @return \Magento\SalesRule\Model\Rule\Action\Discount\Data
     */
    protected function getDiscountData(\Magento\Quote\Model\Quote\Item\AbstractItem $item)
    {
        /** @var \Magento\SalesRule\Model\Rule\Action\Discount\Data $discountData */
        $discountData = $this->dataFactory->create();
        $discountData->setAmount($item->getDiscountAmount());
        $discountData->setBaseAmount($item->getBaseDiscountAmount());
        $discountData->setOriginalAmount($item->getOriginalDiscountAmount());
        $discountData->setBaseOriginalAmount($item->getBaseOriginalDiscountAmount());
        return $discountData;
    }

    protected function loadFreeProductBySku($freeItemSku, $rule, $storeId)
    {
        try {
            $product =  $this->productRepository->get($freeItemSku);
            $product->addCustomOption(
                \Magento\Quote\Api\Data\CartItemInterface::KEY_PRODUCT_TYPE,
                static::PRODUCT_TYPE
            );
            $product->addCustomOption(static::QUOTE_ITEM_UNIQUE_ID, uniqid());
            $customOptions = [];

            $customOptions[] = [
                'label'       => $this->getCartRuleLabel($storeId),
                'value'       => $this->getCartRuleValue($rule, $storeId),
                'print_value' => $this->getCartRuleValue($rule, $storeId)
            ];

            $product->addCustomOption('additional_options', $this->serializer->serialize($customOptions));
        } catch (NoSuchEntityException $exception) {
            return false;
        }
        return $product;
    }

    protected function addFreeGiftItemToQuote($item, $freeProduct, $rule, $freeItemSku)
    {
        $hasFreeGiftItem = $this->hasQuoteFreeItem($item->getQuote());

        if ($hasFreeGiftItem) {
            $this->addAppliedRuleIds($item, $rule);
            return $this;
        }
        try {
            $freeQuoteItem = $item->getQuote()
                ->addProduct(
                    $freeProduct,
                    (int) $rule->getData(static::QTY_DATA_KEY)
                );

            $item->getQuote()->setItemsCount($item->getQuote()->getItemsCount() + 1);
            $item->getQuote()->setItemsQty((float)$item->getQuote()->getItemsQty() + $freeQuoteItem->getQty());
            $this->addAppliedRuleIds($item, $rule);
        } catch (LocalizedException $exception) {
            $message = __("Free gift: %1 is not available.", $freeItemSku);
            $this->messageManager->addWarningMessage($message);
            $this->salesPromotionProHelper->log(__METHOD__ . '::' . $exception->getMessage(), true);
        } catch (\Exception $exception) {
            $message = sprintf(
                'Exception occurred while adding gift product %s to cart. Exception: %s',
                $freeItemSku,
                $exception->getMessage()
            );
            $this->messageManager->addWarningMessage($message);
            $this->salesPromotionProHelper->log(__METHOD__ . '::' . $message, true);
        }
    }

    protected function addAppliedRuleIds($item, $rule)
    {
        $appliedRuleIds = $item->getAddress()->getData(static::APPLIED_RULE_IDS);
        if ($appliedRuleIds == null) {
            $appliedRuleIds = [];
        }
        $appliedRuleIds[(int)$rule->getId()] = (int) $rule->getId();
        $item->getAddress()->setData(static::APPLIED_RULE_IDS, $appliedRuleIds);
    }

    protected function getCartRuleLabel($storeId)
    {
        return $this->salesPromotionProHelper
            ->getConfigHelper()
            ->getCartGiftLabel($storeId);
    }

    protected function getCartRuleValue($rule, $storeId)
    {
        $label = $rule->getStoreLabel($storeId);
        if (empty($label)) {
            $label = $this->salesPromotionProHelper
                ->getConfigHelper()
                ->getCartGiftValue($storeId);
        }
        return $label;
    }

    private function getGiftSku($ruleId)
    {
        try {
            $gift = $this->giftRepository->getByRuleId((int) $ruleId);
            if ($gift->getGiftSku() !== null && strlen($gift->getGiftSku()) > 0) {
                return $gift->getGiftSku();
            }
        } catch (NoSuchEntityException $exception) {
            return false;
        }
        return false;
    }

    private function hasQuoteFreeItem($quote)
    {
        $hasFreeGiftItem        = false;
        $quoteItemCollection    = $quote->getItemsCollection();

        foreach ($quoteItemCollection as $quoteItem) {
            if ($quoteItem->getParentItemId()) {
                continue;
            }

            if ($quoteItem->getOptionByCode(FreeGiftAction::QUOTE_ITEM_UNIQUE_ID)
                instanceof \Magento\Quote\Model\Quote\Item\Option) {
                $hasFreeGiftItem = true;
                break;
            }
        }
        return $hasFreeGiftItem;
    }
}
