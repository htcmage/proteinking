<?php

namespace MagePsycho\SalesPromotionPro\Model;

use MagePsycho\SalesPromotionPro\Model\SalesRule\Action\Discount\FreeGiftAction;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class QuoteItemValidator
{
    public function isFreeGiftItem($item)
    {
        $option = $item->getOptionByCode(\Magento\Quote\Api\Data\CartItemInterface::KEY_PRODUCT_TYPE);
        if ($option instanceof \Magento\Quote\Model\Quote\Item\Option
            && $option->getValue() == FreeGiftAction::PRODUCT_TYPE
        ) {
            return true;
        }
        return false;
    }
}
