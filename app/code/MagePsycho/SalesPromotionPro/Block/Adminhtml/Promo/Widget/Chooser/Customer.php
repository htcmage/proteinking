<?php

namespace MagePsycho\SalesPromotionPro\Block\Adminhtml\Promo\Widget\Chooser;

use MagePsycho\SalesPromotionPro\Model\ResourceModel\Customer\CollectionFactory
    as SalesPromotionProCustomerCollectionFactory;
use Magento\Directory\Model\ResourceModel\Country\CollectionFactory as DirectoryCollectionFactory;
use Magento\Backend\Block\Widget\Grid;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Customer extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var array
     */
    private $countries;

    /**
     * @var SalesPromotionProCustomerCollectionFactory
     */
    protected $customerCollectionFactory;

    /**
     * @var DirectoryCollectionFactory
     */
    protected $countryCollectionFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        SalesPromotionProCustomerCollectionFactory $customerCollectionFactory,
        DirectoryCollectionFactory $countryCollectionFactory,
        array $data = []
    ) {
        $this->customerCollectionFactory    = $customerCollectionFactory;
        $this->countryCollectionFactory     = $countryCollectionFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        if ($this->getRequest()->getParam('current_grid_id')) {
            $this->setId($this->getRequest()->getParam('current_grid_id'));
        } else {
            $this->setId('customerChooserGrid_' . $this->getId());
        }

        $form = $this->getJsFormObject();
        $this->setRowClickCallback("{$form}.chooserGridRowClick.bind({$form})");
        $this->setCheckboxCheckCallback("{$form}.chooserGridCheckboxCheck.bind({$form})");
        $this->setRowInitCallback("{$form}.chooserGridRowInit.bind({$form})");
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
        if ($this->getRequest()->getParam('collapse')) {
            $this->setIsCollapsed(true);
        }
    }

    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in customer flag
        if ($column->getId() == 'in_customers') {
            $selected = $this->_getSelectedCustomers();
            if (empty($selected)) {
                $selected = '';
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('email', ['in' => $selected]);
            } else {
                $this->getCollection()->addFieldToFilter('email', ['nin' => $selected]);
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    /**
     * Prepare Customer for attribute in Promo Conditions email chooser
     * @return Grid\Extended
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareCollection()
    {
        $collection = $this->customerCollectionFactory->create();
        $collection->addAttributeToSelect(
            'email',
            'entity_id'
        );
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Define Cooser Grid Columns and filters
     *
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_customers',
            [
                'header_css_class' => 'a-center',
                'type' => 'checkbox',
                'name' => 'in_customers',
                'values' => $this->_getSelectedCustomers(),
                'align' => 'center',
                'index' => 'entity_id',
                'use_index' => true
            ]
        );
        $this->addColumn(
            'entity_id',
            ['header' => __(' Customer ID'), 'sortable' => true, 'width' => '60px', 'index' => 'entity_id']
        );

        $this->addColumn(
            'email',
            ['header' => __('Email'), 'name' => 'email', 'width' => '80px', 'index' => 'email']
        );
        $this->addColumn(
            'name',
            ['header' => __('Name'), 'name' => 'name', 'index' => 'name']
        );
        $this->addColumn(
            'billing_telephone',
            ['header' => __('Phone'), 'name' => 'billing_telephone', 'index' => 'billing_telephone']
        );
        $this->addColumn(
            'billing_postcode',
            ['header' => __('ZIP/Post Code'), 'name' => 'billing_postcode', 'index' => 'billing_postcode']
        );

        $this->addColumn(
            'billing_country_id',
            ['header' => __('Country'), 'name' => 'billing_country_id', 'index' => 'billing_country_id']
        );
        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl(
            'salespromotionpro/*/chooser',
            ['_current' => true, 'current_grid_id' => $this->getId(), 'collapse' => null]
        );
    }

    /**
     * Returns countries array
     *
     * @return array
     */
    protected function getCountries()
    {
        if (!$this->countries) {
            $this->countries = $this->countryCollectionFactory->create()
                ->loadData()
                ->toOptionHash();
        }
        return $this->countries;
    }

    /**
     * @return mixed
     */
    protected function _getSelectedCustomers()
    {
        $products = $this->getRequest()->getPost('selected', []);
        return $products;
    }
}
