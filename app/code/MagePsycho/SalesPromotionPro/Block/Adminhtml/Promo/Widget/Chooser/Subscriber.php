<?php

namespace MagePsycho\SalesPromotionPro\Block\Adminhtml\Promo\Widget\Chooser;

use MagePsycho\SalesPromotionPro\Model\System\Config\Source\NewsletterStatus;
use MagePsycho\SalesPromotionPro\Model\System\Config\Source\SubscriberType;
use MagePsycho\SalesPromotionPro\Helper\Data as SalesPromotionProHelper;
use Magento\Newsletter\Model\ResourceModel\Subscriber\CollectionFactory as SubscriberCollectionFactory;
use Magento\Newsletter\Model\ResourceModel\Subscriber\Grid\CollectionFactory as SubscriberGridCollectionFactory;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Subscriber extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var SubscriberCollectionFactory
     */
    protected $subscriberCollectionFactory;

    /**
     * @var SalesPromotionProHelper
     */
    private $salesPromotionProHelper;

    /**
     * @var NewsletterStatus
     */
    protected $newsletterStatus;

    /**
     * @var SubscriberType
     */
    protected $newsletterCustomerType;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        SubscriberGridCollectionFactory $subscriberCollectionFactory,
        SalesPromotionProHelper $salesPromotionProHelper,
        NewsletterStatus $newsletterStatus,
        SubscriberType $newsletterCustomerType,
        array $data = []
    ) {
        $this->subscriberCollectionFactory  = $subscriberCollectionFactory;
        $this->salesPromotionProHelper            = $salesPromotionProHelper;
        $this->newsletterStatus             = $newsletterStatus;
        $this->newsletterCustomerType       = $newsletterCustomerType;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        if ($this->getRequest()->getParam('current_grid_id')) {
            $this->setId($this->getRequest()->getParam('current_grid_id'));
        } else {
            $this->setId('newsletterSubscriberChooserGrid_' . $this->getId());
        }

        $form = $this->getJsFormObject();
        $this->setRowClickCallback("{$form}.chooserGridRowClick.bind({$form})");
        $this->setCheckboxCheckCallback("{$form}.chooserGridCheckboxCheck.bind({$form})");
        $this->setRowInitCallback("{$form}.chooserGridRowInit.bind({$form})");
        $this->setDefaultSort('subscriber_id');
        $this->setDefaultDir('desc');
        $this->setUseAjax(true);
        if ($this->getRequest()->getParam('collapse')) {
            $this->setIsCollapsed(true);
        }
    }

    /**
     * prepare Grid Collection
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->subscriberCollectionFactory->create();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Define Cooser Grid Columns and filters
     *
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_customers',
            [
                'header_css_class' => 'a-center',
                'type' => 'checkbox',
                'name' => 'in_customers',
                'values' => $this->_getSelectedCustomers(),
                'align' => 'center',
                'index' => 'subscriber_email',
                'use_index' => true
            ]
        );
        $this->addColumn(
            'subscriber_id',
            ['header' => __(' ID'), 'sortable' => true, 'width' => '60px', 'index' => 'subscriber_id']
        );
        $this->addColumn(
            'subscriber_email',
            [
                'header' => __('Email'),
                'name' => 'subscriber_email',
                'width' => '80px',
                'index' => 'subscriber_email'
            ]
        );
        $this->addColumn(
            'firstname',
            [
                'header' => __('Customer First Name'),
                'name' => 'firstname',
                'index' => 'firstname'
            ]
        );
        $this->addColumn(
            'lastname',
            [
                'header' => __('Customer Last Name'),
                'name' => 'lastname',
                'index' => 'lastname'
            ]
        );
        $this->addColumn(
            'type',
            [
                'header' => __('Type'),
                'name' => 'type',
                'index' => 'type',
                'type' => 'options',
                'options' => $this->newsletterCustomerType->getOptionsArray(false)
            ]
        );
        $this->addColumn(
            'subscriber_status',
            [
                'header' => __('Status'),
                'name' => 'subscriber_status',
                'index' => 'subscriber_status',
                'type' => 'options',
                'options' => $this->newsletterStatus->getOptionsArray(false)
            ]
        );
        return parent::_prepareColumns();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl(
            'salespromotionpro/*/chooser',
            ['_current' => true, 'current_grid_id' => $this->getId(), 'collapse' => null]
        );
    }

    /**
     * @return mixed
     */
    protected function _getSelectedCustomers()
    {
        $products = $this->getRequest()->getPost('selected', []);
        return $products;
    }
}
