<?php

namespace MagePsycho\SalesPromotionPro\Block\Cart\Item\Renderer\Actions;

use Magento\Checkout\Block\Cart\Item\Renderer\Actions\Generic;
use Magento\Framework\View\Element\Template;
use MagePsycho\SalesPromotionPro\Helper\Data as SalesPromotionProHelper;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class FreeGiftIcon extends Generic
{
    /**
     * @var SalesPromotionProHelper
     */
    protected $salesPromotionProHelper;

    public function __construct(
        Template\Context $context,
        SalesPromotionProHelper $salesPromotionProHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->salesPromotionProHelper = $salesPromotionProHelper;
    }

    public function getFreeGiftIcon()
    {
        return $this->salesPromotionProHelper->getFreeGiftIcon();
    }

    public function getGiftCartIconWidth()
    {
        return $this->salesPromotionProHelper->getConfigHelper()->getGiftCartIconWidth();
    }

    public function getGiftCartIconHeight()
    {
        return  $this->salesPromotionProHelper->getConfigHelper()->getGiftCartIconHeight();
    }

    protected function _toHtml()
    {
        if (!$this->salesPromotionProHelper->getConfigHelper()->isGiftIconEnabled()) {
            return '';
        }
        return parent::_toHtml();
    }
}
