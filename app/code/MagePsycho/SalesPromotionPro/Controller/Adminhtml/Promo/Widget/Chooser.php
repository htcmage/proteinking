<?php

namespace MagePsycho\SalesPromotionPro\Controller\Adminhtml\Promo\Widget;

use Magento\CatalogRule\Controller\Adminhtml\Promo\Widget as PromoWidget;
use MagePsycho\SalesPromotionPro\Block\Adminhtml\Promo\Widget\Chooser\Customer as CustomerChooserWidget;
use MagePsycho\SalesPromotionPro\Block\Adminhtml\Promo\Widget\Chooser\Subscriber as SubscriberChooserWidget;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Amit Bera <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Chooser extends PromoWidget
{
    const ADMIN_RESOURCE = 'MagePsycho_SalesPromotionPro::customer_list';

    public function execute()
    {
        $request = $this->getRequest();

        if ($request->getParam('attribute') == 'specific_customer') {
            $html = $this->_view->getLayout()->createBlock(
                CustomerChooserWidget::class,
                'promo_widget_chooser_salespromotionpro_customer',
                ['data' => ['js_form_object' => $request->getParam('form')]]
            )->toHtml();
        } elseif ($request->getParam('attribute') == 'specific_subscriber') {
            $html = $this->_view->getLayout()->createBlock(
                SubscriberChooserWidget::class,
                'promo_widget_chooser_salespromotionpro_specific_subscriber',
                ['data' => ['js_form_object' => $request->getParam('form')]]
            )->toHtml();
        } else {
            $html = false;
        }

        $this->getResponse()->setBody($html);
    }
}
