<?php

namespace MagePsycho\SalesPromotionPro\Plugin\SalesRule\Metadata;

use MagePsycho\SalesPromotionPro\Helper\Data as SalesPromotionProHelper;
use MagePsycho\SalesPromotionPro\Model\SalesRule\Action\Discount\FreeGiftAction;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class AdminhtmlValueProviderPlugin
{
    /**
     * @var SalesPromotionProHelper
     */
    private $salesPromotionProHelper;

    public function __construct(
        SalesPromotionProHelper $salesPromotionProHelper
    ) {
        $this->salesPromotionProHelper    = $salesPromotionProHelper;
    }

    public function afterGetMetadataValues(
        \Magento\SalesRule\Model\Rule\Metadata\ValueProvider $subject,
        $result
    ) {
        if ($this->salesPromotionProHelper->isFxnSkipped()) {
            return $result;
        }

        $result['actions']['children']['simple_action']['arguments']['data']['config']['options'][] = [
            'label' => __('Free Gift (Discount Amount is Qty)'),
            'value' =>  FreeGiftAction::ACTION
        ];
        $options = $result['actions']['children']['simple_action']['arguments']['data']['config']['options'];

        $rules = [];
        foreach ($options as $option) {
            $value = $option['value'];
            $rules[] = [
                'value'     => $value,
                'actions'   => $this->getFreeGiftFieldsSwitcherConfig($value)
            ];
        }

        $result['actions']['children']['simple_action']['arguments']['data']['config']['switcherConfig'] = [
            'enabled'   => true,
            'rules'     => $rules
        ];
        return $result;
    }

    protected function getFreeGiftFieldsSwitcherConfig($value)
    {
        $switcherConfig = [];
        $switcherConfig[] =  [
            'target' => 'sales_rule_form.sales_rule_form.actions.'. FreeGiftAction::SKU_DATA_KEY,
            'callback' =>  $value == FreeGiftAction::ACTION ? 'show' : 'hide'
        ];
        return $switcherConfig;
    }
}
