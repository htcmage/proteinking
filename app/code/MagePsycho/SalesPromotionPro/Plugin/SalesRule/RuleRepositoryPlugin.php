<?php

namespace MagePsycho\SalesPromotionPro\Plugin\SalesRule;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\SalesRule\Api\Data\RuleInterface;
use Magento\SalesRule\Api\RuleRepositoryInterface;
use MagePsycho\SalesPromotionPro\Api\Data\GiftInterface;
use MagePsycho\SalesPromotionPro\Api\GiftRepositoryInterface;
use MagePsycho\SalesPromotionPro\Api\RuleManagementInterface;
use MagePsycho\SalesPromotionPro\Helper\Data as SalesPromotionProHelper;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class RuleRepositoryPlugin
{
    /**
     * @var SalesPromotionProHelper
     */
    private $salesPromotionProHelper;

    /**
     * @var RuleManagementInterface
     */
    private $ruleManagement;

    /**
     * @var GiftInterface
     */
    private $gift;

    /**
     * @var GiftRepositoryInterface
     */
    private $giftRepository;

    public function __construct(
        SalesPromotionProHelper $salesPromotionProHelper,
        GiftInterface $gift,
        GiftRepositoryInterface $giftRepository,
        RuleManagementInterface $ruleManagement
    ) {
        $this->salesPromotionProHelper    = $salesPromotionProHelper;
        $this->gift                 = $gift;
        $this->giftRepository       = $giftRepository;
        $this->ruleManagement       = $ruleManagement;
    }

    /**
     * Add gift_sku to rules data
     *
     * @param RuleRepositoryInterface $subject
     * @param RuleInterface $rule
     * @return RuleInterface
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetById(
        RuleRepositoryInterface $subject,
        RuleInterface $rule
    ) {
        if ($this->salesPromotionProHelper->isFxnSkipped()) {
            return $rule;
        }
        $this->ruleManagement->addGiftSkuToRule($rule);
        return $rule;
    }

    /**
     * Save gift Sku
     *
     * @param RuleRepositoryInterface $subject
     * @param RuleInterface $rule
     * @return RuleInterface
     * @throws CouldNotSaveException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterSave(
        RuleRepositoryInterface $subject,
        RuleInterface $rule
    ) {
        if ($this->salesPromotionProHelper->isFxnSkipped()) {
            return $rule;
        }
        $this->ruleManagement->saveGiftSku($rule);
        return $rule;
    }
}
