<?php

namespace MagePsycho\SalesPromotionPro\Plugin\SalesRule\Model\Converter;

use Magento\SalesRule\Api\Data\RuleInterface;
use Magento\SalesRule\Model\Converter\ToDataModel;
use MagePsycho\SalesPromotionPro\Helper\Data as SalesPromotionProHelper;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class ToDataModelPlugin
{
    /**
     * @var SalesPromotionProHelper
     */
    private $salesPromotionProHelper;

    /**
     * @var \MagePsycho\SalesPromotionPro\Api\RuleManagementInterface
     */
    private $ruleManagement;

    /**
     * @var \Magento\SalesRule\Api\Data\RuleExtensionFactory
     */
    private $extensionFactory;

    public function __construct(
        \Magento\SalesRule\Api\Data\RuleExtensionFactory $extensionFactory,
        \MagePsycho\SalesPromotionPro\Api\RuleManagementInterface $ruleManagement,
        SalesPromotionProHelper $salesPromotionProHelper
    ) {
        $this->extensionFactory     = $extensionFactory;
        $this->ruleManagement       = $ruleManagement;
        $this->salesPromotionProHelper    = $salesPromotionProHelper;
    }

    public function afterToDataModel(
        ToDataModel $subject,
        RuleInterface $result
    ) {
        if ($this->salesPromotionProHelper->isFxnSkipped()) {
            return $result;
        }

        $this->ruleManagement->addGiftSkuToRule($result);
        return $result;
    }
}
