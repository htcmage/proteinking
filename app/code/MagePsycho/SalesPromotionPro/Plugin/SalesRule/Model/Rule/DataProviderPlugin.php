<?php

namespace MagePsycho\SalesPromotionPro\Plugin\SalesRule\Model\Rule;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\SalesRule\Model\ResourceModel\Rule\CollectionFactory;
use Magento\SalesRule\Model\Rule\DataProvider;
use MagePsycho\SalesPromotionPro\Helper\Data as SalesPromotionProHelper;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class DataProviderPlugin
{
    /**
     * @var \MagePsycho\SalesPromotionPro\Api\GiftRepositoryInterface
     */
    private $giftRepository;

    /**
     * @var array
     */
    protected $loadedData;

   /**
    * @var Collection
    */
    protected $collection;

    /**
     * @var SalesPromotionProHelper
     */
    private $salesPromotionProHelper;

    /**
     * @var \MagePsycho\SalesPromotionPro\Api\RuleManagementInterface
     */

    public function __construct(
        SalesPromotionProHelper $salesPromotionProHelper,
        CollectionFactory $collectionFactory,
        \MagePsycho\SalesPromotionPro\Api\GiftRepositoryInterface $giftRepository
    ) {
        $this->salesPromotionProHelper    = $salesPromotionProHelper;
        $this->collection           = $collectionFactory->create();
        $this->giftRepository       = $giftRepository;
    }

    /**
     * @param \Magento\SalesRule\Model\Rule\DataProvider $subject
     * @param \Closure $proceed
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function aroundGetData(
        DataProvider $subject,
        \Closure $proceed
    ) {
        if ($this->salesPromotionProHelper->isFxnSkipped()) {
            return $proceed();
        }
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();
        /** @var Rule $rule */
        foreach ($items as $rule) {
            $rule->load($rule->getId());
            $rule->setDiscountAmount($rule->getDiscountAmount() * 1);
            $rule->setDiscountQty($rule->getDiscountQty() * 1);

            $this->loadedData[$rule->getId()] = $rule->getData();
            if ($giftSku = $this->getGiftSku($rule->getId())) {
                $this->loadedData[$rule->getId()]['gift_sku'] = $giftSku;
            }
        }

        return $this->loadedData;
    }

    /**
     * @param int $ruleId
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getGiftSku($ruleId)
    {
        try {
            $gift = $this->giftRepository->getByRuleId($ruleId);
        } catch (NoSuchEntityException $e) {
            return false;
        }
        return $gift->getGiftSku();
    }
}
