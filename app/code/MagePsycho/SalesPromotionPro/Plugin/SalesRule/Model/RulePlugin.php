<?php

namespace MagePsycho\SalesPromotionPro\Plugin\SalesRule\Model;

use MagePsycho\SalesPromotionPro\Api\Data\GiftInterface;
use MagePsycho\SalesPromotionPro\Api\GiftRepositoryInterface;
use MagePsycho\SalesPromotionPro\Helper\Data as SalesPromotionProHelper;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class RulePlugin
{
    /**
     * @var GiftRepositoryInterface
     */
    private $giftRepository;

    /**
     * @var SalesPromotionProHelper
     */
    private $salesPromotionProHelper;

    public function __construct(
        GiftRepositoryInterface $giftRepository,
        SalesPromotionProHelper $salesPromotionProHelper
    ) {
        $this->giftRepository       = $giftRepository;
        $this->salesPromotionProHelper    = $salesPromotionProHelper;
    }

    public function afterAfterSave(
        \Magento\SalesRule\Model\Rule $subject
    ) {
        if ($this->salesPromotionProHelper->isFxnSkipped()) {
            return $this;
        }

        $data = $subject->getData();
        if ($subject->getId()
            && !empty($data)
            && array_key_exists(GiftInterface::GIFT_SKU, $data)
        ) {
            $this->giftRepository
                ->saveByRuleId(
                    $subject->getRuleId(),
                    $data[GiftInterface::GIFT_SKU]
                )
            ;
        }
    }
}
