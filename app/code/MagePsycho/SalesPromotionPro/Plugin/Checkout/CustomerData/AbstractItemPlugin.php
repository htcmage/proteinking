<?php

namespace MagePsycho\SalesPromotionPro\Plugin\Checkout\CustomerData;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class AbstractItemPlugin
{
    /**
     * @var \MagePsycho\SalesPromotionPro\Model\QuoteItemValidator
     */
    protected $quoteItemValidator;

    /**
     * @var \MagePsycho\SalesPromotionPro\Helper\Data
     */
    private $salesPromotionProHelper;

    public function __construct(
        \MagePsycho\SalesPromotionPro\Model\QuoteItemValidator $quoteItemValidator,
        \MagePsycho\SalesPromotionPro\Helper\Data $salesPromotionProHelper
    ) {
        $this->quoteItemValidator = $quoteItemValidator;
        $this->salesPromotionProHelper  = $salesPromotionProHelper;
    }

    /**
     *  Remove Cart Item edit button from miniCart
     * @param \Magento\Checkout\CustomerData\AbstractItem $subject
     * @param \Closure $proceed
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return mixed
     */
    public function aroundGetItemData(
        \Magento\Checkout\CustomerData\AbstractItem $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote\Item $item
    ) {
        $result = $proceed($item);
        if ($this->salesPromotionProHelper->isFxnSkipped()
            || !$this->quoteItemValidator->isFreeGiftItem($item)
        ) {
            return $result;
        }

        $configHelper = $this->salesPromotionProHelper->getConfigHelper();

        $result['is_visible_in_site_visibility'] = false;
        $result['free_gift_icon']                = $this->salesPromotionProHelper->getFreeGiftIcon();
        $result['free_gift_icon_width']          = $configHelper->getGiftMiniCartIconWidth();
        $result['free_gift_icon_height']         = $configHelper->getGiftMiniCartIconHeight();
        $result['icon_enabled']                  = (bool) $configHelper->isGiftIconEnabled();
        return $result;
    }
}
