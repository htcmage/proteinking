<?php

namespace MagePsycho\SalesPromotionPro\Plugin\Checkout\Block\Cart\Item;

use MagePsycho\SalesPromotionPro\Block\Cart\Item\Renderer\Actions\FreeGiftIcon;
use MagePsycho\SalesPromotionPro\Model\QuoteItemValidator;
use MagePsycho\SalesPromotionPro\Helper\Data as SalesPromotionProHelper;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class RendererPlugin
{
    /**
     * @var SalesPromotionProHelper
     */
    private $salesPromotionProHelper;

    /**
     * @var QuoteItemValidator
     */
    protected $quoteItemValidator;

    public function __construct(
        SalesPromotionProHelper $salesPromotionProHelper,
        QuoteItemValidator $quoteItemValidator
    ) {
        $this->salesPromotionProHelper = $salesPromotionProHelper;
        $this->quoteItemValidator = $quoteItemValidator;
    }

    public function aroundGetActions(
        \Magento\Checkout\Block\Cart\Item\Renderer $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote\Item\AbstractItem $item
    ) {
        if ($this->salesPromotionProHelper->isFxnSkipped()
            || !$this->quoteItemValidator->isFreeGiftItem($item)
        ) {
            return $proceed($item);
        }

        $block = $subject->getLayout()
            ->createBlock(
                FreeGiftIcon::class,
                'checkout.cart.item.renderers.mp.freegift.icon'
            )->setItem($item)
            ->setTemplate('MagePsycho_SalesPromotionPro::cart/item/renderer/actions/gift-icon.phtml')
        ;
        return $block->toHtml();
    }
}
