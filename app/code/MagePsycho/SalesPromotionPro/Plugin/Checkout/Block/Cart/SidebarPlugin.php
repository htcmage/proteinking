<?php

namespace MagePsycho\SalesPromotionPro\Plugin\Checkout\Block\Cart;

use MagePsycho\SalesPromotionPro\Helper\Data as SalesPromotionProHelper;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class SidebarPlugin
{
    /**
     * @var SalesPromotionProHelper
     */
    private $salesPromotionProHelper;

    public function __construct(
        SalesPromotionProHelper $salesPromotionProHelper
    ) {
        $this->salesPromotionProHelper  = $salesPromotionProHelper;
    }

    public function afterGetJsLayout(
        \Magento\Checkout\Block\Cart\Sidebar $subject,
        $jsLayout
    ) {
        if ($this->salesPromotionProHelper->isFxnSkipped()) {
            return $jsLayout;
        }
        $jsLayoutArray = json_decode($jsLayout, true);
        if (isset($jsLayoutArray['components']['minicart_content']['children']
            ['item.renderer']['config']['template'])
        ) {
            $jsLayoutArray['components']['minicart_content']['children']['item.renderer']['config']['template'] =
                'MagePsycho_SalesPromotionPro/minicart/item/default';
            $jsLayoutArray['components']['minicart_content']['children']['item.renderer']['config']['is_enabled'] =
                true;
        }
        return json_encode($jsLayoutArray);
    }
}
