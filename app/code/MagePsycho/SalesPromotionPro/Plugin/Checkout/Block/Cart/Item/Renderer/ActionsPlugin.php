<?php

namespace MagePsycho\SalesPromotionPro\Plugin\Checkout\Block\Cart\Item\Renderer;

use MagePsycho\SalesPromotionPro\Helper\Data as SalesPromotionProHelper;
use MagePsycho\SalesPromotionPro\Model\SalesRule\Action\Discount\FreeGiftAction;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class ActionsPlugin
{
    /**
     * @var SalesPromotionProHelper
     */
    private $salesPromotionProHelper;

    public function __construct(
        SalesPromotionProHelper $salesPromotionProHelper
    ) {
        $this->salesPromotionProHelper  = $salesPromotionProHelper;
    }

    public function afterToHtml(
        \Magento\Checkout\Block\Cart\Item\Renderer\Actions $subject,
        $result
    ) {
        if ($this->salesPromotionProHelper->isFxnSkipped()) {
            return $result;
        }

        $item = $subject->getItem();
        $option = $item->getOptionByCode(\Magento\Quote\Api\Data\CartItemInterface::KEY_PRODUCT_TYPE);

        if ($option instanceof \Magento\Quote\Model\Quote\Item\Option
            && $option->getValue() == FreeGiftAction::PRODUCT_TYPE
        ) {
            return '';
        }
        return $result;
    }
}
