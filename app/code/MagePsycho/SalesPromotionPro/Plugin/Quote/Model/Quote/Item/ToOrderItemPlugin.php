<?php

namespace MagePsycho\SalesPromotionPro\Plugin\Quote\Model\Quote\Item;

use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\App\ObjectManager;
use MagePsycho\SalesPromotionPro\Helper\Data as SalesPromotionProHelper;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class ToOrderItemPlugin
{
    /**
     * @var SalesPromotionProHelper
     */
    private $salesPromotionProHelper;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $serializer;

    public function __construct(
        SalesPromotionProHelper $salesPromotionProHelper,
        Json $serializer = null
    ) {
        $this->salesPromotionProHelper    = $salesPromotionProHelper;
        $this->serializer = $serializer ?: ObjectManager::getInstance()->get(Json::class);
    }

    public function aroundConvert(
        \Magento\Quote\Model\Quote\Item\ToOrderItem $subject,
        \Closure $proceed,
        $item,
        $data = []
    ) {
        $orderItem = $proceed($item, $data);
        if ($this->salesPromotionProHelper->isFxnSkipped()) {
            return $orderItem;
        }

        $additionalOptions = $item->getOptionByCode('additional_options');
        if ($additionalOptions instanceof \Magento\Quote\Model\Quote\Item\Option) {
            $options = $orderItem->getProductOptions();
            $options['additional_options'] = $this->serializer->unserialize($additionalOptions->getValue());
            $orderItem->setProductOptions($options);
        }
        return $orderItem;
    }
}
