<?php

namespace MagePsycho\SalesPromotionPro\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use MagePsycho\SalesPromotionPro\Helper\Data as SalesPromotionProHelper;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @var SalesPromotionProHelper
     */
    private $salesPromotionProHelper;

    public function __construct(
        SalesPromotionProHelper $salesPromotionProHelper
    ) {
        $this->salesPromotionProHelper = $salesPromotionProHelper;
    }

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        // @todo use db_schema.xml approach for 2.3.0+
        /*if (version_compare($this->salesPromotionProHelper->getMagentoVersion(), '2.3.0', '>=')) {
            return $this;
        }*/
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('magepsycho_salespromotionpro_gift')) {
            $table = $installer->getConnection()
                ->newTable(
                    $installer->getTable('magepsycho_salespromotionpro_gift')
                )
                ->addColumn(
                    'gift_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'rule_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                    'Rule Id'
                )
                ->addColumn(
                    'gift_sku',
                    Table::TYPE_TEXT,
                    null,
                    [],
                    'Gift SKU'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_DATETIME,
                    null,
                    [],
                    'Created At'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_DATETIME,
                    null,
                    [],
                    'Updated At'
                )
                ->addIndex(
                    $installer->getIdxName(
                        'magepsycho_salespromotionpro_gift',
                        ['rule_id']
                    ),
                    ['rule_id']
                )
                ->addForeignKey(
                    $installer->getFkName(
                        'magepsycho_salespromotionpro_gift',
                        'rule_id',
                        'salesrule',
                        'rule_id'
                    ),
                    'rule_id',
                    $installer->getTable('salesrule'),
                    'rule_id',
                    Table::ACTION_CASCADE
                )
            ;
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}
