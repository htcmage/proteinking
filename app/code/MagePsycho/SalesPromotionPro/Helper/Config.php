<?php

namespace MagePsycho\SalesPromotionPro\Helper;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Config
{
    /* General Settings */
    const XML_PATH_ENABLED          = 'magepsycho_salespromotionpro/general/enabled';
    const XML_PATH_DEBUG            = 'magepsycho_salespromotionpro/general/debug';

    /* Gift Settings */
    const XML_PATH_GIFT_ICON_ENABLED            = 'magepsycho_salespromotionpro/gift/icon_enabled';
    const XML_PATH_GIFT_ICON                    = 'magepsycho_salespromotionpro/gift/icon';
    const XML_PATH_GIFT_CART_ICON_WIDTH         = 'magepsycho_salespromotionpro/gift/cart_icon_width';
    const XML_PATH_GIFT_CART_ICON_HEIGHT        = 'magepsycho_salespromotionpro/gift/cart_icon_height';
    const XML_PATH_GIFT_MINICART_ICON_WIDTH     = 'magepsycho_salespromotionpro/gift/minicart_icon_width';
    const XML_PATH_GIFT_MINICART_ICON_HEIGHT    = 'magepsycho_salespromotionpro/gift/minicart_icon_height';

    /* Order Settings */
    const XML_PATH_ORDER_VALID_STATUS     = 'magepsycho_salespromotionpro/order/valid_status';
    const XML_PATH_ORDER_NEW_STATUS       = 'magepsycho_salespromotionpro/order/new_status';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    public function getConfigValue($xmlPath, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $xmlPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    public function isEnabled($storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_ENABLED, $storeId);
    }

    public function isActive($storeId = null)
    {
        return $this->isEnabled($storeId);
    }

    public function getDebugStatus($storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_DEBUG, $storeId);
    }

    public function getValidOrderStatuses($storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_ORDER_VALID_STATUS, $storeId);
    }

    public function getNewOrderStatuses($storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_ORDER_NEW_STATUS, $storeId);
    }

    public function isGiftIconEnabled($storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_GIFT_ICON_ENABLED, $storeId);
    }

    public function getFreeGiftIcon($storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_GIFT_ICON, $storeId);
    }

    public function getGiftCartIconWidth($storeId = null)
    {
        return $this->getConfigValue(static::XML_PATH_GIFT_CART_ICON_WIDTH, $storeId);
    }

    public function getGiftCartIconHeight($storeId = null)
    {
        return $this->getConfigValue(static::XML_PATH_GIFT_CART_ICON_HEIGHT, $storeId);
    }

    public function getGiftMiniCartIconWidth($storeId = null)
    {
        return $this->getConfigValue(static::XML_PATH_GIFT_MINICART_ICON_WIDTH, $storeId);
    }

    public function getGiftMiniCartIconHeight($storeId = null)
    {
        return $this->getConfigValue(static::XML_PATH_GIFT_MINICART_ICON_HEIGHT, $storeId);
    }

    public function getCartGiftLabel($storeId = null)
    {
        // @todo make it configurable
        return __('Gift');
    }

    public function getCartGiftValue($storeId = null)
    {
        // @todo make it configurable
        return __('FREE');
    }
}
