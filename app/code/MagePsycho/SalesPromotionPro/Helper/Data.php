<?php

namespace MagePsycho\SalesPromotionPro\Helper;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const FREE_GIFT_ICON_DIRECTORY_PATH = 'magepsycho/salespromotionpro/gift_icon';

    /**
     * @var \MagePsycho\SalesPromotionPro\Logger\Logger
     */
    protected $moduleLogger;

    /**
     * @var \MagePsycho\SalesPromotionPro\Helper\Config
     */
    protected $configHelper;

    /**
     * @var Url
     */
    protected $urlHelper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Module\ModuleListInterface
     */
    protected $moduleList;
    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $assetRepo;
    /**
     * @var \Magento\Cms\Model\PageFactory
     */
    protected $pageFactory;

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    private $productMetaData;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \MagePsycho\SalesPromotionPro\Logger\Logger $moduleLogger,
        \MagePsycho\SalesPromotionPro\Helper\Config $configHelper,
        Url $urlHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Cms\Model\PageFactory $pageFactory,
        \Magento\Framework\View\Asset\Repository $assetRepo,
        \Magento\Framework\App\ProductMetadataInterface $productMetaData
    ) {
        $this->moduleLogger     = $moduleLogger;
        $this->configHelper     = $configHelper;
        $this->urlHelper        = $urlHelper;
        $this->storeManager     = $storeManager;
        $this->moduleList       = $moduleList;
        $this->assetRepo        = $assetRepo;
        $this->pageFactory      = $pageFactory;
        $this->productMetaData  = $productMetaData;

        parent::__construct($context);
        $this->_initialize();
    }

    protected function _initialize()
    {
        $field = base64_decode('ZG9tYWluX3R5cGU=');
        if ($this->configHelper->getConfigValue('magepsycho_salespromotionpro/general/' . $field) == 1) {
            $key        = base64_decode('cHJvZF9saWNlbnNl');
            $this->mode = base64_decode('cHJvZHVjdGlvbg==');
        } else {
            $key        = base64_decode('ZGV2X2xpY2Vuc2U=');
            $this->mode = base64_decode('ZGV2ZWxvcG1lbnQ=');
        }
        $this->temp = $this->configHelper->getConfigValue('magepsycho_salespromotionpro/general/' . $key);
    }

    public function getMessage()
    {
        $message = base64_decode(
            'WW91IGFyZSB1c2luZyB1bmxpY2Vuc2VkIHZlcnNpb24gb2YgJ1NhbGVzIFByb21vdGlvbiBQcm8nIEV4dGVuc2lvbiBmb3IgZG9tYWluOiB7e0RPTUFJTn19LiBQbGVhc2UgZW50ZXIgYSB2YWxpZCBMaWNlbnNlIEtleSBmcm9tIFN5c3RlbSAmcmFxdW87IENvbmZpZ3VyYXRpb24gJnJhcXVvOyBNYWdlUHN5Y2hvIEV4dGVuc2lvbnMgJnJhcXVvOyBTYWxlcyBQcm9tb3Rpb24gUHJvICZyYXF1bzsgTGljZW5zZSBLZXkuIElmIHlvdSBkb24ndCBoYXZlIG9uZSwgcGxlYXNlIHB1cmNoYXNlIGEgdmFsaWQgbGljZW5zZSBmcm9tIDxhIGhyZWY9Imh0dHBzOi8vd3d3Lm1hZ2Vwc3ljaG8uY29tL2NvbnRhY3QiIHRhcmdldD0iX2JsYW5rIj53d3cubWFnZXBzeWNoby5jb208L2E+IG9yIHlvdSBjYW4gZGlyZWN0bHkgZW1haWwgdG8gPGEgaHJlZj0ibWFpbHRvOm1hZ2Vwc3ljaG9AZ21haWwuY29tIj5tYWdlcHN5Y2hvQGdtYWlsLmNvbTwvYT4='
        );
        $message = str_replace('{{DOMAIN}}', $this->getDomain(), $message);

        return $message;
    }

    public function getDomain()
    {
        $domain     = $this->_urlBuilder->getBaseUrl();
        $baseDomain = $this->urlHelper->getBaseDomain($domain);

        return $baseDomain;
    }

    public function checkEntry($domain, $serial)
    {
        $salt = sha1(base64_decode('bTItc2FsZXNwcm9tb3Rpb25wcm8='));
        if (sha1($salt . $domain . $this->mode) == $serial) {
            return true;
        }

        return false;
    }

    public function isValid()
    {
        return $this->checkEntry($this->getDomain(), $this->temp);
    }

    public function isFxnSkipped()
    {
        if (($this->configHelper->isActive() && !$this->isValid()) || !$this->configHelper->isActive()) {
            return true;
        }
        return false;
    }

    public function getDomainFromSystemConfig()
    {
        $websiteCode = $this->_getRequest()->getParam('website');
        $storeCode   = $this->_getRequest()->getParam('store');
        $xmlPath     = 'web/unsecure/base_url';
        if (!empty($storeCode)) {
            $domain = $this->scopeConfig->getValue(
                $xmlPath,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $storeCode
            );
        } elseif (!empty($websiteCode)) {
            $domain = $this->scopeConfig->getValue(
                $xmlPath,
                \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITE,
                $websiteCode
            );
        } else {
            $domain = $this->scopeConfig->getValue(
                $xmlPath
            );
        }
        return $domain;
    }

    public function getConfigHelper()
    {
        return $this->configHelper;
    }

    public function getExtensionVersion()
    {
        $moduleCode = 'MagePsycho_SalesPromotionPro';
        $moduleInfo = $this->moduleList->getOne($moduleCode);
        return $moduleInfo['setup_version'];
    }

    public function getMagentoVersion()
    {
        return $this->productMetaData->getVersion();
    }

    public function getBaseUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl(
            \Magento\Framework\UrlInterface::URL_TYPE_WEB,
            true
        );
    }

    public function isActive($storeId = null)
    {
        return $this->configHelper->isActive($storeId);
    }

    /**
     * Logging Utility
     *
     * @param $message
     * @param bool|false $useSeparator
     */
    public function log($message, $useSeparator = false)
    {
        if ($this->configHelper->isActive()
            && $this->configHelper->getDebugStatus()
        ) {
            if ($useSeparator) {
                $this->moduleLogger->customLog(str_repeat('=', 100));
            }

            $this->moduleLogger->customLog($message);
        }
    }

    public function getFreeGiftIcon($storeId = null)
    {
        $freeGiftIcon = $this->configHelper->getFreeGiftIcon($storeId);

        if (empty($freeGiftIcon)) {
            return $this->assetRepo
                ->getUrl('MagePsycho_SalesPromotionPro::images/gift_icon/default.jpg');
        }

        return $this->_urlBuilder->getBaseUrl(['_type' => \Magento\Framework\UrlInterface::URL_TYPE_MEDIA])
            . static::FREE_GIFT_ICON_DIRECTORY_PATH
            . '/'
            . $freeGiftIcon;
    }
}
