<?php

namespace MagePsycho\SalesPromotionPro\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
interface GiftSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get gift Items
     *
     * @return GiftInterface[]
     */
    public function getItems();

    /**
     *  Set gift Items
     *
     * @param GiftInterface[] $items
     * @return SearchResultsInterface
     */
    public function setItems(array $items);
}
