<?php

namespace MagePsycho\SalesPromotionPro\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
interface GiftInterface extends ExtensibleDataInterface
{
    const GIFT_ID      = 'gift_id';
    const RULE_ID      = 'rule_id';
    const GIFT_SKU     = 'gift_sku';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set ID
     *
     * @param int $id
     * @return GiftInterface
     */
    public function setId($id);

    /**
     * Get rule id
     *
     * @return int|null
     */
    public function getRuleId();

    /**
     * Set rule id
     *
     * @param int $ruleId
     * @return GiftInterface
     */
    public function setRuleId($ruleId);

    /**
     * Get Gift Sku
     *
     * @return string|null
     */
    public function getGiftSku();

    /**
     * Set gift_sku
     *
     * @param string $giftSku
     * @return GiftInterface
     */
    public function setGiftSku($giftSku);
}
