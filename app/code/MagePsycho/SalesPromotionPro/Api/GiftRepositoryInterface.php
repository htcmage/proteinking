<?php

namespace MagePsycho\SalesPromotionPro\Api;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
interface GiftRepositoryInterface
{
    /**
     *  Retrieve gift data
     *
     * @param int $giftId
     * @return Data\GiftInterface
     * @throws LocalizedException
     */
    public function getById($giftId);

    /**
     * Save free gift
     *
     * @param Data\GiftInterface $gift
     * @return Data\GiftInterface
     * @throws LocalizedException
     */
    public function save(Data\GiftInterface $gift);

    /**
     * get List of Free gift
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return Data\GiftSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * @param Data\GiftInterface $gift
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(Data\GiftInterface $gift);

    /**
     * Delete by Primary key gift_id
     *
     * @param int $giftId
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($giftId);

    /**
     *  Retrieve gift data By Rule Id
     *
     * @param int $ruleId
     * @return Data\GiftInterface
     * @throws LocalizedException
     */
    public function getByRuleId($ruleId);

    /**
     * @param int $ruleId
     * @param string $giftSku
     * @return Data\GiftInterface
     * @throws CouldNotSaveException
     * @throws LocalizedException
     */
    public function saveByRuleId($ruleId, $giftSku);
}
