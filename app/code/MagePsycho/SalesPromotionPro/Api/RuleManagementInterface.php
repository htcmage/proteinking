<?php

namespace MagePsycho\SalesPromotionPro\Api;

use Magento\SalesRule\Api\Data\RuleInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
interface RuleManagementInterface
{
    /**
     * Add gift_sku extension attribute to rule
     *
     * @param RuleInterface $rule
     * @return void
     */
    public function addGiftSkuToRule(RuleInterface $rule);

    /**
     * @param RuleInterface $rule
     * @return RuleInterface|Data\GiftInterface
     * @throws CouldNotSaveException
     * @throws LocalizedException
     */
    public function saveGiftSku(RuleInterface $rule);
}
