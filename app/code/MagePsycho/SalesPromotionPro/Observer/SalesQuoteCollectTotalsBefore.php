<?php

namespace MagePsycho\SalesPromotionPro\Observer;

use MagePsycho\SalesPromotionPro\Model\SalesRule\Action\Discount\FreeGiftAction;
use MagePsycho\SalesPromotionPro\Helper\Data as SalesPromotionProHelper;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class SalesQuoteCollectTotalsBefore implements ObserverInterface
{
    /**
     * @var bool
     */
    private $markGiftItemsReset = false;

    /**
     * @var SalesPromotionProHelper
     */
    private $salesPromotionProHelper;

    public function __construct(
        SalesPromotionProHelper $salesPromotionProHelper
    ) {
        $this->salesPromotionProHelper = $salesPromotionProHelper;
    }

    public function execute(Observer $observer)
    {
        if ($this->salesPromotionProHelper->isFxnSkipped()) {
            return $this;
        }
        $this->salesPromotionProHelper->log(__METHOD__, true);

        /** @var Quote $quote */
        $quote = $observer->getEvent()->getData('quote');
        /** @var ShippingAssignmentInterface $shippingAssignment */
        $shippingAssignment = $observer->getEvent()->getData('shipping_assignment');

        if ($quote->getItems() == null || $this->markGiftItemsReset) {
            return $this;
        }

        if ($shippingAssignment instanceof ShippingAssignmentInterface) {
            /** @var Quote\Address $address */
            $address = $shippingAssignment->getShipping()->getAddress();

            if ($address->getAddressType() != Quote\Address::ADDRESS_TYPE_SHIPPING) {
                return $this;
            }
        } else {
            $address = $quote->getShippingAddress();
        }

        $realQuoteItems = $this->resetQuoteGiftItems($quote->getItemsCollection());
        $this->markGiftItemsReset = true;
        $address->unsetData(FreeGiftAction::APPLIED_RULE_IDS);
        $address->unsetData('cached_items_all');

        if ($shippingAssignment instanceof ShippingAssignmentInterface) {
            $shippingAssignment->setItems($realQuoteItems);
            $this->updateExtensionAttributes($quote, $shippingAssignment);
        }
    }

    public function markGiftItemsAdded()
    {
        $this->markGiftItemsReset = false;
    }

    /**
     * @param \Magento\Quote\Model\ResourceModel\Quote\Item\Collection $quoteItemsCollection
     * @return Quote\Item[]
     * @throws \Exception
     */
    protected function resetQuoteGiftItems($quoteItemsCollection)
    {
        $realQuoteItems = [];

        /** @var Quote\Item $quoteItem */
        foreach ($quoteItemsCollection->getItems() as $key => $quoteItem) {
            if ($quoteItem->isDeleted()) {
                continue;
            } elseif ($quoteItem->getOptionByCode(FreeGiftAction::QUOTE_ITEM_UNIQUE_ID)
                instanceof Quote\Item\Option) {
                $quoteItem->isDeleted(true);

                foreach ($quoteItem->getOptions() as $option) {
                    $option->isDeleted(true);
                }
            } else {
                $realQuoteItems[$key] = $quoteItem;
            }
        }

        return $realQuoteItems;
    }

    protected function updateExtensionAttributes(Quote $quote, ShippingAssignmentInterface $shippingAssignment)
    {
        if ($quote->getExtensionAttributes() != null) {
            $shippingAssignmentsExtension = $quote->getExtensionAttributes()->getShippingAssignments();

            if ($shippingAssignmentsExtension != null) {
                $shippingAssignmentsExtension[0] = $shippingAssignment;
            }
        }
    }
}
