<?php

// namespace C4B\FreeProduct\Observer;
namespace MagePsycho\SalesPromotionPro\Observer;

use MagePsycho\SalesPromotionPro\Model\SalesRule\Action\Discount\FreeGiftAction;
use MagePsycho\SalesPromotionPro\Helper\Data as SalesPromotionProHelper;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\Quote;
use Magento\Checkout\Model\Session as CheckoutSession;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class SalesQuoteRemoveItem implements ObserverInterface
{
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var SalesPromotionProHelper
     */
    private $salesPromotionProHelper;

    public function __construct(
        SalesPromotionProHelper $salesPromotionProHelper,
        CheckoutSession $checkoutSession
    ) {
        $this->salesPromotionProHelper  = $salesPromotionProHelper;
        $this->checkoutSession          = $checkoutSession;
    }

    public function execute(Observer $observer)
    {
        if ($this->salesPromotionProHelper->isFxnSkipped()) {
            return $this;
        }
        $this->salesPromotionProHelper->log(__METHOD__, true);

        /** @var \Magento\Quote\Model\Quote  */
        $quote = $this->checkoutSession->getQuote();

        /** @var Quote\Item $quoteItem */
        if ($quote && count($quote->getItems())) {
            foreach ($quote->getItems() as $quoteItem) {
                if ($quoteItem->getOptionByCode(FreeGiftAction::APPLIED_RULE_IDS)
                    instanceof Quote\Item\Option) {
                    $quoteItem->isDeleted(true);
                    foreach ($quoteItem->getOptions() as $option) {
                        $option->isDeleted(true);
                    }
                }
            }
        }
    }
}
