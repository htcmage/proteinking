<?php

namespace MagePsycho\SalesPromotionPro\Observer\Adminhtml;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

use MagePsycho\SalesPromotionPro\Helper\Data as SalesPromotionProHelper;
use MagePsycho\SalesPromotionPro\Model\SalesRule\Condition\Customer\NewCustomer;
use MagePsycho\SalesPromotionPro\Model\SalesRule\Condition\Customer\SubscribedUser;
use MagePsycho\SalesPromotionPro\Model\SalesRule\Condition\Customer\TotalSalesAmount;
use MagePsycho\SalesPromotionPro\Model\SalesRule\Condition\Customer\TotalOrderNumber;
use MagePsycho\SalesPromotionPro\Model\SalesRule\Condition\Customer\SpecificCustomer;
use MagePsycho\SalesPromotionPro\Model\SalesRule\Condition\Customer\SpecificSubscriber;
use MagePsycho\SalesPromotionPro\Model\SalesRule\Condition\Customer\FirstOrder;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class SalesruleRuleConditionCombine implements ObserverInterface
{
    /**
     * @var SalesPromotionProHelper
     */
    protected $salesPromotionProHelper;

    public function __construct(
        SalesPromotionProHelper $salesPromotionProHelper
    ) {
        $this->salesPromotionProHelper = $salesPromotionProHelper;
    }

    public function execute(Observer $observer)
    {
        if ($this->salesPromotionProHelper->isFxnSkipped()) {
            return $this;
        }
        $this->salesPromotionProHelper->log(__METHOD__, true);

        $additional = $observer->getAdditional();
        $conditions = (array)$additional->getConditions();

        if (!is_array($conditions)) {
            $conditions = [];
        }

        $conditions = array_merge_recursive(
            $conditions,
            [
                [
                    'label' => __('Custom Attribute | Sales Promotion Pro'),
                    'value' => [
                        $this->getNewCustomerCondition(),
                        $this->getSpecificCustomerCondition(),
                        $this->getSubscribedUserCondition(),
                        $this->getSpecificSubscriberCondition(),
                        $this->getFirstPurchaseCondition(),
                        $this->getTotalNumberOfOrdersCondition(),
                        $this->getTotalSalesAmountCondition(),
                    ]
                ]
            ]
        );
        $additional->setConditions($conditions);
        return $this;
    }

    protected function getNewCustomerCondition()
    {
        return [
            'label' => __('New Customer'),
            'value' => NewCustomer::class
        ];
    }

    protected function getSubscribedUserCondition()
    {
        return [
            'label' => __('Subscriber'),
            'value' => SubscribedUser::class
        ];
    }

    protected function getTotalSalesAmountCondition()
    {
        return [
            'label' => __('Total Sales Amount'),
            'value' => TotalSalesAmount::class
        ];
    }

    protected function getTotalNumberOfOrdersCondition()
    {
        return [
            'label' => __('Total Number of Orders'),
            'value' => TotalOrderNumber::class
        ];
    }

    protected function getSpecificCustomerCondition()
    {
        return [
            'label' => __('Specific Customer'),
            'value' => SpecificCustomer::class
        ];
    }

    protected function getSpecificSubscriberCondition()
    {
        return [
            'label' => __('Specific Subscriber'),
            'value' => SpecificSubscriber::class
        ];
    }

    protected function getFirstPurchaseCondition()
    {
        return [
            'label' => __('First Order'),
            'value' => FirstOrder::class
        ];
    }
}
