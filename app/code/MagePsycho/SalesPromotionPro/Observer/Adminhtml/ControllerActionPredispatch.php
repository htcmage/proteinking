<?php

namespace MagePsycho\SalesPromotionPro\Observer\Adminhtml;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * @category   MagePsycho
 * @package    MagePsycho_SalesPromotionPro
 * @author     Raj KB <magepsycho@gmail.com>
 * @website    https://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class ControllerActionPredispatch implements ObserverInterface
{
    /**
     * @var \MagePsycho\SalesPromotionPro\Helper\Data
     */
    protected $salesPromotionProHelper;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    public function __construct(
        \MagePsycho\SalesPromotionPro\Helper\Data $salesPromotionProHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->salesPromotionProHelper    = $salesPromotionProHelper;
        $this->messageManager       = $messageManager;
    }

    public function execute(Observer $observer)
    {
        $isValid          = $this->salesPromotionProHelper->isValid();
        $isActive         = $this->salesPromotionProHelper->isActive();
        $request          = $observer->getRequest();
        $fullActionName   = $request->getFullActionName();
        if ($isActive
            && !$isValid
            && 'adminhtml_system_config_edit' == $fullActionName
            && 'magepsycho_salespromotionpro' == $request->getParam('section')
        ) {
            $this->messageManager->addComplexErrorMessage(
                'mpSalesPromotionProComplexMessage',
                [
                    'message' => $this->salesPromotionProHelper->getMessage()
                ]
            );
        }
        return $this;
    }
}
