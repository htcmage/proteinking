define([
    'underscore',
    'uiRegistry',
    'Magento_Ui/js/form/element/abstract',
    'Magento_Ui/js/lib/view/utils/async'
], function (_, uiRegistry, AbstractField) {
    'use strict';
    return AbstractField.extend({
        defaults: {
            simple_action: 'mp_free_gift',
            imports: {
                update: '${ $.parentName }.simple_action:value'
            }
        },
        update: function (currentSimpleAction) {
            if (currentSimpleAction == this.simple_action) {
                uiRegistry.get(this.parentName + '.' + 'discount_amount', function (input) {
                    input.validation['validate-greater-than-zero'] = true;
                    input.required(true);
                });
            } else {
                uiRegistry.get(this.parentName + '.' + 'discount_amount', function (input) {
                    input.validation['validate-greater-than-zero'] = false;
                });
            }
            return this;
        }
    })
});