<?php

namespace Smartwave\Porto\Plugin;

use Magento\Review\Block\Product\ReviewRenderer;

class ChangeRatingSummaryTemplate
{
    public function afterSetTemplate(ReviewRenderer $subject, ReviewRenderer $result)
    {
        $coreTemplates = [
            'Magento_Review::helper/summary.phtml',
        ];
        $currentTemplate = $subject->getTemplate();
        if (in_array($currentTemplate, $coreTemplates)) {
            $currentTemplate = str_replace('Magento_Review', 'Smartwave_Porto', $currentTemplate);
            $subject->setTemplate($currentTemplate);
        }
        return $result;
    }
}
