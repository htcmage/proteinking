<?php

namespace Magebees\Onepagecheckout\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;


class InstallSchema implements InstallSchemaInterface
{
    
    private $blockFactory;
    public function __construct(\Magento\Cms\Model\BlockFactory $blockFactory)
    {
        $this->blockFactory = $blockFactory;
    }
    
    public function install(SchemaSetupInterface $setup,ModuleContextInterface $context) 
	{
        $installer = $setup;
        $installer->startSetup();
		
        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'newsletter_subscribe',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'unsigned' => true,
                'nullable' => false,
                'default' => '0',
                'comment' => 'Newsletter Subscribe'
            ]
        );
		
		 $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'delivery_date',
            [
                'type' => 'datetime',
                'nullable' => false,
                'comment' => 'Delivery Date',
            ]
        );

        /*$installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'delivery_comment',
            [
                'type' => 'text',
                'nullable' => false,
                'comment' => 'Delivery Comment',
            ]
        );*/

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'delivery_date',
            [
                'type' => 'datetime',
                'nullable' => false,
                'comment' => 'Delivery Date',
            ]
        );

        /*$installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'delivery_comment',
            [
                'type' => 'text',
                'nullable' => false,
                'comment' => 'Delivery Comment',
            ]
        );*/

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_grid'),
            'delivery_date',
            [
                'type' => 'datetime',
                'nullable' => false,
                'comment' => 'Delivery Date',
            ]
        );
        
        $table = $installer->getConnection()
				->newTable($installer->getTable('cws_magebees_ocp_successcustom'))
				->addColumn(
					'entity_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					null,
					['identity' => true,'unsigned' => true, 'nullable' => false, 'primary' => true],
					'Entity id'
				)
				->addColumn('oscsection', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, ['nullable' => false])
				->addColumn('oscfieldname', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, ['nullable' => false])
				->addColumn('oscfieldvalue', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 100, ['nullable' => false]);
		$installer->getConnection()->createTable($table);
        
        $cmsBlock1 = $this->blockFactory->create();
        $OpcBlocl1 = $cmsBlock1->load('opc_cms_block_1','identifier');

        if (!$OpcBlocl1->getId()) {
            $OpcBlocl1 = [
                'title' => 'opc_cms_block_1',
                'identifier' => 'opc_cms_block_1',
                'stores' => [0],
                'content' => "opc_cms_block_1",
                'is_active' => 1,
            ];
            //$this->blockFactory->create()->setData($OpcBlocl1)->save();
            $cmsBlock1->setData($OpcBlocl1)->save();
        }
        $cmsBlock2 = $this->blockFactory->create();
        $OpcBlocl2 = $cmsBlock2->load('opc_cms_block_2','identifier');
        if (!$OpcBlocl2->getId()) {
            $OpcBlocl2 = [
                'title' => 'opc_cms_block_2',
                'identifier' => 'opc_cms_block_2',
                'stores' => [0],
                'content' => "opc_cms_block_2",
                'is_active' => 1,
            ];
            //$this->blockFactory->create()->setData($OpcBlocl2)->save();
            $cmsBlock2->setData($OpcBlocl2)->save();
        }
        $cmsBlock3 = $this->blockFactory->create();
        $OpcBlocl3 = $cmsBlock3->load('opc_cms_block_3','identifier');
        if (!$OpcBlocl3->getId()) {
            $OpcBlocl3 = [
                'title' => 'opc_cms_block_3',
                'identifier' => 'opc_cms_block_3',
                'stores' => [0],
                'content' => "opc_cms_block_3",
                'is_active' => 1,
            ];
            $cmsBlock3->setData($OpcBlocl3)->save();
        }
        $cmsBlock4 = $this->blockFactory->create();
        $OpcBlocl4 = $cmsBlock4->load('opc_cms_block_4','identifier');
        if (!$OpcBlocl4->getId()) {
            $OpcBlocl4 = [
                'title' => 'opc_cms_block_4',
                'identifier' => 'opc_cms_block_4',
                'stores' => [0],
                'content' => "opc_cms_block_4",
                'is_active' => 1,
            ];
            $cmsBlock4->setData($OpcBlocl4)->save();
        }
        
        $installer->endSetup();
    }
}
