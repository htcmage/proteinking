<?php
namespace Magebees\Onepagecheckout\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magebees\Onepagecheckout\Model\SuccesscustomFactory;

class InstallData implements InstallDataInterface
{
    
    protected $_postFactory;   
    public function __construct(SuccesscustomFactory $postFactory)
    {
        $this->_postFactory = $postFactory;

    }
    
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $successData =  [
                        [1, 'mockTop', 'block1', 'CMS Block #1'],
                        [2, 'mockLeft', 'thankUnote', 'Thank You Note'],
                        [3, 'mockLeft', 'quickReg', 'Quick Registration'],
                        [4, 'mockRight', 'orderDetails', 'Order Details'],
                        [5, 'mockRight', 'shipingPayment', 'Shipping & Payment Info'],
                        [6, 'mockRight', 'additionalInfo', 'Additional Info'],
                        [7, 'mockBottom', 'block2', 'CMS Block #2'],
                        [8, 'mockAll', 'block3', 'CMS Block #3'],
                        [9, 'mockAll', 'block4', 'CMS Block #4']
                    ];
                        
        foreach ($successData as $cdata) {
            $data = [
                'oscsection' => $cdata[1],
                'oscfieldname' => $cdata[2],
                'oscfieldvalue' => $cdata[3]
            ];
            $post = $this->_postFactory->create();
            $post->addData($data)->save();
        }
    }
}
