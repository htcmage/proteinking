<?php
namespace Magebees\Onepagecheckout\Block;

class Thankyou extends \Magento\Sales\Block\Order\Totals
{
    protected $checkoutSession;
    protected $customerSession;
    protected $_orderFactory;
    protected $imageHelper;
    protected $productRepository;
    protected $productFactory;
    
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        parent::__construct($context, $registry, $data);
        $this->checkoutSession = $checkoutSession;
        $this->customerSession = $customerSession;
        $this->_orderFactory = $orderFactory;
        $this->imageHelper = $imageHelper;
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
    }

    public function getOrder()
    {
        return  $this->_order = $this->_orderFactory->create()->loadByIncrementId(
            $this->checkoutSession->getLastRealOrderId());
    }

    public function getCustomerId()
    {
        return $this->customerSession->getCustomer()->getId();
    }
	
	public function getRealOrderId()
	{
    	$lastorderId = $this->checkoutSession->getLastOrderId();
    	return $lastorderId;
	}

    public function getProductImage($sku) 
    {
        $product = $this->loadProduct($sku);
        if($product->getImage() != "no_selection" && $product->getImage() != ""){
            $imagePath = $this->imageHelper->init($product, 'small_image')
                ->setImageFile($product->getImage())
                ->resize(100)
                ->getUrl();
        }else{
            $imagePath =  $this->imageHelper->getDefaultPlaceholderUrl('image'); 
        }           
        return $imagePath;
    }
    public function loadProduct($sku) {
        return $this->productRepository->get($sku);
    }

     public function getBundleOptionsHtml($pid)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $_product = $objectManager->create('\Magento\Catalog\Model\Product')->load($pid);
    }

    /*
    public function getBundleProductOptionsData($pid)
    {
        $product = $this->productFactory->create()->load($pid);
        //get all the selection products used in bundle product.
        $selectionCollection = $product->getTypeInstance(true)->getSelectionsCollection($product->getTypeInstance(true)->getOptionsIds($product),$product);

        foreach ($selectionCollection as $proselection) {
            $selectionArray = [];
            $selectionArray['selection_product_name'] = $proselection->getName();
            $selectionArray['selection_product_quantity'] = $proselection->getPrice();
            $selectionArray['selection_product_price'] = $proselection->getSelectionQty();
            $selectionArray['selection_product_id'] = $proselection->getProductId();
            $productsArray[$proselection->getOptionId()][$proselection->getSelectionId()] = $selectionArray;
        }

        //get all options of product
        $optionsCollection = $product->getTypeInstance(true)
            ->getOptionsCollection($product);

        foreach ($optionsCollection as $options) {
            echo "<pre>";
            print_r($options->getData()); die;
            $optionArray[$options->getOptionId()]['option_title'] = $options->getDefaultTitle();
            $optionArray[$options->getOptionId()]['option_type'] = $options->getType();
        }
        return $optionArray;
    }
    */
}