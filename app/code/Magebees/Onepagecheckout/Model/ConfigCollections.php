<?php
namespace Magebees\Onepagecheckout\Model;
class ConfigCollections implements \Magento\Checkout\Model\ConfigProviderInterface
{
    protected $_configHelper;
    protected $_magebeesHelper;
    public function __construct(
		\Magebees\Onepagecheckout\Helper\Configurations $configHelper,
		\Magebees\Onepagecheckout\Helper\Data $magebeesHelper,
		\Magento\Framework\ObjectManagerInterface $objectmanager
	
    ) {
        $this->_configHelper = $configHelper;
        $this->_magebeesHelper = $magebeesHelper;
        $this->objectmanager  = $objectmanager;
    }
	
    public function getConfig()
    {
		$configData['shipping_address'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/general/shipping_address');
		$configData['product_image_enabled'] = (boolean) $this->_configHelper->getConfig('magebees_Onepagecheckout/general/product_image_enabled');
		$configData['product_image_width'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/general/product_image_width');
		$configData['product_image_height'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/general/product_image_height');

		$configData['country_id'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/general/country_id');

		$configData['region_id'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/general/region_id');

		$configData['default_shipping_method'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/general/default_shipping_method');
		$configData['default_payment_method'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/general/default_payment_method');
		$configData['enable_terms'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/osp_terms_conditions/enable_terms');
		$configData['term_title'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/osp_terms_conditions/term_title');
		//$configData['term_html'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/osp_terms_conditions/term_html');

		$configData['term_html'] = $this->objectmanager->get('\Magento\Cms\Model\Template\FilterProvider')->getBlockFilter()->filter($this->_configHelper->getConfig('magebees_Onepagecheckout/osp_terms_conditions/term_html'));

		$configData['term_checkboxtitle'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/osp_terms_conditions/term_checkboxtitle');
		$configData['enable_comment'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/general/add_comment');
		$configData['show_discount'] = (boolean) $this->_configHelper->getConfig('magebees_Onepagecheckout/general/show_discount');
		$configData['suggest_address'] = (boolean) $this->_configHelper->getConfig('magebees_Onepagecheckout/general/suggest_address');
        $configData['google_api_key'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/general/google_api_key');
		$configData['enable_delivery_date'] = (boolean) $this->_configHelper->getConfig('magebees_Onepagecheckout/opc_deliverydate/enable_delivery_date');
        $configData['deldate_label'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/opc_deliverydate/deldate_label');
        $configData['deldate_available_from'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/opc_deliverydate/deldate_available_from');
        $configData['deldate_available_to'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/opc_deliverydate/deldate_available_to');
		$configData['disabled'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/opc_deliverydate/disabled');
        $configData['hourMin'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/opc_deliverydate/hourMin');
        $configData['hourMax'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/opc_deliverydate/hourMax');
        $configData['format'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/opc_deliverydate/format');
		$configData['enable_vat_validator'] = (boolean) $this->_configHelper->getConfig('magebees_Onepagecheckout/vat_validator/enable_vat_validator');
		$configData['checkout_page_layout'] = (boolean) $this->_configHelper->getConfig('magebees_Onepagecheckout/general/checkout_page_layout');

		$configData['tsEnabled'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/trust_seals/enabled');
		$configData['tsLabel'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/trust_seals/label');
		$configData['tsText'] = $this->_configHelper->getConfig('magebees_Onepagecheckout/trust_seals/text');
		$bvalue = $this->_configHelper->getConfig('magebees_Onepagecheckout/trust_seals/badges');
		//$configData['tsBadges'] = $bvalue ? unserialize($bvalue) : [];

		$configData['tsBadges'] = $bvalue ? $this->_magebeesHelper->mbunserialize($bvalue) : [];

		$noday = 0;
        if($configData['disabled'] == -1) {
            $noday = 1;
        }
		$configData['noday'] = $noday;

		$configData['telephoneval'] = $this->_configHelper->getConfig('customer/address/telephone_show');
		
        return $configData;
    }    
}
