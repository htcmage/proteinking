<?php

namespace Magebees\Onepagecheckout\Model;

class Successcustom extends \Magento\Framework\Model\AbstractModel
{
	
	protected function _construct()
	{
		$this->_init('Magebees\Onepagecheckout\Model\ResourceModel\Successcustom');
	}
	
}
