<?php
namespace Magebees\Onepagecheckout\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface;
class OnestepcheckoutObserver implements ObserverInterface
{
	/* protected $fieldconfigFactory; */
	protected $scopeConfig;
	public function __construct(
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		/* \Magebees\Onepagecheckout\Model\FieldconfigFactory $fieldconfigFactory, */
		\Magento\Framework\App\RequestInterface $request,
		\Magento\Framework\App\Config\ConfigResource\ConfigInterface $configFactory

		) {

		$this->scopeConfig = $scopeConfig;
		$this->_storeManager = $storeManager;
		/* $this->fieldconfigFactory = $fieldconfigFactory; */
		$this->_request = $request;
  		$this->_configFactory = $configFactory;
	}
	 
    public function execute(\Magento\Framework\Event\Observer $observer)
	{
		return true;
	}
}