<?php

namespace Magebees\Onepagecheckout\Controller\Index;

class Successpreview extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
    protected $registry;
	protected $checkoutSession;
	protected $_configHelper;
	protected $orderHelper;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magebees\Onepagecheckout\Helper\Order $orderHelper,
		\Magebees\Onepagecheckout\Helper\Configurations $configHelper
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->registry = $registry;
        $this->checkoutSession = $checkoutSession;
        $this->orderHelper = $orderHelper;
		$this->_configHelper = $configHelper;
    }
   
    public function execute()
    {
		// Fetch the order
        $order = $this->getOrder();

        // Fail when there is no valid order
        if (!$order->getEntityId()) {
            throw new \Magebees\Onepagecheckout\Exception\InvalidOrderId('Invalid order ID');
        }

        // Register this order
        $this->registerOrder($order);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->addHandle('successpreview_index_index');

        return $resultPage;
    }

    //Method to fetch the current order
    //protected function getOrder() : \Magento\Sales\Api\Data\OrderInterface
    protected function getOrder()
    {
        //$orderIdFromUrl = (int)$this->getRequest()->getParam('order_id');
        //$order = $this->orderHelper->getOrderById($orderIdFromUrl);
        //if ($order->getEntityId()) {
            //return $order;
        //}

        $orderIdFromConfig = $this->_configHelper->getConfig('magebees_Onepagecheckout/opc_successpage/magebeesorder_id');
        //$order = $this->orderHelper->getOrderById($orderIdFromConfig);
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId($orderIdFromConfig);

        if ($order->getEntityId()) {
            return $order;
        }

        $lastOrderId = $this->orderHelper->getLastInsertedOrderId();
        $order = $this->orderHelper->getOrderById($lastOrderId);

        if ($order->getEntityId()) {
            return $order;
        }

        return $this->orderHelper->getEmptyOrder();
    }

    //Method to register the order in this session
	
    protected function registerOrder(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        // Register this order as the current order
        $currentOrder = $this->registry->registry('current_order');
        if (empty($currentOrder)) {
            $this->registry->register('current_order', $order);
        }

        // Load the session with this order
        $this->checkoutSession->setLastOrderId($order->getEntityId())->setLastRealOrderId($order->getIncrementId());

        // Optionally dispatch an event
        //$this->dispatchEvents($order);
    }

}