<?php
namespace Magebees\Onepagecheckout\Controller\Index;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\AccountManagement;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Controller\Account\ForgotPasswordPost;
use Magento\Framework\Controller\ResultFactory;

class ForgotPassword extends ForgotPasswordPost
{

    protected $customerAccountManagement;
    protected $escaper;
    protected $session;
    public function __construct(
        Context $context,
        Session $customerSession,
        AccountManagementInterface $customerAccountManagement,
        Escaper $escaper
    ) {
        parent::__construct(
            $context,
            $customerSession,
            $customerAccountManagement,
            $escaper
        );
    }

	public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
		$data = $this->getRequest()->getParams();
		
        if (!empty($data)) {
            $email = $data['email'];
			$responseData = array();
            $responseData['error'] = false;

            if (!\Zend_Validate::is($email, 'EmailAddress')) {
                $this->session->setForgottenEmail($email);
                $responseData['message'] = __('Please correct the email address.');
            }

            try {
                $this->customerAccountManagement->initiatePasswordReset(
                    $email,
                    AccountManagement::EMAIL_RESET
                );
            } catch (NoSuchEntityException $e) {
            } catch (\Exception $exception) {
                $responseData['message'] = __('We\'re unable to send the password reset email.');
            }
            $this->messageManager->addSuccessMessage($this->getSuccessMessage($email));
            $responseData['message'] = $this->getSuccessMessage($email);
        } else {
            $responseData['message'] = __('Please enter your email.');
        }
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($responseData);
		
		$result = "";
		$result = "<div class='message message-success success'><div data-ui-id='messages-message-success'><b style='font-size:12px'>".$responseData['message']."</b></div></div>";
		
		$this->getResponse()->representJson($this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result));
       // return $resultJson;
    }

    protected function getSuccessMessage($email)
    {
        return __(
            'If there is an account associated with %1 you will receive an email with a link to reset your password.',
            $this->escaper->escapeHtml($email)
        );
    }
}