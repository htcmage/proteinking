define(
    [
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/shipping-rate-registry',
        'Magento_Checkout/js/model/shipping-rate-processor/new-address'
    ],
    function (quote, rateRegistry, defaultProcessor) {
        'use strict';

        return function () {
            var address = quote.shippingAddress();
            //rateRegistry.set(address.getCacheKey(),'');   //Modified File 14-02-2018
            rateRegistry.set(address.getKey(), null);
            rateRegistry.set(address.getCacheKey(), null);
            quote.shippingAddress(address);
            //defaultProcessor.getRates(address);           //Modified File 14-02-2018
        };
    }
);
