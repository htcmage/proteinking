/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define*/
define(
    [
        'Magebees_Onepagecheckout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/quote'
    ],
    function (Component, quote) {
        "use strict";
        return Component.extend({
            defaults: {
                template: 'Magebees_Onepagecheckout/summary/subtotal'
            },
            getPureValue: function() {
                var totals = quote.getTotals()();
				// console.log("------------");
				// console.log(totals);
                if (totals) {
                    // return totals.subtotal;
                    return totals.subtotal_incl_tax;
                }
				
                // return quote.subtotal;
                return quote.grand_total;
            },
            getValue: function () {
                return this.getFormattedPrice(this.getPureValue());
            }

        });
    }
);
