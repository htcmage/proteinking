/*global define,alert*/
define(
    [
        'ko',
        'jquery',
        'mage/storage',
        'mage/translate',
    ],
    function (
        ko,
        $,
        storage,
        $t
    ) {
        'use strict';
        return function (vatnumber) {
            
            if($.trim(vatnumber) == ""){
                $("#vatErr").removeClass("error-msg");
                $("#vatErr").html("");
                return;
            }

            return storage.post(
                'onepage/index/checkvatnumber',
                JSON.stringify(vatnumber),
                false
            ).done(
                function (response) {
                    if (response) {
						//alert("Done: "+response);
						//$("#co-shipping-form input[name=vat_id]").after( "<p>"+response+"</p>" );
                        $("#vatErr").addClass("error-msg");
						$("#vatErr").html(response);
                    }
                }
            ).fail(
                function (response) {
					//alert("fail: "+response);
					//$("#co-shipping-form input[name=vat_id]").after( "<p>"+response+"</p>" );
                    $("#vatErr").addClass("error-msg");
					$("#vatErr").html(response);
                }
            );
        };
    }
);