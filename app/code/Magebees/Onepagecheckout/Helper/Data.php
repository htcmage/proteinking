<?php

namespace Magebees\Onepagecheckout\Helper;
use Magento\Store\Model\ScopeInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_storeManager;
	protected $scopeConfig;
	protected $_customerSession;
	protected $serialize;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Serialize\Serializer\Serialize $serialize,
		\Magento\Customer\Model\Session $customerSession
		/* \Magebees\Onepagecheckout\Model\FieldconfigFactory $fieldconfigFactory    */
    ) {
		$this->_storeManager = $storeManager;
		$this->serialize = $serialize;
		$this->_customerSession = $customerSession;
		/* $this->fieldconfigFactory = $fieldconfigFactory; */
        parent::__construct($context);
	}
	
	public function isLoggedIn()
    {
        return $this->_customerSession->isLoggedIn();
    }
    public function mbserialize($data){
		return $this->serialize->serialize($data);
	}
	public function mbunserialize($data){
		return $this->serialize->unserialize($data);
	}
	
}