<?php
 namespace Magebees\Productfeed\Controller\Adminhtml\Templates;
 
class Validate extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
     /**
      * @var string
      */
    protected $_customerEntityTypeId;
 
    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Cache\FrontendInterface $attributeLabelCache
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
    }
 
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $response = new \Magento\Framework\DataObject();
        if ($data['type']=="xml") {
            $xml_content = $data['xml']['xml_header']."<".$data['xml']['feed_xml_item'].">".$data['xml']['xml_body']."</".$data['xml']['feed_xml_item'].">".$data['xml']['xml_footer'];
            
            
            libxml_use_internal_errors(true);
            $foo = @simplexml_load_string($xml_content);
            if ($foo === false) {
                $response->setError(true);
                $errors = [];
                foreach (libxml_get_errors() as $error) {
                    $response->setMessage(__($error->message));
                }
                libxml_clear_errors();
                return $this->resultJsonFactory->create()->setJsonData($response->toJson());
            } else {
                $response->setError(false);
                return $this->resultJsonFactory->create()->setJsonData($response->toJson());
            }
        } else {
            $response = new \Magento\Framework\DataObject();
            $response->setError(false);
            return $this->resultJsonFactory->create()->setJsonData($response->toJson());
        }
    }
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magebees_Productfeed::templates');
    }
}
