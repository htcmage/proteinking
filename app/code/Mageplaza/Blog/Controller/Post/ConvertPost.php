<?php
/***************************************************************************
Extension Name  : Magento2 Shop By Brand Extension
Extension URL   : http://www.magebees.com/magento2-shop-by-brand-extension.html
Copyright    : Copyright (c) 2016 MageBees, http://www.magebees.com
Support Email   : support@magebees.com
 ***************************************************************************/
?>
<?php
namespace Mageplaza\Blog\Controller\Post;
use Magento\Framework\Exception\LocalizedException;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Mageplaza\Blog\Model\PostFactory;
use Mageplaza\Blog\Helper\Image;
use Magento\Backend\Helper\Js;

class ConvertPost extends \Magento\Framework\App\Action\Action {

    protected $topicFactory;
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    public $coreRegistry;

    /**
     * Post Factory
     *
     * @var \Mageplaza\Blog\Model\PostFactory
     */
    public $postFactory;

    /**
     * JS helper
     *
     * @var \Magento\Backend\Helper\Js
     */
    public $jsHelper;

    public function __construct(
        Context $context,
        PostFactory $postFactory,
        Registry $coreRegistry,
        Js $jsHelper,
        \Mageplaza\Blog\Model\TopicFactory $topicFactory
    )
    {
        $this->postFactory = $postFactory;
        $this->coreRegistry = $coreRegistry;
        $this->topicFactory = $topicFactory;
        $this->jsHelper = $jsHelper;
        parent::__construct($context);

    }

    public function execute(){
//        $model = $this->_objectManager->create('Mageplaza\Blog\Model\Post');
//        $model = $this->_objectManager->create('Mageplaza\Blog\Model\Topic');
//        $model = $this->_objectManager->create('Mageplaza\Blog\Model\Author');
        $url = "https://www.proteinking.com.au/helpk/blog/index/";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
        curl_setopt($ch, CURLOPT_ENCODING, "gzip,deflate");
        $response = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($response);
        $data = get_object_vars($data);

//        $model = $this->_objectManager->create('Mageplaza\Blog\Model\topicFactory');
//        $authorCollection = $model->getCollection();
        $success = 0;
        $fails = 0;
//        var_dump("<pre>");
        try {
            foreach ($data as $pkPost) {
                $pkPost = get_object_vars($pkPost);

                $topics = $pkPost['topics'];
                $resultTopics = [];
                $arrayTopics = explode(" ", $topics);
                foreach ($arrayTopics as $topicId) {
                    switch ($topicId){
                        case 1:
                            array_push($resultTopics, 1);
                            break;
                        case 2:
                            array_push($resultTopics, 2);
                            break;
                        case 3:
                            array_push($resultTopics, 3);
                            break;
                        case 4:
                            array_push($resultTopics, 4);
                            break;
                        case 5:
                            array_push($resultTopics, 5);
                            break;
                        case 6:
                            array_push($resultTopics, 6);
                            break;
                        case 7:
                            array_push($resultTopics, 7);
                            break;
                        case 8:
                            array_push($resultTopics, 8);
                            break;
                        case 9:
                            array_push($resultTopics, 9);
                            break;
                        case 10:
                            array_push($resultTopics, 10);
                            break;
                        case 11:
                            array_push($resultTopics, 11);
                            break;
                        case 14:
                            array_push($resultTopics, 3);
                            break;
                        case 17:
                            array_push($resultTopics, 4);
                            break;
                        case 18:
                            array_push($resultTopics, 5);
                            break;
                        case 19:
                            array_push($resultTopics, 6);
                            break;
                        case 20:
                            array_push($resultTopics, 7);
                            break;
                        case 24:
                            array_push($resultTopics, 8);
                            break;
                        case 25:
                            array_push($resultTopics, 9);
                            break;
                        case 27:
                            array_push($resultTopics, 10);
                            break;
                        case 28:
                            array_push($resultTopics, 11);
                            break;
                        default:
                            array_push($resultTopics, 1);
                            break;
                    }
                }
                $pkPost['topics'] = implode(",",$resultTopics);
//                var_dump('<pre>');
//                var_dump($pkPost['topics']);
//                var_dump($pkPost['topics']);
//                var_dump('</pre>');
//                die();

                $currentPost['name'] = $pkPost['title'];
                $currentPost['short_description'] = $pkPost['short_description']; //same value
                $currentPost['post_content'] = str_replace("media/import/","pub/media/import/",$pkPost['description']);
                $currentPost['store_ids'] = array(0,1);
                $currentPost['image'] = $pkPost['image'];
                $currentPost['enabled'] = $pkPost['status'];
                $currentPost['url_key'] = $pkPost['url_key'];
                $currentPost['in_rss'] = 0;
                $currentPost['allow_comment'] = 1;
                $currentPost['meta_title'] = $pkPost['meta_title'];
                $currentPost['meta_description'] = $pkPost['meta_description'];
                $currentPost['meta_keywords'] = $pkPost['meta_keywords'];
                $currentPost['meta_robots'] = "INDEX,FOLLOW";
                $currentPost['updated_at'] = $pkPost['updated_at'];
                $currentPost['created_at'] = $pkPost['created_at'];
                $currentPost['author_id'] = $pkPost['author_id'];
                $currentPost['modifier_id'] = 3;
                $currentPost['publish_date'] = $pkPost['publish_date'];
                $currentPost['categories_ids'] = [];
                $currentPost['topics_ids'] = $pkPost['topics'];
                $currentPost['tags_ids'] = array();

                $post = $this->initPost();
                $this->prepareData($post, $currentPost);

                try {
                    $post->save();
                    $success++;
                } catch (LocalizedException $e) {
                    $fails++;
                    $this->messageManager->addError($e->getMessage());
                }
//                $success++;
            }
        } catch (\Magento\Framework\Model\Exception $e) {
            $fails++;
            var_dump('aaaaaaaaaaaaa '. $e->getMessage());
            $this->messageManager->addError($e->getMessage());
        } catch (\RuntimeException $e) {
            $fails++;
            var_dump('bbbbbbbbbbbbb '. $e->getMessage());
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $fails++;
            var_dump('ccccccccccccc '. $e->getMessage());
            $this->messageManager->addException($e, __('Something went wrong while saving the slide.'));
        }
        var_dump("<pre>");
        var_dump('success: '.$success);
        var_dump('fails  : '.$fails);
        var_dump("</pre>");

        die("ggggggggggggggggg");
    }

    protected function initPost($register = false)
    {
        $postId = (int)$this->getRequest()->getParam('id');

        /** @var \Mageplaza\Blog\Model\Post $post */
        $post = $this->postFactory->create();
        if ($postId) {
            $post->load($postId);
            if (!$post->getId()) {
                $this->messageManager->addErrorMessage(__('This post no longer exists.'));

                return false;
            }
        }

        if (!$post->getAuthorId()) {
            $post->setAuthorId(3);
        }

        if ($register) {
            $this->coreRegistry->register('mageplaza_blog_post', $post);
        }

        return $post;
    }

    /**
     * @param $post
     * @param array $data
     * @return $this
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    protected function prepareData($post, $data = [])
    {

//        $this->imageHelper->uploadImage($data, 'image', Image::TEMPLATE_MEDIA_TYPE_POST, $post->getImage());

        //set specify field data
        $timezone = $this->_objectManager->create('Magento\Framework\Stdlib\DateTime\TimezoneInterface');
        $data['publish_date'] = $timezone->convertConfigTimeToUtc(isset($data['publish_date']) ? $data['publish_date'] : null);
        $data['modifier_id'] = 2;
        $data['categories_ids'] = '';
        $data['tags_ids'] = '';
        $data['topics_ids'] = (isset($data['topics_ids']) && $data['topics_ids']) ? explode(',', $data['topics_ids']) : [];

        $post->addData($data);

//        if ($topics = $data['topics_ids']) {
//            $post->setTopicsData(
//                $this->jsHelper->decodeGridSerializedInput($topics)
//            );
//        }
        return $this;
    }
}
