<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_Blog
 * @copyright   Copyright (c) 2018 Mageplaza (http://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\Blog\Block;

use Mageplaza\Blog\Model\Config\Source\DisplayType;

/**
 * Class Listpost
 * @package Mageplaza\Blog\Block\Post
 */
class ListpostHomepage extends Frontend
{
    /**
     * @return \Mageplaza\Blog\Model\ResourceModel\Post\Collection
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getPostCollectionHomePage()
    {
        $collection = $this->getCollection()->setPageSize(10)
            ->setCurPage(1)
            ->load();
        return $collection;
    }

    /**
     * Override this function to apply collection for each type
     *
     * @return \Mageplaza\Blog\Model\ResourceModel\Post\Collection
     */
    protected function getCollection()
    {
        return $this->helperData->getPostCollection();
    }
}
