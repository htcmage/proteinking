One Step Checkout Magento 2 - CHANGELOGS

+v2.0.5 (Dec 13, 2019)
+- Fix redirect to 404 page not found
+- Fix can not run compile from cmd
+- Fix js errors

v2.0.4 (July 30, 2019)
- Competible with magento 2.3.2:
- Fix redirect to 404 page not found
- Fix don't dhow payment method

v2.0.3 (November 14, 2018)
Fix bug
+ Billing Address not show in checkout page
+ Show delivery in order view

v2.0.2 (October 4, 2018)
+Updating drag & drop field for shipping address
+Integrate One Step Checkout with MageWorld Delivery Date extension

v2.0.1 (August 22, 2018)
Update payments
+ Stripe
+ Amazon Pay
+ Braintree
+ Klarna
+ Paymentsense
+ Knet

v2.0.0 (August 16, 2018)
Rebuild Magento 2 One Step Checkout extension
+ Redesign template
+ Allow to hide header and footer in checkout page
+ Allow to insert a static block to header or footer
+ Allow to sort order shipping address fields

v1.0.1 (Feb 21, 2018)
Fix small bugs and integrate with reward porint pro magento 2

v1.0.0 (Dec 01, 2017)
Extension Release