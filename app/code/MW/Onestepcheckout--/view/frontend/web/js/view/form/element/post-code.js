/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'underscore',
    'uiRegistry',
    'MW_Onestepcheckout/js/view/form/element/input',
    'MW_Onestepcheckout/js/action/reload-shipping-method'
], function (_, registry, Abstract, reloadShippingMethod) {
    'use strict';

    return Abstract.extend({
        defaults: {
            imports: {
                update: '${ $.parentName }.country_id:value'
            }
        },

        /**
         * @param {String} value
         */
        update: function (value) {
            var country = registry.get(this.parentName + '.' + 'country_id'),
                options = country.indexedOptions,
                option;

            if (!value) {
                return;
            }

            option = options[value];

            if (option['is_zipcode_optional']) {
                this.error(false);
                this.validation = _.omit(this.validation, 'required-entry');
            } else {
                this.validation['required-entry'] = true;
            }
            reloadShippingMethod();
            console.log("reloadShippingMethod");
            this.required(!option['is_zipcode_optional']);
        }
    });
});
