/*
 * *
 *  Copyright © 2016 MW. All rights reserved.
 *  See COPYING.txt for license details.
 *  
 */
/*browser:true*/
/*global define*/
define([
    'jquery',
    'Magento_Ui/js/form/element/abstract',
    'MW_Onestepcheckout/js/action/validate-shipping-info',
    'MW_Onestepcheckout/js/action/save-shipping-address',
    'MW_Onestepcheckout/js/action/reload-shipping-method'
], function ($, abstract,ValidateShippingInfo,SaveAddressBeforePlaceOrder,reloadShippingMethod) {
    'use strict';

    return abstract.extend({
        saveShippingAddress: function(){
            if(ValidateShippingInfo()){
                SaveAddressBeforePlaceOrder();
            }
            // console.log(this.inputName);
            var currentFieldEdit = this.inputName;
            if ((currentFieldEdit != 'telephone')
                && (currentFieldEdit != 'firstname')
                &&  (currentFieldEdit != 'lastname')
                &&  (currentFieldEdit != 'street[0]')
                &&  (currentFieldEdit != 'street[1]')
                &&  (currentFieldEdit != 'city')
            ) {
                reloadShippingMethod();
            }
        }
    });
});
