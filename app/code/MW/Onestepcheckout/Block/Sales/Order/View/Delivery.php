<?php

/**
 * Mage-World
 *
 * @category    Mage-World
 * @package     MW
 * @author      Mage-world Developer
 *
 * @copyright   Copyright (c) 2018 Mage-World (https://www.mage-world.com/)
 */

namespace MW\Onestepcheckout\Block\Sales\Order\View;

/**
 * Class Information
 * @package MW\Onestepcheckout\Block\Sales\Order\View
 */
class Delivery extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @param \Magento\Backend\Block\Template\Context   $context
     * @param \Magento\Framework\Registry               $registry
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param array                                     $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->_objectManager = $objectManager;
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->_coreRegistry->registry('current_order');
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->getRequest()->getParam('order_id');
    }

    /**
     * @return mixed
     */
    public function getDelivery()
    {

        $order = $this->getOrder();
        $delivery = [];
        if ($order->getData('mw_deliverydate_date')) {
            $delivery['mw_deliverydate_date'] = $order->getData('mw_deliverydate_date');
            $mwDeliverydateTime = $order->getData('mw_deliverydate_time');
            if ($mwDeliverydateTime) {
                $delivery['mw_deliverydate_time'] = $mwDeliverydateTime;
            } else {
                $delivery['mw_deliverydate_time'] = '';
            }

            $mwDeliverydateSecuritycode = $order->getData('mw_deliverydate_securitycode');
            if ($mwDeliverydateSecuritycode) {
                $delivery['mw_deliverydate_securitycode'] = $mwDeliverydateSecuritycode;
            } else {
                $delivery['mw_deliverydate_securitycode'] = '';
            }
        }
        return $delivery;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if ($this->_objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('mw_ddate/group_general/active', \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
            return '';
        }
        return parent::_toHtml();
    }
}
