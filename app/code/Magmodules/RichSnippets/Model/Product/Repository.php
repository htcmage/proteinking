<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Model\Product;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Helper\Image;
use Magento\Catalog\Model\Product;
use Magento\CatalogInventory\Model\Stock\Item as StockItem;
use Magento\Framework\Filter\FilterManager;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\UrlInterface;
use Magento\Review\Model\ResourceModel\Review\CollectionFactory as ReviewCollectionFactory;
use Magmodules\RichSnippets\Api\Config\RepositoryInterface as RichSnippetsConfigRepository;
use Magmodules\RichSnippets\Api\Log\RepositoryInterface as LogRepository;
use Magmodules\RichSnippets\Api\Product\RepositoryInterface as ProductRepositoryInterface;
use Magmodules\RichSnippets\Service\Product\AggregateRating;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;
use Magento\Catalog\Helper\Data as TaxHelper;

/**
 * Product provider class
 */
class Repository implements ProductRepositoryInterface
{

    /**
     * @var RichSnippetsConfigRepository
     */
    private $config;
    /**
     * @var StockItem
     */
    private $stockItem;
    /**
     * @var ReviewCollectionFactory
     */
    private $reviewCollection;
    /**
     * @var AggregateRating
     */
    private $aggregateRating;
    /**
     * @var Image
     */
    private $imgHelper;
    /**
     * @var UrlInterface
     */
    private $url;
    /**
     * @var DateTime
     */
    private $date;
    /**
     * @var FilterManager
     */
    private $filterManager;
    /**
     * @var LogRepository
     */
    private $logger;
    /**
     * @var PriceHelper
     */
    private $priceHelper;
    /**
     * @var TaxHelper
     */
    private $taxHelper;

    /**
     * Repository constructor.
     * @param RichSnippetsConfigRepository $config
     * @param StockItem $stockItem
     * @param Image $imgHelper
     * @param ReviewCollectionFactory $reviewCollection
     * @param AggregateRating $aggregateRating
     * @param UrlInterface $url
     * @param DateTime $date
     * @param FilterManager $filterManager
     * @param LogRepository $logger
     * @param PriceHelper $priceHelper
     * @param TaxHelper $taxHelper
     */
    public function __construct(
        RichSnippetsConfigRepository $config,
        StockItem $stockItem,
        Image $imgHelper,
        ReviewCollectionFactory $reviewCollection,
        AggregateRating $aggregateRating,
        UrlInterface $url,
        DateTime $date,
        FilterManager $filterManager,
        LogRepository $logger,
        PriceHelper $priceHelper,
        TaxHelper $taxHelper
    ) {
        $this->config = $config;
        $this->stockItem = $stockItem;
        $this->reviewCollection = $reviewCollection;
        $this->aggregateRating = $aggregateRating;
        $this->imgHelper = $imgHelper;
        $this->url = $url;
        $this->date = $date;
        $this->filterManager = $filterManager;
        $this->logger = $logger;
        $this->priceHelper = $priceHelper;
        $this->taxHelper = $taxHelper;
    }

    /**
     * {@inheritDoc}
     */
    public function getSchemaData($product): array
    {
        if (!$this->config->getProductEnabled()) {
            return [];
        }

        try {
            return $this->getProductSchema($product);
        } catch (\Exception $exception) {
            $this->logger->addErrorLog('Exception', $exception->getMessage());
            return [];
        }
    }

    /**
     * Get Product Schema data
     *
     * @param ProductInterface $product
     *
     * @return array
     */
    private function getProductSchema($product): array
    {
        $productSnippets = [];
        $children = [];
        switch ($product->getTypeId()) {
            case 'configurable':
                if ($this->config->getProductMultiConfigurable()) {
                    $children = $product->getTypeInstance()->getUsedProducts($product);
                }
                break;
            case 'bundle':
                if ($this->config->getProductMultiBundle()) {
                    $children = $product->getTypeInstance()
                        ->getSelectionsCollection($product->getTypeInstance()->getOptionsIds($product), $product)
                        ->getItems();
                }
                break;
            case 'grouped':
                if ($this->config->getProductMultiGrouped()) {
                    $children = $product->getTypeInstance()->getAssociatedProducts($product);
                }
                break;
        }

        if (empty($children)) {
            return [$this->getProductSchemaData($product)];
        }

        /* @var \Magento\Catalog\Model\Product $child */
        foreach ($children as $child) {
            if ($child->getStatus() == 1) {
                $productSnippets[] = $this->getProductSchemaData($child, $product);
            }
        }
        return $productSnippets;
    }

    /**
     * Get Product Schema data
     *
     * @param ProductInterface $product
     * @param ProductInterface|null $parent
     *
     * @return array
     */
    private function getProductSchemaData($product, $parent = null): array
    {
        $productSnippets = [
            '@context' => 'http://schema.org',
            '@type' => 'Product',
            'name' => $product->getName()
        ];

        if ($description = $this->getDescription($product)) {
            $productSnippets['description'] = $description;
        }

        if ($img = $this->getImage($product, $parent)) {
            $productSnippets['image'] = $img;
        }

        if ($offers = $this->getOffers($product)) {
            $productSnippets += $offers;
        }

        if ($aggregatedRating = $this->aggregateRating->execute($product)) {
            $productSnippets += $aggregatedRating;
        }

        if ($lastReview = $this->getLastReview($product)) {
            $productSnippets += $lastReview;
        }

        if ($extraFields = $this->getExtraFields($product, $parent)) {
            $productSnippets += $extraFields;
        }

        return $productSnippets;
    }

    /**
     * @inheritDoc
     */
    public function getDescription($product): string
    {
        $attribute = $this->config->getDescriptionAttribute($this->config->getStoreId());
        return (string)$this->filterManager->stripTags($this->getAttributeValue($product, $attribute));
    }

    /**
     * Get Attribute value
     *
     * @param Product $product
     * @param string $attribute
     *
     * @return string
     */
    private function getAttributeValue($product, $attribute): string
    {
        $value = '';

        try {
            /* @var \Magento\Eav\Model\Entity\Attribute\AbstractAttribute $attr */
            if ($attr = $product->getResource()->getAttribute($attribute)) {
                $value = $product->getData($attribute);
                if ($attr->usesSource()) {
                    $value = $attr->getSource()->getOptionText($value);
                    if (is_array($value)) {
                        $value = implode(', ', $value);
                    }
                }
            }
        } catch (\Exception $exception) {
            $this->logger->addErrorLog('LocalizedException', $exception->getMessage());
            return $value;
        }

        return (string)$value;
    }

    /**
     * Get Product Image
     *
     * @param Product $product
     * @param Product|null $parent
     *
     * @return string
     */
    private function getImage($product, $parent = null): string
    {
        $img = $this->imgHelper->init($product, 'product_base_image')->getUrl();
        if ($parent && $parent->getImage()) {
            $img = $this->imgHelper->init($parent, 'product_base_image')->getUrl();
        }

        return $img;
    }

    /**
     * Get Product Offer
     *
     * @param Product $product
     * @return array
     */
    private function getOffers($product): array
    {
        $price = $this->getPrice($product);
        if ($price == 0 && $this->config->getProductHideZero()) {
            return [];
        }

        $offers = [
            '@type' => 'Offer',
            'priceCurrency' => $this->config->getCurrencyCode(),
            'price' => $this->priceHelper->currency($price, false),
            'url' => $this->getCurrentUrl(),
            'priceValidUntil' => ($this->getPriceValidUntil($product))
                ? ($this->getPriceValidUntil($product))
                : ($this->date->date('Y-m-d', strtotime(' +100 days')))
        ];
        if ($itemCondition = $this->getItemCondition($product)) {
            $offers['itemCondition'] = $itemCondition;
        }
        if ($availability = $this->getAvailability($product)) {
            $offers['availability'] = $availability;
        }
        if ($sellerName = $this->config->getWebsiteNameValue()) {
            $offers['seller'] = [
                '@type' => 'Organization',
                'name' => $sellerName
            ];
        }

        return ['offers' => $offers];
    }

    /**
     * Get Product Price
     *
     * @param Product $product
     *
     * @return string
     */
    private function getPrice($product)
    {
        if ($this->config->useMsrpForPrice() && $product->getData('msrp') > 0) {
            $price = $product->getData('msrp');
        } else {
            $price = $product->getFinalPrice();
        }

        $price = $this->taxHelper->getTaxPrice($product, $price, true);
        if (in_array($product->getTypeId(), ['grouped', 'configurable', 'bundle'])) {
            /* @var \Magento\Framework\Pricing\Amount\AmountInterface $priceData */
            $priceData = $product->getPriceInfo()
                ->getPrice('final_price')
                ->getMinimalPrice();
            $price = $priceData->getValue();
        }

        return number_format((float)$price, 2, '.', '');
    }

    /**
     * Get Current Clean Url
     *
     * @return string
     */
    private function getCurrentUrl(): string
    {
        return preg_replace(
            '/\?.*/',
            '',
            $this->url->getCurrentUrl()
        );
    }

    /**
     * Calculate if price has a valid until
     *
     * @param Product $product
     *
     * @return string|null
     */
    private function getPriceValidUntil($product)
    {
        return ($product->getSpecialPrice() == $product->getFinalPrice())
            ? $product->getSpecialToDate()
            : '';
    }

    /**
     * Get Item Condition of product
     *
     * @param Product $product
     *
     * @return string
     */
    private function getItemCondition($product): string
    {
        switch ($this->config->getProductConditionEnable()) {
            case 1:
                if ($itemCondition = $this->config->getProductConditionDefault()) {
                    return sprintf('http://schema.org/%sCondition', $itemCondition);
                }
                break;
            case 2:
                if ($attribute = $this->config->getProductConditionAttribute()) {
                    $itemCondition = $this->getAttributeValue($product, $attribute);
                    if (!empty($itemCondition)) {
                        return sprintf('http://schema.org/%sCondition', ucfirst($itemCondition));
                    }
                }
                break;
        }
        return '';
    }

    /**
     * Get Product Availability
     *
     * @param Product $product
     *
     * @return string
     */
    private function getAvailability($product): string
    {
        if (!$this->config->getProductStock()) {
            return '';
        }

        if ($product->isAvailable()) {
            return 'http://schema.org/InStock';
        } else {
            return 'http://schema.org/OutOfStock';
        }
    }

    /**
     * Return Last Review Data
     *
     * @param Product $product
     *
     * @return array
     */
    private function getLastReview($product)
    {
        $reviewData = [];
        if (!$this->config->getProductReviewsEnabled()) {
            return $reviewData;
        }

        $lastReview = $this->reviewCollection->create()
            ->addStatusFilter(\Magento\Review\Model\Review::STATUS_APPROVED)
            ->addEntityFilter('product', $product->getId())
            ->setPageSize(1)
            ->addRateVotes()
            ->setDateOrder()
            ->getLastItem();

        if (!$lastReview->getData()) {
            return $reviewData;
        } elseif (!$lastReview->getRatingVotes()) {
            return [];
        }

        $totalScore = 0;
        $totalScores = count($lastReview->getRatingVotes());

        foreach ($lastReview->getRatingVotes() as $vote) {
            $totalScore += $vote->getPercent();
        }

        $metric = $this->config->getProductRatingMetric();

        if ($totalScores !== 0) {
            if ($metric == 5) {
                $avgRating = round(($totalScore / $totalScores / 20), 2);
                $bestRating = 5;
            } else {
                $avgRating = round($totalScore / $totalScores);
                $bestRating = 100;
            }
        } else {
            $avgRating = $bestRating = ($metric == 5) ? ($metric) : (100);
        }

        $reviewData = [
            '@type' => 'Review',
            'reviewRating' => [
                '@type' => 'Rating',
                'ratingValue' => $avgRating,
                'bestRating' => $bestRating
            ],
            'author' => [
                '@type' => 'Person',
                'name' => $lastReview->getNickname()
            ]
        ];
        return ['review' => $reviewData];
    }

    /**
     * Get Array Data of extra fields set in configuration
     *
     * @param Product $product
     * @param Product|null $parent
     *
     * @return array
     */
    private function getExtraFields($product, $parent = null): array
    {
        $extraFields = [];
        if (!$attributes = $this->config->getProductAttributesValues()) {
            return $extraFields;
        }

        foreach ($attributes as $attribute) {
            $value = $this->getAttributeValue($product, $attribute['attribute']);
            if (!$value && $parent !== null) {
                $value = $this->getAttributeValue($parent, $attribute['attribute']);
            }
            $label = $attribute['type'];
            if (trim((string)$value)) {
                $extraFields[$label] = trim(strip_tags($value));
            }
        }

        return $extraFields;
    }
}
