<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Module\Manager as ModuleManager;

/**
 * Source of option values in a form of value-label pairs
 */
class AggregateRating implements OptionSourceInterface
{

    /**
     * Options array
     *
     * @var array
     */
    public $options = null;
    /**
     * @var ModuleManager
     */
    private $moduleManager;

    /**
     * AggregateRating constructor.
     * @param ModuleManager $moduleManager
     */
    public function __construct(
        ModuleManager $moduleManager
    ) {
        $this->moduleManager = $moduleManager;
    }

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        if (!$this->options) {
            $this->options[] = [
                'value' => '',
                'label' => __('Default Magento Reviews')
            ];
                $this->options[] = [
                    'value' => 'Aheadworks_AdvancedReviews',
                    'label' => __('Aheadworks Advanced Reviews')
                ];

        }

        return $this->options;
    }
}
