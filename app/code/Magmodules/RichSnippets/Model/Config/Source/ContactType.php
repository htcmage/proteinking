<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Source of option values in a form of value-label pairs
 */
class ContactType implements OptionSourceInterface
{

    /**
     * Options array
     *
     * @var array
     */
    public $options = null;

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        if (!$this->options) {
            $this->options = [
                ['value' => 'customer service', 'label' => 'customer service'],
                ['value' => 'technical support', 'label' => 'technical support'],
                ['value' => 'billing support', 'label' => 'billing support'],
                ['value' => 'bill payment', 'label' => 'bill payment'],
                ['value' => 'sales', 'label' => 'sales'],
                ['value' => 'reservations', 'label' => 'reservations'],
                ['value' => 'credit card support', 'label' => 'credit card support'],
                ['value' => 'emergency', 'label' => 'emergency'],
                ['value' => 'baggage tracking', 'label' => 'baggage tracking'],
                ['value' => 'roadside assistance', 'label' => 'roadside assistance'],
                ['value' => 'package tracking', 'label' => 'package tracking'],
            ];
        }

        return $this->options;
    }
}
