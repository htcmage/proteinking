<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Source of option values in a form of value-label pairs
 */
class AttributeType implements OptionSourceInterface
{

    /**
     * Options array
     *
     * @var array
     */
    public $options = null;

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        if (!$this->options) {
            $this->options = [
                ['value' => 'description', 'label' => __('Description')],
                ['value' => 'brand', 'label' => __('Brand')],
                ['value' => 'model', 'label' => __('Model')],
                ['value' => 'sku', 'label' => __('SKU')],
                ['value' => 'gtin8', 'label' => __('Gtin8')],
                ['value' => 'gtin12', 'label' => __('Gtin12')],
                ['value' => 'gtin13', 'label' => __('Gtin13')],
                ['value' => 'gtin14', 'label' => __('Gtin14')],
                ['value' => 'mpn', 'label' => __('Mpn')],
                ['value' => 'isbn', 'label' => __('Isbn')],
            ];
        }

        return $this->options;
    }
}
