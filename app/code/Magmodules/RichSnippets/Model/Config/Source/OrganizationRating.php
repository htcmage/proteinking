<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Source of option values in a form of value-label pairs
 */
class OrganizationRating implements OptionSourceInterface
{

    /**
     * Options array
     *
     * @var array
     */
    public $options = null;

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        if (!$this->options) {
            $this->options = [
                ['value' => '', 'label' => __('--Please Select--')->render()],
                ['value' => 'Magmodules_WebwinkelKeurSR', 'label' => 'WebwinkelKeur Reviews (Magmodules)'],
                ['value' => 'Magmodules_KiyOhSR', 'label' => 'KiyOh Reviews (Magmodules)'],
                ['value' => 'Magmodules_FeedbackCompanySR', 'label' => 'Feedbackcompany Reviews (Magmodules)'],
                ['value' => 'Magmodules_EkomiSR', 'label' => 'Ekomi Reviews (Magmdodules)']
            ];
        }

        return $this->options;
    }
}
