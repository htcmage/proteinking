<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Source of option values in a form of value-label pairs
 */
class LocalBusiness implements OptionSourceInterface
{

    /**
     * Options array
     *
     * @var array
     */
    public $options = null;

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        if (!$this->options) {
            $this->options = [
                ['value' => 'Store', 'label' => 'Store (general)'],
                ['value' => 'BikeStore', 'label' => 'BikeStore'],
                ['value' => 'BookStore', 'label' => 'BookStore'],
                ['value' => 'ClothingStore', 'label' => 'ClothingStore'],
                ['value' => 'ComputerStore', 'label' => 'ComputerStore'],
                ['value' => 'ConvenienceStore', 'label' => 'ConvenienceStore'],
                ['value' => 'DepartmentStore', 'label' => 'DepartmentStore'],
                ['value' => 'ElectronicsStore', 'label' => 'ElectronicsStore'],
                ['value' => 'Florist', 'label' => 'Florist'],
                ['value' => 'FurnitureStore', 'label' => 'FurnitureStore'],
                ['value' => 'GardenStore', 'label' => 'GardenStore'],
                ['value' => 'GardenStore', 'label' => 'GardenStore'],
                ['value' => 'HardwareStore', 'label' => 'HardwareStore'],
                ['value' => 'HobbyShop', 'label' => 'HobbyShop'],
                ['value' => 'HomeGoodsStore', 'label' => 'HomeGoodsStore'],
                ['value' => 'JewelryStore', 'label' => 'JewelryStore'],
                ['value' => 'LiquorStore', 'label' => 'LiquorStore'],
                ['value' => 'MensClothingStore', 'label' => 'MensClothingStore'],
                ['value' => 'MobilePhoneStore', 'label' => 'MobilePhoneStore'],
                ['value' => 'MovieRentalStore', 'label' => 'MovieRentalStore'],
                ['value' => 'MusicStore', 'label' => 'MusicStore'],
                ['value' => 'OfficeEquipmentStore', 'label' => 'OfficeEquipmentStore'],
                ['value' => 'OutletStore', 'label' => 'OutletStore'],
                ['value' => 'PawnShop', 'label' => 'PawnShop'],
                ['value' => 'PetStore', 'label' => 'PetStore'],
                ['value' => 'ShoeStore', 'label' => 'ShoeStore'],
                ['value' => 'SportingGoodsStore', 'label' => 'SportingGoodsStore'],
                ['value' => 'TireShop', 'label' => 'TireShop'],
                ['value' => 'ToyStore', 'label' => 'ToyStore'],
                ['value' => 'WholesaleStore', 'label' => 'WholesaleStore'],
            ];
        }

        return $this->options;
    }
}
