<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Source of option values in a form of value-label pairs
 */
class Days implements OptionSourceInterface
{

    /**
     * Options array
     *
     * @var array
     */
    public $options = null;

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        if (!$this->options) {
            $this->options = [
                ['value' => 'Monday', 'label' => __('Monday')],
                ['value' => 'Tuesday', 'label' => __('Tuesday')],
                ['value' => 'Wednesday', 'label' => __('Wednesday')],
                ['value' => 'Thursday', 'label' => __('Thursday')],
                ['value' => 'Friday', 'label' => __('Friday')],
                ['value' => 'Saturday', 'label' => __('Saturday')],
                ['value' => 'Sunday', 'label' => __('Sunday')],
            ];
        }

        return $this->options;
    }
}
