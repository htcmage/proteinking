<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Model\Config;

use Magento\Directory\Model\CountryFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\Locale\Resolver as LocaleResolver;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magmodules\RichSnippets\Api\Config\RepositoryInterface as ConfigRepositoryInterface;
use Magmodules\RichSnippets\Api\Log\RepositoryInterface as LogRepository;

/**
 * Config provider class
 */
class Repository implements ConfigRepositoryInterface
{

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var CountryFactory
     */
    private $countryFactory;
    /**
     * @var ProductMetadataInterface
     */
    private $metadata;
    /**
     * @var LogRepository
     */
    private $logger;
    /**
     * @var Json
     */
    private $json;
    /**
     * @var LocaleResolver
     */
    private $locale;

    /**
     * Repository constructor.
     * @param StoreManagerInterface $storeManager
     * @param ScopeConfigInterface $scopeConfig
     * @param CountryFactory $countryFactory
     * @param ProductMetadataInterface $metadata
     * @param LogRepository $logger
     * @param Json $json
     * @param LocaleResolver $locale
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig,
        CountryFactory $countryFactory,
        ProductMetadataInterface $metadata,
        LogRepository $logger,
        Json $json,
        LocaleResolver $locale
    ) {
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->countryFactory = $countryFactory;
        $this->metadata = $metadata;
        $this->logger = $logger;
        $this->json = $json;
        $this->locale = $locale;
    }

    /**
     * {@inheritDoc}
     */
    public function getMagentoVersion(): string
    {
        return $this->metadata->getVersion();
    }

    /**
     * {@inheritDoc}
     */
    public function getExtensionVersion(): string
    {
        return $this->getStoreValue(self::XML_PATH_EXTENSION_VERSION);
    }

    /**
     * {@inheritDoc}
     */
    public function getStoreValue(string $path, int $storeId = null, string $scope = null): string
    {
        if (!$storeId) {
            $storeId = (int)$this->getStore()->getId();
        }
        $scope = $scope ?? ScopeInterface::SCOPE_STORE;
        return (string)$this->scopeConfig->getValue($path, $scope, (int)$storeId);
    }

    /**
     * {@inheritDoc}
     */
    public function getStore(): \Magento\Store\Api\Data\StoreInterface
    {
        try {
            return $this->storeManager->getStore();
        } catch (\Exception $e) {
            $this->logger->addErrorLog('NoSuchEntityException', $e->getMessage());
            if ($store = $this->storeManager->getDefaultStoreView()) {
                return $store;
            }
        }
        $stores = $this->storeManager->getStores();
        return reset($stores);
    }

    /**
     * {@inheritDoc}
     */
    public function getEnabled(string $path, int $storeId = null, string $scope = null): bool
    {
        if (!$this->isEnabled()) {
            return false;
        }

        $scope = $scope ?? ScopeInterface::SCOPE_STORE;
        return $this->getFlag($path, (int)$storeId, $scope);
    }

    /**
     * @inheritDoc
     */
    public function isEnabled(int $storeId = null): bool
    {
        return $this->getFlag(self::XML_PATH_EXTENSION_ENABLE, $storeId);
    }

    /**
     * {@inheritDoc}
     */
    public function getFlag(string $path, int $storeId = null, string $scope = null): bool
    {
        if (!$storeId) {
            $storeId = (int)$this->getStore()->getId();
        }
        $scope = $scope ?? ScopeInterface::SCOPE_STORE;
        return $this->scopeConfig->isSetFlag($path, $scope, (int)$storeId);
    }

    /**
     * {@inheritDoc}
     */
    public function getBaseUrl(): string
    {
        return $this->getStore()->getBaseUrl();
    }

    /**
     * {@inheritDoc}
     */
    public function getCurrencyCode(): string
    {
        return (string)$this->getStore()->getCurrentCurrencyCode();
    }

    /**
     * {@inheritDoc}
     */
    public function getLogo()
    {
        if ($logo = $this->getStoreLogo()) {
            return $logo;
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getStoreId(): int
    {
        try {
            return (int)$this->getStore()->getId();
        } catch (\Exception $e) {
            $this->logger->addErrorLog('NoSuchEntityException', $e->getMessage());
            return 0;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getCountryname(string $countryCode)
    {
        $country = $this->countryFactory->create()->loadByCode($countryCode);
        if ($country->getName()) {
            return $country->getName();
        } else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getStoreValueArray(string $path, int $storeId = null, string $scope = null): array
    {
        $value = $this->getStoreValue($path, (int)$storeId, $scope);
        return $this->getValueArray($value);
    }

    /**
     * @param string $value
     * @return array
     */
    private function getValueArray(string $value): array
    {
        if (empty($value)) {
            return [];
        }
        try {
            return $this->json->unserialize($value);
        } catch (\Exception $exception) {
            $this->logger->addErrorLog('InvalidArgumentException', $exception->getMessage());
            return [];
        }
    }

    /**
     * {@inheritDoc}
     */
    public function removeThemeSnippets(): bool
    {
        if (!$this->isEnabled()) {
            return false;
        }

        return $this->getFlag(self::XML_PATH_REMOVE_THEME_SNIPPETS);
    }

    /**
     * @inheritDoc
     */
    public function getExtensionCode(): string
    {
        return self::EXTENSION_CODE;
    }

    /**
     * @inheritDoc
     */
    public function isDebugMode(int $storeId = null): bool
    {
        $scope = $scope ?? ScopeInterface::SCOPE_STORE;
        return $this->getFlag(
            self::XML_PATH_DEBUG,
            $storeId,
            $scope
        );
    }

    /**
     * Support link for extension.
     *
     * @return string
     */
    public function getSupportLink(): string
    {
        return self::MODULE_SUPPORT_LINK;
    }

    /**
     * @inheritDoc
     */
    public function getFacebookAppId(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_FACEBOOK_APP_ID, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getDescriptionAttribute(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_PRODUCT_DESCRIPTION_ATTRIBUTE, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getProductRatingEnabled(): bool
    {
        return $this->getFlag(self::XML_PATH_PRODUCT_RATING_ENABLE);
    }

    /**
     * @inheritDoc
     */
    public function getProductRatingSource(): string
    {
        return $this->getStoreValue(self::XML_PATH_PRODUCT_RATING_SOURCE);
    }

    public function getProductRatingMetric(): string
    {
        return $this->getStoreValue(self::XML_PATH_PRODUCT_RATING_METRIC);
    }

    /**
     * @inheritDoc
     */
    public function getLocale(): string
    {
        return (string)$this->locale->getDefaultLocale();
    }

    /**
     * @inheritDoc
     */
    public function getLocalBusinessEnabled(int $storeId = null): bool
    {
        return $this->getFlag(self::XML_PATH_LOCAL_BUSINESS_ENABLE, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getLocalBusinessType(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_LOCAL_BUSINESS_TYPE, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getLocalBusinessName(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_LOCAL_BUSINESS_NAME, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getLocalBusinessId(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_LOCAL_BUSINESS_ID, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getLocalBusinessAddress(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_LOCAL_BUSINESS_ADDRESS, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getLocalBusinessCity(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_LOCAL_BUSINESS_CITY, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getLocalBusinessPostalCode(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_LOCAL_BUSINESS_POSTALCODE, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getLocalBusinessRegion(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_LOCAL_BUSINESS_REGION, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getLocalBusinessCountry(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_LOCAL_BUSINESS_COUNTRY, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getLocalBusinessTelephone(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_LOCAL_BUSINESS_TELEPHONE, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getLocalBusinessLatitude(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_LOCAL_BUSINESS_LATITUDE, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getLocalBusinessLongitude(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_LOCAL_BUSINESS_LONGITUDE, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getLocalBusinessPriceRange(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_LOCAL_BUSINESS_PRICERANGE, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getLocalBusinessOpenHours(int $storeId = null): array
    {
        $value = $this->getStoreValue(self::XML_PATH_LOCAL_BUSINESS_OPENING_HOURS, $storeId);
        return $this->getValueArray($value);
    }

    /**
     * @inheritDoc
     */
    public function getOrganizationEnabled(int $storeId = null): bool
    {
        return $this->getFlag(self::XML_PATH_ORGANIZATION_ENABLE, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getOrganizationContact(int $storeId = null): array
    {
        $value = $this->getStoreValue(self::XML_PATH_ORGANIZATION_CONTACT_INFORMATION, $storeId);
        return $this->getValueArray($value);
    }

    /**
     * @inheritDoc
     */
    public function getOrganizationSocial(int $storeId = null): array
    {
        $value = $this->getStoreValue(self::XML_PATH_ORGANIZATION_SOCIAL_LINKS, $storeId);
        return $this->getValueArray($value);
    }

    /**
     * @inheritDoc
     */
    public function getOrganizationRatingSource(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_ORGANIZATION_RATING_SOURCE, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getProductEnabled(int $storeId = null): bool
    {
        return $this->getFlag(self::XML_PATH_PRODUCT_ENABLE, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getProductStock(int $storeId = null): bool
    {
        return $this->getFlag(self::XML_PATH_PRODUCT_STOCK, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getProductConditionEnable(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_PRODUCT_CONDITION_ENABLE, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getProductConditionDefault(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_PRODUCT_CONDITION_DEFAULT, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getProductConditionAttribute(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_PRODUCT_CONDITION_ATTRIBUTE, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getProductReviewsEnabled(int $storeId = null): bool
    {
        return $this->getFlag(self::XML_PATH_PRODUCT_REVIEW_ENABLE, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getProductAttributesValues(int $storeId = null): array
    {
        $value = $this->getStoreValue(self::XML_PATH_PRODUCT_ATTRIBUTES, $storeId);
        return $this->getValueArray($value);
    }

    /**
     * @inheritDoc
     */
    public function getProductMultiConfigurable(int $storeId = null): bool
    {
        return $this->getFlag(self::XML_PATH_MULTI_CONFIGURABLE, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getProductMultiGrouped(int $storeId = null): bool
    {
        return $this->getFlag(self::XML_PATH_MULTI_GROUPED, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getProductMultiBundle(int $storeId = null): bool
    {
        return $this->getFlag(self::XML_PATH_MULTI_BUNDLE, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getProductHideZero(int $storeId = null): bool
    {
        return $this->getFlag(self::XML_PATH_HIDE_ZERO_OFFER, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function useMsrpForPrice(int $storeId = null): bool
    {
        return $this->getFlag(self::XML_PATH_USE_MSRP, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getWebsiteNameValue(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_WEBSITE_NAME, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getWebsiteNameEnabled(int $storeId = null): bool
    {
        return $this->getFlag(self::XML_PATH_WEBSITE_SITENAME, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getWebsiteSearchEnabled(int $storeId = null): bool
    {
        return $this->getFlag(self::XML_PATH_WEBSITE_SITESEARCH, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getWebsiteAlternateValue(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_WEBSITE_ALTERNATENAME, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getBreadcrumbsCustomHomeTitle(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_BREADCRUMBS_CUSTOM_HOME_TITLE, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getBreadcrumbsCustomHomeEnabled(int $storeId = null): bool
    {
        return $this->getFlag(self::XML_PATH_BREADCRUMBS_CUSTOM_HOME, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getStoreLogo(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_STORE_LOGO, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getSocialTwitterUsername(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_SOCIAL_TWITTER_USERNAME, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getCmsTitle(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_CMS_TITLE, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getCmsDescription(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_CMS_DESCRIPTION, $storeId);
    }

    /**
     * @inheritDoc
     */
    public function getCmsLogo(int $storeId = null): string
    {
        return $this->getStoreValue(self::XML_PATH_CMS_LOGO, $storeId);
    }
}
