<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Model\Organization;

use Magmodules\RichSnippets\Api\Config\RepositoryInterface as RichSnippetsConfigRepository;
use Magmodules\RichSnippets\Api\Log\RepositoryInterface as LogRepository;
use Magmodules\RichSnippets\Api\Organization\RepositoryInterface as OrganizationRepositoryInterface;
use Magmodules\RichSnippets\Api\Organization\ServiceInterface as OrganizationServiceInterface;

/**
 * LocalBusiness provider class
 */
class Repository implements OrganizationRepositoryInterface
{

    /**
     * @var RichSnippetsConfigRepository
     */
    private $config;
    /**
     * @var LogRepository
     */
    private $logger;
    /**
     * @var OrganizationServiceInterface
     */
    private $service;

    /**
     * Repository constructor.
     * @param RichSnippetsConfigRepository $config
     * @param LogRepository $logger
     * @param OrganizationServiceInterface $service
     */
    public function __construct(
        RichSnippetsConfigRepository $config,
        LogRepository $logger,
        OrganizationServiceInterface $service
    ) {
        $this->config = $config;
        $this->logger = $logger;
        $this->service = $service;
    }

    /**
     * {@inheritDoc}
     */
    public function getSchemaData(): array
    {
        if (!$this->config->getOrganizationEnabled()) {
            return [];
        }

        try {
            return [$this->getOrganizationSchema()];
        } catch (\Exception $exception) {
            $this->logger->addErrorLog('Exception', $exception->getMessage());
            return [];
        }
    }

    /**
     * @return array
     */
    private function getOrganizationSchema(): array
    {
        $organizationSnippets = [];
        $organizationSnippets['@context'] = 'http://schema.org';
        $organizationSnippets['@type'] = 'Organization';
        $organizationSnippets['url'] = $this->config->getBaseUrl();

        if ($logo = $this->config->getLogo()) {
            $organizationSnippets['logo'] = $logo;
        }

        if ($contactSchema = $this->getContactInformationSchema()) {
            $organizationSnippets['contactPoint'] = $contactSchema;
        }

        if ($socialSchema = $this->getSocialLinksSchema()) {
            $organizationSnippets['sameAs'] = $socialSchema;
        }

        if ($ratingSchema = $this->getRatingSchema()) {
            $organizationSnippets = array_merge($organizationSnippets, $ratingSchema);
        }

        return $organizationSnippets;
    }

    /**
     * @return array
     */
    private function getContactInformationSchema(): array
    {
        $contactSchema = [];
        if (!$contactInformation = $this->config->getOrganizationContact()) {
            return $contactSchema;
        }

        foreach ($contactInformation as $info) {
            $contact = [];
            if (!empty($info['telephone'])) {
                $contact['@type'] = 'ContactPoint';
                $contact['telephone'] = $info['telephone'];
                if (!empty($info['contact_type'])) {
                    $contact['contactType'] = $info['contact_type'];
                }
                if (!empty($info['contact_option'])) {
                    if (strpos($info['contact_option'], ',') !== false) {
                        $contact['contactOption'] = explode(',', $info['contact_option']);
                    } else {
                        $contact['contactOption'] = $info['contact_option'];
                    }
                }
                if (!empty($info['area'])) {
                    if (strpos($info['area'], ',') !== false) {
                        $contact['areaServed'] = explode(',', $info['area']);
                    } else {
                        $contact['areaServed'] = $info['area'];
                    }
                }
                if (!empty($info['languages'])) {
                    if (strpos($info['languages'], ',') !== false) {
                        $contact['availableLanguage'] = explode(',', $info['languages']);
                    } else {
                        $contact['availableLanguage'] = $info['languages'];
                    }
                }
            }
            if (!empty($contact)) {
                $contactSchema[] = $contact;
                unset($contact);
            }
        }

        return $contactSchema;
    }

    /**
     * @return array
     */
    private function getSocialLinksSchema(): array
    {
        $socialSchema = [];
        if (!$socialLinks = $this->config->getOrganizationSocial()) {
            return $socialSchema;
        }

        foreach ($socialLinks as $link) {
            $socialSchema[] = $link['url'];
        }

        return $socialSchema;
    }

    /**
     * @return array
     */
    private function getRatingSchema()
    {
        $ratingSchema = [];
        if ($moduleName = $this->config->getOrganizationRatingSource()) {
            $ratingSchema = $this->service->getRatingInfo($moduleName);
        }
        return $ratingSchema;
    }
}
