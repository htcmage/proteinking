<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Model\LocalBusiness;

use Magmodules\RichSnippets\Api\Config\RepositoryInterface as RichSnippetsConfigRepository;
use Magmodules\RichSnippets\Api\LocalBusiness\RepositoryInterface as LocalBusinessRepositoryInterface;
use Magmodules\RichSnippets\Api\Log\RepositoryInterface as LogRepository;

/**
 * LocalBusiness provider class
 */
class Repository implements LocalBusinessRepositoryInterface
{

    /**
     * @var RichSnippetsConfigRepository
     */
    private $config;
    /**
     * @var LogRepository
     */
    private $logger;

    /**
     * Repository constructor.
     * @param RichSnippetsConfigRepository $config
     * @param LogRepository $logger
     */
    public function __construct(
        RichSnippetsConfigRepository $config,
        LogRepository $logger
    ) {
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * {@inheritDoc}
     */
    public function getSchemaData(): array
    {
        if (!$this->config->getLocalBusinessEnabled()) {
            return [];
        }

        try {
            return [$this->getLocalBusinessSchema()];
        } catch (\Exception $exception) {
            $this->logger->addErrorLog('Exception', $exception->getMessage());
            return [];
        }
    }

    /**
     * @return array
     */
    private function getLocalBusinessSchema(): array
    {
        $businessSnippets = [
            '@context' => 'http://schema.org',
            '@type' => $this->config->getLocalBusinessType(),
        ] + $this->getAddressSchema() + $this->getOpeningHoursSchema();

        if ($name = $this->config->getLocalBusinessName()) {
            $businessSnippets['name'] = $name;
        }

        if ($id = $this->config->getLocalBusinessId()) {
            $businessSnippets['@id'] = $id;
        }

        if ($telephone = $this->config->getLocalBusinessTelephone()) {
            $businessSnippets['telephone'] = $telephone;
        }

        if ($logo = $this->config->getLogo()) {
            $businessSnippets['image'] = $logo;
        }

        if ($pricerange = $this->config->getLocalBusinessPriceRange()) {
            $businessSnippets['priceRange'] = $pricerange;
        }

        return $businessSnippets;
    }

    /**
     * @return array
     */
    private function getAddressSchema(): array
    {
        $businessSnippets = [];

        if (!$address = $this->config->getLocalBusinessAddress()) {
            return $businessSnippets;
        }

        $businessSnippets['address']['@type'] = 'PostalAddress';
        $businessSnippets['address']['streetAddress'] = $address;
        if ($city = $this->config->getLocalBusinessCity()) {
            $businessSnippets['address']['addressLocality'] = $city;
        }
        if ($region = $this->config->getLocalBusinessRegion()) {
            $businessSnippets['address']['addressRegion'] = $region;
        }
        if ($postalcode = $this->config->getLocalBusinessPostalCode()) {
            $businessSnippets['address']['postalCode'] = $postalcode;
        }
        if ($countyCode = $this->config->getLocalBusinessCountry()) {
            if ($country = $this->config->getCountryname($countyCode)) {
                $businessSnippets['address']['addressCountry'] = $country;
            }
        }
        $latitude = $this->config->getLocalBusinessLatitude();
        $longitude = $this->config->getLocalBusinessLongitude();
        if (!empty($latitude) && !empty($longitude)) {
            $businessSnippets['geo'] = [
                '@type' => 'GeoCoordinates',
                'latitude' => $latitude,
                'longitude' => $longitude
            ];
        }

        return $businessSnippets;
    }

    /**
     * @return array
     */
    private function getOpeningHoursSchema(): array
    {
        $businessSnippets = [];

        if (!$openingHours = $this->config->getLocalBusinessOpenHours()) {
            return $businessSnippets;
        }

        foreach ($openingHours as $open) {
            $businessSnippets['openingHoursSpecification'][] = [
                '@type' => 'OpeningHoursSpecification',
                'dayOfWeek' => $open['day'],
                'opens' => $open['time_open'],
                'closes' => $open['time_close']
            ];
        }

        return $businessSnippets;
    }
}
