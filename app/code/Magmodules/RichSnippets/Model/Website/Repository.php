<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Model\Website;

use Magmodules\RichSnippets\Api\Log\RepositoryInterface as LogRepository;
use Magmodules\RichSnippets\Api\Config\RepositoryInterface as RichSnippetsConfigRepository;
use Magmodules\RichSnippets\Api\Website\RepositoryInterface as WebsiteRepositoryInterface;

/**
 * Website provider class
 */
class Repository implements WebsiteRepositoryInterface
{

    /**
     * @var RichSnippetsConfigRepository
     */
    private $config;
    /**
     * @var LogRepository
     */
    private $logger;

    /**
     * Repository constructor.
     * @param RichSnippetsConfigRepository $config
     * @param LogRepository $logger
     */
    public function __construct(
        RichSnippetsConfigRepository $config,
        LogRepository $logger
    ) {
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * {@inheritDoc}
     */
    public function getSchemaData(): array
    {
        try {
            return [$this->getWebSiteSchema()];
        } catch (\Exception $exception) {
            $this->logger->addErrorLog('Exception', $exception->getMessage());
            return [];
        }
    }

    /**
     * @return array
     */
    private function getWebSiteSchema(): array
    {
        $sitenameSnippets = [];

        if (!$this->config->getWebsiteNameEnabled()) {
            return $sitenameSnippets;
        }

        $name = $this->config->getWebsiteNameValue();
        $search = $this->getSiteLinkSearch();

        if (empty($name) && empty($search)) {
            return $sitenameSnippets;
        }

        $alternateName = $this->config->getWebsiteAlternateValue();
        $sitenameSnippets['@context'] = 'http://schema.org';
        $sitenameSnippets['@type'] = 'WebSite';
        $sitenameSnippets['url'] = $this->config->getBaseUrl();

        if (!empty($name)) {
            $sitenameSnippets['name'] = $name;
        }

        if (!empty($alternateName)) {
            $sitenameSnippets['alternateName'] = $alternateName;
        }

        if ($search = $this->getSiteLinkSearch()) {
            $sitenameSnippets['potentialAction'] = $search;
        }

        return $sitenameSnippets;
    }

    /**
     * @return array
     */
    private function getSiteLinkSearch(): array
    {
        $searchSnippets = [];
        if (!$this->config->getWebsiteSearchEnabled()) {
            return $searchSnippets;
        }

        $searchSnippets['@type'] = 'SearchAction';
        $searchSnippets['target'] = $this->config->getBaseUrl() . 'catalogsearch/result/?q={search_term_string}';
        $searchSnippets['query-input'] = 'required name=search_term_string';

        return $searchSnippets;
    }
}
