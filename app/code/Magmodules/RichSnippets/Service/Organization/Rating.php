<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Service\Organization;

use Magmodules\RichSnippets\Api\Organization\ServiceInterface;

/**
 * Get rating from reviews modules
 */
class Rating implements ServiceInterface
{
    /**
     * @var object[]
     */
    protected $services;

    /**
     * Constructor
     *
     * @param array $services
     */
    public function __construct(array $services = [])
    {
        $this->services = $services;
    }

    /**
     * Call external service method
     *
     * @param string $module
     * @return array
     */
    public function getRatingInfo(string $module): array
    {
        $result = [];
        if (isset($this->services[$module])) {
            $result = $this->services[$module]->getRatingInfo();
        }
        return $result;
    }
}
