<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Service\Product\AggregateRating;

use Aheadworks\AdvancedReviews\Block\Product\ReviewSummaryRenderer;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\ObjectManagerInterface;
use Magmodules\RichSnippets\Api\Config\RepositoryInterface as ConfigRepository;

/**
 * Aheadworks AdvancedReviews AggregateRating collector
 * Note: We use ObjectManager for this implementation to not break compilation
 */
class AheadworksAdvancedReviews
{

    /**
     * @var ConfigRepository
     */
    private $config;
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Magento constructor.
     * @param ConfigRepository $config
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(
        ConfigRepository $config,
        ObjectManagerInterface $objectManager
    ) {
        $this->config = $config;
        $this->objectManager = $objectManager;
    }

    /**
     * AggregateRating array Based on Aheadworks AdvancedReviews
     *
     * @param ProductInterface $product
     * @return array
     */
    public function execute($product): array
    {
        $aggregateRating['@type'] = 'AggregateRating';
        $reviewSummaryRenderer = $this->objectManager->get(ReviewSummaryRenderer::class);
        $reviewSummaryRenderer->getReviewsSummaryHtml($product);
        $snippetSchema = $reviewSummaryRenderer->getSnippetSchema();
        if ($snippetSchema->getReviewsCount()) {
            $aggregateRatingData['reviewCount'] = $snippetSchema->getReviewsCount();
            $aggregateRatingData['ratingValue'] = $snippetSchema->getRatingValue();
            $aggregateRatingData['bestRating'] = $snippetSchema->getRatingMaximumValue();
            $aggregateRatingData['worstRating'] = $snippetSchema->getratingMinimumValue();
            return ['aggregateRating' => $aggregateRatingData];
        }

        return [];
    }
}
