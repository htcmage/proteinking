<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Service\Product\AggregateRating;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Review\Model\Review\SummaryFactory;
use Magmodules\RichSnippets\Api\Config\RepositoryInterface as ConfigRepository;

/**
 * Default Magento AggregateRating collector
 */
class Magento
{

    /**
     * @var ConfigRepository
     */
    private $config;
    /**
     * @var SummaryFactory
     */
    private $reviewSummary;

    /**
     * Magento constructor.
     * @param SummaryFactory $reviewSummary
     * @param ConfigRepository $config
     */
    public function __construct(
        SummaryFactory $reviewSummary,
        ConfigRepository $config
    ) {
        $this->reviewSummary = $reviewSummary;
        $this->config = $config;
    }

    /**
     * AggregateRating array Based on Magento Reviews
     *
     * @param ProductInterface $product
     * @return array
     */
    public function execute($product): array
    {
        $aggregateRating['@type'] = 'AggregateRating';
        $reviewSummary = $this->reviewSummary->create();
        $reviewSummary->setData('store_id', $this->config->getStoreId());
        $summaryModel = $reviewSummary->load($product->getId());

        if ($summaryModel->getRatingSummary() > 0) {
            $metric = $this->config->getProductRatingMetric();
            $aggregateRating['bestRating'] = $metric;
            $aggregateRating['reviewCount'] = $summaryModel->getReviewsCount();
            $aggregateRating['ratingValue'] = ($metric == 5)
                ? round(($summaryModel->getRatingSummary() / 20), 2)
                : $summaryModel->getRatingSummary();

            return ['aggregateRating' => $aggregateRating];
        }

        return [];
    }
}
