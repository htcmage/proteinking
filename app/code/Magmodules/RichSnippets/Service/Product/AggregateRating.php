<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Service\Product;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Module\Manager as ModuleManager;
use Magmodules\RichSnippets\Api\Config\RepositoryInterface as ConfigRepository;

/**
 * AggregateRating Service Model
 */
class AggregateRating
{
    /**
     * @var ConfigRepository
     */
    private $config;
    /**
     * @var AggregateRating\Magento
     */
    private $magentoAggerateRating;
    /**
     * @var AggregateRating\AheadworksAdvancedReviews
     */
    private $aheadworksAggerateRating;
    /**
     * @var ModuleManager
     */
    private $moduleManager;

    /**
     * Magento constructor.
     * @param ConfigRepository $config
     * @param ModuleManager $moduleManager
     * @param AggregateRating\Magento $magentoAggerateRating
     * @param AggregateRating\AheadworksAdvancedReviews $aheadworksAggerateRating
     */
    public function __construct(
        ConfigRepository $config,
        ModuleManager $moduleManager,
        AggregateRating\Magento $magentoAggerateRating,
        AggregateRating\AheadworksAdvancedReviews $aheadworksAggerateRating
    ) {
        $this->config = $config;
        $this->moduleManager = $moduleManager;
        $this->magentoAggerateRating = $magentoAggerateRating;
        $this->aheadworksAggerateRating = $aheadworksAggerateRating;
    }

    /**
     * @param ProductInterface $product
     * @return array
     */
    public function execute($product): array
    {
        $aggregateRating = [];
        if (!$this->config->getProductRatingEnabled()) {
            return $aggregateRating;
        }

        if (($this->config->getProductRatingSource() == 'ahead_advanced_reviews'
            || $this->config->getProductRatingSource() == 'Aheadworks_AdvancedReviews')
            && $this->moduleManager->isEnabled('Aheadworks_AdvancedReviews')) {
            return $this->aheadworksAggerateRating->execute($product);
        }

        return $this->magentoAggerateRating->execute($product);
    }
}
