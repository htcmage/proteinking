Prepare environment:


- https://devdocs.magento.com/mftf/docs/getting-started.html

on windows:

1. Download chromedriver.exe & selenium-server-standalone-3.141.59.jar
2. Run Selenium Server:
   java -jar selenium-server-standalone-3.141.59.jar

Run MFTF Test: 

$ vendor/bin/mftf generate:tests AdminRichsnippetsConfigurationTest -f
$ vendor/bin/mftf run:test AdminRichsnippetsConfigurationTest -f -r