<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Plugin\Block\Product;

use Magento\Review\Block\Product\ReviewRenderer as SubjectBlock;
use Magmodules\RichSnippets\Api\Config\RepositoryInterface as ConfigRepositoryInterface;

/**
 * Plugin to Remove ItemProperties from Review Block
 */
class ReviewRenderer
{
    /**
     * @var ConfigRepositoryInterface
     */
    private $config;

    /**
     * ReviewRenderer constructor.
     * @param ConfigRepositoryInterface $config
     */
    public function __construct(
        ConfigRepositoryInterface $config
    ) {
        $this->config = $config;
    }

    /**
     * Remove ItemProperties from Review Block
     *
     * @param SubjectBlock $subject
     * @param string $result
     * @return mixed|string
     */
    public function afterGetReviewsSummaryHtml(SubjectBlock $subject, $result = '')
    {
        if (!$this->config->removeThemeSnippets()) {
            return $result;
        }

        if ($result != ''
            && $subject->getRequest() !== null
            && $subject->getRequest()->getFullActionName() == 'catalog_product_view'
            && $product = $subject->getProduct()) {
            $data = [
                'itemprop="aggregateRating"',
                'itemscope',
                'itemtype="http://schema.org/AggregateRating"',
                'itemprop="ratingValue"',
                'itemprop="bestRating"',
                'itemprop="reviewCount"'
            ];
            $result = str_replace($data, '', $result);
        }

        return $result;
    }
}
