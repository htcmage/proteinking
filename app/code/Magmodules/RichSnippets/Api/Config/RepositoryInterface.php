<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Api\Config;

/**
 * Config repository interface
 */
interface RepositoryInterface
{
    const EXTENSION_CODE = 'Magmodules_RichSnippets';
    const XML_PATH_EXTENSION_VERSION = 'magmodules_richsnippets/general/version';
    const XML_PATH_EXTENSION_ENABLE = 'magmodules_richsnippets/general/enable';

    const XML_PATH_LOCAL_BUSINESS_ENABLE = 'magmodules_richsnippets/localbusiness/enable';
    const XML_PATH_LOCAL_BUSINESS_TYPE = 'magmodules_richsnippets/localbusiness/type';
    const XML_PATH_LOCAL_BUSINESS_NAME = 'magmodules_richsnippets/localbusiness/name';
    const XML_PATH_LOCAL_BUSINESS_ID = 'magmodules_richsnippets/localbusiness/id';
    const XML_PATH_LOCAL_BUSINESS_ADDRESS = 'magmodules_richsnippets/localbusiness/address';
    const XML_PATH_LOCAL_BUSINESS_CITY = 'magmodules_richsnippets/localbusiness/city';
    const XML_PATH_LOCAL_BUSINESS_POSTALCODE = 'magmodules_richsnippets/localbusiness/postalcode';
    const XML_PATH_LOCAL_BUSINESS_REGION = 'magmodules_richsnippets/localbusiness/region';
    const XML_PATH_LOCAL_BUSINESS_COUNTRY = 'magmodules_richsnippets/localbusiness/country';
    const XML_PATH_LOCAL_BUSINESS_TELEPHONE = 'magmodules_richsnippets/localbusiness/telehone';
    const XML_PATH_LOCAL_BUSINESS_LATITUDE = 'magmodules_richsnippets/localbusiness/latitude';
    const XML_PATH_LOCAL_BUSINESS_LONGITUDE = 'magmodules_richsnippets/localbusiness/longitude';
    const XML_PATH_LOCAL_BUSINESS_PRICERANGE = 'magmodules_richsnippets/localbusiness/pricerange';
    const XML_PATH_LOCAL_BUSINESS_OPENING_HOURS = 'magmodules_richsnippets/localbusiness/opening_hours';

    const XML_PATH_ORGANIZATION_ENABLE = 'magmodules_richsnippets/organization/enable';
    const XML_PATH_ORGANIZATION_CONTACT_INFORMATION = 'magmodules_richsnippets/organization/contact_information';
    const XML_PATH_ORGANIZATION_SOCIAL_LINKS = 'magmodules_richsnippets/organization/social_links';
    const XML_PATH_ORGANIZATION_RATING_SOURCE = 'magmodules_richsnippets/organization/rating';

    const XML_PATH_PRODUCT_ENABLE = 'magmodules_richsnippets/product/enable';
    const XML_PATH_PRODUCT_STOCK = 'magmodules_richsnippets/product/stock';
    const XML_PATH_PRODUCT_CONDITION_ENABLE = 'magmodules_richsnippets/product/condition';
    const XML_PATH_PRODUCT_CONDITION_DEFAULT = 'magmodules_richsnippets/product/condition_default';
    const XML_PATH_PRODUCT_CONDITION_ATTRIBUTE = 'magmodules_richsnippets/product/condition_attribute';
    const XML_PATH_PRODUCT_RATING_SOURCE = 'magmodules_richsnippets/product/rating_source';
    const XML_PATH_PRODUCT_RATING_ENABLE = 'magmodules_richsnippets/product/rating';
    const XML_PATH_PRODUCT_RATING_METRIC = 'magmodules_richsnippets/product/rating_metric';
    const XML_PATH_PRODUCT_REVIEW_ENABLE = 'magmodules_richsnippets/product/review';
    const XML_PATH_PRODUCT_ATTRIBUTES = 'magmodules_richsnippets/product/attributes';
    const XML_PATH_PRODUCT_DESCRIPTION_ATTRIBUTE = 'magmodules_richsnippets/product/description';
    const XML_PATH_MULTI_CONFIGURABLE = 'magmodules_richsnippets/product/multi_configurable';
    const XML_PATH_MULTI_GROUPED = 'magmodules_richsnippets/product/multi_grouped';
    const XML_PATH_MULTI_BUNDLE = 'magmodules_richsnippets/product/multi_bundle';
    const XML_PATH_HIDE_ZERO_OFFER = 'magmodules_richsnippets/product/hide_offer';
    const XML_PATH_USE_MSRP = 'magmodules_richsnippets/product/use_msrp';

    const XML_PATH_WEBSITE_NAME = 'magmodules_richsnippets/store/name';
    const XML_PATH_WEBSITE_SITENAME = 'magmodules_richsnippets/website/sitename';
    const XML_PATH_WEBSITE_SITESEARCH = 'magmodules_richsnippets/website/sitesearch';
    const XML_PATH_WEBSITE_ALTERNATENAME = 'magmodules_richsnippets/store/alternate_name';

    const XML_PATH_STORE_LOGO = 'magmodules_richsnippets/store/logo';
    const XML_PATH_DEBUG = 'magmodules_richsnippets/general/debug';
    const XML_PATH_REMOVE_THEME_SNIPPETS = 'magmodules_richsnippets/advanced/remove_snippets';
    const MODULE_SUPPORT_LINK = 'https://www.magmodules.eu/help/richsnippets-magento2';
    const XML_PATH_SOCIAL_TWITTER_USERNAME = 'magmodules_richsnippets/social/twitter_username';
    const XML_PATH_SOCIAL_OPENGRAPH = 'magmodules_richsnippets/social/opengraph';
    const XML_PATH_SOCIAL_TWITTER_CARDS = 'magmodules_richsnippets/social/twitter_cards';
    const XML_PATH_BREADCRUMBS_CUSTOM_HOME = 'magmodules_richsnippets/breadcrumbs/custom_home';
    const XML_PATH_BREADCRUMBS_CUSTOM_HOME_TITLE = 'magmodules_richsnippets/breadcrumbs/custom_home_title';
    const XML_PATH_CMS_TWITTER_SUMMARY = 'magmodules_richsnippets/social/cms_twitter';

    const XML_PATH_CMS_TITLE = 'magmodules_richsnippets/social/cms_opengraph_title';
    const XML_PATH_CMS_DESCRIPTION = 'magmodules_richsnippets/social/cms_opengraph_description';
    const XML_PATH_CMS_LOGO = 'magmodules_richsnippets/social/cms_opengraph_logo';

    const XML_PATH_FACEBOOK_APP_ID = 'magmodules_richsnippets/social/facebook_app_id';

    /**
     * Get Configuration data
     *
     * @param string $path
     * @param int|null $storeId
     * @param string|null $scope
     * @return string
     */
    public function getStoreValue(string $path, int $storeId = null, string $scope = null): string;

    /**
     * Get Configuration data array
     *
     * @param string $path
     * @param int|null $storeId
     * @param string|null $scope
     * @return array
     */
    public function getStoreValueArray(string $path, int $storeId = null, string $scope = null): array;

    /**
     * Get Magento Version
     *
     * @return string
     */
    public function getMagentoVersion(): string;

    /**
     * Get Extension Version
     *
     * @return string
     */
    public function getExtensionVersion(): string;

    /**
     * Check if module is enabled
     *
     * @param string $path
     * @param int|null $storeId
     * @param string|null $scope
     *
     * @return bool
     */
    public function getEnabled(string $path, int $storeId = null, string $scope = null): bool;

    /**
     * Get config value flag
     *
     * @param string $path
     * @param int $storeId
     * @param string|null $scope
     *
     * @return bool
     */
    public function getFlag(string $path, int $storeId, string $scope = null): bool;

    /**
     * Get base url of current store
     *
     * @return string
     */
    public function getBaseUrl(): string;

    /**
     * Get currency code of current store
     *
     * @return string
     */
    public function getCurrencyCode(): string;

    /**
     * Returns current store id
     *
     * @return int
     */
    public function getStoreId(): int;

    /**
     * Get set logo
     *
     * @return mixed
     */
    public function getLogo();

    /**
     * Get country name by code
     *
     * @param string $countryCode
     *
     * @return bool|string
     */
    public function getCountryname(string $countryCode);

    /**
     * Flag for removing Theme Snippets
     *
     * @return bool
     */
    public function removeThemeSnippets(): bool;

    /**
     * Get current store
     *
     * @return \Magento\Store\Api\Data\StoreInterface
     */
    public function getStore(): \Magento\Store\Api\Data\StoreInterface;

    /**
     * Get extension code
     *
     * @return string
     */
    public function getExtensionCode(): string;

    /**
     * Check if module is enabled
     *
     * @param int|null $storeId
     *
     * @return bool
     */
    public function isEnabled(int $storeId = null): bool;

    /**
     * Check if debug mode is enabled
     *
     * @param int|null $storeId
     *
     * @return bool
     */
    public function isDebugMode(int $storeId = null): bool;

    /**
     * Support link for extension.
     *
     * @return string
     */
    public function getSupportLink(): string;

    /**
     * Facebook app ID
     *
     * @param int|null $storeId
     *
     * @return string
     */
    public function getFacebookAppId(int $storeId = null): string;

    /**
     * Description attribute
     *
     * @param int|null $storeId
     *
     * @return string
     */
    public function getDescriptionAttribute(int $storeId = null): string;

    /**
     * Product Rating Enabled
     *
     * @return bool
     */
    public function getProductRatingEnabled(): bool;

    /**
     * Product Rating Source
     *
     * @return string
     */
    public function getProductRatingSource(): string;

    /**
     * Product Reating Metric
     *
     * @return string
     */
    public function getProductRatingMetric(): string;

    /**
     * Get default locale code
     *
     * @return string
     */
    public function getLocale(): string;

    /**
     * Get local business section flag
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getLocalBusinessEnabled(int $storeId = null): bool;

    /**
     * Get type value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getLocalBusinessType(int $storeId = null): string;

    /**
     * Get name
     *
     * @param int|null $storeId
     * @return string
     */
    public function getLocalBusinessName(int $storeId = null): string;

    /**
     * Get ID
     *
     * @param int|null $storeId
     * @return string
     */
    public function getLocalBusinessId(int $storeId = null): string;

    /**
     * Get address value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getLocalBusinessAddress(int $storeId = null): string;

    /**
     * Get city value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getLocalBusinessCity(int $storeId = null): string;

    /**
     * Get postal code
     *
     * @param int|null $storeId
     * @return string
     */
    public function getLocalBusinessPostalCode(int $storeId = null): string;

    /**
     * Get region value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getLocalBusinessRegion(int $storeId = null): string;

    /**
     * Get country value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getLocalBusinessCountry(int $storeId = null): string;

    /**
     * Get phone value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getLocalBusinessTelephone(int $storeId = null): string;

    /**
     * Get latitude value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getLocalBusinessLatitude(int $storeId = null): string;

    /**
     * Get longitude value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getLocalBusinessLongitude(int $storeId = null): string;

    /**
     * Get price range value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getLocalBusinessPriceRange(int $storeId = null): string;

    /**
     * Get open hours array
     *
     * @param int|null $storeId
     * @return array
     */
    public function getLocalBusinessOpenHours(int $storeId = null): array;

    /**
     * Get organization section flag
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getOrganizationEnabled(int $storeId = null): bool;

    /**
     * Get organization contact information
     *
     * @param int|null $storeId
     * @return array
     */
    public function getOrganizationContact(int $storeId = null): array;

    /**
     * Get organization social links
     *
     * @param int|null $storeId
     * @return array
     */
    public function getOrganizationSocial(int $storeId = null): array;

    /**
     * Get organization rating source
     *
     * @param int|null $storeId
     * @return string
     */
    public function getOrganizationRatingSource(int $storeId = null): string;

    /**
     * Get product section flag
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getProductEnabled(int $storeId = null): bool;

    /**
     * Get product stock flag
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getProductStock(int $storeId = null): bool;

    /**
     * Get condition display option
     *
     * @param int|null $storeId
     * @return string
     */
    public function getProductConditionEnable(int $storeId = null): string;

    /**
     * Get condition default value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getProductConditionDefault(int $storeId = null): string;

    /**
     * Get condition attribute value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getProductConditionAttribute(int $storeId = null): string;

    /**
     * Get product reviews flag
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getProductReviewsEnabled(int $storeId = null): bool;

    /**
     * Get product attributes array
     *
     * @param int|null $storeId
     * @return array
     */
    public function getProductAttributesValues(int $storeId = null): array;

    /**
     * Get multi config flag
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getProductMultiConfigurable(int $storeId = null): bool;

    /**
     * Get multi grouped flag
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getProductMultiGrouped(int $storeId = null): bool;

    /**
     * Get multi bundle flag
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getProductMultiBundle(int $storeId = null): bool;

    /**
     * Get hide zero offer flag
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getProductHideZero(int $storeId = null): bool;

    /**
     * Show MSRP instead of price
     *
     * @param int|null $storeId
     * @return bool
     */
    public function useMsrpForPrice(int $storeId = null): bool;

    /**
     * Get site name value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getWebsiteNameValue(int $storeId = null): string;

    /**
     * Get website name flag
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getWebsiteNameEnabled(int $storeId = null): bool;

    /**
     * Get website search flag
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getWebsiteSearchEnabled(int $storeId = null): bool;

    /**
     * Get site alternate name value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getWebsiteAlternateValue(int $storeId = null): string;

    /**
     * Get custom home title value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getBreadcrumbsCustomHomeTitle(int $storeId = null): string;

    /**
     * Get custom home flag
     *
     * @param int|null $storeId
     * @return bool
     */
    public function getBreadcrumbsCustomHomeEnabled(int $storeId = null): bool;

    /**
     * Get store logo value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getStoreLogo(int $storeId = null): string;

    /**
     * Get social twitter name
     *
     * @param int|null $storeId
     * @return string
     */
    public function getSocialTwitterUsername(int $storeId = null): string;

    /**
     * Get cms title value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getCmsTitle(int $storeId = null): string;

    /**
     * Get cms description value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getCmsDescription(int $storeId = null): string;

    /**
     * Get cms logo value
     *
     * @param int|null $storeId
     * @return string
     */
    public function getCmsLogo(int $storeId = null): string;
}
