<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Api\Website;

/**
 * Website repository interface
 */
interface RepositoryInterface
{

    /**
     * Get Website Schema Data
     *
     * @return array
     */
    public function getSchemaData(): array;
}
