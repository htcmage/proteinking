<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Api\Organization;

/**
 * Organization repository interface
 */
interface RepositoryInterface
{

    /**
     * Get Organization Schema Data
     *
     * @return array
     */
    public function getSchemaData(): array;
}
