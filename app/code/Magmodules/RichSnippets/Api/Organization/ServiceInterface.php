<?php

/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Api\Organization;

/**
 * Get rating from reviews modules
 */
interface ServiceInterface
{
    /**
     * @param string $module
     * @return array
     */
    public function getRatingInfo(string $module): array;
}
