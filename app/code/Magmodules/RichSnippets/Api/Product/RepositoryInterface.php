<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Api\Product;

use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Product repository interface
 */
interface RepositoryInterface
{

    /**
     * Get Product Schema Data
     *
     * @param ProductInterface $product
     * @return array
     */
    public function getSchemaData($product): array;

    /**
     * Get Product Description
     *
     * @param ProductInterface $product
     *
     * @return string
     */
    public function getDescription($product): string;
}
