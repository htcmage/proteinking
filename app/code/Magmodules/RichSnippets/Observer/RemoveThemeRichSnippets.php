<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Request\Http as HttpRequest;
use Magmodules\RichSnippets\Api\Config\RepositoryInterface as ConfigRepositoryInterface;

/**
 * Observer to remove theme rich snippets
 */
class RemoveThemeRichSnippets implements ObserverInterface
{
    /**
     * @var ConfigRepositoryInterface
     */
    private $config;

    /**
     * @var HttpRequest
     */
    private $httpRequest;

    /**
     * RemoveThemeRichSnippets constructor.
     * @param ConfigRepositoryInterface $config
     * @param HttpRequest $httpRequest
     */
    public function __construct(
        ConfigRepositoryInterface $config,
        HttpRequest $httpRequest
    ) {
        $this->config = $config;
        $this->httpRequest = $httpRequest;
    }

    /**
     * @param Observer $observer
     *
     * @return $this
     */
    public function execute(Observer $observer)
    {
        if (!$this->config->removeThemeSnippets()) {
            return $this;
        }

        $layout = $observer->getData('layout');
        if ($this->httpRequest->getFullActionName() == 'catalog_product_view') {
            $layout->getUpdate()->addHandle('catalog_product_view_remove_snippets');
        }

        return $this;
    }
}
