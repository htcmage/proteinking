<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Block\Adminhtml\Magmodules;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Data\Form\Element\Select;
use Magento\Framework\Module\Manager as ModuleManager;

/**
 * Disable options if module is disabled
 */
class DisableSelectOptions extends Field
{
    /**
     * @var ModuleManager
     */
    private $moduleManager;

    /**
     * @param Context $context
     * @param ModuleManager $moduleManager
     */
    public function __construct(
        Context $context,
        ModuleManager $moduleManager
    ) {
        $this->moduleManager = $moduleManager;
        parent::__construct($context);
    }

    /**
     * @param AbstractElement $element
     *
     * @return string
     */
    public function render(AbstractElement $element): string
    {
        /** @var Select $element */
        $html = '<tr id="row_' . $element->getHtmlId() . '">';
        $html .= '  <td class="label">' . $element->getData('label') . '</td>';
        $html .= '  <td class="value">';
        $html .=  $this->getSelectHtml($element);
        $html .= '  </td>';
        $html .= '  <td></td>';
        $html .= '</tr>';

        return $html;
    }

    /**
     * @param Select $element
     * @return string
     */
    private function getSelectHtml(Select $element)
    {
        $renderedOptions = explode('<option', $element->getElementHtml());
        foreach ($element->getValues() as $option) {
            if ($option['value']) {
                foreach ($renderedOptions as $index => $optionHtml) {
                    if (strpos($optionHtml, '"' . $option['value'] . '"') !== false
                        && !$this->moduleManager->isEnabled($option['value'])) {
                        $renderedOptions[$index] = ' disabled="disabled"' . $optionHtml;
                    }
                }
            }
        }
        return implode('<option', $renderedOptions);
    }
}
