<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Block\Adminhtml\Magmodules;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magmodules\RichSnippets\Api\Config\RepositoryInterface as ConfigRepositoryInterface;

/**
 * System Configration Module information Block
 */
class Header extends Field
{

    /**
     * @var string
     */
    protected $_template = 'Magmodules_RichSnippets::system/config/fieldset/header.phtml';

    /**
     * @var ConfigRepositoryInterface
     */
    private $config;

    /**
     * Header constructor.
     * @param Context $context
     * @param ConfigRepositoryInterface $config
     */
    public function __construct(
        Context $context,
        ConfigRepositoryInterface $config
    ) {
        $this->config = $config;
        parent::__construct($context);
    }

    /**
     * @param AbstractElement $element
     *
     * @return string
     */
    public function render(AbstractElement $element) : string
    {
        $element->addClass('magmodules');

        return $this->toHtml();
    }

    /**
     * Image with extension and magento version.
     *
     * @return string
     */
    public function getImage(): string
    {
        return sprintf(
            'https://www.magmodules.eu/logo/%s/%s/%s/logo.png',
            $this->config->getExtensionCode(),
            $this->config->getExtensionVersion(),
            $this->config->getMagentoVersion()
        );
    }

    /**
     * Support link for extension.
     *
     * @return string
     */
    public function getSupportLink(): string
    {
        return $this->config->getSupportLink();
    }
}
