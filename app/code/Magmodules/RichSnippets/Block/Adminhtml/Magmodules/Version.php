<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Block\Adminhtml\Magmodules;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magmodules\RichSnippets\Api\Config\RepositoryInterface as ConfigRepositoryInterface;

/**
 * Block for returing module version in system configuration
 */
class Version extends Field
{

    /**
     * @var ConfigRepositoryInterface
     */
    private $config;

    /**
     * Version constructor.
     * @param Context $context
     * @param ConfigRepositoryInterface $config
     */
    public function __construct(
        Context $context,
        ConfigRepositoryInterface $config
    ) {
        $this->config = $config;
        parent::__construct($context);
    }

    /**
     * Version display in config.
     *
     * @param AbstractElement $element
     *
     * @return string
     */
    public function render(AbstractElement $element): string
    {
        $html = '<tr id="row_' . $element->getHtmlId() . '">';
        $html .= '  <td class="label">' . $element->getData('label') . '</td>';
        $html .= '  <td class="value">' . $this->config->getExtensionVersion() . '</td>';
        $html .= '  <td></td>';
        $html .= '</tr>';

        return $html;
    }
}
