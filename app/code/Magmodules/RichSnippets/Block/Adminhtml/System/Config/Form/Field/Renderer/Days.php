<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Block\Adminhtml\System\Config\Form\Field\Renderer;

use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;
use Magmodules\RichSnippets\Model\Config\Source\Days as DaysSource;

/**
 * HTML select Days element block
 */
class Days extends Select
{

    /**
     * @var array
     */
    private $days = [];

    /**
     * @var DaysSource
     */
    private $daysSource;

    /**
     * Days constructor.
     * @param Context $context
     * @param DaysSource $daysSource
     * @param array $data
     */
    public function __construct(
        Context $context,
        DaysSource $daysSource,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->daysSource = $daysSource;
    }

    /**
     * Render block HTML.
     *
     * @return string
     */
    public function _toHtml() : string
    {
        if (!$this->getOptions()) {
            foreach ($this->getDaysSource() as $day) {
                $this->addOption($day['value'], $day['label']);
            }
        }

        return parent::_toHtml();
    }

    /**
     * Get all days.
     *
     * @return array
     */
    public function getDaysSource() : array
    {
        if (!$this->days) {
            $this->days = $this->daysSource->toOptionArray();
        }

        return $this->days;
    }

    /**
     * Sets name for input element
     *
     * @param string $value
     *
     * @return $this
     */
    public function setInputName(string $value) : self
    {
        return $this->setData('name', $value);
    }
}
