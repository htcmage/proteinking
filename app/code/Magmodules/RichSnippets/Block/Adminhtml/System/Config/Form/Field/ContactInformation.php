<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Block\Adminhtml\System\Config\Form\Field;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\DataObject;
use Magmodules\RichSnippets\Api\Log\RepositoryInterface as LogRepository;

/**
 * Backend ContactInformation array field renderer
 */
class ContactInformation extends AbstractFieldArray
{

    /**
     * @var \Magento\Framework\View\Element\BlockInterface|array
     */
    private $contactTypeRenderer;

    /**
     * @var LogRepository
     */
    private $logger;

    /**
     * ContactInformation constructor.
     *
     * @param Context $context
     * @param LogRepository $logger
     */
    public function __construct(
        Context $context,
        LogRepository $logger
    ) {
        $this->logger = $logger;
        parent::__construct($context);
    }

    /**
     * Render block.
     *
     */
    public function _prepareToRender()
    {
        $this->addColumn('telephone', [
            'label' => __('Telephone'),
        ]);
        $this->addColumn('contact_type', [
            'label'    => __('Contact Type'),
            'renderer' => $this->getContactTypeRenderer()
        ]);
        $this->addColumn('contact_option', [
            'label' => __('Contact Option'),
        ]);
        $this->addColumn('area', [
            'label' => __('Area Code'),
        ]);
        $this->addColumn('languages', [
            'label' => __('Languages'),
        ]);
        $this->_addAfter = false;
        $this->_addButtonLabel = (string)__('Add');
    }

    /**
     * Returns render of contact types.
     *
     * @return \Magento\Framework\View\Element\BlockInterface
     */
    public function getContactTypeRenderer() : \Magento\Framework\View\Element\BlockInterface
    {
        if (!$this->contactTypeRenderer) {
            try {
                $this->contactTypeRenderer = $this->getLayout()->createBlock(
                    Renderer\ContactType::class,
                    '',
                    ['data' => ['is_render_to_js_template' => true]]
                );
            } catch (\Exception $e) {
                $this->logger->addDebugLog('LocalizedException', $e->getMessage());
                $this->contactTypeRenderer = [];
            }
        }

        return $this->contactTypeRenderer;
    }

    /**
     * Prepare existing row data object.
     *
     * @param DataObject $row
     *
     */
    public function _prepareArrayRow(DataObject $row)
    {
        $type = $row->getData('contact_type');
        $options = [];
        if ($type) {
            $options['option_' . $this->getContactTypeRenderer()->calcOptionHash($type)] = 'selected="selected"';
        }
        $row->setData('option_extra_attrs', $options);
    }
}
