<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Block\Adminhtml\System\Config\Form\Field;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\DataObject;
use Magmodules\RichSnippets\Api\Log\RepositoryInterface as LogRepository;

/**
 * Backend OpeningHours array field renderer
 */
class OpeningHours extends AbstractFieldArray
{

    /**
     * @var \Magento\Framework\View\Element\BlockInterface|array
     */
    private $daysRenderer;

    /**
     * @var LogRepository
     */
    private $logger;

    /**
     * Repository constructor.
     *
     * @param Context $context
     * @param LogRepository $logger
     */
    public function __construct(
        Context $context,
        LogRepository $logger
    ) {
        $this->logger = $logger;
        parent::__construct($context);
    }

    /**
     * Render block.
     *
     */
    public function _prepareToRender()
    {
        $this->addColumn('day', [
            'label' => __('Day'),
            'renderer' => $this->getDaysRenderer()
        ]);
        $this->addColumn('time_open', [
            'label' => __('Open')
        ]);
        $this->addColumn('time_close', [
            'label' => __('Close'),
        ]);
        $this->_addAfter = false;
        $this->_addButtonLabel = (string)__('Add');
    }

    /**
     * Returns render of days.
     *
     * @return \Magento\Framework\View\Element\BlockInterface
     */
    public function getDaysRenderer() : \Magento\Framework\View\Element\BlockInterface
    {
        if (!$this->daysRenderer) {
            try {
                $this->daysRenderer = $this->getLayout()->createBlock(
                    Renderer\Days::class,
                    '',
                    ['data' => ['is_render_to_js_template' => true]]
                );
            } catch (\Exception $e) {
                $this->logger->addDebugLog('LocalizedException', $e->getMessage());
                $this->daysRenderer = [];
            }
        }

        return $this->daysRenderer;
    }

    /**
     * Prepare existing row data object.
     *
     * @param DataObject $row
     *
     */
    public function _prepareArrayRow(DataObject $row)
    {
        $day = $row->getData('day');
        $options = [];
        if ($day) {
            $options[sprintf('option_%s', $this->getDaysRenderer()->calcOptionHash($day))] = 'selected="selected"';
        }
        $row->setData('option_extra_attrs', $options);
    }
}
