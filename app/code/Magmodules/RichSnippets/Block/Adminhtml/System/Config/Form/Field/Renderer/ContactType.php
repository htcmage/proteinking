<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Block\Adminhtml\System\Config\Form\Field\Renderer;

use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;
use Magmodules\RichSnippets\Model\Config\Source\ContactType as ContactTypeSource;

/**
 * HTML select ContactType element block
 */
class ContactType extends Select
{

    /**
     * @var array
     */
    private $type = [];
    /**
     * @var ContactTypeSource
     */
    private $typeSource;

    /**
     * ContactType constructor.
     * @param Context $context
     * @param ContactTypeSource $typeSource
     * @param array $data
     */
    public function __construct(
        Context $context,
        ContactTypeSource $typeSource,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->typeSource = $typeSource;
    }

    /**
     * Render block HTML.
     *
     * @return string
     */
    public function _toHtml() : string
    {
        if (!$this->getOptions()) {
            foreach ($this->getTypeSource() as $type) {
                $this->addOption($type['value'], $type['label']);
            }
        }

        return parent::_toHtml();
    }

    /**
     * Get all contact types.
     *
     * @return array
     */
    public function getTypeSource() : array
    {
        if (!$this->type) {
            $this->type = $this->typeSource->toOptionArray();
        }

        return $this->type;
    }

    /**
     * Sets name for input element.
     *
     * @param string $value
     *
     * @return $this
     */
    public function setInputName(string $value) : self
    {
        return $this->setData('name', $value);
    }
}
