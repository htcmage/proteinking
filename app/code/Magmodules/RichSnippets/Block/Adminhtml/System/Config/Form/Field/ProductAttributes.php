<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\Block\Adminhtml\System\Config\Form\Field;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\DataObject;
use Magmodules\RichSnippets\Api\Log\RepositoryInterface as LogRepository;

/**
 * Backend ProductAttributes array field renderer
 */
class ProductAttributes extends AbstractFieldArray
{

    /**
     * @var \Magento\Framework\View\Element\BlockInterface|array
     */
    private $typeRenderer;

    /**
     * @var \Magento\Framework\View\Element\BlockInterface|array
     */
    private $attributeRenderer;

    /**
     * @var LogRepository
     */
    private $logger;

    /**
     * ProductAttributes constructor.
     *
     * @param Context $context
     * @param LogRepository $logger
     */
    public function __construct(
        Context $context,
        LogRepository $logger
    ) {
        $this->logger = $logger;
        parent::__construct($context);
    }

    /**
     * Render block.
     *
     */
    public function _prepareToRender()
    {
        $this->addColumn('type', [
            'label'    => __('Type'),
            'renderer' => $this->getTypeRenderer()
        ]);
        $this->addColumn('attribute', [
            'label'    => __('Attribute'),
            'renderer' => $this->getAttributeRenderer()
        ]);
        $this->_addAfter = false;
        $this->_addButtonLabel = (string)__('Add');
    }

    /**
     * Returns render of stores.
     *
     * @return \Magento\Framework\View\Element\BlockInterface
     */
    public function getTypeRenderer() : \Magento\Framework\View\Element\BlockInterface
    {
        if (!$this->typeRenderer) {
            try {
                $this->typeRenderer = $this->getLayout()->createBlock(
                    Renderer\AttributeType::class,
                    '',
                    ['data' => ['is_render_to_js_template' => true]]
                );
            } catch (\Exception $e) {
                $this->typeRenderer = [];
                $this->logger->addDebugLog('LocalizedException', $e->getMessage());
            }
        }

        return $this->typeRenderer;
    }

    /**
     * Retruns renderer of attributes.
     *
     * @return \Magento\Framework\View\Element\BlockInterface
     */
    public function getAttributeRenderer() : \Magento\Framework\View\Element\BlockInterface
    {
        if (!$this->attributeRenderer) {
            try {
                $this->attributeRenderer = $this->getLayout()->createBlock(
                    Renderer\Attributes::class,
                    '',
                    ['data' => ['is_render_to_js_template' => true]]
                );
            } catch (\Exception $e) {
                $this->attributeRenderer = [];
                $this->logger->addDebugLog('LocalizedException', $e->getMessage());
            }
        }

        return $this->attributeRenderer;
    }

    /**
     * Prepare existing row data object.
     *
     * @param DataObject $row
     *
     */
    public function _prepareArrayRow(DataObject $row)
    {
        $type = $row->getData('type');
        $attribute = $row->getData('attribute');
        $options = [];
        if ($type) {
            $options[
                sprintf('option_%s', $this->getTypeRenderer()->calcOptionHash($type))
            ] = 'selected="selected"';
        }
        if ($attribute) {
            $options[
                sprintf('option_%s', $this->getAttributeRenderer()->calcOptionHash($attribute))
            ] = 'selected="selected"';
        }
        $row->setData('option_extra_attrs', $options);
    }
}
