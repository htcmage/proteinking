<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\ViewModel;

use Magento\Catalog\Block\Product\ImageFactory;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filter\FilterManager;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magmodules\RichSnippets\Api\Config\RepositoryInterface as RichSnippetsConfigRepository;
use Magmodules\RichSnippets\Api\Log\RepositoryInterface as LogRepository;
use Magmodules\RichSnippets\Api\Product\RepositoryInterface as ProductRepository;

/**
 * MetaProduct snippet data class
 *
 */
class MetaProduct implements ArgumentInterface
{

    /**
     * @var RichSnippetsConfigRepository
     */
    private $rsConfigRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var ImageFactory
     */
    private $imageFactory;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var FilterManager
     */
    private $filterManager;

    /**
     * @var Escaper
     */
    private $escaper;

    /**
     * @var StockItemRepository
     */
    private $stockItemRepository;

    /**
     * @var LogRepository
     */
    private $logger;

    /**
     * MetaProduct constructor.
     * @param RichSnippetsConfigRepository $rsConfigRepository
     * @param ProductRepository $productRepository
     * @param Registry $registry
     * @param ImageFactory $imageFactory
     * @param StoreManagerInterface $storeManager
     * @param FilterManager $filterManager
     * @param Escaper $escaper
     * @param StockItemRepository $stockItemRepository
     * @param LogRepository $logger
     */
    public function __construct(
        RichSnippetsConfigRepository $rsConfigRepository,
        ProductRepository $productRepository,
        Registry $registry,
        ImageFactory $imageFactory,
        StoreManagerInterface $storeManager,
        FilterManager $filterManager,
        Escaper $escaper,
        StockItemRepository $stockItemRepository,
        LogRepository $logger
    ) {
        $this->rsConfigRepository = $rsConfigRepository;
        $this->productRepository = $productRepository;
        $this->registry = $registry;
        $this->imageFactory = $imageFactory;
        $this->storeManager = $storeManager;
        $this->filterManager = $filterManager;
        $this->escaper = $escaper;
        $this->stockItemRepository = $stockItemRepository;
        $this->logger = $logger;
    }

    /**
     * Cet twitter username
     *
     * @return string
     */
    public function getTwitterUsername() : string
    {
        return (string)$this->rsConfigRepository->getSocialTwitterUsername();
    }

    /**
     * Get page title
     *
     * @return string
     */
    public function getTitle() : string
    {
        return $this->filterManager->stripTags(
            (string)$this->registry->registry('current_product')->getName()
        );
    }

    /**
     * Get product description
     *
     * @return string
     */
    public function getDescription() : string
    {
        return $this->productRepository->getDescription($this->registry->registry('current_product'));
    }

    /**
     * Get product availability
     *
     * @return string
     */
    public function getAvailability() : string
    {
        if ($this->registry->registry('current_product')->isAvailable()) {
            return 'in stock';
        } else {
            return 'out of stock';
        }
    }

    /**
     * Get product image
     *
     * @param boolean|null $secure
     * @return string
     */
    public function getImage($secure = null) : string
    {
        if ($image = $this->registry->registry('current_product')->getImage()) {
            $productImageUrl = $this->rsConfigRepository->getStore()
                    ->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA, $secure)
                . 'catalog/product' . $image;
            return $this->escaper->escapeUrl((string)$productImageUrl);
        }
        return $this->rsConfigRepository->getStoreLogo();
    }

    /**
     * Get secure product image
     *
     * @return string
     */
    public function getSecureImage() : string
    {
        $secureImage = $this->getImage(true);
        return substr($secureImage, 0, 5) === "https" ? $secureImage : '';
    }

    /**
     * Get Facebook app ID
     *
     * @return string
     */
    public function getFacebookAppId() : string
    {
        return $this->rsConfigRepository->getFacebookAppId(
            $this->rsConfigRepository->getStoreId()
        );
    }

    /**
     * Retrieve Current Currency code
     *
     * @return string
     */
    public function getCurrencyCode() : string
    {
        try {
            return (string)$this->storeManager->getStore()->getCurrentCurrency()->getCode();
        } catch (NoSuchEntityException $entityException) {
            return '';
        }
    }

    /**
     * Get Price of current Product
     *
     * @return mixed
     */
    public function getProductPrice()
    {
        return $this->registry->registry('current_product')
            ->getPriceInfo()
            ->getPrice(\Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE)
            ->getAmount();
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->rsConfigRepository->getLocale();
    }
}
