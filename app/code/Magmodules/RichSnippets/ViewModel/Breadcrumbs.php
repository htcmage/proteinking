<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\UrlInterface;
use Magmodules\RichSnippets\Api\Config\RepositoryInterface
    as ConfigRepository;
use Magento\Framework\Serialize\Serializer\Base64Json;
use Magento\Framework\Serialize\Serializer\Json;
use Exception;
use Magento\Catalog\Helper\Data as CatalogHelper;
use Magento\Store\Model\StoreManagerInterface as StoreManager;
use Magmodules\RichSnippets\Api\Log\RepositoryInterface as LogRepository;

/**
 * Breadcrumbs snippet data class
 *
 */
class Breadcrumbs implements ArgumentInterface
{

    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @var ConfigRepository
     */
    private $configRepository;

    /**
     * @var Base64Json
     */
    private $base64json;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var CatalogHelper
     */
    private $catalogHelper;

    /**
     * @var StoreManager
     */
    private $storeManager;

    /**
     * @var LogRepository
     */
    private $logger;

    /**
     * Breadcrumbs constructor.
     *
     * @param UrlInterface $url
     * @param ConfigRepository $configRepository
     * @param Base64Json $base64json
     * @param Json $json
     * @param CatalogHelper $catalogHelper
     * @param StoreManager $storeManager
     * @param LogRepository $logger
     */
    public function __construct(
        UrlInterface $url,
        ConfigRepository $configRepository,
        Base64Json $base64json,
        Json $json,
        CatalogHelper $catalogHelper,
        StoreManager $storeManager,
        LogRepository $logger
    ) {
        $this->url = $url;
        $this->configRepository = $configRepository;
        $this->base64json = $base64json;
        $this->json = $json;
        $this->catalogHelper = $catalogHelper;
        $this->storeManager = $storeManager;
        $this->logger = $logger;
    }

    /**
     * Get breadcrumbs JSON
     *
     * @param string $crumbs
     * @return string
     */
    public function getBreadcrumbs(string $crumbs) : string
    {
        $breadCrumbs = $this->base64json->unserialize($crumbs);
        $position = 1;
        $data = [];
        if ($breadCrumbs == null) {
            $breadCrumbs = ['home' => [
                'label' => __('Home'),
                'title' => __('Go to Home Page'),
                'link' => $this->configRepository->getBaseUrl()
            ]
            ];
            $breadCrumbs += $this->catalogHelper->getBreadcrumbPath();
        }
        foreach ((array)$breadCrumbs as $id => $breadCrumb) {
            if ($id == 'home') {
                $name = $this->getCustomHomeCrumb()
                    ? $this->getCustomHomeCrumb()
                    : $breadCrumb['label'];
            } else {
                $name = $breadCrumb['label'];
            }
            if (!array_key_exists('link', $breadCrumb)) {
                $breadCrumb['link'] = $this->url->getCurrentUrl();
            }
            $data['itemListElement'][] = [
                '@type' => 'ListItem',
                'position' => $position,
                'item' => [
                    '@id' => $breadCrumb['link'] ? $breadCrumb['link'] : $this->url->getCurrentUrl(),
                    'name' => $name,
                ]
            ];
            $position++;
        }

        if (!empty($data)) {
            $data = [['@context' => 'http://schema.org', '@type' => 'BreadcrumbList'] + $data];
        }

        return $this->json->serialize($data);
    }

    /**
     * Get homepage breadcrumb data
     *
     * @return bool|string
     */
    private function getCustomHomeCrumb()
    {
        $customHome = (bool)$this->configRepository->getBreadcrumbsCustomHomeEnabled();
        if ($customHome) {
            return $customHome = $this->configRepository->getBreadcrumbsCustomHomeTitle();
        } else {
            return false;
        }
    }
}
