<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\ViewModel;

use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magmodules\RichSnippets\Api\Log\RepositoryInterface as LogRepository;

/**
 * Metaclass for snippet data
 *
 */
class Meta implements ArgumentInterface
{

    /**
     * Snippet code pattern
     */
    const SNIPPET = '<script type="application/ld+json">%s</script>';

    /**
     * @var mixed
     */
    private $repository;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var LogRepository
     */
    private $logger;

    /**
     * Meta constructor.
     * @param string $repository
     * @param Json $json
     * @param ObjectManagerInterface $objectManager
     * @param LogRepository $logger
     */
    public function __construct(
        $repository,
        Json $json,
        ObjectManagerInterface $objectManager,
        LogRepository $logger
    ) {
        $this->json = $json;
        $this->objectManager = $objectManager;
        $this->repository = $this->getNewInstance($repository);
        $this->logger = $logger;
    }

    /**
     * @param string $repository
     * @return mixed
     */
    public function getNewInstance(string $repository)
    {
        return $this->objectManager->create($repository);
    }

    /**
     * Get Website Schema Data
     *
     * @return string|bool
     */
    public function getSchemaData()
    {
        $data = $this->repository->getSchemaData();
        return !empty($data) ? $this->json->serialize($data) : false;
    }

    /**
     * Cet snippet code
     *
     * @return string
     */
    public function getSnippet()
    {
        try {
            $snippets = $this->getSchemaData();
        } catch (\Exception $e) {
            $this->logger->addDebugLog('getSchemaData()', $e->getMessage());
            return '';
        }
        if ($snippets) {
            return sprintf(self::SNIPPET, $snippets);
        }
        return '';
    }
}
