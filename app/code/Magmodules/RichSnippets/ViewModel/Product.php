<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\ViewModel;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface as CatalogProductRepository;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magmodules\RichSnippets\Api\Log\RepositoryInterface as LogRepository;
use Magmodules\RichSnippets\Api\Product\RepositoryInterface as RichSnippetsProductRepository;

/**
 * Product snippet data class
 */
class Product implements ArgumentInterface
{

    /**
     * Snippet code pattern
     */
    const SNIPPET = '<script type="application/ld+json">%s</script>';

    /**
     * @var RichSnippetsProductRepository
     */
    private $richSnippetsproductRepository;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var CatalogProductRepository
     */
    private $catalogProductRepository;

    /**
     * @var LogRepository
     */
    private $logger;

    /**
     * Product constructor.
     * @param Json $json
     * @param RichSnippetsProductRepository $richSnippetsproductRepository
     * @param CatalogProductRepository $catalogProductRepository
     * @param Registry $registry
     * @param LogRepository $logger
     */
    public function __construct(
        Json $json,
        RichSnippetsProductRepository $richSnippetsproductRepository,
        CatalogProductRepository $catalogProductRepository,
        Registry $registry,
        LogRepository $logger
    ) {
        $this->richSnippetsproductRepository = $richSnippetsproductRepository;
        $this->catalogProductRepository = $catalogProductRepository;
        $this->registry = $registry;
        $this->json = $json;
        $this->logger = $logger;
    }

    /**
     * Get Product Schema Data
     *
     * @return bool|false|string
     */
    private function getSchemaData()
    {
        $data = $this->richSnippetsproductRepository->getSchemaData($this->getCurrentProduct());
        return !empty($data) ? $this->json->serialize($data) : false;
    }

    /**
     * Cet snippet code
     *
     * @return string
     */
    public function getSnippet()
    {
        try {
            $snippets = $this->getSchemaData();
        } catch (\Exception $e) {
            $this->logger->addDebugLog('getSchemaData()', $e->getMessage());
            return '';
        }
        if ($snippets) {
            return sprintf(self::SNIPPET, $snippets);
        }
        return '';
    }

    /**
     * Get current product
     *
     * @return ProductInterface
     */
    private function getCurrentProduct()
    {
        return $this->registry->registry('current_product');
    }
}
