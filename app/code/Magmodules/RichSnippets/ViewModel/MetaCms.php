<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magmodules\RichSnippets\Api\Config\RepositoryInterface
    as RichSnippetsConfigRepository;
use Magento\Framework\Filter\FilterManager;
use Magento\Framework\Escaper;
use Magento\Cms\Model\Page;
use Magento\Framework\App\Request\Http;

/**
 * MetaCms snippet data class
 *
 */
class MetaCms implements ArgumentInterface
{

    /**
     * @var RichSnippetsConfigRepository
     */
    private $rsConfigRepository;

    /**
     * @var FilterManager
     */
    private $filterManager;

    /**
     * @var Escaper
     */
    private $escaper;

    /**
     * @var Page
     */
    private $page;

    /**
     * @var Http
     */
    private $request;

    /**
     * OgCategory constructor.
     * @param RichSnippetsConfigRepository $rsConfigRepository
     * @param FilterManager $filterManager
     * @param Escaper $escaper
     * @param Page $page
     */
    public function __construct(
        RichSnippetsConfigRepository $rsConfigRepository,
        FilterManager $filterManager,
        Escaper $escaper,
        Page $page,
        Http $request
    ) {
        $this->rsConfigRepository = $rsConfigRepository;
        $this->filterManager = $filterManager;
        $this->escaper = $escaper;
        $this->page = $page;
        $this->request = $request;
    }

    /**
     * Cet twitter username
     *
     * @return string
     */
    public function getTwitterUsername() : string
    {
        return (string)$this->rsConfigRepository->getSocialTwitterUsername();
    }

    /**
     * @return string
     */
    public function getType() : string
    {
        if ($this->request->getFullActionName() == 'cms_index_index') {
            return 'website';
        } else {
            return 'article';
        }
    }

    /**
     * Get page title
     *
     * @return string
     */
    public function getTitle() : string
    {
        if (!$this->rsConfigRepository->getCmsTitle()) {
            return '';
        }
        return $this->filterManager->stripTags(
            (string)$this->page->getTitle()
        );
    }

    /**
     * Get product description
     *
     * @return string
     */
    public function getDescription() : string
    {
        if (!$this->rsConfigRepository->getCmsDescription()) {
            return '';
        }
        return (string)$this->filterManager->stripTags(
            (string)$this->page->getMetaDescription()
        );
    }

    /**
     * Get product image
     *
     * @return string
     */
    public function getImage() : string
    {
        if (!$this->rsConfigRepository->getCmsLogo()) {
            return '';
        }
        return $this->rsConfigRepository->getStoreLogo();
    }

    /**
     * Get secure image
     *
     * @return string
     */
    public function getSecureImage() : string
    {
        $secureImage = $this->getImage();
        return substr($secureImage, 0, 5) === "https" ? $secureImage : '';
    }

    /**
     * Get Facebook app ID
     *
     * @return string
     */
    public function getFacebookAppId() : string
    {
        return $this->rsConfigRepository->getFacebookAppId(
            $this->rsConfigRepository->getStoreId()
        );
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->rsConfigRepository->getLocale();
    }
}
