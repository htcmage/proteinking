<?php
/**
 * Copyright © Magmodules.eu. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Magmodules\RichSnippets\ViewModel;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magmodules\RichSnippets\Api\Config\RepositoryInterface
    as RichSnippetsConfigRepository;
use Magento\Framework\Registry ;
use Magento\Framework\Filter\FilterManager;
use Magento\Framework\Escaper;

/**
 * MetaCategory snippet data class
 *
 */
class MetaCategory implements ArgumentInterface
{

    /**
     * @var RichSnippetsConfigRepository
     */
    private $rsConfigRepository;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var FilterManager
     */
    private $filterManager;

    /**
     * @var Escaper
     */
    private $escaper;

    /**
     * OgCategory constructor.
     * @param RichSnippetsConfigRepository $rsConfigRepository
     * @param Registry $registry
     * @param FilterManager $filterManager
     * @param Escaper $escaper
     */
    public function __construct(
        RichSnippetsConfigRepository $rsConfigRepository,
        Registry $registry,
        FilterManager $filterManager,
        Escaper $escaper
    ) {
        $this->rsConfigRepository = $rsConfigRepository;
        $this->registry = $registry;
        $this->filterManager = $filterManager;
        $this->escaper = $escaper;
    }

    /**
     * Cet twitter username
     *
     * @return string
     */
    public function getTwitterUsername() : string
    {
        return (string)$this->rsConfigRepository->getSocialTwitterUsername();
    }

    /**
     * Get page title
     *
     * @return string
     */
    public function getTitle() : string
    {
        return $this->filterManager->stripTags(
            (string)$this->registry->registry('current_category')->getName()
        );
    }

    /**
     * Get product description
     *
     * @return string
     */
    public function getDescription() : string
    {
        return (string)$this->filterManager->stripTags(
            $this->registry->registry('current_category')->getDescription()
        );
    }

    /**
     * Get product image
     *
     * @return string
     */
    public function getImage() : string
    {
        if ($image = $this->registry->registry('current_category')->getImageUrl()) {
            return $this->escaper->escapeUrl((string)$image);
        }
        return $this->rsConfigRepository->getStoreLogo();
    }

    /**
     * Get secure image
     *
     * @return string
     */
    public function getSecureImage() : string
    {
        $secureImage = $this->getImage();
        return substr($secureImage, 0, 5) === "https" ? $secureImage : '';
    }

    /**
     * Get Facebook app ID
     *
     * @return string
     */
    public function getFacebookAppId() : string
    {
        return $this->rsConfigRepository->getFacebookAppId(
            $this->rsConfigRepository->getStoreId()
        );
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->rsConfigRepository->getLocale();
    }
}
