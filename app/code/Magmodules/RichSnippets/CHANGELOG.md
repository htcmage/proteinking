# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.2] - 2020-06-11

### Fixed

- Fixed issue with products without images
- Added GitHub Actions 

## [1.2.1] - 2020-05-14

### Fixed

- Store image on store level
- Check on breadcrumbs block


## [1.2.0] - 2020-04-30

### Added

- Breadcrumbs markup
- Extended social (OG Data & TwitterCards) integration
